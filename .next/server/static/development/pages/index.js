module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/Calendar.js":
/*!********************************!*\
  !*** ./components/Calendar.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_helperfunctions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/helperfunctions */ "./components/helperfunctions.js");
/* harmony import */ var _components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/EmployeeButton */ "./components/EmployeeButton.js");
var _jsxFileName = "/Users/paulbeaudon/calendar/next-calendar/components/Calendar.js";


var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;



const Calendar = props => {
  const getTable = (month, year, monthInfo) => {
    let rows = 6;
    let cols = 7;
    let dayCount = 1;
    let start_day = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_2__["startDay"](month, year);
    let days_in_month = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_2__["daysInMonth"](month, year);
    let table = [];
    let currentDay = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_2__["currentDay"]();
    let currentMonth = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_2__["currentMonth"]();
    let currentYear = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_2__["currentYear"]();
    let monthInfoCount = 0;
    table.push(__jsx("tr", {
      key: "header",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: undefined
    }, __jsx("td", {
      className: "headertd",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: undefined
    }, "Sunday"), __jsx("td", {
      className: "headertd",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: undefined
    }, "Monday"), __jsx("td", {
      className: "headertd",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: undefined
    }, "Tuesday"), __jsx("td", {
      className: "headertd",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: undefined
    }, "Wednesday"), __jsx("td", {
      className: "headertd",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: undefined
    }, "Thursday"), __jsx("td", {
      className: "headertd",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: undefined
    }, "Friday"), __jsx("td", {
      className: "headertd",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: undefined
    }, "Saturday")));

    for (let row = 0; row < rows; row++) {
      let day = [];

      for (let col = 0; col < cols; col++) {
        if (row === 0 && col === start_day) {
          let morning = [];
          morning = monthInfo.filter(morn => morn.id.split('-')[0] === `${row}_${col}_am`);
          let afternoon = [];
          afternoon = monthInfo.filter(aft => aft.id.split('-')[0] === `${row}_${col}_pm`);

          if (currentDay === dayCount && currentMonth === month && currentYear === year) {
            day.push(__jsx("td", {
              id: `${row}_${col}`,
              key: `${row}_${col}`,
              className: "currentdaytd",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 30
              },
              __self: undefined
            }, __jsx("div", {
              className: "datePos",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 30
              },
              __self: undefined
            }, `${dayCount}`), __jsx("div", {
              id: `${row}_${col}_am`,
              className: "am",
              onDrop: props.onDropCopyIndex,
              onDragOver: props.onDragOverIndex,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 30
              },
              __self: undefined
            }, morning.map(morn => __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
              key: morn.id,
              onDragStartIndex: props.onDragStartIndex,
              id: morn.id,
              name: morn.user_name,
              signedinuser: props.signedinuser,
              buttonsize: "small",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 30
              },
              __self: undefined
            }))), __jsx("div", {
              id: `${row}_${col}_pm`,
              className: "pm",
              onDrop: props.onDropCopyIndex,
              onDragOver: props.onDragOverIndex,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 30
              },
              __self: undefined
            }, afternoon.map(aft => __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
              key: aft.id,
              onDragStartIndex: props.onDragStartIndex,
              id: aft.id,
              name: aft.user_name,
              signedinuser: props.signedinuser,
              buttonsize: "small",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 30
              },
              __self: undefined
            })))));
          } else {
            day.push(__jsx("td", {
              id: `${row}_${col}`,
              key: `${row}_${col}`,
              className: "daytd",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 32
              },
              __self: undefined
            }, __jsx("div", {
              className: "datePos",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 32
              },
              __self: undefined
            }, `${dayCount}`), __jsx("div", {
              id: `${row}_${col}_am`,
              className: "am",
              onDrop: props.onDropCopyIndex,
              onDragOver: props.onDragOverIndex,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 32
              },
              __self: undefined
            }, morning.map(morn => __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
              key: morn.id,
              onDragStartIndex: props.onDragStartIndex,
              id: morn.id,
              name: morn.user_name,
              signedinuser: props.signedinuser,
              buttonsize: "small",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 32
              },
              __self: undefined
            }))), __jsx("div", {
              id: `${row}_${col}_pm`,
              className: "pm",
              onDrop: props.onDropCopyIndex,
              onDragOver: props.onDragOverIndex,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 32
              },
              __self: undefined
            }, afternoon.map(aft => __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
              key: aft.id,
              onDragStartIndex: props.onDragStartIndex,
              id: aft.id,
              name: aft.user_name,
              signedinuser: props.signedinuser,
              buttonsize: "small",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 32
              },
              __self: undefined
            })))));
          }

          dayCount++;
        } else {
          if (dayCount > 1 && dayCount <= days_in_month) {
            let morning = [];
            morning = monthInfo.filter(morn => morn.id.split('-')[0] === `${row}_${col}_am`);
            let afternoon = [];
            afternoon = monthInfo.filter(aft => aft.id.split('-')[0] === `${row}_${col}_pm`);

            if (currentDay === dayCount && currentMonth === month && currentYear === year) {
              day.push(__jsx("td", {
                id: `${row}_${col}`,
                key: `${row}_${col}`,
                className: "currentdaytd",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 43
                },
                __self: undefined
              }, __jsx("div", {
                className: "datePos",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 43
                },
                __self: undefined
              }, `${dayCount}`), __jsx("div", {
                id: `${row}_${col}_am`,
                className: "am",
                onDrop: props.onDropCopyIndex,
                onDragOver: props.onDragOverIndex,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 43
                },
                __self: undefined
              }, morning.map(morn => __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
                key: morn.id,
                onDragStartIndex: props.onDragStartIndex,
                id: morn.id,
                name: morn.user_name,
                signedinuser: props.signedinuser,
                buttonsize: "small",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 43
                },
                __self: undefined
              }))), __jsx("div", {
                id: `${row}_${col}_pm`,
                className: "pm",
                onDrop: props.onDropCopyIndex,
                onDragOver: props.onDragOverIndex,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 43
                },
                __self: undefined
              }, afternoon.map(aft => __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
                key: aft.id,
                onDragStartIndex: props.onDragStartIndex,
                id: aft.id,
                name: aft.user_name,
                signedinuser: props.signedinuser,
                buttonsize: "small",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 43
                },
                __self: undefined
              })))));
            } else {
              day.push(__jsx("td", {
                id: `${row}_${col}`,
                key: `${row}_${col}`,
                className: "daytd",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 45
                },
                __self: undefined
              }, __jsx("div", {
                className: "datePos",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 45
                },
                __self: undefined
              }, `${dayCount}`), __jsx("div", {
                id: `${row}_${col}_am`,
                className: "am",
                onDrop: props.onDropCopyIndex,
                onDragOver: props.onDragOverIndex,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 45
                },
                __self: undefined
              }, morning.map(morn => __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
                onDragStartIndex: props.onDragStartIndex,
                key: morn.id,
                id: morn.id,
                name: morn.user_name,
                signedinuser: props.signedinuser,
                buttonsize: "small",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 45
                },
                __self: undefined
              }))), __jsx("div", {
                id: `${row}_${col}_pm`,
                className: "pm",
                onDrop: props.onDropCopyIndex,
                onDragOver: props.onDragOverIndex,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 45
                },
                __self: undefined
              }, afternoon.map(aft => __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
                key: aft.id,
                onDragStartIndex: props.onDragStartIndex,
                id: aft.id,
                name: aft.user_name,
                signedinuser: props.signedinuser,
                buttonsize: "small",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 45
                },
                __self: undefined
              })))));
            }

            dayCount++;
          } else {
            day.push(__jsx("td", {
              key: `${row}_${col}`,
              className: "daytd",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 49
              },
              __self: undefined
            }));
          }
        }
      }

      table.push(__jsx("tr", {
        key: `${row}`,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 53
        },
        __self: undefined
      }, day));
    }

    return table;
  };

  return __jsx("div", {
    className: "jsx-3120944804",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    },
    __self: undefined
  }, __jsx("table", {
    className: "jsx-3120944804" + " " + "caltable",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60
    },
    __self: undefined
  }, __jsx("tbody", {
    className: "jsx-3120944804",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61
    },
    __self: undefined
  }, getTable(props.month, props.year, props.monthInfo))), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    id: "3120944804",
    __self: undefined
  }, ".caltable{margin:auto;border-spacing:0px;padding:4px;font-size:1em;border-collapse:separate;border:0px solid #354980;}.headertd{text-align:center;width:14%;padding:4px 50px 4px 50px;font-size:1em;border:1px solid #354980;background-color:#354980;color:#fbca37;}.daytd{position:relative;text-align:center;font-size:1em;border:1px solid #354980;height:100px;min-height:100px;}.currentdaytd{position:relative;text-align:center;font-size:1em;border:1px solid #354980;height:100px;background-color:#fbca37;color:#fbca37;z-index:-1;}.datePos{font-size:1em;position:absolute;left:0px;top:0px;right:0px;text-align:right;padding-right:5px;background-color:#fbca37;color:#354980;z-index:-1;}.am{width:100%;height:auto;min-height:50px !important;border:1px dotted #fbca37;padding-top:15px;}.amCurrentDay{width:100%;height:auto;min-height:50px !important;border-bottom:1px dotted white;padding-top:15px;}.pm{width:100%;height:auto;min-height:50px !important;border:1px dotted #fbca37;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wYXVsYmVhdWRvbi9jYWxlbmRhci9uZXh0LWNhbGVuZGFyL2NvbXBvbmVudHMvQ2FsZW5kYXIuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBZ0V5QixBQUd1QixBQVFNLEFBU0QsQUFRQSxBQVVKLEFBWUgsQUFPQSxBQU9BLFdBYkMsQUFPQSxBQU9BLENBN0RRLEVBbUNGLElBM0JSLEFBU1MsQUFRQSxLQXVCUSxBQU9BLEFBT0EsS0FyREEsR0FSZCxDQW1DSixJQWxCTSxBQVFBLEtBV1AsRUFuQ08sTUFvQ0wsQ0FuQmdCLEFBUUEsQUFzQkEsQUFPSyxBQU9MLElBckRYLEdBUFcsRUFvQ1QsU0E1QlMsT0FTYixBQVFDLENBWUksQUFVRCxBQWNsQixLQVBrQixDQXJEUyxNQWlCVCxBQVFTLEtBakJBLEFBdUMzQixDQVYyQixJQWlCM0IsT0FwQ0EsRUFqQkEsTUF5QmdCLEtBakJBLENBNkJBLFFBWEgsS0FqQmIsQ0E2QmEsS0FYYixNQVlBIiwiZmlsZSI6Ii9Vc2Vycy9wYXVsYmVhdWRvbi9jYWxlbmRhci9uZXh0LWNhbGVuZGFyL2NvbXBvbmVudHMvQ2FsZW5kYXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBoZWxwRnVuYyBmcm9tIFwiLi4vY29tcG9uZW50cy9oZWxwZXJmdW5jdGlvbnNcIjtcbmltcG9ydCBFbXBsb3llZUJ1dHRvbiBmcm9tIFwiLi4vY29tcG9uZW50cy9FbXBsb3llZUJ1dHRvblwiO1xuXG5jb25zdCBDYWxlbmRhciA9IHByb3BzID0+e1xuICBjb25zdCBnZXRUYWJsZSA9IChtb250aCwgeWVhciwgbW9udGhJbmZvKT0+e1xuICAgICAgbGV0IHJvd3MgPSA2O1xuICAgICAgbGV0IGNvbHMgPSA3O1xuICAgICAgbGV0IGRheUNvdW50ID0gMTtcbiAgICAgIGxldCBzdGFydF9kYXkgPSBoZWxwRnVuYy5zdGFydERheShtb250aCwgeWVhcik7XG4gICAgICBsZXQgZGF5c19pbl9tb250aCA9IGhlbHBGdW5jLmRheXNJbk1vbnRoKG1vbnRoLCB5ZWFyKTtcbiAgICAgIGxldCB0YWJsZSA9IFtdO1xuXG4gICAgICBsZXQgY3VycmVudERheSA9IGhlbHBGdW5jLmN1cnJlbnREYXkoKTtcbiAgICAgIGxldCBjdXJyZW50TW9udGggPSBoZWxwRnVuYy5jdXJyZW50TW9udGgoKTtcbiAgICAgIGxldCBjdXJyZW50WWVhciA9IGhlbHBGdW5jLmN1cnJlbnRZZWFyKCk7XG5cbiAgICAgIGxldCBtb250aEluZm9Db3VudCA9IDA7XG5cbiAgICAgIHRhYmxlLnB1c2goPHRyIGtleT1cImhlYWRlclwiPjx0ZCBjbGFzc05hbWU9XCJoZWFkZXJ0ZFwiPlN1bmRheTwvdGQ+PHRkIGNsYXNzTmFtZT1cImhlYWRlcnRkXCI+TW9uZGF5PC90ZD48dGQgY2xhc3NOYW1lPVwiaGVhZGVydGRcIj5UdWVzZGF5PC90ZD48dGQgY2xhc3NOYW1lPVwiaGVhZGVydGRcIj5XZWRuZXNkYXk8L3RkPjx0ZCBjbGFzc05hbWU9XCJoZWFkZXJ0ZFwiPlRodXJzZGF5PC90ZD48dGQgY2xhc3NOYW1lPVwiaGVhZGVydGRcIj5GcmlkYXk8L3RkPjx0ZCBjbGFzc05hbWU9XCJoZWFkZXJ0ZFwiPlNhdHVyZGF5PC90ZD48L3RyPik7XG4gICAgICBmb3IobGV0IHJvdz0wOyByb3c8cm93czsgcm93Kyspe1xuICAgICAgICBsZXQgZGF5ID0gW107XG4gICAgICAgIGZvcihsZXQgY29sPTA7IGNvbDxjb2xzOyBjb2wrKyl7XG4gICAgICAgICAgaWYocm93ID09PSAwICYmIGNvbCA9PT0gc3RhcnRfZGF5KXtcbiAgICAgICAgICAgIGxldCBtb3JuaW5nID0gW107XG4gICAgICAgICAgICBtb3JuaW5nID0gbW9udGhJbmZvLmZpbHRlcihtb3JuID0+IG1vcm4uaWQuc3BsaXQoJy0nKVswXSA9PT0gYCR7cm93fV8ke2NvbH1fYW1gKTtcbiAgICAgICAgICAgIGxldCBhZnRlcm5vb24gPSBbXTtcbiAgICAgICAgICAgIGFmdGVybm9vbiA9IG1vbnRoSW5mby5maWx0ZXIoYWZ0ID0+IGFmdC5pZC5zcGxpdCgnLScpWzBdID09PSBgJHtyb3d9XyR7Y29sfV9wbWApO1xuXG4gICAgICAgICAgICBpZihjdXJyZW50RGF5ID09PSBkYXlDb3VudCAmJiBjdXJyZW50TW9udGggPT09IG1vbnRoICYmIGN1cnJlbnRZZWFyID09PSB5ZWFyKXtcbiAgICAgICAgICAgICAgZGF5LnB1c2goPHRkIGlkPXtgJHtyb3d9XyR7Y29sfWB9IGtleT17YCR7cm93fV8ke2NvbH1gfSBjbGFzc05hbWU9XCJjdXJyZW50ZGF5dGRcIj48ZGl2IGNsYXNzTmFtZT1cImRhdGVQb3NcIj57YCR7ZGF5Q291bnR9YH08L2Rpdj48ZGl2IGlkPXtgJHtyb3d9XyR7Y29sfV9hbWB9IGNsYXNzTmFtZT1cImFtXCIgb25Ecm9wPXtwcm9wcy5vbkRyb3BDb3B5SW5kZXh9IG9uRHJhZ092ZXI9e3Byb3BzLm9uRHJhZ092ZXJJbmRleH0+e21vcm5pbmcubWFwKG1vcm49PjxFbXBsb3llZUJ1dHRvbiBrZXk9e21vcm4uaWR9IG9uRHJhZ1N0YXJ0SW5kZXg9e3Byb3BzLm9uRHJhZ1N0YXJ0SW5kZXh9IGlkPXttb3JuLmlkfSBuYW1lPXttb3JuLnVzZXJfbmFtZX0gc2lnbmVkaW51c2VyPXtwcm9wcy5zaWduZWRpbnVzZXJ9IGJ1dHRvbnNpemU9XCJzbWFsbFwiLz4pfTwvZGl2PjxkaXYgaWQ9e2Ake3Jvd31fJHtjb2x9X3BtYH0gY2xhc3NOYW1lPVwicG1cIiBvbkRyb3A9e3Byb3BzLm9uRHJvcENvcHlJbmRleH0gb25EcmFnT3Zlcj17cHJvcHMub25EcmFnT3ZlckluZGV4fT57YWZ0ZXJub29uLm1hcChhZnQ9PjxFbXBsb3llZUJ1dHRvbiBrZXk9e2FmdC5pZH0gb25EcmFnU3RhcnRJbmRleD17cHJvcHMub25EcmFnU3RhcnRJbmRleH0gaWQ9e2FmdC5pZH0gbmFtZT17YWZ0LnVzZXJfbmFtZX0gc2lnbmVkaW51c2VyPXtwcm9wcy5zaWduZWRpbnVzZXJ9IGJ1dHRvbnNpemU9XCJzbWFsbFwiLz4pfTwvZGl2PjwvdGQ+KTtcbiAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICBkYXkucHVzaCg8dGQgaWQ9e2Ake3Jvd31fJHtjb2x9YH0ga2V5PXtgJHtyb3d9XyR7Y29sfWB9IGNsYXNzTmFtZT1cImRheXRkXCI+PGRpdiBjbGFzc05hbWU9XCJkYXRlUG9zXCI+e2Ake2RheUNvdW50fWB9PC9kaXY+PGRpdiBpZD17YCR7cm93fV8ke2NvbH1fYW1gfSBjbGFzc05hbWU9XCJhbVwiIG9uRHJvcD17cHJvcHMub25Ecm9wQ29weUluZGV4fSBvbkRyYWdPdmVyPXtwcm9wcy5vbkRyYWdPdmVySW5kZXh9Pnttb3JuaW5nLm1hcChtb3JuPT48RW1wbG95ZWVCdXR0b24ga2V5PXttb3JuLmlkfSBvbkRyYWdTdGFydEluZGV4PXtwcm9wcy5vbkRyYWdTdGFydEluZGV4fSBpZD17bW9ybi5pZH0gbmFtZT17bW9ybi51c2VyX25hbWV9IHNpZ25lZGludXNlcj17cHJvcHMuc2lnbmVkaW51c2VyfSBidXR0b25zaXplPVwic21hbGxcIi8+KX08L2Rpdj48ZGl2IGlkPXtgJHtyb3d9XyR7Y29sfV9wbWB9IGNsYXNzTmFtZT1cInBtXCIgb25Ecm9wPXtwcm9wcy5vbkRyb3BDb3B5SW5kZXh9IG9uRHJhZ092ZXI9e3Byb3BzLm9uRHJhZ092ZXJJbmRleH0+e2FmdGVybm9vbi5tYXAoYWZ0PT48RW1wbG95ZWVCdXR0b24ga2V5PXthZnQuaWR9IG9uRHJhZ1N0YXJ0SW5kZXg9e3Byb3BzLm9uRHJhZ1N0YXJ0SW5kZXh9IGlkPXthZnQuaWR9IG5hbWU9e2FmdC51c2VyX25hbWV9IHNpZ25lZGludXNlcj17cHJvcHMuc2lnbmVkaW51c2VyfSBidXR0b25zaXplPVwic21hbGxcIi8+KX08L2Rpdj48L3RkPik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBkYXlDb3VudCsrO1xuICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgaWYoZGF5Q291bnQgPiAxICYmIGRheUNvdW50IDw9IGRheXNfaW5fbW9udGgpe1xuICAgICAgICAgICAgICBsZXQgbW9ybmluZyA9IFtdO1xuICAgICAgICAgICAgICBtb3JuaW5nID0gbW9udGhJbmZvLmZpbHRlcihtb3JuID0+IG1vcm4uaWQuc3BsaXQoJy0nKVswXSA9PT0gYCR7cm93fV8ke2NvbH1fYW1gKTtcbiAgICAgICAgICAgICAgbGV0IGFmdGVybm9vbiA9IFtdO1xuICAgICAgICAgICAgICBhZnRlcm5vb24gPSBtb250aEluZm8uZmlsdGVyKGFmdCA9PiBhZnQuaWQuc3BsaXQoJy0nKVswXSA9PT0gYCR7cm93fV8ke2NvbH1fcG1gKTtcblxuICAgICAgICAgICAgICBpZihjdXJyZW50RGF5ID09PSBkYXlDb3VudCAmJiBjdXJyZW50TW9udGggPT09IG1vbnRoICYmIGN1cnJlbnRZZWFyID09PSB5ZWFyKXtcbiAgICAgICAgICAgICAgICBkYXkucHVzaCg8dGQgaWQ9e2Ake3Jvd31fJHtjb2x9YH0ga2V5PXtgJHtyb3d9XyR7Y29sfWB9IGNsYXNzTmFtZT1cImN1cnJlbnRkYXl0ZFwiPjxkaXYgY2xhc3NOYW1lPVwiZGF0ZVBvc1wiPntgJHtkYXlDb3VudH1gfTwvZGl2PjxkaXYgaWQ9e2Ake3Jvd31fJHtjb2x9X2FtYH0gY2xhc3NOYW1lPVwiYW1cIiBvbkRyb3A9e3Byb3BzLm9uRHJvcENvcHlJbmRleH0gb25EcmFnT3Zlcj17cHJvcHMub25EcmFnT3ZlckluZGV4fT57bW9ybmluZy5tYXAobW9ybj0+PEVtcGxveWVlQnV0dG9uIGtleT17bW9ybi5pZH0gb25EcmFnU3RhcnRJbmRleD17cHJvcHMub25EcmFnU3RhcnRJbmRleH0gaWQ9e21vcm4uaWR9IG5hbWU9e21vcm4udXNlcl9uYW1lfSBzaWduZWRpbnVzZXI9e3Byb3BzLnNpZ25lZGludXNlcn0gYnV0dG9uc2l6ZT1cInNtYWxsXCIvPil9PC9kaXY+PGRpdiBpZD17YCR7cm93fV8ke2NvbH1fcG1gfSBjbGFzc05hbWU9XCJwbVwiIG9uRHJvcD17cHJvcHMub25Ecm9wQ29weUluZGV4fSBvbkRyYWdPdmVyPXtwcm9wcy5vbkRyYWdPdmVySW5kZXh9PnthZnRlcm5vb24ubWFwKGFmdD0+PEVtcGxveWVlQnV0dG9uIGtleT17YWZ0LmlkfSBvbkRyYWdTdGFydEluZGV4PXtwcm9wcy5vbkRyYWdTdGFydEluZGV4fSBpZD17YWZ0LmlkfSBuYW1lPXthZnQudXNlcl9uYW1lfSBzaWduZWRpbnVzZXI9e3Byb3BzLnNpZ25lZGludXNlcn0gYnV0dG9uc2l6ZT1cInNtYWxsXCIvPil9PC9kaXY+PC90ZD4pO1xuICAgICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICBkYXkucHVzaCg8dGQgaWQ9e2Ake3Jvd31fJHtjb2x9YH0ga2V5PXtgJHtyb3d9XyR7Y29sfWB9IGNsYXNzTmFtZT1cImRheXRkXCI+PGRpdiBjbGFzc05hbWU9XCJkYXRlUG9zXCI+e2Ake2RheUNvdW50fWB9PC9kaXY+PGRpdiBpZD17YCR7cm93fV8ke2NvbH1fYW1gfSBjbGFzc05hbWU9XCJhbVwiIG9uRHJvcD17cHJvcHMub25Ecm9wQ29weUluZGV4fSBvbkRyYWdPdmVyPXtwcm9wcy5vbkRyYWdPdmVySW5kZXh9Pnttb3JuaW5nLm1hcChtb3JuPT48RW1wbG95ZWVCdXR0b24gb25EcmFnU3RhcnRJbmRleD17cHJvcHMub25EcmFnU3RhcnRJbmRleH0ga2V5PXttb3JuLmlkfSBpZD17bW9ybi5pZH0gbmFtZT17bW9ybi51c2VyX25hbWV9IHNpZ25lZGludXNlcj17cHJvcHMuc2lnbmVkaW51c2VyfSBidXR0b25zaXplPVwic21hbGxcIi8+KX08L2Rpdj48ZGl2IGlkPXtgJHtyb3d9XyR7Y29sfV9wbWB9IGNsYXNzTmFtZT1cInBtXCIgb25Ecm9wPXtwcm9wcy5vbkRyb3BDb3B5SW5kZXh9IG9uRHJhZ092ZXI9e3Byb3BzLm9uRHJhZ092ZXJJbmRleH0+e2FmdGVybm9vbi5tYXAoYWZ0PT48RW1wbG95ZWVCdXR0b24ga2V5PXthZnQuaWR9IG9uRHJhZ1N0YXJ0SW5kZXg9e3Byb3BzLm9uRHJhZ1N0YXJ0SW5kZXh9IGlkPXthZnQuaWR9IG5hbWU9e2FmdC51c2VyX25hbWV9IHNpZ25lZGludXNlcj17cHJvcHMuc2lnbmVkaW51c2VyfSBidXR0b25zaXplPVwic21hbGxcIi8+KX08L2Rpdj48L3RkPik7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZGF5Q291bnQrKztcbiAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICBkYXkucHVzaCg8dGQga2V5PXtgJHtyb3d9XyR7Y29sfWB9IGNsYXNzTmFtZT1cImRheXRkXCI+PC90ZD4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0YWJsZS5wdXNoKDx0ciBrZXk9e2Ake3Jvd31gfT57ZGF5fTwvdHI+KTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0YWJsZTtcbiAgICB9O1xuXG4gIHJldHVybihcbiAgICA8ZGl2PlxuICAgICAgPHRhYmxlIGNsYXNzTmFtZT1cImNhbHRhYmxlXCI+XG4gICAgICAgIDx0Ym9keT5cbiAgICAgICAgICB7Z2V0VGFibGUocHJvcHMubW9udGgsIHByb3BzLnllYXIsIHByb3BzLm1vbnRoSW5mbyl9XG4gICAgICAgIDwvdGJvZHk+XG4gICAgICA8L3RhYmxlPlxuICAgICAgPHN0eWxlIGpzeCBnbG9iYWw+e2BcbiAgICAgICAgLmNhbHRhYmxlIHtcbiAgICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAgICAgYm9yZGVyLXNwYWNpbmc6IDBweDtcbiAgICAgICAgICBwYWRkaW5nOiA0cHg7XG4gICAgICAgICAgZm9udC1zaXplOiAxZW07XG4gICAgICAgICAgYm9yZGVyLWNvbGxhcHNlOiBzZXBhcmF0ZTtcbiAgICAgICAgICBib3JkZXI6IDBweCBzb2xpZCAjMzU0OTgwO1xuICAgICAgICB9XG4gICAgICAgIC5oZWFkZXJ0ZHtcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgd2lkdGg6MTQlO1xuICAgICAgICAgIHBhZGRpbmc6IDRweCA1MHB4IDRweCA1MHB4O1xuICAgICAgICAgIGZvbnQtc2l6ZTogMWVtO1xuICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMzNTQ5ODA7XG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM1NDk4MDtcbiAgICAgICAgICBjb2xvcjogI2ZiY2EzNztcbiAgICAgICAgfVxuICAgICAgICAuZGF5dGR7XG4gICAgICBcdCAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICBmb250LXNpemU6IDFlbTtcbiAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjMzU0OTgwO1xuICAgICAgXHQgIGhlaWdodDogMTAwcHg7XG4gICAgICBcdCAgbWluLWhlaWdodDogMTAwcHg7XG4gICAgICAgIH1cbiAgICAgICAgLmN1cnJlbnRkYXl0ZHtcbiAgICAgIFx0ICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgIGZvbnQtc2l6ZTogMWVtO1xuICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMzNTQ5ODA7XG4gICAgICAgICAgaGVpZ2h0OiAxMDBweDtcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmJjYTM3O1xuICAgICAgICAgIGNvbG9yOiAjZmJjYTM3O1xuICAgICAgICAgIHotaW5kZXg6IC0xO1xuICAgICAgICB9XG4gICAgICAgIC5kYXRlUG9ze1xuICAgICAgICBcdGZvbnQtc2l6ZTogMWVtO1xuICAgICAgICBcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgXHRsZWZ0OiAwcHg7XG4gICAgICAgIFx0dG9wOiAwcHg7XG4gICAgICAgIFx0cmlnaHQ6IDBweDtcbiAgICAgICAgXHR0ZXh0LWFsaWduOiByaWdodDtcbiAgICAgICAgXHRwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZiY2EzNztcbiAgICAgICAgICBjb2xvcjogIzM1NDk4MDtcbiAgICAgICAgICB6LWluZGV4OiAtMTtcbiAgICAgICAgfVxuICAgICAgICAuYW17XG4gICAgICAgIFx0d2lkdGg6IDEwMCU7XG4gICAgICAgIFx0aGVpZ2h0OiBhdXRvO1xuICAgICAgICBcdG1pbi1oZWlnaHQ6IDUwcHggIWltcG9ydGFudDtcbiAgICAgICAgXHRib3JkZXI6IDFweCBkb3R0ZWQgI2ZiY2EzNztcbiAgICAgICAgXHRwYWRkaW5nLXRvcDogMTVweDtcbiAgICAgICAgfVxuICAgICAgICAuYW1DdXJyZW50RGF5e1xuICAgICAgICBcdHdpZHRoOiAxMDAlO1xuICAgICAgICBcdGhlaWdodDogYXV0bztcbiAgICAgICAgXHRtaW4taGVpZ2h0OiA1MHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIFx0Ym9yZGVyLWJvdHRvbTogMXB4IGRvdHRlZCB3aGl0ZTtcbiAgICAgICAgXHRwYWRkaW5nLXRvcDogMTVweDtcbiAgICAgICAgfVxuICAgICAgICAucG17XG4gICAgICAgIFx0d2lkdGg6IDEwMCU7XG4gICAgICAgIFx0aGVpZ2h0OiBhdXRvO1xuICAgICAgICBcdG1pbi1oZWlnaHQ6IDUwcHggIWltcG9ydGFudDtcbiAgICAgICAgXHRib3JkZXI6IDFweCBkb3R0ZWQgI2ZiY2EzNztcbiAgICAgICAgfVxuICAgICAgYH08L3N0eWxlPlxuICAgIDwvZGl2PlxuICApO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgQ2FsZW5kYXI7XG4iXX0= */\n/*@ sourceURL=/Users/paulbeaudon/calendar/next-calendar/components/Calendar.js */"));
};

/* harmony default export */ __webpack_exports__["default"] = (Calendar);

/***/ }),

/***/ "./components/EmployeeButton.js":
/*!**************************************!*\
  !*** ./components/EmployeeButton.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/Users/paulbeaudon/calendar/next-calendar/components/EmployeeButton.js";


var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;

const EmployeeButton = props => {
  let loggedInUser = false;

  if (props.signedinuser === props.name) {
    loggedInUser = true;
  }

  ;
  return __jsx("div", {
    className: "jsx-3436715881",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: undefined
  }, props.buttonsize === 'small' ? loggedInUser ? __jsx("div", {
    id: props.id,
    draggable: true,
    onDragStart: props.onDragStartIndex,
    className: "jsx-3436715881" + " " + "myDList small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: undefined
  }, props.name) : __jsx("div", {
    id: props.id,
    draggable: true,
    onDragStart: props.onDragStartIndex,
    className: "jsx-3436715881" + " " + "dListBorder small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: undefined
  }, props.name) : loggedInUser ? __jsx("div", {
    draggable: true,
    onDragStart: props.onDragStartIndex,
    className: "jsx-3436715881" + " " + "myDList",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: undefined
  }, props.name) : __jsx("div", {
    draggable: true,
    onDragStart: props.onDragStartIndex,
    className: "jsx-3436715881" + " " + "dListBorder",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: undefined
  }, props.name), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    id: "3436715881",
    __self: undefined
  }, ".dListBorder.jsx-3436715881{text-align:center;margin:10px 10px 10px 10px;padding:10px 10px 10px 10px;border:1px solid #354980;background-color:white;color:#354980;box-shadow:2px 2px 2px grey;}.myDList.jsx-3436715881{text-align:center;margin:10px 10px 10px 10px;padding:10px 10px 10px 10px;border:1px solid #354980;background-color:#354980;color:#fbca37;box-shadow:2px 2px 2px grey;}.small.jsx-3436715881{font-size:.8em;margin:5px 10px 5px 5px;padding:2px 2px 2px 2px;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wYXVsYmVhdWRvbi9jYWxlbmRhci9uZXh0LWNhbGVuZGFyL2NvbXBvbmVudHMvRW1wbG95ZWVCdXR0b24uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBY2tCLEFBRzRCLEFBU0EsQUFTRixlQUNTLEdBbEJFLEFBU0EscUJBVUYsTUFsQkcsQUFTQSxrQkFVN0IsVUFsQjBCLEFBU0EseUJBUkYsQUFTRSx1QkFSVixFQVNBLFlBUmEsRUFTQSwwQkFSN0IsRUFTQSIsImZpbGUiOiIvVXNlcnMvcGF1bGJlYXVkb24vY2FsZW5kYXIvbmV4dC1jYWxlbmRhci9jb21wb25lbnRzL0VtcGxveWVlQnV0dG9uLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG5jb25zdCBFbXBsb3llZUJ1dHRvbiA9IHByb3BzID0+e1xuXG4gIGxldCBsb2dnZWRJblVzZXIgPSBmYWxzZTtcbiAgaWYocHJvcHMuc2lnbmVkaW51c2VyID09PSBwcm9wcy5uYW1lKXtcbiAgICBsb2dnZWRJblVzZXIgPSB0cnVlO1xuICB9O1xuICByZXR1cm4oXG4gICAgPGRpdj5cbiAgICAgIHsgcHJvcHMuYnV0dG9uc2l6ZSA9PT0gJ3NtYWxsJyA/XG4gICAgICAgIChsb2dnZWRJblVzZXIgPyA8ZGl2IGNsYXNzTmFtZT1cIm15RExpc3Qgc21hbGxcIiBpZD17cHJvcHMuaWR9IGRyYWdnYWJsZSBvbkRyYWdTdGFydD17cHJvcHMub25EcmFnU3RhcnRJbmRleH0+e3Byb3BzLm5hbWV9PC9kaXY+IDogPGRpdiBjbGFzc05hbWU9XCJkTGlzdEJvcmRlciBzbWFsbFwiIGlkPXtwcm9wcy5pZH0gZHJhZ2dhYmxlIG9uRHJhZ1N0YXJ0PXtwcm9wcy5vbkRyYWdTdGFydEluZGV4fT57cHJvcHMubmFtZX08L2Rpdj4pXG4gICAgICAgIDpcbiAgICAgICAgKChsb2dnZWRJblVzZXIgPyA8ZGl2IGNsYXNzTmFtZT1cIm15RExpc3RcIiBkcmFnZ2FibGUgb25EcmFnU3RhcnQ9e3Byb3BzLm9uRHJhZ1N0YXJ0SW5kZXh9Pntwcm9wcy5uYW1lfTwvZGl2PiA6IDxkaXYgY2xhc3NOYW1lPVwiZExpc3RCb3JkZXJcIiBkcmFnZ2FibGUgb25EcmFnU3RhcnQ9e3Byb3BzLm9uRHJhZ1N0YXJ0SW5kZXh9Pntwcm9wcy5uYW1lfTwvZGl2PikpXG4gICAgICB9XG4gICAgICA8c3R5bGUganN4PntgXG4gICAgICAgIC5kTGlzdEJvcmRlcntcbiAgICAgICAgXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIFx0bWFyZ2luOiAxMHB4IDEwcHggMTBweCAxMHB4O1xuICAgICAgICBcdHBhZGRpbmc6IDEwcHggMTBweCAxMHB4IDEwcHg7XG4gICAgICAgIFx0Ym9yZGVyOiAxcHggc29saWQgIzM1NDk4MDtcbiAgICAgICAgXHRiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICBjb2xvcjogIzM1NDk4MDtcbiAgICAgIFx0ICBib3gtc2hhZG93OiAycHggMnB4IDJweCBncmV5O1xuICAgICAgICB9XG4gICAgICAgIC5teURMaXN0e1xuICAgICAgXHQgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgXHRtYXJnaW46IDEwcHggMTBweCAxMHB4IDEwcHg7XG4gICAgICAgIFx0cGFkZGluZzogMTBweCAxMHB4IDEwcHggMTBweDtcbiAgICAgICAgXHRib3JkZXI6IDFweCBzb2xpZCAjMzU0OTgwO1xuICAgICAgICBcdGJhY2tncm91bmQtY29sb3I6ICMzNTQ5ODA7XG4gICAgICAgICAgY29sb3I6ICNmYmNhMzc7XG4gICAgICBcdCAgYm94LXNoYWRvdzogMnB4IDJweCAycHggZ3JleTtcbiAgICAgICAgfVxuICAgICAgICAuc21hbGx7XG4gICAgICAgICAgZm9udC1zaXplOiAuOGVtO1xuICAgICAgICAgIG1hcmdpbjogNXB4IDEwcHggNXB4IDVweDtcbiAgICAgICAgICBwYWRkaW5nOiAycHggMnB4IDJweCAycHg7XG4gICAgICAgIH1cbiAgICAgIGB9PC9zdHlsZT5cbiAgICA8L2Rpdj5cbiAgKTtcblxufTtcblxuZXhwb3J0IGRlZmF1bHQgRW1wbG95ZWVCdXR0b247XG4iXX0= */\n/*@ sourceURL=/Users/paulbeaudon/calendar/next-calendar/components/EmployeeButton.js */"));
};

/* harmony default export */ __webpack_exports__["default"] = (EmployeeButton);

/***/ }),

/***/ "./components/helperfunctions.js":
/*!***************************************!*\
  !*** ./components/helperfunctions.js ***!
  \***************************************/
/*! exports provided: currentDay, currentMonth, currentYear, daysInMonth, startDay, getWrittenMonth, getIntMonth */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "currentDay", function() { return currentDay; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "currentMonth", function() { return currentMonth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "currentYear", function() { return currentYear; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "daysInMonth", function() { return daysInMonth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startDay", function() { return startDay; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getWrittenMonth", function() { return getWrittenMonth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getIntMonth", function() { return getIntMonth; });
let currentDay = () => {
  return new Date().getDate();
};

let currentMonth = () => {
  return new Date().getMonth() + 1;
};

let currentYear = () => {
  return new Date().getFullYear();
};

let daysInMonth = (month, year) => {
  return new Date(year, month, 0).getDate();
};

let startDay = (month, year) => {
  //let daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  return new Date(year, month - 1, 1).getDay();
};

let getWrittenMonth = monthInt => {
  let month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  return month[monthInt - 1];
};

let getIntMonth = monthString => {
  let month = {
    "Jan": 1,
    "Feb": 2,
    "Mar": 3,
    "Apr": 4,
    "May": 5,
    "Jun": 6,
    "Jul": 7,
    "Aug": 8,
    "Sep": 9,
    "Oct": 10,
    "Nov": 11,
    "Dec": 12
  };
  return month[monthString];
};



/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_Calendar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/Calendar */ "./components/Calendar.js");
/* harmony import */ var _components_EmployeeButton__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/EmployeeButton */ "./components/EmployeeButton.js");
/* harmony import */ var _components_helperfunctions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/helperfunctions */ "./components/helperfunctions.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! isomorphic-unfetch */ "isomorphic-unfetch");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_spinners_kit__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-spinners-kit */ "react-spinners-kit");
/* harmony import */ var react_spinners_kit__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_spinners_kit__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/Users/paulbeaudon/calendar/next-calendar/pages/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement;







const Index = props => {
  const location = props.location;
  let initialMonthInfo = props.collections.length > 0 ? props.collections[0].data[_components_helperfunctions__WEBPACK_IMPORTED_MODULE_3__["getWrittenMonth"](props.currentMonth)] : [];
  let users = props.users.map(user => {
    return user.data;
  });
  users = users.filter(user => {
    if (user.status === 'activated' && (user.access === 'dispatch' || user.access === 'admin')) {
      return user;
    }
  });
  users.sort(function (a, b) {
    if (a.user_name.toUpperCase() < b.user_name.toUpperCase()) return -1;
    if (a.user_name.toUpperCase() > b.user_name.toUpperCase()) return 1;
    return 0;
  });
  let signedInUser = 'Beaudon';
  const {
    0: month,
    1: setMonth
  } = Object(react__WEBPACK_IMPORTED_MODULE_5__["useState"])(props.currentMonth);
  const {
    0: year,
    1: setYear
  } = Object(react__WEBPACK_IMPORTED_MODULE_5__["useState"])(props.currentYear);
  const {
    0: draggedButton,
    1: setDraggedButton
  } = Object(react__WEBPACK_IMPORTED_MODULE_5__["useState"])({});
  const {
    0: monthInfo,
    1: setMonthInfo
  } = Object(react__WEBPACK_IMPORTED_MODULE_5__["useState"])(initialMonthInfo);
  const {
    0: monthInfoCount,
    1: setMonthInfoCount
  } = Object(react__WEBPACK_IMPORTED_MODULE_5__["useState"])(monthInfo.length);
  const {
    0: monthInfoSaveFlag,
    1: setMonthInfoSaveFlag
  } = Object(react__WEBPACK_IMPORTED_MODULE_5__["useState"])(0);
  const {
    0: loading,
    1: setLoading
  } = Object(react__WEBPACK_IMPORTED_MODULE_5__["useState"])(false);

  const onDragStart = (event, user) => {
    //event.preventDefault();
    setDraggedButton({
      userInfo: user,
      userId: event.target.id
    });
    console.log('on drag start');
    console.log(event.target.id);
  };
  /*const onDrag = (event, user) =>{
    event.preventDefault();
    console.log('dragging');
    console.log(draggedButton.userInfo.user_name);
  };*/


  const onDragOver = event => {
    event.preventDefault();
  };

  const onDropDelete = event => {
    console.log('Delete:');
    console.log(draggedButton);
    console.log(event.target.id);

    if (draggedButton.userId) {
      console.log('delete this');

      for (let x = 0; x < monthInfo.length; x++) {
        if (monthInfo[x].id === draggedButton.userId) {
          console.log('delete: ' + draggedButton.userId);
          monthInfo.splice(x, 1);
          setMonthInfo[monthInfo => monthInfo];
          setMonthInfoSaveFlag(monthInfoSaveFlag => monthInfoSaveFlag + 1);
          break;
        }
      }

      console.log(monthInfo);
    } else {
      console.log('do not delete this');
    }

    setDraggedButton({});
  };

  const onDropCopy = event => {
    if (!draggedButton.userId) {
      console.log(event.target.id);
      console.log(monthInfoCount);
      console.log('Copy:');
      console.log(draggedButton.userInfo.user_name);
      let newId = event.target.id.split('-')[0] + '-' + monthInfoCount;

      if (event.target.id) {
        // if target is not blank, then update props
        setMonthInfo([...monthInfo, {
          access: draggedButton.userInfo.access,
          user_name: draggedButton.userInfo.user_name,
          status: draggedButton.userInfo.status,
          id: newId
        }]);
        setMonthInfoCount(monthInfoCount => monthInfoCount + 1);
        setMonthInfoSaveFlag(monthInfoSaveFlag => monthInfoSaveFlag + 1);
      }
    } else {
      console.log('move');
      let newId = event.target.id + '-' + draggedButton.userId.split('-')[1];
      console.log(newId);

      for (let x = 0; x < monthInfo.length; x++) {
        if (monthInfo[x].id === draggedButton.userId) {
          console.log('change: ' + draggedButton.userId + ' to ' + newId);
          monthInfo[x].id = newId;
          setMonthInfo[monthInfo => monthInfo];
          setMonthInfoSaveFlag(monthInfoSaveFlag => monthInfoSaveFlag + 1);
          break;
        }
      }
    }

    setDraggedButton({});
  };

  let previousMonthCal = () => {
    setLoading(true);

    if (month - 1 < 1) {
      const newMonth = 12;
      const newYear = year - 1;
      loadCalendar(newYear, newMonth);
    } else {
      const newMonth = month - 1;
      const newYear = year;
      loadCalendar(newYear, newMonth);
    }
  };

  let nextMonthCal = () => {
    setLoading(true);

    if (month + 1 > 12) {
      const newMonth = 1;
      const newYear = year + 1;
      loadCalendar(newYear, newMonth);
    } else {
      const newMonth = month + 1;
      const newYear = year;
      loadCalendar(newYear, newMonth);
    }
  };

  let currentMonthCal = () => {
    setLoading(true);
    const newMonth = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_3__["currentMonth"]();
    const newYear = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_3__["currentYear"]();
    loadCalendar(newYear, newMonth);
  };

  let loadCalendar = async (newYear, newMonth) => {
    const resC = await isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4___default()(`${location}/api/collections?year=${newYear}&month=${_components_helperfunctions__WEBPACK_IMPORTED_MODULE_3__["getWrittenMonth"](newMonth)}`);
    const {
      collections,
      err
    } = await resC.json();
    const calJson = collections.length > 0 ? collections[0].data[_components_helperfunctions__WEBPACK_IMPORTED_MODULE_3__["getWrittenMonth"](newMonth)] : [];
    setMonthInfo(calJson);
    setMonthInfoCount(calJson.length);
    setMonth(newMonth);
    setYear(newYear);
    setLoading(false);
  };

  Object(react__WEBPACK_IMPORTED_MODULE_5__["useEffect"])(() => {
    // used to save calendar when a drop has taken place, and need to wait for monthInfo to update before calling save.
    saveCalendar({
      type: 'drop'
    });
  }, [monthInfoSaveFlag]);

  let saveCalendar = action => {
    console.log(action);
    let data = {
      "year": year.toString(),
      "month": _components_helperfunctions__WEBPACK_IMPORTED_MODULE_3__["getWrittenMonth"](month),
      [_components_helperfunctions__WEBPACK_IMPORTED_MODULE_3__["getWrittenMonth"](month)]: monthInfo
    };
    isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4___default()(`${location}/api/collections`, {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => {
      return res.json();
    }).then(json => {
      if (action.type === 'none') {
        console.log(json.my_status);
      }

      if (action.type === 'next') {
        console.log('next');
        console.log(json.my_status);
        nextMonthCal();
      }

      if (action.type === 'previous') {
        console.log('previous');
        console.log(json.my_status);
        previousMonthCal();
      }

      if (action.type === 'current') {
        console.log('current');
        conosle.log(json.my_status);
        currentMonthCal();
      }

      if (action.type === 'drop') {
        console.log('drop');
        console.log(json.my_status);
      }
    });
  };

  return __jsx("div", {
    className: "jsx-4013782617",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 199
    },
    __self: undefined
  }, loading ? __jsx("div", {
    className: "jsx-4013782617" + " " + "spinner",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 200
    },
    __self: undefined
  }, __jsx(react_spinners_kit__WEBPACK_IMPORTED_MODULE_6__["CircleSpinner"], {
    size: 80,
    color: "#354980",
    loading: loading,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 200
    },
    __self: undefined
  })) : __jsx("div", {
    className: "jsx-4013782617",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 200
    },
    __self: undefined
  }), __jsx("div", {
    id: "left_pane",
    className: "jsx-4013782617" + " " + "leftPane",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 201
    },
    __self: undefined
  }, __jsx("div", {
    className: "jsx-4013782617" + " " + "headerDiv",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 202
    },
    __self: undefined
  }, "Dispatch Staff"), users.map(user => __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_2__["default"], {
    key: user.user_name,
    onDragStartIndex: event => onDragStart(event, user),
    name: user.user_name,
    signedinuser: signedInUser,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 204
    },
    __self: undefined
  })), __jsx("div", {
    id: "delete_pane",
    onDrop: event => onDropDelete(event),
    onDragOver: event => onDragOver(event),
    className: "jsx-4013782617" + " " + "deletePane",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 206
    },
    __self: undefined
  }, "Drag here to Delete")), __jsx("div", {
    className: "jsx-4013782617" + " " + "rightPane",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 208
    },
    __self: undefined
  }, __jsx("div", {
    id: "monthTitle",
    className: "jsx-4013782617" + " " + "headerDiv title",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 209
    },
    __self: undefined
  }, _components_helperfunctions__WEBPACK_IMPORTED_MODULE_3__["getWrittenMonth"](month), " ", year, " Desk Schedule"), __jsx("div", {
    className: "jsx-4013782617" + " " + "headerDiv",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 210
    },
    __self: undefined
  }, __jsx("button", {
    id: "today",
    onClick: e => currentMonthCal(),
    className: "jsx-4013782617" + " " + "button",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 211
    },
    __self: undefined
  }, "Today"), " ", __jsx("button", {
    id: "leftarrow",
    onClick: e => saveCalendar({
      type: 'previous'
    }),
    className: "jsx-4013782617" + " " + "button",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 211
    },
    __self: undefined
  }, "<<"), " ", __jsx("button", {
    id: "rightarrow",
    onClick: e => saveCalendar({
      type: 'next'
    }),
    className: "jsx-4013782617" + " " + "button",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 211
    },
    __self: undefined
  }, ">>")), __jsx(_components_Calendar__WEBPACK_IMPORTED_MODULE_1__["default"], {
    onDragStartIndex: event => onDragStart(event, ""),
    onDragOverIndex: event => onDragOver(event),
    onDropCopyIndex: event => onDropCopy(event),
    month: month,
    year: year,
    monthInfo: monthInfo,
    signedinuser: signedInUser,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 213
    },
    __self: undefined
  })), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    id: "4013782617",
    __self: undefined
  }, ".spinner{position:absolute;left:50%;top:50%;z-index:1;}.title{font-size:1.9em !important;}.headerDiv{margin:auto;width:80%;text-align:center;font-size:1.2em;padding:10px;}.fullPane{position:relative;width:100%;}.leftPane{position:absolute;left:60px;top:68px;bottom:10px;width:15%;}.rightPane{position:absolute;right:10px;top:10px;bottom:10px;width:80%;}.deletePane{text-align:center;margin:10px 10px 10px 10px;padding:10px 10px 10px 10px;border:1px dashed #354980;background-color:white;color:#354980;height:50%;}.button{background-color:#354980;border:none;color:#fbca37;padding:5px 25px;text-align:center;-webkit-text-decoration:none;text-decoration:none;display:inline-block;margin:4px 2px;cursor:pointer;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wYXVsYmVhdWRvbi9jYWxlbmRhci9uZXh0LWNhbGVuZGFyL3BhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXNOeUIsQUFHNkIsQUFNUyxBQUdoQixBQU9NLEFBSUEsQUFPQSxBQU9DLEFBU08sWUFqQ2hCLE1BVEEsQUFnQkMsQUFJRCxBQU9DLEFBT2dCLElBeEJULEdBaUNMLEVBMUNKLEFBS1YsQ0FlVSxDQUpWLEFBV1UsTUExQkUsRUFvQkMsQUFzQkcsQ0FmSCxFQWxCSSxLQVJqQixBQWdDNkIsSUFabEIsQ0FPRCxDQWVTLEtBakNMLEdBWWQsQ0FPQSxRQWVvQixDQWpDcEIsSUF1QjJCLGFBV0osYUFWQyx1QkFDUixjQUNILEFBU1UsV0FSdkIsVUFTaUIsZUFDQSxlQUNqQiIsImZpbGUiOiIvVXNlcnMvcGF1bGJlYXVkb24vY2FsZW5kYXIvbmV4dC1jYWxlbmRhci9wYWdlcy9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBDYWwgZnJvbSBcIi4uL2NvbXBvbmVudHMvQ2FsZW5kYXJcIjtcbmltcG9ydCBFbXBsb3llZUJ1dHRvbiBmcm9tIFwiLi4vY29tcG9uZW50cy9FbXBsb3llZUJ1dHRvblwiO1xuaW1wb3J0ICogYXMgaGVscEZ1bmMgZnJvbSBcIi4uL2NvbXBvbmVudHMvaGVscGVyZnVuY3Rpb25zXCI7XG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLXVuZmV0Y2gnO1xuaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBDaXJjbGVTcGlubmVyIH0gZnJvbSBcInJlYWN0LXNwaW5uZXJzLWtpdFwiO1xuXG5jb25zdCBJbmRleCA9IHByb3BzID0+e1xuICBjb25zdCBsb2NhdGlvbiA9IHByb3BzLmxvY2F0aW9uO1xuXG4gIGxldCBpbml0aWFsTW9udGhJbmZvID0gcHJvcHMuY29sbGVjdGlvbnMubGVuZ3RoID4gMCA/IHByb3BzLmNvbGxlY3Rpb25zWzBdLmRhdGFbaGVscEZ1bmMuZ2V0V3JpdHRlbk1vbnRoKHByb3BzLmN1cnJlbnRNb250aCldIDogW107XG5cbiAgbGV0IHVzZXJzID0gcHJvcHMudXNlcnMubWFwKCh1c2VyKT0+e1xuICAgIHJldHVybiB1c2VyLmRhdGE7XG4gIH0pO1xuXG4gIHVzZXJzID0gdXNlcnMuZmlsdGVyKCh1c2VyKT0+e1xuICAgIGlmKHVzZXIuc3RhdHVzID09PSAnYWN0aXZhdGVkJyAmJiAodXNlci5hY2Nlc3MgPT09ICdkaXNwYXRjaCcgfHwgdXNlci5hY2Nlc3MgPT09ICdhZG1pbicpKXtcbiAgICAgIHJldHVybiB1c2VyO1xuICAgIH1cbiAgfSk7XG4gIHVzZXJzLnNvcnQoZnVuY3Rpb24oYSxiKXtcbiAgICBpZihhLnVzZXJfbmFtZS50b1VwcGVyQ2FzZSgpIDwgYi51c2VyX25hbWUudG9VcHBlckNhc2UoKSkgcmV0dXJuIC0xO1xuICAgIGlmKGEudXNlcl9uYW1lLnRvVXBwZXJDYXNlKCkgPiBiLnVzZXJfbmFtZS50b1VwcGVyQ2FzZSgpKSByZXR1cm4gMTtcbiAgICByZXR1cm4gMDtcbiAgfSk7XG4gIGxldCBzaWduZWRJblVzZXIgPSAnQmVhdWRvbic7XG5cbiAgY29uc3QgW21vbnRoLCBzZXRNb250aF0gPSB1c2VTdGF0ZShwcm9wcy5jdXJyZW50TW9udGgpO1xuICBjb25zdCBbeWVhciwgc2V0WWVhcl0gPSB1c2VTdGF0ZShwcm9wcy5jdXJyZW50WWVhcik7XG4gIGNvbnN0IFtkcmFnZ2VkQnV0dG9uLCBzZXREcmFnZ2VkQnV0dG9uXSA9IHVzZVN0YXRlKHt9KTtcbiAgY29uc3QgW21vbnRoSW5mbywgc2V0TW9udGhJbmZvXSA9IHVzZVN0YXRlKGluaXRpYWxNb250aEluZm8pO1xuXG4gIGNvbnN0IFttb250aEluZm9Db3VudCwgc2V0TW9udGhJbmZvQ291bnRdID0gdXNlU3RhdGUobW9udGhJbmZvLmxlbmd0aCk7XG4gIGNvbnN0IFttb250aEluZm9TYXZlRmxhZywgc2V0TW9udGhJbmZvU2F2ZUZsYWddID0gdXNlU3RhdGUoMCk7XG4gIGNvbnN0IFtsb2FkaW5nLCBzZXRMb2FkaW5nXSA9IHVzZVN0YXRlKGZhbHNlKTtcblxuICBjb25zdCBvbkRyYWdTdGFydCA9IChldmVudCwgdXNlcikgPT4ge1xuICAgIC8vZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICBzZXREcmFnZ2VkQnV0dG9uKHtcbiAgICAgIHVzZXJJbmZvOiB1c2VyLFxuICAgICAgdXNlcklkOiBldmVudC50YXJnZXQuaWRcbiAgICB9KTtcbiAgICBjb25zb2xlLmxvZygnb24gZHJhZyBzdGFydCcpO1xuICAgIGNvbnNvbGUubG9nKGV2ZW50LnRhcmdldC5pZCk7XG4gIH07XG5cbiAgLypjb25zdCBvbkRyYWcgPSAoZXZlbnQsIHVzZXIpID0+e1xuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgY29uc29sZS5sb2coJ2RyYWdnaW5nJyk7XG4gICAgY29uc29sZS5sb2coZHJhZ2dlZEJ1dHRvbi51c2VySW5mby51c2VyX25hbWUpO1xuICB9OyovXG5cbiAgY29uc3Qgb25EcmFnT3ZlciA9IChldmVudCk9PntcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICB9O1xuXG4gIGNvbnN0IG9uRHJvcERlbGV0ZSA9IChldmVudCk9PntcbiAgICBjb25zb2xlLmxvZygnRGVsZXRlOicpO1xuICAgIGNvbnNvbGUubG9nKGRyYWdnZWRCdXR0b24pO1xuICAgIGNvbnNvbGUubG9nKGV2ZW50LnRhcmdldC5pZCk7XG4gICAgaWYoZHJhZ2dlZEJ1dHRvbi51c2VySWQpe1xuICAgICAgY29uc29sZS5sb2coJ2RlbGV0ZSB0aGlzJyk7XG4gICAgICBmb3IobGV0IHg9MDsgeDxtb250aEluZm8ubGVuZ3RoOyB4Kyspe1xuICAgICAgICBpZihtb250aEluZm9beF0uaWQgPT09IGRyYWdnZWRCdXR0b24udXNlcklkKXtcbiAgICAgICAgICBjb25zb2xlLmxvZygnZGVsZXRlOiAnICsgZHJhZ2dlZEJ1dHRvbi51c2VySWQpO1xuICAgICAgICAgIG1vbnRoSW5mby5zcGxpY2UoeCwgMSk7XG4gICAgICAgICAgc2V0TW9udGhJbmZvW21vbnRoSW5mbyA9PiBtb250aEluZm9dO1xuICAgICAgICAgIHNldE1vbnRoSW5mb1NhdmVGbGFnKG1vbnRoSW5mb1NhdmVGbGFnID0+IG1vbnRoSW5mb1NhdmVGbGFnICsgMSk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGNvbnNvbGUubG9nKG1vbnRoSW5mbyk7XG4gICAgfWVsc2V7XG4gICAgICBjb25zb2xlLmxvZygnZG8gbm90IGRlbGV0ZSB0aGlzJyk7XG4gICAgfVxuXG4gICAgc2V0RHJhZ2dlZEJ1dHRvbih7fSk7XG4gIH07XG5cbiAgY29uc3Qgb25Ecm9wQ29weSA9IChldmVudCk9PntcbiAgICBpZighZHJhZ2dlZEJ1dHRvbi51c2VySWQpe1xuICAgICAgY29uc29sZS5sb2coZXZlbnQudGFyZ2V0LmlkKTtcbiAgICAgIGNvbnNvbGUubG9nKG1vbnRoSW5mb0NvdW50KTtcbiAgICAgIGNvbnNvbGUubG9nKCdDb3B5OicpO1xuICAgICAgY29uc29sZS5sb2coZHJhZ2dlZEJ1dHRvbi51c2VySW5mby51c2VyX25hbWUpO1xuICAgICAgbGV0IG5ld0lkID0gZXZlbnQudGFyZ2V0LmlkLnNwbGl0KCctJylbMF0gKyAnLScgKyBtb250aEluZm9Db3VudDtcbiAgICAgIGlmKGV2ZW50LnRhcmdldC5pZCl7IC8vIGlmIHRhcmdldCBpcyBub3QgYmxhbmssIHRoZW4gdXBkYXRlIHByb3BzXG4gICAgICAgIHNldE1vbnRoSW5mbyhbLi4ubW9udGhJbmZvLCB7YWNjZXNzOiBkcmFnZ2VkQnV0dG9uLnVzZXJJbmZvLmFjY2VzcywgdXNlcl9uYW1lOmRyYWdnZWRCdXR0b24udXNlckluZm8udXNlcl9uYW1lLCBzdGF0dXM6ZHJhZ2dlZEJ1dHRvbi51c2VySW5mby5zdGF0dXMsIGlkOiBuZXdJZH1dKTtcbiAgICAgICAgc2V0TW9udGhJbmZvQ291bnQobW9udGhJbmZvQ291bnQgPT4gbW9udGhJbmZvQ291bnQgKyAxKTtcbiAgICAgICAgc2V0TW9udGhJbmZvU2F2ZUZsYWcobW9udGhJbmZvU2F2ZUZsYWcgPT4gbW9udGhJbmZvU2F2ZUZsYWcgKyAxKTtcbiAgICAgIH1cbiAgICB9ZWxzZXtcbiAgICAgIGNvbnNvbGUubG9nKCdtb3ZlJyk7XG4gICAgICBsZXQgbmV3SWQgPSBldmVudC50YXJnZXQuaWQgKyAnLScgKyBkcmFnZ2VkQnV0dG9uLnVzZXJJZC5zcGxpdCgnLScpWzFdO1xuICAgICAgY29uc29sZS5sb2cobmV3SWQpO1xuICAgICAgZm9yKGxldCB4PTA7IHg8bW9udGhJbmZvLmxlbmd0aDsgeCsrKXtcbiAgICAgICAgaWYobW9udGhJbmZvW3hdLmlkID09PSBkcmFnZ2VkQnV0dG9uLnVzZXJJZCl7XG4gICAgICAgICAgY29uc29sZS5sb2coJ2NoYW5nZTogJyArIGRyYWdnZWRCdXR0b24udXNlcklkICsgJyB0byAnICsgbmV3SWQpO1xuICAgICAgICAgIG1vbnRoSW5mb1t4XS5pZCA9IG5ld0lkO1xuICAgICAgICAgIHNldE1vbnRoSW5mb1ttb250aEluZm8gPT4gbW9udGhJbmZvXTtcbiAgICAgICAgICBzZXRNb250aEluZm9TYXZlRmxhZyhtb250aEluZm9TYXZlRmxhZyA9PiBtb250aEluZm9TYXZlRmxhZyArIDEpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgICAgc2V0RHJhZ2dlZEJ1dHRvbih7fSk7XG4gIH07XG5cbiAgbGV0IHByZXZpb3VzTW9udGhDYWwgPSAoKT0+e1xuICAgIHNldExvYWRpbmcodHJ1ZSk7XG4gICAgaWYobW9udGgtMSA8IDEpe1xuICAgICAgY29uc3QgbmV3TW9udGggPSAxMjtcbiAgICAgIGNvbnN0IG5ld1llYXIgPSB5ZWFyLTE7XG4gICAgICBsb2FkQ2FsZW5kYXIobmV3WWVhciwgbmV3TW9udGgpO1xuICAgIH1lbHNle1xuICAgICAgY29uc3QgbmV3TW9udGggPSBtb250aC0xO1xuICAgICAgY29uc3QgbmV3WWVhciA9IHllYXI7XG4gICAgICBsb2FkQ2FsZW5kYXIobmV3WWVhciwgbmV3TW9udGgpO1xuICAgIH1cbiAgfTtcblxuICBsZXQgbmV4dE1vbnRoQ2FsID0gKCk9PntcbiAgICBzZXRMb2FkaW5nKHRydWUpO1xuICAgIGlmKG1vbnRoKzEgPiAxMil7XG4gICAgICBjb25zdCBuZXdNb250aCA9IDE7XG4gICAgICBjb25zdCBuZXdZZWFyID0geWVhcisxO1xuICAgICAgbG9hZENhbGVuZGFyKG5ld1llYXIsIG5ld01vbnRoKTtcbiAgICB9ZWxzZXtcbiAgICAgIGNvbnN0IG5ld01vbnRoID0gbW9udGgrMTtcbiAgICAgIGNvbnN0IG5ld1llYXIgPSB5ZWFyO1xuICAgICAgbG9hZENhbGVuZGFyKG5ld1llYXIsIG5ld01vbnRoKTtcbiAgICB9XG4gIH07XG5cbiAgbGV0IGN1cnJlbnRNb250aENhbCA9ICgpPT57XG4gICAgc2V0TG9hZGluZyh0cnVlKTtcbiAgICBjb25zdCBuZXdNb250aCA9IGhlbHBGdW5jLmN1cnJlbnRNb250aCgpO1xuICAgIGNvbnN0IG5ld1llYXIgPSBoZWxwRnVuYy5jdXJyZW50WWVhcigpO1xuICAgIGxvYWRDYWxlbmRhcihuZXdZZWFyLCBuZXdNb250aCk7XG4gIH07XG5cbiAgbGV0IGxvYWRDYWxlbmRhciA9IGFzeW5jKG5ld1llYXIsIG5ld01vbnRoKT0+e1xuICAgIGNvbnN0IHJlc0MgPSBhd2FpdCBmZXRjaChgJHtsb2NhdGlvbn0vYXBpL2NvbGxlY3Rpb25zP3llYXI9JHtuZXdZZWFyfSZtb250aD0ke2hlbHBGdW5jLmdldFdyaXR0ZW5Nb250aChuZXdNb250aCl9YCk7XG4gICAgY29uc3QgeyBjb2xsZWN0aW9ucywgZXJyIH0gPSBhd2FpdCByZXNDLmpzb24oKTtcbiAgICBjb25zdCBjYWxKc29uID0gY29sbGVjdGlvbnMubGVuZ3RoID4gMCA/IGNvbGxlY3Rpb25zWzBdLmRhdGFbaGVscEZ1bmMuZ2V0V3JpdHRlbk1vbnRoKG5ld01vbnRoKV0gOiBbXTtcblxuICAgIHNldE1vbnRoSW5mbyhjYWxKc29uKTtcbiAgICBzZXRNb250aEluZm9Db3VudChjYWxKc29uLmxlbmd0aCk7XG4gICAgc2V0TW9udGgobmV3TW9udGgpO1xuICAgIHNldFllYXIobmV3WWVhcik7XG4gICAgc2V0TG9hZGluZyhmYWxzZSk7XG4gIH07XG5cbiAgdXNlRWZmZWN0KCgpID0+IHsgLy8gdXNlZCB0byBzYXZlIGNhbGVuZGFyIHdoZW4gYSBkcm9wIGhhcyB0YWtlbiBwbGFjZSwgYW5kIG5lZWQgdG8gd2FpdCBmb3IgbW9udGhJbmZvIHRvIHVwZGF0ZSBiZWZvcmUgY2FsbGluZyBzYXZlLlxuICAgICBzYXZlQ2FsZW5kYXIoe3R5cGU6ICdkcm9wJ30pO1xuICB9LCBbbW9udGhJbmZvU2F2ZUZsYWddKTtcblxuICBsZXQgc2F2ZUNhbGVuZGFyID0gKGFjdGlvbik9PntcbiAgICBjb25zb2xlLmxvZyhhY3Rpb24pO1xuICAgIGxldCBkYXRhID0ge1wieWVhclwiOiB5ZWFyLnRvU3RyaW5nKCksIFwibW9udGhcIjogaGVscEZ1bmMuZ2V0V3JpdHRlbk1vbnRoKG1vbnRoKSwgW2hlbHBGdW5jLmdldFdyaXR0ZW5Nb250aChtb250aCldOiBtb250aEluZm99O1xuICAgIGZldGNoKGAke2xvY2F0aW9ufS9hcGkvY29sbGVjdGlvbnNgLCB7XG4gICAgICBtZXRob2Q6ICdwb3N0JyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uLCB0ZXh0L3BsYWluLCAqLyonLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nXG4gICAgICB9LFxuICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoZGF0YSlcbiAgICB9KS50aGVuKChyZXMpID0+IHtcbiAgICAgIHJldHVybiByZXMuanNvbigpO1xuICAgIH0pLnRoZW4oKGpzb24pPT57XG4gICAgICBpZihhY3Rpb24udHlwZSA9PT0gJ25vbmUnKXtcbiAgICAgICAgY29uc29sZS5sb2coanNvbi5teV9zdGF0dXMpO1xuICAgICAgfVxuICAgICAgaWYoYWN0aW9uLnR5cGUgPT09ICduZXh0Jyl7XG4gICAgICAgIGNvbnNvbGUubG9nKCduZXh0Jyk7XG4gICAgICAgIGNvbnNvbGUubG9nKGpzb24ubXlfc3RhdHVzKTtcbiAgICAgICAgbmV4dE1vbnRoQ2FsKCk7XG4gICAgICB9XG4gICAgICBpZihhY3Rpb24udHlwZSA9PT0gJ3ByZXZpb3VzJyl7XG4gICAgICAgIGNvbnNvbGUubG9nKCdwcmV2aW91cycpO1xuICAgICAgICBjb25zb2xlLmxvZyhqc29uLm15X3N0YXR1cyk7XG4gICAgICAgIHByZXZpb3VzTW9udGhDYWwoKTtcbiAgICAgIH1cbiAgICAgIGlmKGFjdGlvbi50eXBlID09PSAnY3VycmVudCcpe1xuICAgICAgICBjb25zb2xlLmxvZygnY3VycmVudCcpO1xuICAgICAgICBjb25vc2xlLmxvZyhqc29uLm15X3N0YXR1cyk7XG4gICAgICAgIGN1cnJlbnRNb250aENhbCgpO1xuICAgICAgfVxuICAgICAgaWYoYWN0aW9uLnR5cGUgPT09ICdkcm9wJyl7XG4gICAgICAgIGNvbnNvbGUubG9nKCdkcm9wJyk7XG4gICAgICAgIGNvbnNvbGUubG9nKGpzb24ubXlfc3RhdHVzKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfTtcblxuICByZXR1cm4oXG4gICAgPGRpdj5cbiAgICAgIHtsb2FkaW5nID8gPGRpdiBjbGFzc05hbWU9XCJzcGlubmVyXCI+PENpcmNsZVNwaW5uZXIgc2l6ZT17ODB9IGNvbG9yPVwiIzM1NDk4MFwiIGxvYWRpbmc9e2xvYWRpbmd9IC8+PC9kaXY+IDogPGRpdj48L2Rpdj59XG4gICAgICA8ZGl2IGlkPVwibGVmdF9wYW5lXCIgY2xhc3NOYW1lPVwibGVmdFBhbmVcIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJoZWFkZXJEaXZcIj5EaXNwYXRjaCBTdGFmZjwvZGl2PlxuICAgICAgICB7dXNlcnMubWFwKHVzZXI9PihcbiAgICAgICAgICAgIDxFbXBsb3llZUJ1dHRvbiBrZXk9e3VzZXIudXNlcl9uYW1lfSBvbkRyYWdTdGFydEluZGV4PXtldmVudCA9PiBvbkRyYWdTdGFydChldmVudCwgdXNlcil9IG5hbWU9e3VzZXIudXNlcl9uYW1lfSBzaWduZWRpbnVzZXI9e3NpZ25lZEluVXNlcn0vPlxuICAgICAgICApKX1cbiAgICAgICAgPGRpdiBpZD1cImRlbGV0ZV9wYW5lXCIgY2xhc3NOYW1lPVwiZGVsZXRlUGFuZVwiIG9uRHJvcD17ZXZlbnQgPT4gb25Ecm9wRGVsZXRlKGV2ZW50KX0gb25EcmFnT3Zlcj17ZXZlbnQgPT4gb25EcmFnT3ZlcihldmVudCl9PkRyYWcgaGVyZSB0byBEZWxldGU8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJyaWdodFBhbmVcIj5cbiAgICBcdCAgPGRpdiBjbGFzc05hbWU9XCJoZWFkZXJEaXYgdGl0bGVcIiBpZD0nbW9udGhUaXRsZSc+e2hlbHBGdW5jLmdldFdyaXR0ZW5Nb250aChtb250aCl9IHt5ZWFyfSBEZXNrIFNjaGVkdWxlPC9kaXY+XG4gICAgXHQgIDxkaXYgY2xhc3NOYW1lPVwiaGVhZGVyRGl2XCI+XG4gICAgXHRcdCAgPGJ1dHRvbiBjbGFzc05hbWU9XCJidXR0b25cIiBpZD1cInRvZGF5XCIgb25DbGljaz17ZT0+KGN1cnJlbnRNb250aENhbCgpKX0+VG9kYXk8L2J1dHRvbj4gPGJ1dHRvbiBjbGFzc05hbWU9XCJidXR0b25cIiBpZD0nbGVmdGFycm93JyBvbkNsaWNrPXtlPT4oc2F2ZUNhbGVuZGFyKHt0eXBlOiAncHJldmlvdXMnfSkpfT4mbHQ7Jmx0OzwvYnV0dG9uPiA8YnV0dG9uIGNsYXNzTmFtZT1cImJ1dHRvblwiIGlkPSdyaWdodGFycm93JyBvbkNsaWNrPXtlPT4oc2F2ZUNhbGVuZGFyKHt0eXBlOiAnbmV4dCd9KSl9PiZndDsmZ3Q7PC9idXR0b24+XG4gICAgXHQgIDwvZGl2PlxuICAgIFx0ICA8Q2FsIG9uRHJhZ1N0YXJ0SW5kZXg9e2V2ZW50ID0+IG9uRHJhZ1N0YXJ0KGV2ZW50LCBcIlwiKX0gb25EcmFnT3ZlckluZGV4PXtldmVudCA9PiBvbkRyYWdPdmVyKGV2ZW50KX0gb25Ecm9wQ29weUluZGV4PXtldmVudCA9PiBvbkRyb3BDb3B5KGV2ZW50KX0gbW9udGg9e21vbnRofSB5ZWFyPXt5ZWFyfSBtb250aEluZm89e21vbnRoSW5mb30gc2lnbmVkaW51c2VyPXtzaWduZWRJblVzZXJ9Lz5cbiAgICBcdDwvZGl2PlxuICAgICAgPHN0eWxlIGpzeCBnbG9iYWw+e2BcbiAgICAgICAgLnNwaW5uZXJ7XG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgIGxlZnQ6IDUwJTtcbiAgICAgICAgICB0b3A6IDUwJTtcbiAgICAgICAgICB6LWluZGV4OiAxO1xuICAgICAgICB9XG4gICAgICAgIC50aXRsZXtcbiAgICAgICAgICBmb250LXNpemU6IDEuOWVtICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cbiAgICAgICAgLmhlYWRlckRpdntcbiAgICAgICAgXHRtYXJnaW46IGF1dG87XG4gICAgICAgIFx0d2lkdGg6IDgwJTtcbiAgICAgICAgXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIFx0Zm9udC1zaXplOiAxLjJlbTtcbiAgICAgICAgXHRwYWRkaW5nOiAxMHB4O1xuICAgICAgICB9XG4gICAgICAgIC5mdWxsUGFuZXtcbiAgICAgICAgXHRwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIFx0d2lkdGg6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgICAgLmxlZnRQYW5le1xuICAgICAgICBcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgXHRsZWZ0OiA2MHB4O1xuICAgICAgICBcdHRvcDogNjhweDtcbiAgICAgICAgXHRib3R0b206IDEwcHg7XG4gICAgICAgIFx0d2lkdGg6IDE1JTtcbiAgICAgICAgfVxuICAgICAgICAucmlnaHRQYW5le1xuICAgICAgICBcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgXHRyaWdodDogMTBweDtcbiAgICAgICAgXHR0b3A6IDEwcHg7XG4gICAgICAgIFx0Ym90dG9tOiAxMHB4O1xuICAgICAgICBcdHdpZHRoOjgwJTtcbiAgICAgICAgfVxuICAgICAgICAuZGVsZXRlUGFuZXtcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIFx0bWFyZ2luOiAxMHB4IDEwcHggMTBweCAxMHB4O1xuICAgICAgICBcdHBhZGRpbmc6IDEwcHggMTBweCAxMHB4IDEwcHg7XG4gICAgICAgIFx0Ym9yZGVyOiAxcHggZGFzaGVkICMzNTQ5ODA7XG4gICAgICAgIFx0YmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgICAgICAgY29sb3I6ICMzNTQ5ODA7XG4gICAgICAgICAgaGVpZ2h0OiA1MCU7XG4gICAgICAgIH1cbiAgICAgICAgLmJ1dHRvbiB7XG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM1NDk4MDtcbiAgICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgICAgICAgY29sb3I6ICNmYmNhMzc7XG4gICAgICAgICAgcGFkZGluZzogNXB4IDI1cHg7XG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgICAgbWFyZ2luOiA0cHggMnB4O1xuICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgfVxuICAgICAgYH08L3N0eWxlPlxuICAgIDwvZGl2PlxuICApO1xufTtcblxuSW5kZXguZ2V0SW5pdGlhbFByb3BzID0gYXN5bmMgKHtyZXEsIHJlc3BvbnNlfSk9PntcbiAgdHJ5e1xuXG4gICAgLy9jb25zdCB7ICd4LW5vdy1kZXBsb3ltZW50LXVybCc6IG5vd1VSTCB9ID0gcmVxLmhlYWRlcnM7XG4gICAgY29uc3QgeyAnaG9zdCc6IFVSTCB9ID0gcmVxLmhlYWRlcnM7XG4gICAgY29uc3QgbG9jYXRpb24gPSAoVVJMID09PSAnbG9jYWxob3N0OjMwMDAnKSA/ICdodHRwOi8vbG9jYWxob3N0OjMwMDAnIDogJ2h0dHBzOi8vbmV4dC1jYWxlbmRhci5ub3cuc2gnO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2goYCR7bG9jYXRpb259L2FwaS9nZXRVc2Vyc2ApO1xuICAgIC8vY29uc3QgcmVzID0gYXdhaXQgZmV0Y2goYGh0dHBzOi8vbmV4dC1jYWxlbmRhci5ub3cuc2gvYXBpL2dldFVzZXJzYCk7IC8vIGluaXRpYWwgY2FsbHMgdG8gZGF0YWJhc2UgLSBzZXJ2ZXIgc2lkZSByZW5kZXIgZm9yIHNwZWVkXG4gICAgY29uc3QgeyB1c2VycywgZXJyb3IgfSA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICBjb25zdCBjdXJyZW50TW9udGggPSBoZWxwRnVuYy5jdXJyZW50TW9udGgoKTtcbiAgICBjb25zdCBjdXJyZW50TW9udGhXcml0dGVuID0gaGVscEZ1bmMuZ2V0V3JpdHRlbk1vbnRoKGN1cnJlbnRNb250aCk7XG4gICAgY29uc3QgY3VycmVudFllYXIgPSBoZWxwRnVuYy5jdXJyZW50WWVhcigpO1xuXG4gICAgY29uc3QgcmVzQyA9IGF3YWl0IGZldGNoKGAke2xvY2F0aW9ufS9hcGkvY29sbGVjdGlvbnM/eWVhcj0ke2N1cnJlbnRZZWFyfSZtb250aD0ke2N1cnJlbnRNb250aFdyaXR0ZW59YCk7XG4gICAgLy9jb25zdCByZXNDID0gYXdhaXQgZmV0Y2goYGh0dHBzOi8vbmV4dC1jYWxlbmRhci5ub3cuc2gvYXBpL2NvbGxlY3Rpb25zP3llYXI9JHtjdXJyZW50WWVhcn0mbW9udGg9JHtjdXJyZW50TW9udGhXcml0dGVufWApO1xuICAgIGNvbnN0IHsgY29sbGVjdGlvbnMsIGVyciB9ID0gYXdhaXQgcmVzQy5qc29uKCk7XG5cbiAgICBjb25zb2xlLmxvZygnSW5pdGlhbFByb3BzOiBjYWxsIHRvIGRhdGFiYXNlJyk7XG4gICAgY29uc29sZS5sb2coJ3YyIGluZGV4LmpzJyk7XG4gICAgcmV0dXJuIHtsb2NhdGlvbjogbG9jYXRpb24sIGN1cnJlbnRNb250aDogY3VycmVudE1vbnRoLCBjdXJyZW50WWVhcjogY3VycmVudFllYXIsIGNvbGxlY3Rpb25zLCB1c2Vyc307XG4gIH1jYXRjaCAoZXJyb3Ipe1xuICAgIGNvbnNvbGUubG9nKGVycm9yKVxuICB9XG59O1xuXG5leHBvcnQgZGVmYXVsdCBJbmRleDtcbiJdfQ== */\n/*@ sourceURL=/Users/paulbeaudon/calendar/next-calendar/pages/index.js */"));
};

Index.getInitialProps = async ({
  req,
  response
}) => {
  try {
    //const { 'x-now-deployment-url': nowURL } = req.headers;
    const {
      'host': URL
    } = req.headers;
    const location = URL === 'localhost:3000' ? 'http://localhost:3000' : 'https://next-calendar.now.sh';
    const res = await isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4___default()(`${location}/api/getUsers`); //const res = await fetch(`https://next-calendar.now.sh/api/getUsers`); // initial calls to database - server side render for speed

    const {
      users,
      error
    } = await res.json();
    const currentMonth = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_3__["currentMonth"]();
    const currentMonthWritten = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_3__["getWrittenMonth"](currentMonth);
    const currentYear = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_3__["currentYear"]();
    const resC = await isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4___default()(`${location}/api/collections?year=${currentYear}&month=${currentMonthWritten}`); //const resC = await fetch(`https://next-calendar.now.sh/api/collections?year=${currentYear}&month=${currentMonthWritten}`);

    const {
      collections,
      err
    } = await resC.json();
    console.log('InitialProps: call to database');
    console.log('v2 index.js');
    return {
      location: location,
      currentMonth: currentMonth,
      currentYear: currentYear,
      collections,
      users
    };
  } catch (error) {
    console.log(error);
  }
};

/* harmony default export */ __webpack_exports__["default"] = (Index);

/***/ }),

/***/ 4:
/*!******************************!*\
  !*** multi ./pages/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/paulbeaudon/calendar/next-calendar/pages/index.js */"./pages/index.js");


/***/ }),

/***/ "isomorphic-unfetch":
/*!*************************************!*\
  !*** external "isomorphic-unfetch" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-spinners-kit":
/*!*************************************!*\
  !*** external "react-spinners-kit" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-spinners-kit");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map