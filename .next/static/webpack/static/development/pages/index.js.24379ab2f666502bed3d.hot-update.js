webpackHotUpdate("static/development/pages/index.js",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/json/stringify */ "./node_modules/@babel/runtime-corejs2/core-js/json/stringify.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/toConsumableArray */ "./node_modules/@babel/runtime-corejs2/helpers/esm/toConsumableArray.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_Calendar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/Calendar */ "./components/Calendar.js");
/* harmony import */ var _components_EmployeeButton__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/EmployeeButton */ "./components/EmployeeButton.js");
/* harmony import */ var _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/helperfunctions */ "./components/helperfunctions.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/next/dist/build/polyfills/fetch/index.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_spinners_kit__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-spinners-kit */ "./node_modules/react-spinners-kit/lib/index.js");
/* harmony import */ var react_spinners_kit__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_spinners_kit__WEBPACK_IMPORTED_MODULE_10__);




var _jsxFileName = "/Users/paulbeaudon/calendar/next-calendar/pages/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement;







var Index = function Index(props) {
  var location = props.location;
  var initialMonthInfo = props.collections.length > 0 ? props.collections[0].data[_components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["getWrittenMonth"](props.currentMonth)] : [];
  var users = props.users.map(function (user) {
    return user.data;
  });
  users = users.filter(function (user) {
    if (user.status === 'activated' && (user.access === 'dispatch' || user.access === 'admin')) {
      return user;
    }
  });
  users.sort(function (a, b) {
    if (a.user_name.toUpperCase() < b.user_name.toUpperCase()) return -1;
    if (a.user_name.toUpperCase() > b.user_name.toUpperCase()) return 1;
    return 0;
  });
  var signedInUser = 'Beaudon';

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_9__["useState"])(props.currentMonth),
      month = _useState[0],
      setMonth = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_9__["useState"])(props.currentYear),
      year = _useState2[0],
      setYear = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_9__["useState"])({}),
      draggedButton = _useState3[0],
      setDraggedButton = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_9__["useState"])(initialMonthInfo),
      monthInfo = _useState4[0],
      setMonthInfo = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_9__["useState"])(monthInfo.length),
      monthInfoCount = _useState5[0],
      setMonthInfoCount = _useState5[1];

  var _useState6 = Object(react__WEBPACK_IMPORTED_MODULE_9__["useState"])(0),
      monthInfoSaveFlag = _useState6[0],
      setMonthInfoSaveFlag = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_9__["useState"])(false),
      loading = _useState7[0],
      setLoading = _useState7[1];

  var onDragStart = function onDragStart(event, user) {
    //event.preventDefault();
    setDraggedButton({
      userInfo: user,
      userId: event.target.id
    });
    console.log('on drag start');
    console.log(event.target.id);
  };
  /*const onDrag = (event, user) =>{
    event.preventDefault();
    console.log('dragging');
    console.log(draggedButton.userInfo.user_name);
  };*/


  var _onDragOver = function onDragOver(event) {
    event.preventDefault();
  };

  var onDropDelete = function onDropDelete(event) {
    console.log('Delete:');
    console.log(draggedButton);
    console.log(event.target.id);

    if (draggedButton.userId) {
      console.log('delete this');

      for (var x = 0; x < monthInfo.length; x++) {
        if (monthInfo[x].id === draggedButton.userId) {
          console.log('delete: ' + draggedButton.userId);
          monthInfo.splice(x, 1);
          setMonthInfo[function (monthInfo) {
            return monthInfo;
          }];
          setMonthInfoSaveFlag(function (monthInfoSaveFlag) {
            return monthInfoSaveFlag + 1;
          });
          break;
        }
      }

      console.log(monthInfo);
    } else {
      console.log('do not delete this');
    }

    setDraggedButton({});
  };

  var onDropCopy = function onDropCopy(event) {
    if (!draggedButton.userId) {
      console.log(event.target.id);
      console.log(monthInfoCount);
      console.log('Copy:');
      console.log(draggedButton.userInfo.user_name);
      var newId = event.target.id.split('-')[0] + '-' + monthInfoCount;

      if (event.target.id) {
        // if target is not blank, then update props
        setMonthInfo([].concat(Object(_babel_runtime_corejs2_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_3__["default"])(monthInfo), [{
          access: draggedButton.userInfo.access,
          user_name: draggedButton.userInfo.user_name,
          status: draggedButton.userInfo.status,
          id: newId
        }]));
        setMonthInfoCount(function (monthInfoCount) {
          return monthInfoCount + 1;
        });
        setMonthInfoSaveFlag(function (monthInfoSaveFlag) {
          return monthInfoSaveFlag + 1;
        });
      }
    } else {
      console.log('move');

      var _newId = event.target.id + '-' + draggedButton.userId.split('-')[1];

      console.log(_newId);

      for (var x = 0; x < monthInfo.length; x++) {
        if (monthInfo[x].id === draggedButton.userId) {
          console.log('change: ' + draggedButton.userId + ' to ' + _newId);
          monthInfo[x].id = _newId;
          setMonthInfo[function (monthInfo) {
            return monthInfo;
          }];
          setMonthInfoSaveFlag(function (monthInfoSaveFlag) {
            return monthInfoSaveFlag + 1;
          });
          break;
        }
      }
    }

    setDraggedButton({});
  };

  var previousMonthCal = function previousMonthCal() {
    setLoading(true);

    if (month - 1 < 1) {
      var newMonth = 12;
      var newYear = year - 1;
      loadCalendar(newYear, newMonth);
    } else {
      var _newMonth = month - 1;

      var _newYear = year;
      loadCalendar(_newYear, _newMonth);
    }
  };

  var nextMonthCal = function nextMonthCal() {
    setLoading(true);

    if (month + 1 > 12) {
      var newMonth = 1;
      var newYear = year + 1;
      loadCalendar(newYear, newMonth);
    } else {
      var _newMonth2 = month + 1;

      var _newYear2 = year;
      loadCalendar(_newYear2, _newMonth2);
    }
  };

  var currentMonthCal = function currentMonthCal() {
    setLoading(true);
    var newMonth = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["currentMonth"]();
    var newYear = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["currentYear"]();
    loadCalendar(newYear, newMonth);
  };

  var loadCalendar = function loadCalendar(newYear, newMonth) {
    var resC, _ref, collections, err, calJson;

    return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.async(function loadCalendar$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8___default()("".concat(location, "/api/collections?year=").concat(newYear, "&month=").concat(_components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["getWrittenMonth"](newMonth))));

          case 2:
            resC = _context.sent;
            _context.next = 5;
            return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.awrap(resC.json());

          case 5:
            _ref = _context.sent;
            collections = _ref.collections;
            err = _ref.err;
            calJson = collections.length > 0 ? collections[0].data[_components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["getWrittenMonth"](newMonth)] : [];
            setMonthInfo(calJson);
            setMonthInfoCount(calJson.length);
            setMonth(newMonth);
            setYear(newYear);
            setLoading(false);

          case 14:
          case "end":
            return _context.stop();
        }
      }
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_9__["useEffect"])(function () {
    // used to save calendar when a drop has taken place, and need to wait for monthInfo to update before calling save.
    saveCalendar({
      type: 'drop'
    });
  }, [monthInfoSaveFlag]);

  var saveCalendar = function saveCalendar(action) {
    console.log(action);

    var data = Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({
      "year": year.toString(),
      "month": _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["getWrittenMonth"](month)
    }, _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["getWrittenMonth"](month), monthInfo);

    isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8___default()("".concat(location, "/api/collections"), {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: _babel_runtime_corejs2_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0___default()(data)
    }).then(function (res) {
      return res.json();
    }).then(function (json) {
      if (action.type === 'none') {
        console.log(json.my_status);
      }

      if (action.type === 'next') {
        console.log('next');
        console.log(json.my_status);
        nextMonthCal();
      }

      if (action.type === 'previous') {
        console.log('previous');
        console.log(json.my_status);
        previousMonthCal();
      }

      if (action.type === 'current') {
        console.log('current');
        conosle.log(json.my_status);
        currentMonthCal();
      }

      if (action.type === 'drop') {
        console.log('drop');
        console.log(json.my_status);
      }
    });
  };

  return __jsx("div", {
    className: "jsx-4013782617",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 199
    },
    __self: this
  }, loading ? __jsx("div", {
    className: "jsx-4013782617" + " " + "spinner",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 200
    },
    __self: this
  }, __jsx(react_spinners_kit__WEBPACK_IMPORTED_MODULE_10__["CircleSpinner"], {
    size: 80,
    color: "#354980",
    loading: loading,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 200
    },
    __self: this
  })) : __jsx("div", {
    className: "jsx-4013782617",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 200
    },
    __self: this
  }), __jsx("div", {
    id: "left_pane",
    className: "jsx-4013782617" + " " + "leftPane",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 201
    },
    __self: this
  }, __jsx("div", {
    className: "jsx-4013782617" + " " + "headerDiv",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 202
    },
    __self: this
  }, "Dispatch Staff"), users.map(function (user) {
    return __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_6__["default"], {
      key: user.user_name,
      onDragStartIndex: function onDragStartIndex(event) {
        return onDragStart(event, user);
      },
      name: user.user_name,
      signedinuser: signedInUser,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 204
      },
      __self: this
    });
  }), __jsx("div", {
    id: "delete_pane",
    onDrop: function onDrop(event) {
      return onDropDelete(event);
    },
    onDragOver: function onDragOver(event) {
      return _onDragOver(event);
    },
    className: "jsx-4013782617" + " " + "deletePane",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 206
    },
    __self: this
  }, "Drag here to Delete")), __jsx("div", {
    className: "jsx-4013782617" + " " + "rightPane",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 208
    },
    __self: this
  }, __jsx("div", {
    id: "monthTitle",
    className: "jsx-4013782617" + " " + "headerDiv title",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 209
    },
    __self: this
  }, _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["getWrittenMonth"](month), " ", year, " Desk Schedule"), __jsx("div", {
    className: "jsx-4013782617" + " " + "headerDiv",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 210
    },
    __self: this
  }, __jsx("button", {
    id: "today",
    onClick: function onClick(e) {
      return currentMonthCal();
    },
    className: "jsx-4013782617" + " " + "button",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 211
    },
    __self: this
  }, "Today"), " ", __jsx("button", {
    id: "leftarrow",
    onClick: function onClick(e) {
      return saveCalendar({
        type: 'previous'
      });
    },
    className: "jsx-4013782617" + " " + "button",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 211
    },
    __self: this
  }, "<<"), " ", __jsx("button", {
    id: "rightarrow",
    onClick: function onClick(e) {
      return saveCalendar({
        type: 'next'
      });
    },
    className: "jsx-4013782617" + " " + "button",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 211
    },
    __self: this
  }, ">>")), __jsx(_components_Calendar__WEBPACK_IMPORTED_MODULE_5__["default"], {
    onDragStartIndex: function onDragStartIndex(event) {
      return onDragStart(event, "");
    },
    onDragOverIndex: function onDragOverIndex(event) {
      return _onDragOver(event);
    },
    onDropCopyIndex: function onDropCopyIndex(event) {
      return onDropCopy(event);
    },
    month: month,
    year: year,
    monthInfo: monthInfo,
    signedinuser: signedInUser,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 213
    },
    __self: this
  })), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_4___default.a, {
    id: "4013782617",
    __self: this
  }, ".spinner{position:absolute;left:50%;top:50%;z-index:1;}.title{font-size:1.9em !important;}.headerDiv{margin:auto;width:80%;text-align:center;font-size:1.2em;padding:10px;}.fullPane{position:relative;width:100%;}.leftPane{position:absolute;left:60px;top:68px;bottom:10px;width:15%;}.rightPane{position:absolute;right:10px;top:10px;bottom:10px;width:80%;}.deletePane{text-align:center;margin:10px 10px 10px 10px;padding:10px 10px 10px 10px;border:1px dashed #354980;background-color:white;color:#354980;height:50%;}.button{background-color:#354980;border:none;color:#fbca37;padding:5px 25px;text-align:center;-webkit-text-decoration:none;text-decoration:none;display:inline-block;margin:4px 2px;cursor:pointer;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wYXVsYmVhdWRvbi9jYWxlbmRhci9uZXh0LWNhbGVuZGFyL3BhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXNOeUIsQUFHNkIsQUFNUyxBQUdoQixBQU9NLEFBSUEsQUFPQSxBQU9DLEFBU08sWUFqQ2hCLE1BVEEsQUFnQkMsQUFJRCxBQU9DLEFBT2dCLElBeEJULEdBaUNMLEVBMUNKLEFBS1YsQ0FlVSxDQUpWLEFBV1UsTUExQkUsRUFvQkMsQUFzQkcsQ0FmSCxFQWxCSSxLQVJqQixBQWdDNkIsSUFabEIsQ0FPRCxDQWVTLEtBakNMLEdBWWQsQ0FPQSxRQWVvQixDQWpDcEIsSUF1QjJCLGFBV0osYUFWQyx1QkFDUixjQUNILEFBU1UsV0FSdkIsVUFTaUIsZUFDQSxlQUNqQiIsImZpbGUiOiIvVXNlcnMvcGF1bGJlYXVkb24vY2FsZW5kYXIvbmV4dC1jYWxlbmRhci9wYWdlcy9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBDYWwgZnJvbSBcIi4uL2NvbXBvbmVudHMvQ2FsZW5kYXJcIjtcbmltcG9ydCBFbXBsb3llZUJ1dHRvbiBmcm9tIFwiLi4vY29tcG9uZW50cy9FbXBsb3llZUJ1dHRvblwiO1xuaW1wb3J0ICogYXMgaGVscEZ1bmMgZnJvbSBcIi4uL2NvbXBvbmVudHMvaGVscGVyZnVuY3Rpb25zXCI7XG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLXVuZmV0Y2gnO1xuaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBDaXJjbGVTcGlubmVyIH0gZnJvbSBcInJlYWN0LXNwaW5uZXJzLWtpdFwiO1xuXG5jb25zdCBJbmRleCA9IHByb3BzID0+e1xuICBjb25zdCBsb2NhdGlvbiA9IHByb3BzLmxvY2F0aW9uO1xuXG4gIGxldCBpbml0aWFsTW9udGhJbmZvID0gcHJvcHMuY29sbGVjdGlvbnMubGVuZ3RoID4gMCA/IHByb3BzLmNvbGxlY3Rpb25zWzBdLmRhdGFbaGVscEZ1bmMuZ2V0V3JpdHRlbk1vbnRoKHByb3BzLmN1cnJlbnRNb250aCldIDogW107XG5cbiAgbGV0IHVzZXJzID0gcHJvcHMudXNlcnMubWFwKCh1c2VyKT0+e1xuICAgIHJldHVybiB1c2VyLmRhdGE7XG4gIH0pO1xuXG4gIHVzZXJzID0gdXNlcnMuZmlsdGVyKCh1c2VyKT0+e1xuICAgIGlmKHVzZXIuc3RhdHVzID09PSAnYWN0aXZhdGVkJyAmJiAodXNlci5hY2Nlc3MgPT09ICdkaXNwYXRjaCcgfHwgdXNlci5hY2Nlc3MgPT09ICdhZG1pbicpKXtcbiAgICAgIHJldHVybiB1c2VyO1xuICAgIH1cbiAgfSk7XG4gIHVzZXJzLnNvcnQoZnVuY3Rpb24oYSxiKXtcbiAgICBpZihhLnVzZXJfbmFtZS50b1VwcGVyQ2FzZSgpIDwgYi51c2VyX25hbWUudG9VcHBlckNhc2UoKSkgcmV0dXJuIC0xO1xuICAgIGlmKGEudXNlcl9uYW1lLnRvVXBwZXJDYXNlKCkgPiBiLnVzZXJfbmFtZS50b1VwcGVyQ2FzZSgpKSByZXR1cm4gMTtcbiAgICByZXR1cm4gMDtcbiAgfSk7XG4gIGxldCBzaWduZWRJblVzZXIgPSAnQmVhdWRvbic7XG5cbiAgY29uc3QgW21vbnRoLCBzZXRNb250aF0gPSB1c2VTdGF0ZShwcm9wcy5jdXJyZW50TW9udGgpO1xuICBjb25zdCBbeWVhciwgc2V0WWVhcl0gPSB1c2VTdGF0ZShwcm9wcy5jdXJyZW50WWVhcik7XG4gIGNvbnN0IFtkcmFnZ2VkQnV0dG9uLCBzZXREcmFnZ2VkQnV0dG9uXSA9IHVzZVN0YXRlKHt9KTtcbiAgY29uc3QgW21vbnRoSW5mbywgc2V0TW9udGhJbmZvXSA9IHVzZVN0YXRlKGluaXRpYWxNb250aEluZm8pO1xuXG4gIGNvbnN0IFttb250aEluZm9Db3VudCwgc2V0TW9udGhJbmZvQ291bnRdID0gdXNlU3RhdGUobW9udGhJbmZvLmxlbmd0aCk7XG4gIGNvbnN0IFttb250aEluZm9TYXZlRmxhZywgc2V0TW9udGhJbmZvU2F2ZUZsYWddID0gdXNlU3RhdGUoMCk7XG4gIGNvbnN0IFtsb2FkaW5nLCBzZXRMb2FkaW5nXSA9IHVzZVN0YXRlKGZhbHNlKTtcblxuICBjb25zdCBvbkRyYWdTdGFydCA9IChldmVudCwgdXNlcikgPT4ge1xuICAgIC8vZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICBzZXREcmFnZ2VkQnV0dG9uKHtcbiAgICAgIHVzZXJJbmZvOiB1c2VyLFxuICAgICAgdXNlcklkOiBldmVudC50YXJnZXQuaWRcbiAgICB9KTtcbiAgICBjb25zb2xlLmxvZygnb24gZHJhZyBzdGFydCcpO1xuICAgIGNvbnNvbGUubG9nKGV2ZW50LnRhcmdldC5pZCk7XG4gIH07XG5cbiAgLypjb25zdCBvbkRyYWcgPSAoZXZlbnQsIHVzZXIpID0+e1xuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgY29uc29sZS5sb2coJ2RyYWdnaW5nJyk7XG4gICAgY29uc29sZS5sb2coZHJhZ2dlZEJ1dHRvbi51c2VySW5mby51c2VyX25hbWUpO1xuICB9OyovXG5cbiAgY29uc3Qgb25EcmFnT3ZlciA9IChldmVudCk9PntcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICB9O1xuXG4gIGNvbnN0IG9uRHJvcERlbGV0ZSA9IChldmVudCk9PntcbiAgICBjb25zb2xlLmxvZygnRGVsZXRlOicpO1xuICAgIGNvbnNvbGUubG9nKGRyYWdnZWRCdXR0b24pO1xuICAgIGNvbnNvbGUubG9nKGV2ZW50LnRhcmdldC5pZCk7XG4gICAgaWYoZHJhZ2dlZEJ1dHRvbi51c2VySWQpe1xuICAgICAgY29uc29sZS5sb2coJ2RlbGV0ZSB0aGlzJyk7XG4gICAgICBmb3IobGV0IHg9MDsgeDxtb250aEluZm8ubGVuZ3RoOyB4Kyspe1xuICAgICAgICBpZihtb250aEluZm9beF0uaWQgPT09IGRyYWdnZWRCdXR0b24udXNlcklkKXtcbiAgICAgICAgICBjb25zb2xlLmxvZygnZGVsZXRlOiAnICsgZHJhZ2dlZEJ1dHRvbi51c2VySWQpO1xuICAgICAgICAgIG1vbnRoSW5mby5zcGxpY2UoeCwgMSk7XG4gICAgICAgICAgc2V0TW9udGhJbmZvW21vbnRoSW5mbyA9PiBtb250aEluZm9dO1xuICAgICAgICAgIHNldE1vbnRoSW5mb1NhdmVGbGFnKG1vbnRoSW5mb1NhdmVGbGFnID0+IG1vbnRoSW5mb1NhdmVGbGFnICsgMSk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGNvbnNvbGUubG9nKG1vbnRoSW5mbyk7XG4gICAgfWVsc2V7XG4gICAgICBjb25zb2xlLmxvZygnZG8gbm90IGRlbGV0ZSB0aGlzJyk7XG4gICAgfVxuXG4gICAgc2V0RHJhZ2dlZEJ1dHRvbih7fSk7XG4gIH07XG5cbiAgY29uc3Qgb25Ecm9wQ29weSA9IChldmVudCk9PntcbiAgICBpZighZHJhZ2dlZEJ1dHRvbi51c2VySWQpe1xuICAgICAgY29uc29sZS5sb2coZXZlbnQudGFyZ2V0LmlkKTtcbiAgICAgIGNvbnNvbGUubG9nKG1vbnRoSW5mb0NvdW50KTtcbiAgICAgIGNvbnNvbGUubG9nKCdDb3B5OicpO1xuICAgICAgY29uc29sZS5sb2coZHJhZ2dlZEJ1dHRvbi51c2VySW5mby51c2VyX25hbWUpO1xuICAgICAgbGV0IG5ld0lkID0gZXZlbnQudGFyZ2V0LmlkLnNwbGl0KCctJylbMF0gKyAnLScgKyBtb250aEluZm9Db3VudDtcbiAgICAgIGlmKGV2ZW50LnRhcmdldC5pZCl7IC8vIGlmIHRhcmdldCBpcyBub3QgYmxhbmssIHRoZW4gdXBkYXRlIHByb3BzXG4gICAgICAgIHNldE1vbnRoSW5mbyhbLi4ubW9udGhJbmZvLCB7YWNjZXNzOiBkcmFnZ2VkQnV0dG9uLnVzZXJJbmZvLmFjY2VzcywgdXNlcl9uYW1lOmRyYWdnZWRCdXR0b24udXNlckluZm8udXNlcl9uYW1lLCBzdGF0dXM6ZHJhZ2dlZEJ1dHRvbi51c2VySW5mby5zdGF0dXMsIGlkOiBuZXdJZH1dKTtcbiAgICAgICAgc2V0TW9udGhJbmZvQ291bnQobW9udGhJbmZvQ291bnQgPT4gbW9udGhJbmZvQ291bnQgKyAxKTtcbiAgICAgICAgc2V0TW9udGhJbmZvU2F2ZUZsYWcobW9udGhJbmZvU2F2ZUZsYWcgPT4gbW9udGhJbmZvU2F2ZUZsYWcgKyAxKTtcbiAgICAgIH1cbiAgICB9ZWxzZXtcbiAgICAgIGNvbnNvbGUubG9nKCdtb3ZlJyk7XG4gICAgICBsZXQgbmV3SWQgPSBldmVudC50YXJnZXQuaWQgKyAnLScgKyBkcmFnZ2VkQnV0dG9uLnVzZXJJZC5zcGxpdCgnLScpWzFdO1xuICAgICAgY29uc29sZS5sb2cobmV3SWQpO1xuICAgICAgZm9yKGxldCB4PTA7IHg8bW9udGhJbmZvLmxlbmd0aDsgeCsrKXtcbiAgICAgICAgaWYobW9udGhJbmZvW3hdLmlkID09PSBkcmFnZ2VkQnV0dG9uLnVzZXJJZCl7XG4gICAgICAgICAgY29uc29sZS5sb2coJ2NoYW5nZTogJyArIGRyYWdnZWRCdXR0b24udXNlcklkICsgJyB0byAnICsgbmV3SWQpO1xuICAgICAgICAgIG1vbnRoSW5mb1t4XS5pZCA9IG5ld0lkO1xuICAgICAgICAgIHNldE1vbnRoSW5mb1ttb250aEluZm8gPT4gbW9udGhJbmZvXTtcbiAgICAgICAgICBzZXRNb250aEluZm9TYXZlRmxhZyhtb250aEluZm9TYXZlRmxhZyA9PiBtb250aEluZm9TYXZlRmxhZyArIDEpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgICAgc2V0RHJhZ2dlZEJ1dHRvbih7fSk7XG4gIH07XG5cbiAgbGV0IHByZXZpb3VzTW9udGhDYWwgPSAoKT0+e1xuICAgIHNldExvYWRpbmcodHJ1ZSk7XG4gICAgaWYobW9udGgtMSA8IDEpe1xuICAgICAgY29uc3QgbmV3TW9udGggPSAxMjtcbiAgICAgIGNvbnN0IG5ld1llYXIgPSB5ZWFyLTE7XG4gICAgICBsb2FkQ2FsZW5kYXIobmV3WWVhciwgbmV3TW9udGgpO1xuICAgIH1lbHNle1xuICAgICAgY29uc3QgbmV3TW9udGggPSBtb250aC0xO1xuICAgICAgY29uc3QgbmV3WWVhciA9IHllYXI7XG4gICAgICBsb2FkQ2FsZW5kYXIobmV3WWVhciwgbmV3TW9udGgpO1xuICAgIH1cbiAgfTtcblxuICBsZXQgbmV4dE1vbnRoQ2FsID0gKCk9PntcbiAgICBzZXRMb2FkaW5nKHRydWUpO1xuICAgIGlmKG1vbnRoKzEgPiAxMil7XG4gICAgICBjb25zdCBuZXdNb250aCA9IDE7XG4gICAgICBjb25zdCBuZXdZZWFyID0geWVhcisxO1xuICAgICAgbG9hZENhbGVuZGFyKG5ld1llYXIsIG5ld01vbnRoKTtcbiAgICB9ZWxzZXtcbiAgICAgIGNvbnN0IG5ld01vbnRoID0gbW9udGgrMTtcbiAgICAgIGNvbnN0IG5ld1llYXIgPSB5ZWFyO1xuICAgICAgbG9hZENhbGVuZGFyKG5ld1llYXIsIG5ld01vbnRoKTtcbiAgICB9XG4gIH07XG5cbiAgbGV0IGN1cnJlbnRNb250aENhbCA9ICgpPT57XG4gICAgc2V0TG9hZGluZyh0cnVlKTtcbiAgICBjb25zdCBuZXdNb250aCA9IGhlbHBGdW5jLmN1cnJlbnRNb250aCgpO1xuICAgIGNvbnN0IG5ld1llYXIgPSBoZWxwRnVuYy5jdXJyZW50WWVhcigpO1xuICAgIGxvYWRDYWxlbmRhcihuZXdZZWFyLCBuZXdNb250aCk7XG4gIH07XG5cbiAgbGV0IGxvYWRDYWxlbmRhciA9IGFzeW5jKG5ld1llYXIsIG5ld01vbnRoKT0+e1xuICAgIGNvbnN0IHJlc0MgPSBhd2FpdCBmZXRjaChgJHtsb2NhdGlvbn0vYXBpL2NvbGxlY3Rpb25zP3llYXI9JHtuZXdZZWFyfSZtb250aD0ke2hlbHBGdW5jLmdldFdyaXR0ZW5Nb250aChuZXdNb250aCl9YCk7XG4gICAgY29uc3QgeyBjb2xsZWN0aW9ucywgZXJyIH0gPSBhd2FpdCByZXNDLmpzb24oKTtcbiAgICBjb25zdCBjYWxKc29uID0gY29sbGVjdGlvbnMubGVuZ3RoID4gMCA/IGNvbGxlY3Rpb25zWzBdLmRhdGFbaGVscEZ1bmMuZ2V0V3JpdHRlbk1vbnRoKG5ld01vbnRoKV0gOiBbXTtcblxuICAgIHNldE1vbnRoSW5mbyhjYWxKc29uKTtcbiAgICBzZXRNb250aEluZm9Db3VudChjYWxKc29uLmxlbmd0aCk7XG4gICAgc2V0TW9udGgobmV3TW9udGgpO1xuICAgIHNldFllYXIobmV3WWVhcik7XG4gICAgc2V0TG9hZGluZyhmYWxzZSk7XG4gIH07XG5cbiAgdXNlRWZmZWN0KCgpID0+IHsgLy8gdXNlZCB0byBzYXZlIGNhbGVuZGFyIHdoZW4gYSBkcm9wIGhhcyB0YWtlbiBwbGFjZSwgYW5kIG5lZWQgdG8gd2FpdCBmb3IgbW9udGhJbmZvIHRvIHVwZGF0ZSBiZWZvcmUgY2FsbGluZyBzYXZlLlxuICAgICBzYXZlQ2FsZW5kYXIoe3R5cGU6ICdkcm9wJ30pO1xuICB9LCBbbW9udGhJbmZvU2F2ZUZsYWddKTtcblxuICBsZXQgc2F2ZUNhbGVuZGFyID0gKGFjdGlvbik9PntcbiAgICBjb25zb2xlLmxvZyhhY3Rpb24pO1xuICAgIGxldCBkYXRhID0ge1wieWVhclwiOiB5ZWFyLnRvU3RyaW5nKCksIFwibW9udGhcIjogaGVscEZ1bmMuZ2V0V3JpdHRlbk1vbnRoKG1vbnRoKSwgW2hlbHBGdW5jLmdldFdyaXR0ZW5Nb250aChtb250aCldOiBtb250aEluZm99O1xuICAgIGZldGNoKGAke2xvY2F0aW9ufS9hcGkvY29sbGVjdGlvbnNgLCB7XG4gICAgICBtZXRob2Q6ICdwb3N0JyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uLCB0ZXh0L3BsYWluLCAqLyonLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nXG4gICAgICB9LFxuICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoZGF0YSlcbiAgICB9KS50aGVuKChyZXMpID0+IHtcbiAgICAgIHJldHVybiByZXMuanNvbigpO1xuICAgIH0pLnRoZW4oKGpzb24pPT57XG4gICAgICBpZihhY3Rpb24udHlwZSA9PT0gJ25vbmUnKXtcbiAgICAgICAgY29uc29sZS5sb2coanNvbi5teV9zdGF0dXMpO1xuICAgICAgfVxuICAgICAgaWYoYWN0aW9uLnR5cGUgPT09ICduZXh0Jyl7XG4gICAgICAgIGNvbnNvbGUubG9nKCduZXh0Jyk7XG4gICAgICAgIGNvbnNvbGUubG9nKGpzb24ubXlfc3RhdHVzKTtcbiAgICAgICAgbmV4dE1vbnRoQ2FsKCk7XG4gICAgICB9XG4gICAgICBpZihhY3Rpb24udHlwZSA9PT0gJ3ByZXZpb3VzJyl7XG4gICAgICAgIGNvbnNvbGUubG9nKCdwcmV2aW91cycpO1xuICAgICAgICBjb25zb2xlLmxvZyhqc29uLm15X3N0YXR1cyk7XG4gICAgICAgIHByZXZpb3VzTW9udGhDYWwoKTtcbiAgICAgIH1cbiAgICAgIGlmKGFjdGlvbi50eXBlID09PSAnY3VycmVudCcpe1xuICAgICAgICBjb25zb2xlLmxvZygnY3VycmVudCcpO1xuICAgICAgICBjb25vc2xlLmxvZyhqc29uLm15X3N0YXR1cyk7XG4gICAgICAgIGN1cnJlbnRNb250aENhbCgpO1xuICAgICAgfVxuICAgICAgaWYoYWN0aW9uLnR5cGUgPT09ICdkcm9wJyl7XG4gICAgICAgIGNvbnNvbGUubG9nKCdkcm9wJyk7XG4gICAgICAgIGNvbnNvbGUubG9nKGpzb24ubXlfc3RhdHVzKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfTtcblxuICByZXR1cm4oXG4gICAgPGRpdj5cbiAgICAgIHtsb2FkaW5nID8gPGRpdiBjbGFzc05hbWU9XCJzcGlubmVyXCI+PENpcmNsZVNwaW5uZXIgc2l6ZT17ODB9IGNvbG9yPVwiIzM1NDk4MFwiIGxvYWRpbmc9e2xvYWRpbmd9IC8+PC9kaXY+IDogPGRpdj48L2Rpdj59XG4gICAgICA8ZGl2IGlkPVwibGVmdF9wYW5lXCIgY2xhc3NOYW1lPVwibGVmdFBhbmVcIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJoZWFkZXJEaXZcIj5EaXNwYXRjaCBTdGFmZjwvZGl2PlxuICAgICAgICB7dXNlcnMubWFwKHVzZXI9PihcbiAgICAgICAgICAgIDxFbXBsb3llZUJ1dHRvbiBrZXk9e3VzZXIudXNlcl9uYW1lfSBvbkRyYWdTdGFydEluZGV4PXtldmVudCA9PiBvbkRyYWdTdGFydChldmVudCwgdXNlcil9IG5hbWU9e3VzZXIudXNlcl9uYW1lfSBzaWduZWRpbnVzZXI9e3NpZ25lZEluVXNlcn0vPlxuICAgICAgICApKX1cbiAgICAgICAgPGRpdiBpZD1cImRlbGV0ZV9wYW5lXCIgY2xhc3NOYW1lPVwiZGVsZXRlUGFuZVwiIG9uRHJvcD17ZXZlbnQgPT4gb25Ecm9wRGVsZXRlKGV2ZW50KX0gb25EcmFnT3Zlcj17ZXZlbnQgPT4gb25EcmFnT3ZlcihldmVudCl9PkRyYWcgaGVyZSB0byBEZWxldGU8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJyaWdodFBhbmVcIj5cbiAgICBcdCAgPGRpdiBjbGFzc05hbWU9XCJoZWFkZXJEaXYgdGl0bGVcIiBpZD0nbW9udGhUaXRsZSc+e2hlbHBGdW5jLmdldFdyaXR0ZW5Nb250aChtb250aCl9IHt5ZWFyfSBEZXNrIFNjaGVkdWxlPC9kaXY+XG4gICAgXHQgIDxkaXYgY2xhc3NOYW1lPVwiaGVhZGVyRGl2XCI+XG4gICAgXHRcdCAgPGJ1dHRvbiBjbGFzc05hbWU9XCJidXR0b25cIiBpZD1cInRvZGF5XCIgb25DbGljaz17ZT0+KGN1cnJlbnRNb250aENhbCgpKX0+VG9kYXk8L2J1dHRvbj4gPGJ1dHRvbiBjbGFzc05hbWU9XCJidXR0b25cIiBpZD0nbGVmdGFycm93JyBvbkNsaWNrPXtlPT4oc2F2ZUNhbGVuZGFyKHt0eXBlOiAncHJldmlvdXMnfSkpfT4mbHQ7Jmx0OzwvYnV0dG9uPiA8YnV0dG9uIGNsYXNzTmFtZT1cImJ1dHRvblwiIGlkPSdyaWdodGFycm93JyBvbkNsaWNrPXtlPT4oc2F2ZUNhbGVuZGFyKHt0eXBlOiAnbmV4dCd9KSl9PiZndDsmZ3Q7PC9idXR0b24+XG4gICAgXHQgIDwvZGl2PlxuICAgIFx0ICA8Q2FsIG9uRHJhZ1N0YXJ0SW5kZXg9e2V2ZW50ID0+IG9uRHJhZ1N0YXJ0KGV2ZW50LCBcIlwiKX0gb25EcmFnT3ZlckluZGV4PXtldmVudCA9PiBvbkRyYWdPdmVyKGV2ZW50KX0gb25Ecm9wQ29weUluZGV4PXtldmVudCA9PiBvbkRyb3BDb3B5KGV2ZW50KX0gbW9udGg9e21vbnRofSB5ZWFyPXt5ZWFyfSBtb250aEluZm89e21vbnRoSW5mb30gc2lnbmVkaW51c2VyPXtzaWduZWRJblVzZXJ9Lz5cbiAgICBcdDwvZGl2PlxuICAgICAgPHN0eWxlIGpzeCBnbG9iYWw+e2BcbiAgICAgICAgLnNwaW5uZXJ7XG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgIGxlZnQ6IDUwJTtcbiAgICAgICAgICB0b3A6IDUwJTtcbiAgICAgICAgICB6LWluZGV4OiAxO1xuICAgICAgICB9XG4gICAgICAgIC50aXRsZXtcbiAgICAgICAgICBmb250LXNpemU6IDEuOWVtICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cbiAgICAgICAgLmhlYWRlckRpdntcbiAgICAgICAgXHRtYXJnaW46IGF1dG87XG4gICAgICAgIFx0d2lkdGg6IDgwJTtcbiAgICAgICAgXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIFx0Zm9udC1zaXplOiAxLjJlbTtcbiAgICAgICAgXHRwYWRkaW5nOiAxMHB4O1xuICAgICAgICB9XG4gICAgICAgIC5mdWxsUGFuZXtcbiAgICAgICAgXHRwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIFx0d2lkdGg6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgICAgLmxlZnRQYW5le1xuICAgICAgICBcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgXHRsZWZ0OiA2MHB4O1xuICAgICAgICBcdHRvcDogNjhweDtcbiAgICAgICAgXHRib3R0b206IDEwcHg7XG4gICAgICAgIFx0d2lkdGg6IDE1JTtcbiAgICAgICAgfVxuICAgICAgICAucmlnaHRQYW5le1xuICAgICAgICBcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgXHRyaWdodDogMTBweDtcbiAgICAgICAgXHR0b3A6IDEwcHg7XG4gICAgICAgIFx0Ym90dG9tOiAxMHB4O1xuICAgICAgICBcdHdpZHRoOjgwJTtcbiAgICAgICAgfVxuICAgICAgICAuZGVsZXRlUGFuZXtcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIFx0bWFyZ2luOiAxMHB4IDEwcHggMTBweCAxMHB4O1xuICAgICAgICBcdHBhZGRpbmc6IDEwcHggMTBweCAxMHB4IDEwcHg7XG4gICAgICAgIFx0Ym9yZGVyOiAxcHggZGFzaGVkICMzNTQ5ODA7XG4gICAgICAgIFx0YmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgICAgICAgY29sb3I6ICMzNTQ5ODA7XG4gICAgICAgICAgaGVpZ2h0OiA1MCU7XG4gICAgICAgIH1cbiAgICAgICAgLmJ1dHRvbiB7XG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM1NDk4MDtcbiAgICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgICAgICAgY29sb3I6ICNmYmNhMzc7XG4gICAgICAgICAgcGFkZGluZzogNXB4IDI1cHg7XG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgICAgbWFyZ2luOiA0cHggMnB4O1xuICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgfVxuICAgICAgYH08L3N0eWxlPlxuICAgIDwvZGl2PlxuICApO1xufTtcblxuSW5kZXguZ2V0SW5pdGlhbFByb3BzID0gYXN5bmMgKHtyZXEsIHJlc3BvbnNlfSk9PntcbiAgdHJ5e1xuXG4gICAgLy9jb25zdCB7ICd4LW5vdy1kZXBsb3ltZW50LXVybCc6IG5vd1VSTCB9ID0gcmVxLmhlYWRlcnM7XG4gICAgY29uc3QgeyAnaG9zdCc6IFVSTCB9ID0gcmVxLmhlYWRlcnM7XG4gICAgY29uc3QgbG9jYXRpb24gPSAoVVJMID09PSAnbG9jYWxob3N0OjMwMDAnKSA/ICdodHRwOi8vbG9jYWxob3N0OjMwMDAnIDogJ2h0dHBzOi8vbmV4dC1jYWxlbmRhci5ub3cuc2gnO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2goYCR7bG9jYXRpb259L2FwaS9nZXRVc2Vyc2ApO1xuICAgIC8vY29uc3QgcmVzID0gYXdhaXQgZmV0Y2goYGh0dHBzOi8vbmV4dC1jYWxlbmRhci5ub3cuc2gvYXBpL2dldFVzZXJzYCk7IC8vIGluaXRpYWwgY2FsbHMgdG8gZGF0YWJhc2UgLSBzZXJ2ZXIgc2lkZSByZW5kZXIgZm9yIHNwZWVkXG4gICAgY29uc3QgeyB1c2VycywgZXJyb3IgfSA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICBjb25zdCBjdXJyZW50TW9udGggPSBoZWxwRnVuYy5jdXJyZW50TW9udGgoKTtcbiAgICBjb25zdCBjdXJyZW50TW9udGhXcml0dGVuID0gaGVscEZ1bmMuZ2V0V3JpdHRlbk1vbnRoKGN1cnJlbnRNb250aCk7XG4gICAgY29uc3QgY3VycmVudFllYXIgPSBoZWxwRnVuYy5jdXJyZW50WWVhcigpO1xuXG4gICAgY29uc3QgcmVzQyA9IGF3YWl0IGZldGNoKGAke2xvY2F0aW9ufS9hcGkvY29sbGVjdGlvbnM/eWVhcj0ke2N1cnJlbnRZZWFyfSZtb250aD0ke2N1cnJlbnRNb250aFdyaXR0ZW59YCk7XG4gICAgLy9jb25zdCByZXNDID0gYXdhaXQgZmV0Y2goYGh0dHBzOi8vbmV4dC1jYWxlbmRhci5ub3cuc2gvYXBpL2NvbGxlY3Rpb25zP3llYXI9JHtjdXJyZW50WWVhcn0mbW9udGg9JHtjdXJyZW50TW9udGhXcml0dGVufWApO1xuICAgIGNvbnN0IHsgY29sbGVjdGlvbnMsIGVyciB9ID0gYXdhaXQgcmVzQy5qc29uKCk7XG5cbiAgICBjb25zb2xlLmxvZygnSW5pdGlhbFByb3BzOiBjYWxsIHRvIGRhdGFiYXNlJyk7XG4gICAgY29uc29sZS5sb2coJ3YyIGluZGV4LmpzJyk7XG4gICAgcmV0dXJuIHtsb2NhdGlvbjogbG9jYXRpb24sIGN1cnJlbnRNb250aDogY3VycmVudE1vbnRoLCBjdXJyZW50WWVhcjogY3VycmVudFllYXIsIGNvbGxlY3Rpb25zLCB1c2Vyc307XG4gIH1jYXRjaCAoZXJyb3Ipe1xuICAgIGNvbnNvbGUubG9nKGVycm9yKVxuICB9XG59O1xuXG5leHBvcnQgZGVmYXVsdCBJbmRleDtcbiJdfQ== */\n/*@ sourceURL=/Users/paulbeaudon/calendar/next-calendar/pages/index.js */"));
};

Index.getInitialProps = function _callee(_ref2) {
  var req, response, URL, location, res, _ref3, users, error, currentMonth, currentMonthWritten, currentYear, resC, _ref4, collections, err;

  return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.async(function _callee$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          req = _ref2.req, response = _ref2.response;
          _context2.prev = 1;
          //const { 'x-now-deployment-url': nowURL } = req.headers;
          URL = req.headers['host'];
          location = URL === 'localhost:3000' ? 'http://localhost:3000' : 'https://next-calendar.now.sh';
          _context2.next = 6;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8___default()("".concat(location, "/api/getUsers")));

        case 6:
          res = _context2.sent;
          _context2.next = 9;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.awrap(res.json());

        case 9:
          _ref3 = _context2.sent;
          users = _ref3.users;
          error = _ref3.error;
          currentMonth = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["currentMonth"]();
          currentMonthWritten = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["getWrittenMonth"](currentMonth);
          currentYear = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["currentYear"]();
          _context2.next = 17;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8___default()("".concat(location, "/api/collections?year=").concat(currentYear, "&month=").concat(currentMonthWritten)));

        case 17:
          resC = _context2.sent;
          _context2.next = 20;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.awrap(resC.json());

        case 20:
          _ref4 = _context2.sent;
          collections = _ref4.collections;
          err = _ref4.err;
          console.log('InitialProps: call to database');
          console.log('v2 index.js');
          return _context2.abrupt("return", {
            location: location,
            currentMonth: currentMonth,
            currentYear: currentYear,
            collections: collections,
            users: users
          });

        case 28:
          _context2.prev = 28;
          _context2.t0 = _context2["catch"](1);
          console.log(_context2.t0);

        case 31:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, [[1, 28]]);
};

/* harmony default export */ __webpack_exports__["default"] = (Index);

/***/ })

})
//# sourceMappingURL=index.js.24379ab2f666502bed3d.hot-update.js.map