webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/helperfunctions.js":
/*!***************************************!*\
  !*** ./components/helperfunctions.js ***!
  \***************************************/
/*! exports provided: currentDay, currentMonth, currentYear, daysInMonth, startDay, getWrittenMonth, getIntMonth */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "currentDay", function() { return currentDay; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "currentMonth", function() { return currentMonth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "currentYear", function() { return currentYear; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "daysInMonth", function() { return daysInMonth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startDay", function() { return startDay; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getWrittenMonth", function() { return getWrittenMonth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getIntMonth", function() { return getIntMonth; });
var currentDay = function currentDay() {
  console.log('helper function');
  console.log(new Date());
  return new Date().getDate();
};

var currentMonth = function currentMonth() {
  return new Date().getMonth() + 1;
};

var currentYear = function currentYear() {
  return new Date().getFullYear();
};

var daysInMonth = function daysInMonth(month, year) {
  return new Date(year, month, 0).getDate();
};

var startDay = function startDay(month, year) {
  //let daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  return new Date(year, month - 1, 1).getDay();
};

var getWrittenMonth = function getWrittenMonth(monthInt) {
  var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  return month[monthInt - 1];
};

var getIntMonth = function getIntMonth(monthString) {
  var month = {
    "Jan": 1,
    "Feb": 2,
    "Mar": 3,
    "Apr": 4,
    "May": 5,
    "Jun": 6,
    "Jul": 7,
    "Aug": 8,
    "Sep": 9,
    "Oct": 10,
    "Nov": 11,
    "Dec": 12
  };
  return month[monthString];
};



/***/ })

})
//# sourceMappingURL=index.js.43685b144a2bd2cbc303.hot-update.js.map