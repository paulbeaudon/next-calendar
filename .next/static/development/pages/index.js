(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["static/development/pages/index.js"],{

/***/ "./components/Calendar.js":
/*!********************************!*\
  !*** ./components/Calendar.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_helperfunctions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/helperfunctions */ "./components/helperfunctions.js");
/* harmony import */ var _components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/EmployeeButton */ "./components/EmployeeButton.js");
var _jsxFileName = "/Users/paulbeaudon/calendar/next-calendar/components/Calendar.js";


var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;



var Calendar = function Calendar(props) {
  var getTable = function getTable(month, year, monthInfo) {
    var rows = 6;
    var cols = 7;
    var dayCount = 1;
    var start_day = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_2__["startDay"](month, year);
    var days_in_month = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_2__["daysInMonth"](month, year);
    var table = [];
    var currentDay = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_2__["currentDay"]();
    var currentMonth = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_2__["currentMonth"]();
    var currentYear = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_2__["currentYear"]();
    var monthInfoCount = 0;
    table.push(__jsx("tr", {
      key: "header",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, __jsx("td", {
      className: "headertd",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, "Sunday"), __jsx("td", {
      className: "headertd",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, "Monday"), __jsx("td", {
      className: "headertd",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, "Tuesday"), __jsx("td", {
      className: "headertd",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, "Wednesday"), __jsx("td", {
      className: "headertd",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, "Thursday"), __jsx("td", {
      className: "headertd",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, "Friday"), __jsx("td", {
      className: "headertd",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      },
      __self: this
    }, "Saturday")));

    var _loop = function _loop(row) {
      var day = [];

      var _loop2 = function _loop2(col) {
        if (row === 0 && col === start_day) {
          var morning = [];
          morning = monthInfo.filter(function (morn) {
            return morn.id.split('-')[0] === "".concat(row, "_").concat(col, "_am");
          });
          var afternoon = [];
          afternoon = monthInfo.filter(function (aft) {
            return aft.id.split('-')[0] === "".concat(row, "_").concat(col, "_pm");
          });

          if (currentDay === dayCount && currentMonth === month && currentYear === year) {
            day.push(__jsx("td", {
              id: "".concat(row, "_").concat(col),
              key: "".concat(row, "_").concat(col),
              className: "currentdaytd",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 30
              },
              __self: this
            }, __jsx("div", {
              className: "datePos",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 30
              },
              __self: this
            }, "".concat(dayCount)), __jsx("div", {
              id: "".concat(row, "_").concat(col, "_am"),
              className: "am",
              onDrop: props.onDropCopyIndex,
              onDragOver: props.onDragOverIndex,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 30
              },
              __self: this
            }, morning.map(function (morn) {
              return __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
                key: morn.id,
                onDragStartIndex: props.onDragStartIndex,
                id: morn.id,
                name: morn.user_name,
                signedinuser: props.signedinuser,
                buttonsize: "small",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 30
                },
                __self: this
              });
            })), __jsx("div", {
              id: "".concat(row, "_").concat(col, "_pm"),
              className: "pm",
              onDrop: props.onDropCopyIndex,
              onDragOver: props.onDragOverIndex,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 30
              },
              __self: this
            }, afternoon.map(function (aft) {
              return __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
                key: aft.id,
                onDragStartIndex: props.onDragStartIndex,
                id: aft.id,
                name: aft.user_name,
                signedinuser: props.signedinuser,
                buttonsize: "small",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 30
                },
                __self: this
              });
            }))));
          } else {
            day.push(__jsx("td", {
              id: "".concat(row, "_").concat(col),
              key: "".concat(row, "_").concat(col),
              className: "daytd",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 32
              },
              __self: this
            }, __jsx("div", {
              className: "datePos",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 32
              },
              __self: this
            }, "".concat(dayCount)), __jsx("div", {
              id: "".concat(row, "_").concat(col, "_am"),
              className: "am",
              onDrop: props.onDropCopyIndex,
              onDragOver: props.onDragOverIndex,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 32
              },
              __self: this
            }, morning.map(function (morn) {
              return __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
                key: morn.id,
                onDragStartIndex: props.onDragStartIndex,
                id: morn.id,
                name: morn.user_name,
                signedinuser: props.signedinuser,
                buttonsize: "small",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 32
                },
                __self: this
              });
            })), __jsx("div", {
              id: "".concat(row, "_").concat(col, "_pm"),
              className: "pm",
              onDrop: props.onDropCopyIndex,
              onDragOver: props.onDragOverIndex,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 32
              },
              __self: this
            }, afternoon.map(function (aft) {
              return __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
                key: aft.id,
                onDragStartIndex: props.onDragStartIndex,
                id: aft.id,
                name: aft.user_name,
                signedinuser: props.signedinuser,
                buttonsize: "small",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 32
                },
                __self: this
              });
            }))));
          }

          dayCount++;
        } else {
          if (dayCount > 1 && dayCount <= days_in_month) {
            var _morning = [];
            _morning = monthInfo.filter(function (morn) {
              return morn.id.split('-')[0] === "".concat(row, "_").concat(col, "_am");
            });
            var _afternoon = [];
            _afternoon = monthInfo.filter(function (aft) {
              return aft.id.split('-')[0] === "".concat(row, "_").concat(col, "_pm");
            });

            if (currentDay === dayCount && currentMonth === month && currentYear === year) {
              day.push(__jsx("td", {
                id: "".concat(row, "_").concat(col),
                key: "".concat(row, "_").concat(col),
                className: "currentdaytd",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 43
                },
                __self: this
              }, __jsx("div", {
                className: "datePos",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 43
                },
                __self: this
              }, "".concat(dayCount)), __jsx("div", {
                id: "".concat(row, "_").concat(col, "_am"),
                className: "am",
                onDrop: props.onDropCopyIndex,
                onDragOver: props.onDragOverIndex,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 43
                },
                __self: this
              }, _morning.map(function (morn) {
                return __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
                  key: morn.id,
                  onDragStartIndex: props.onDragStartIndex,
                  id: morn.id,
                  name: morn.user_name,
                  signedinuser: props.signedinuser,
                  buttonsize: "small",
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 43
                  },
                  __self: this
                });
              })), __jsx("div", {
                id: "".concat(row, "_").concat(col, "_pm"),
                className: "pm",
                onDrop: props.onDropCopyIndex,
                onDragOver: props.onDragOverIndex,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 43
                },
                __self: this
              }, _afternoon.map(function (aft) {
                return __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
                  key: aft.id,
                  onDragStartIndex: props.onDragStartIndex,
                  id: aft.id,
                  name: aft.user_name,
                  signedinuser: props.signedinuser,
                  buttonsize: "small",
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 43
                  },
                  __self: this
                });
              }))));
            } else {
              day.push(__jsx("td", {
                id: "".concat(row, "_").concat(col),
                key: "".concat(row, "_").concat(col),
                className: "daytd",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 45
                },
                __self: this
              }, __jsx("div", {
                className: "datePos",
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 45
                },
                __self: this
              }, "".concat(dayCount)), __jsx("div", {
                id: "".concat(row, "_").concat(col, "_am"),
                className: "am",
                onDrop: props.onDropCopyIndex,
                onDragOver: props.onDragOverIndex,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 45
                },
                __self: this
              }, _morning.map(function (morn) {
                return __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
                  onDragStartIndex: props.onDragStartIndex,
                  key: morn.id,
                  id: morn.id,
                  name: morn.user_name,
                  signedinuser: props.signedinuser,
                  buttonsize: "small",
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 45
                  },
                  __self: this
                });
              })), __jsx("div", {
                id: "".concat(row, "_").concat(col, "_pm"),
                className: "pm",
                onDrop: props.onDropCopyIndex,
                onDragOver: props.onDragOverIndex,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 45
                },
                __self: this
              }, _afternoon.map(function (aft) {
                return __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
                  key: aft.id,
                  onDragStartIndex: props.onDragStartIndex,
                  id: aft.id,
                  name: aft.user_name,
                  signedinuser: props.signedinuser,
                  buttonsize: "small",
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 45
                  },
                  __self: this
                });
              }))));
            }

            dayCount++;
          } else {
            day.push(__jsx("td", {
              key: "".concat(row, "_").concat(col),
              className: "daytd",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 49
              },
              __self: this
            }));
          }
        }
      };

      for (var col = 0; col < cols; col++) {
        _loop2(col);
      }

      table.push(__jsx("tr", {
        key: "".concat(row),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 53
        },
        __self: this
      }, day));
    };

    for (var row = 0; row < rows; row++) {
      _loop(row);
    }

    return table;
  };

  return __jsx("div", {
    className: "jsx-3120944804",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    },
    __self: this
  }, __jsx("table", {
    className: "jsx-3120944804" + " " + "caltable",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60
    },
    __self: this
  }, __jsx("tbody", {
    className: "jsx-3120944804",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61
    },
    __self: this
  }, getTable(props.month, props.year, props.monthInfo))), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    id: "3120944804",
    __self: this
  }, ".caltable{margin:auto;border-spacing:0px;padding:4px;font-size:1em;border-collapse:separate;border:0px solid #354980;}.headertd{text-align:center;width:14%;padding:4px 50px 4px 50px;font-size:1em;border:1px solid #354980;background-color:#354980;color:#fbca37;}.daytd{position:relative;text-align:center;font-size:1em;border:1px solid #354980;height:100px;min-height:100px;}.currentdaytd{position:relative;text-align:center;font-size:1em;border:1px solid #354980;height:100px;background-color:#fbca37;color:#fbca37;z-index:-1;}.datePos{font-size:1em;position:absolute;left:0px;top:0px;right:0px;text-align:right;padding-right:5px;background-color:#fbca37;color:#354980;z-index:-1;}.am{width:100%;height:auto;min-height:50px !important;border:1px dotted #fbca37;padding-top:15px;}.amCurrentDay{width:100%;height:auto;min-height:50px !important;border-bottom:1px dotted white;padding-top:15px;}.pm{width:100%;height:auto;min-height:50px !important;border:1px dotted #fbca37;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wYXVsYmVhdWRvbi9jYWxlbmRhci9uZXh0LWNhbGVuZGFyL2NvbXBvbmVudHMvQ2FsZW5kYXIuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBZ0V5QixBQUd1QixBQVFNLEFBU0QsQUFRQSxBQVVKLEFBWUgsQUFPQSxBQU9BLFdBYkMsQUFPQSxBQU9BLENBN0RRLEVBbUNGLElBM0JSLEFBU1MsQUFRQSxLQXVCUSxBQU9BLEFBT0EsS0FyREEsR0FSZCxDQW1DSixJQWxCTSxBQVFBLEtBV1AsRUFuQ08sTUFvQ0wsQ0FuQmdCLEFBUUEsQUFzQkEsQUFPSyxBQU9MLElBckRYLEdBUFcsRUFvQ1QsU0E1QlMsT0FTYixBQVFDLENBWUksQUFVRCxBQWNsQixLQVBrQixDQXJEUyxNQWlCVCxBQVFTLEtBakJBLEFBdUMzQixDQVYyQixJQWlCM0IsT0FwQ0EsRUFqQkEsTUF5QmdCLEtBakJBLENBNkJBLFFBWEgsS0FqQmIsQ0E2QmEsS0FYYixNQVlBIiwiZmlsZSI6Ii9Vc2Vycy9wYXVsYmVhdWRvbi9jYWxlbmRhci9uZXh0LWNhbGVuZGFyL2NvbXBvbmVudHMvQ2FsZW5kYXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBoZWxwRnVuYyBmcm9tIFwiLi4vY29tcG9uZW50cy9oZWxwZXJmdW5jdGlvbnNcIjtcbmltcG9ydCBFbXBsb3llZUJ1dHRvbiBmcm9tIFwiLi4vY29tcG9uZW50cy9FbXBsb3llZUJ1dHRvblwiO1xuXG5jb25zdCBDYWxlbmRhciA9IHByb3BzID0+e1xuICBjb25zdCBnZXRUYWJsZSA9IChtb250aCwgeWVhciwgbW9udGhJbmZvKT0+e1xuICAgICAgbGV0IHJvd3MgPSA2O1xuICAgICAgbGV0IGNvbHMgPSA3O1xuICAgICAgbGV0IGRheUNvdW50ID0gMTtcbiAgICAgIGxldCBzdGFydF9kYXkgPSBoZWxwRnVuYy5zdGFydERheShtb250aCwgeWVhcik7XG4gICAgICBsZXQgZGF5c19pbl9tb250aCA9IGhlbHBGdW5jLmRheXNJbk1vbnRoKG1vbnRoLCB5ZWFyKTtcbiAgICAgIGxldCB0YWJsZSA9IFtdO1xuXG4gICAgICBsZXQgY3VycmVudERheSA9IGhlbHBGdW5jLmN1cnJlbnREYXkoKTtcbiAgICAgIGxldCBjdXJyZW50TW9udGggPSBoZWxwRnVuYy5jdXJyZW50TW9udGgoKTtcbiAgICAgIGxldCBjdXJyZW50WWVhciA9IGhlbHBGdW5jLmN1cnJlbnRZZWFyKCk7XG5cbiAgICAgIGxldCBtb250aEluZm9Db3VudCA9IDA7XG5cbiAgICAgIHRhYmxlLnB1c2goPHRyIGtleT1cImhlYWRlclwiPjx0ZCBjbGFzc05hbWU9XCJoZWFkZXJ0ZFwiPlN1bmRheTwvdGQ+PHRkIGNsYXNzTmFtZT1cImhlYWRlcnRkXCI+TW9uZGF5PC90ZD48dGQgY2xhc3NOYW1lPVwiaGVhZGVydGRcIj5UdWVzZGF5PC90ZD48dGQgY2xhc3NOYW1lPVwiaGVhZGVydGRcIj5XZWRuZXNkYXk8L3RkPjx0ZCBjbGFzc05hbWU9XCJoZWFkZXJ0ZFwiPlRodXJzZGF5PC90ZD48dGQgY2xhc3NOYW1lPVwiaGVhZGVydGRcIj5GcmlkYXk8L3RkPjx0ZCBjbGFzc05hbWU9XCJoZWFkZXJ0ZFwiPlNhdHVyZGF5PC90ZD48L3RyPik7XG4gICAgICBmb3IobGV0IHJvdz0wOyByb3c8cm93czsgcm93Kyspe1xuICAgICAgICBsZXQgZGF5ID0gW107XG4gICAgICAgIGZvcihsZXQgY29sPTA7IGNvbDxjb2xzOyBjb2wrKyl7XG4gICAgICAgICAgaWYocm93ID09PSAwICYmIGNvbCA9PT0gc3RhcnRfZGF5KXtcbiAgICAgICAgICAgIGxldCBtb3JuaW5nID0gW107XG4gICAgICAgICAgICBtb3JuaW5nID0gbW9udGhJbmZvLmZpbHRlcihtb3JuID0+IG1vcm4uaWQuc3BsaXQoJy0nKVswXSA9PT0gYCR7cm93fV8ke2NvbH1fYW1gKTtcbiAgICAgICAgICAgIGxldCBhZnRlcm5vb24gPSBbXTtcbiAgICAgICAgICAgIGFmdGVybm9vbiA9IG1vbnRoSW5mby5maWx0ZXIoYWZ0ID0+IGFmdC5pZC5zcGxpdCgnLScpWzBdID09PSBgJHtyb3d9XyR7Y29sfV9wbWApO1xuXG4gICAgICAgICAgICBpZihjdXJyZW50RGF5ID09PSBkYXlDb3VudCAmJiBjdXJyZW50TW9udGggPT09IG1vbnRoICYmIGN1cnJlbnRZZWFyID09PSB5ZWFyKXtcbiAgICAgICAgICAgICAgZGF5LnB1c2goPHRkIGlkPXtgJHtyb3d9XyR7Y29sfWB9IGtleT17YCR7cm93fV8ke2NvbH1gfSBjbGFzc05hbWU9XCJjdXJyZW50ZGF5dGRcIj48ZGl2IGNsYXNzTmFtZT1cImRhdGVQb3NcIj57YCR7ZGF5Q291bnR9YH08L2Rpdj48ZGl2IGlkPXtgJHtyb3d9XyR7Y29sfV9hbWB9IGNsYXNzTmFtZT1cImFtXCIgb25Ecm9wPXtwcm9wcy5vbkRyb3BDb3B5SW5kZXh9IG9uRHJhZ092ZXI9e3Byb3BzLm9uRHJhZ092ZXJJbmRleH0+e21vcm5pbmcubWFwKG1vcm49PjxFbXBsb3llZUJ1dHRvbiBrZXk9e21vcm4uaWR9IG9uRHJhZ1N0YXJ0SW5kZXg9e3Byb3BzLm9uRHJhZ1N0YXJ0SW5kZXh9IGlkPXttb3JuLmlkfSBuYW1lPXttb3JuLnVzZXJfbmFtZX0gc2lnbmVkaW51c2VyPXtwcm9wcy5zaWduZWRpbnVzZXJ9IGJ1dHRvbnNpemU9XCJzbWFsbFwiLz4pfTwvZGl2PjxkaXYgaWQ9e2Ake3Jvd31fJHtjb2x9X3BtYH0gY2xhc3NOYW1lPVwicG1cIiBvbkRyb3A9e3Byb3BzLm9uRHJvcENvcHlJbmRleH0gb25EcmFnT3Zlcj17cHJvcHMub25EcmFnT3ZlckluZGV4fT57YWZ0ZXJub29uLm1hcChhZnQ9PjxFbXBsb3llZUJ1dHRvbiBrZXk9e2FmdC5pZH0gb25EcmFnU3RhcnRJbmRleD17cHJvcHMub25EcmFnU3RhcnRJbmRleH0gaWQ9e2FmdC5pZH0gbmFtZT17YWZ0LnVzZXJfbmFtZX0gc2lnbmVkaW51c2VyPXtwcm9wcy5zaWduZWRpbnVzZXJ9IGJ1dHRvbnNpemU9XCJzbWFsbFwiLz4pfTwvZGl2PjwvdGQ+KTtcbiAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICBkYXkucHVzaCg8dGQgaWQ9e2Ake3Jvd31fJHtjb2x9YH0ga2V5PXtgJHtyb3d9XyR7Y29sfWB9IGNsYXNzTmFtZT1cImRheXRkXCI+PGRpdiBjbGFzc05hbWU9XCJkYXRlUG9zXCI+e2Ake2RheUNvdW50fWB9PC9kaXY+PGRpdiBpZD17YCR7cm93fV8ke2NvbH1fYW1gfSBjbGFzc05hbWU9XCJhbVwiIG9uRHJvcD17cHJvcHMub25Ecm9wQ29weUluZGV4fSBvbkRyYWdPdmVyPXtwcm9wcy5vbkRyYWdPdmVySW5kZXh9Pnttb3JuaW5nLm1hcChtb3JuPT48RW1wbG95ZWVCdXR0b24ga2V5PXttb3JuLmlkfSBvbkRyYWdTdGFydEluZGV4PXtwcm9wcy5vbkRyYWdTdGFydEluZGV4fSBpZD17bW9ybi5pZH0gbmFtZT17bW9ybi51c2VyX25hbWV9IHNpZ25lZGludXNlcj17cHJvcHMuc2lnbmVkaW51c2VyfSBidXR0b25zaXplPVwic21hbGxcIi8+KX08L2Rpdj48ZGl2IGlkPXtgJHtyb3d9XyR7Y29sfV9wbWB9IGNsYXNzTmFtZT1cInBtXCIgb25Ecm9wPXtwcm9wcy5vbkRyb3BDb3B5SW5kZXh9IG9uRHJhZ092ZXI9e3Byb3BzLm9uRHJhZ092ZXJJbmRleH0+e2FmdGVybm9vbi5tYXAoYWZ0PT48RW1wbG95ZWVCdXR0b24ga2V5PXthZnQuaWR9IG9uRHJhZ1N0YXJ0SW5kZXg9e3Byb3BzLm9uRHJhZ1N0YXJ0SW5kZXh9IGlkPXthZnQuaWR9IG5hbWU9e2FmdC51c2VyX25hbWV9IHNpZ25lZGludXNlcj17cHJvcHMuc2lnbmVkaW51c2VyfSBidXR0b25zaXplPVwic21hbGxcIi8+KX08L2Rpdj48L3RkPik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBkYXlDb3VudCsrO1xuICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgaWYoZGF5Q291bnQgPiAxICYmIGRheUNvdW50IDw9IGRheXNfaW5fbW9udGgpe1xuICAgICAgICAgICAgICBsZXQgbW9ybmluZyA9IFtdO1xuICAgICAgICAgICAgICBtb3JuaW5nID0gbW9udGhJbmZvLmZpbHRlcihtb3JuID0+IG1vcm4uaWQuc3BsaXQoJy0nKVswXSA9PT0gYCR7cm93fV8ke2NvbH1fYW1gKTtcbiAgICAgICAgICAgICAgbGV0IGFmdGVybm9vbiA9IFtdO1xuICAgICAgICAgICAgICBhZnRlcm5vb24gPSBtb250aEluZm8uZmlsdGVyKGFmdCA9PiBhZnQuaWQuc3BsaXQoJy0nKVswXSA9PT0gYCR7cm93fV8ke2NvbH1fcG1gKTtcblxuICAgICAgICAgICAgICBpZihjdXJyZW50RGF5ID09PSBkYXlDb3VudCAmJiBjdXJyZW50TW9udGggPT09IG1vbnRoICYmIGN1cnJlbnRZZWFyID09PSB5ZWFyKXtcbiAgICAgICAgICAgICAgICBkYXkucHVzaCg8dGQgaWQ9e2Ake3Jvd31fJHtjb2x9YH0ga2V5PXtgJHtyb3d9XyR7Y29sfWB9IGNsYXNzTmFtZT1cImN1cnJlbnRkYXl0ZFwiPjxkaXYgY2xhc3NOYW1lPVwiZGF0ZVBvc1wiPntgJHtkYXlDb3VudH1gfTwvZGl2PjxkaXYgaWQ9e2Ake3Jvd31fJHtjb2x9X2FtYH0gY2xhc3NOYW1lPVwiYW1cIiBvbkRyb3A9e3Byb3BzLm9uRHJvcENvcHlJbmRleH0gb25EcmFnT3Zlcj17cHJvcHMub25EcmFnT3ZlckluZGV4fT57bW9ybmluZy5tYXAobW9ybj0+PEVtcGxveWVlQnV0dG9uIGtleT17bW9ybi5pZH0gb25EcmFnU3RhcnRJbmRleD17cHJvcHMub25EcmFnU3RhcnRJbmRleH0gaWQ9e21vcm4uaWR9IG5hbWU9e21vcm4udXNlcl9uYW1lfSBzaWduZWRpbnVzZXI9e3Byb3BzLnNpZ25lZGludXNlcn0gYnV0dG9uc2l6ZT1cInNtYWxsXCIvPil9PC9kaXY+PGRpdiBpZD17YCR7cm93fV8ke2NvbH1fcG1gfSBjbGFzc05hbWU9XCJwbVwiIG9uRHJvcD17cHJvcHMub25Ecm9wQ29weUluZGV4fSBvbkRyYWdPdmVyPXtwcm9wcy5vbkRyYWdPdmVySW5kZXh9PnthZnRlcm5vb24ubWFwKGFmdD0+PEVtcGxveWVlQnV0dG9uIGtleT17YWZ0LmlkfSBvbkRyYWdTdGFydEluZGV4PXtwcm9wcy5vbkRyYWdTdGFydEluZGV4fSBpZD17YWZ0LmlkfSBuYW1lPXthZnQudXNlcl9uYW1lfSBzaWduZWRpbnVzZXI9e3Byb3BzLnNpZ25lZGludXNlcn0gYnV0dG9uc2l6ZT1cInNtYWxsXCIvPil9PC9kaXY+PC90ZD4pO1xuICAgICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICBkYXkucHVzaCg8dGQgaWQ9e2Ake3Jvd31fJHtjb2x9YH0ga2V5PXtgJHtyb3d9XyR7Y29sfWB9IGNsYXNzTmFtZT1cImRheXRkXCI+PGRpdiBjbGFzc05hbWU9XCJkYXRlUG9zXCI+e2Ake2RheUNvdW50fWB9PC9kaXY+PGRpdiBpZD17YCR7cm93fV8ke2NvbH1fYW1gfSBjbGFzc05hbWU9XCJhbVwiIG9uRHJvcD17cHJvcHMub25Ecm9wQ29weUluZGV4fSBvbkRyYWdPdmVyPXtwcm9wcy5vbkRyYWdPdmVySW5kZXh9Pnttb3JuaW5nLm1hcChtb3JuPT48RW1wbG95ZWVCdXR0b24gb25EcmFnU3RhcnRJbmRleD17cHJvcHMub25EcmFnU3RhcnRJbmRleH0ga2V5PXttb3JuLmlkfSBpZD17bW9ybi5pZH0gbmFtZT17bW9ybi51c2VyX25hbWV9IHNpZ25lZGludXNlcj17cHJvcHMuc2lnbmVkaW51c2VyfSBidXR0b25zaXplPVwic21hbGxcIi8+KX08L2Rpdj48ZGl2IGlkPXtgJHtyb3d9XyR7Y29sfV9wbWB9IGNsYXNzTmFtZT1cInBtXCIgb25Ecm9wPXtwcm9wcy5vbkRyb3BDb3B5SW5kZXh9IG9uRHJhZ092ZXI9e3Byb3BzLm9uRHJhZ092ZXJJbmRleH0+e2FmdGVybm9vbi5tYXAoYWZ0PT48RW1wbG95ZWVCdXR0b24ga2V5PXthZnQuaWR9IG9uRHJhZ1N0YXJ0SW5kZXg9e3Byb3BzLm9uRHJhZ1N0YXJ0SW5kZXh9IGlkPXthZnQuaWR9IG5hbWU9e2FmdC51c2VyX25hbWV9IHNpZ25lZGludXNlcj17cHJvcHMuc2lnbmVkaW51c2VyfSBidXR0b25zaXplPVwic21hbGxcIi8+KX08L2Rpdj48L3RkPik7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZGF5Q291bnQrKztcbiAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICBkYXkucHVzaCg8dGQga2V5PXtgJHtyb3d9XyR7Y29sfWB9IGNsYXNzTmFtZT1cImRheXRkXCI+PC90ZD4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0YWJsZS5wdXNoKDx0ciBrZXk9e2Ake3Jvd31gfT57ZGF5fTwvdHI+KTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0YWJsZTtcbiAgICB9O1xuXG4gIHJldHVybihcbiAgICA8ZGl2PlxuICAgICAgPHRhYmxlIGNsYXNzTmFtZT1cImNhbHRhYmxlXCI+XG4gICAgICAgIDx0Ym9keT5cbiAgICAgICAgICB7Z2V0VGFibGUocHJvcHMubW9udGgsIHByb3BzLnllYXIsIHByb3BzLm1vbnRoSW5mbyl9XG4gICAgICAgIDwvdGJvZHk+XG4gICAgICA8L3RhYmxlPlxuICAgICAgPHN0eWxlIGpzeCBnbG9iYWw+e2BcbiAgICAgICAgLmNhbHRhYmxlIHtcbiAgICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAgICAgYm9yZGVyLXNwYWNpbmc6IDBweDtcbiAgICAgICAgICBwYWRkaW5nOiA0cHg7XG4gICAgICAgICAgZm9udC1zaXplOiAxZW07XG4gICAgICAgICAgYm9yZGVyLWNvbGxhcHNlOiBzZXBhcmF0ZTtcbiAgICAgICAgICBib3JkZXI6IDBweCBzb2xpZCAjMzU0OTgwO1xuICAgICAgICB9XG4gICAgICAgIC5oZWFkZXJ0ZHtcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgd2lkdGg6MTQlO1xuICAgICAgICAgIHBhZGRpbmc6IDRweCA1MHB4IDRweCA1MHB4O1xuICAgICAgICAgIGZvbnQtc2l6ZTogMWVtO1xuICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMzNTQ5ODA7XG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM1NDk4MDtcbiAgICAgICAgICBjb2xvcjogI2ZiY2EzNztcbiAgICAgICAgfVxuICAgICAgICAuZGF5dGR7XG4gICAgICBcdCAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICBmb250LXNpemU6IDFlbTtcbiAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjMzU0OTgwO1xuICAgICAgXHQgIGhlaWdodDogMTAwcHg7XG4gICAgICBcdCAgbWluLWhlaWdodDogMTAwcHg7XG4gICAgICAgIH1cbiAgICAgICAgLmN1cnJlbnRkYXl0ZHtcbiAgICAgIFx0ICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgIGZvbnQtc2l6ZTogMWVtO1xuICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMzNTQ5ODA7XG4gICAgICAgICAgaGVpZ2h0OiAxMDBweDtcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmJjYTM3O1xuICAgICAgICAgIGNvbG9yOiAjZmJjYTM3O1xuICAgICAgICAgIHotaW5kZXg6IC0xO1xuICAgICAgICB9XG4gICAgICAgIC5kYXRlUG9ze1xuICAgICAgICBcdGZvbnQtc2l6ZTogMWVtO1xuICAgICAgICBcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgXHRsZWZ0OiAwcHg7XG4gICAgICAgIFx0dG9wOiAwcHg7XG4gICAgICAgIFx0cmlnaHQ6IDBweDtcbiAgICAgICAgXHR0ZXh0LWFsaWduOiByaWdodDtcbiAgICAgICAgXHRwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZiY2EzNztcbiAgICAgICAgICBjb2xvcjogIzM1NDk4MDtcbiAgICAgICAgICB6LWluZGV4OiAtMTtcbiAgICAgICAgfVxuICAgICAgICAuYW17XG4gICAgICAgIFx0d2lkdGg6IDEwMCU7XG4gICAgICAgIFx0aGVpZ2h0OiBhdXRvO1xuICAgICAgICBcdG1pbi1oZWlnaHQ6IDUwcHggIWltcG9ydGFudDtcbiAgICAgICAgXHRib3JkZXI6IDFweCBkb3R0ZWQgI2ZiY2EzNztcbiAgICAgICAgXHRwYWRkaW5nLXRvcDogMTVweDtcbiAgICAgICAgfVxuICAgICAgICAuYW1DdXJyZW50RGF5e1xuICAgICAgICBcdHdpZHRoOiAxMDAlO1xuICAgICAgICBcdGhlaWdodDogYXV0bztcbiAgICAgICAgXHRtaW4taGVpZ2h0OiA1MHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIFx0Ym9yZGVyLWJvdHRvbTogMXB4IGRvdHRlZCB3aGl0ZTtcbiAgICAgICAgXHRwYWRkaW5nLXRvcDogMTVweDtcbiAgICAgICAgfVxuICAgICAgICAucG17XG4gICAgICAgIFx0d2lkdGg6IDEwMCU7XG4gICAgICAgIFx0aGVpZ2h0OiBhdXRvO1xuICAgICAgICBcdG1pbi1oZWlnaHQ6IDUwcHggIWltcG9ydGFudDtcbiAgICAgICAgXHRib3JkZXI6IDFweCBkb3R0ZWQgI2ZiY2EzNztcbiAgICAgICAgfVxuICAgICAgYH08L3N0eWxlPlxuICAgIDwvZGl2PlxuICApO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgQ2FsZW5kYXI7XG4iXX0= */\n/*@ sourceURL=/Users/paulbeaudon/calendar/next-calendar/components/Calendar.js */"));
};

/* harmony default export */ __webpack_exports__["default"] = (Calendar);

/***/ }),

/***/ "./components/EmployeeButton.js":
/*!**************************************!*\
  !*** ./components/EmployeeButton.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/Users/paulbeaudon/calendar/next-calendar/components/EmployeeButton.js";


var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;

var EmployeeButton = function EmployeeButton(props) {
  var loggedInUser = false;

  if (props.signedinuser === props.name) {
    loggedInUser = true;
  }

  ;
  return __jsx("div", {
    className: "jsx-3436715881",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, props.buttonsize === 'small' ? loggedInUser ? __jsx("div", {
    id: props.id,
    draggable: true,
    onDragStart: props.onDragStartIndex,
    className: "jsx-3436715881" + " " + "myDList small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, props.name) : __jsx("div", {
    id: props.id,
    draggable: true,
    onDragStart: props.onDragStartIndex,
    className: "jsx-3436715881" + " " + "dListBorder small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, props.name) : loggedInUser ? __jsx("div", {
    draggable: true,
    onDragStart: props.onDragStartIndex,
    className: "jsx-3436715881" + " " + "myDList",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  }, props.name) : __jsx("div", {
    draggable: true,
    onDragStart: props.onDragStartIndex,
    className: "jsx-3436715881" + " " + "dListBorder",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  }, props.name), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    id: "3436715881",
    __self: this
  }, ".dListBorder.jsx-3436715881{text-align:center;margin:10px 10px 10px 10px;padding:10px 10px 10px 10px;border:1px solid #354980;background-color:white;color:#354980;box-shadow:2px 2px 2px grey;}.myDList.jsx-3436715881{text-align:center;margin:10px 10px 10px 10px;padding:10px 10px 10px 10px;border:1px solid #354980;background-color:#354980;color:#fbca37;box-shadow:2px 2px 2px grey;}.small.jsx-3436715881{font-size:.8em;margin:5px 10px 5px 5px;padding:2px 2px 2px 2px;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wYXVsYmVhdWRvbi9jYWxlbmRhci9uZXh0LWNhbGVuZGFyL2NvbXBvbmVudHMvRW1wbG95ZWVCdXR0b24uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBY2tCLEFBRzRCLEFBU0EsQUFTRixlQUNTLEdBbEJFLEFBU0EscUJBVUYsTUFsQkcsQUFTQSxrQkFVN0IsVUFsQjBCLEFBU0EseUJBUkYsQUFTRSx1QkFSVixFQVNBLFlBUmEsRUFTQSwwQkFSN0IsRUFTQSIsImZpbGUiOiIvVXNlcnMvcGF1bGJlYXVkb24vY2FsZW5kYXIvbmV4dC1jYWxlbmRhci9jb21wb25lbnRzL0VtcGxveWVlQnV0dG9uLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG5jb25zdCBFbXBsb3llZUJ1dHRvbiA9IHByb3BzID0+e1xuXG4gIGxldCBsb2dnZWRJblVzZXIgPSBmYWxzZTtcbiAgaWYocHJvcHMuc2lnbmVkaW51c2VyID09PSBwcm9wcy5uYW1lKXtcbiAgICBsb2dnZWRJblVzZXIgPSB0cnVlO1xuICB9O1xuICByZXR1cm4oXG4gICAgPGRpdj5cbiAgICAgIHsgcHJvcHMuYnV0dG9uc2l6ZSA9PT0gJ3NtYWxsJyA/XG4gICAgICAgIChsb2dnZWRJblVzZXIgPyA8ZGl2IGNsYXNzTmFtZT1cIm15RExpc3Qgc21hbGxcIiBpZD17cHJvcHMuaWR9IGRyYWdnYWJsZSBvbkRyYWdTdGFydD17cHJvcHMub25EcmFnU3RhcnRJbmRleH0+e3Byb3BzLm5hbWV9PC9kaXY+IDogPGRpdiBjbGFzc05hbWU9XCJkTGlzdEJvcmRlciBzbWFsbFwiIGlkPXtwcm9wcy5pZH0gZHJhZ2dhYmxlIG9uRHJhZ1N0YXJ0PXtwcm9wcy5vbkRyYWdTdGFydEluZGV4fT57cHJvcHMubmFtZX08L2Rpdj4pXG4gICAgICAgIDpcbiAgICAgICAgKChsb2dnZWRJblVzZXIgPyA8ZGl2IGNsYXNzTmFtZT1cIm15RExpc3RcIiBkcmFnZ2FibGUgb25EcmFnU3RhcnQ9e3Byb3BzLm9uRHJhZ1N0YXJ0SW5kZXh9Pntwcm9wcy5uYW1lfTwvZGl2PiA6IDxkaXYgY2xhc3NOYW1lPVwiZExpc3RCb3JkZXJcIiBkcmFnZ2FibGUgb25EcmFnU3RhcnQ9e3Byb3BzLm9uRHJhZ1N0YXJ0SW5kZXh9Pntwcm9wcy5uYW1lfTwvZGl2PikpXG4gICAgICB9XG4gICAgICA8c3R5bGUganN4PntgXG4gICAgICAgIC5kTGlzdEJvcmRlcntcbiAgICAgICAgXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIFx0bWFyZ2luOiAxMHB4IDEwcHggMTBweCAxMHB4O1xuICAgICAgICBcdHBhZGRpbmc6IDEwcHggMTBweCAxMHB4IDEwcHg7XG4gICAgICAgIFx0Ym9yZGVyOiAxcHggc29saWQgIzM1NDk4MDtcbiAgICAgICAgXHRiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICBjb2xvcjogIzM1NDk4MDtcbiAgICAgIFx0ICBib3gtc2hhZG93OiAycHggMnB4IDJweCBncmV5O1xuICAgICAgICB9XG4gICAgICAgIC5teURMaXN0e1xuICAgICAgXHQgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgXHRtYXJnaW46IDEwcHggMTBweCAxMHB4IDEwcHg7XG4gICAgICAgIFx0cGFkZGluZzogMTBweCAxMHB4IDEwcHggMTBweDtcbiAgICAgICAgXHRib3JkZXI6IDFweCBzb2xpZCAjMzU0OTgwO1xuICAgICAgICBcdGJhY2tncm91bmQtY29sb3I6ICMzNTQ5ODA7XG4gICAgICAgICAgY29sb3I6ICNmYmNhMzc7XG4gICAgICBcdCAgYm94LXNoYWRvdzogMnB4IDJweCAycHggZ3JleTtcbiAgICAgICAgfVxuICAgICAgICAuc21hbGx7XG4gICAgICAgICAgZm9udC1zaXplOiAuOGVtO1xuICAgICAgICAgIG1hcmdpbjogNXB4IDEwcHggNXB4IDVweDtcbiAgICAgICAgICBwYWRkaW5nOiAycHggMnB4IDJweCAycHg7XG4gICAgICAgIH1cbiAgICAgIGB9PC9zdHlsZT5cbiAgICA8L2Rpdj5cbiAgKTtcblxufTtcblxuZXhwb3J0IGRlZmF1bHQgRW1wbG95ZWVCdXR0b247XG4iXX0= */\n/*@ sourceURL=/Users/paulbeaudon/calendar/next-calendar/components/EmployeeButton.js */"));
};

/* harmony default export */ __webpack_exports__["default"] = (EmployeeButton);

/***/ }),

/***/ "./components/helperfunctions.js":
/*!***************************************!*\
  !*** ./components/helperfunctions.js ***!
  \***************************************/
/*! exports provided: currentDay, currentMonth, currentYear, daysInMonth, startDay, getWrittenMonth, getIntMonth */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "currentDay", function() { return currentDay; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "currentMonth", function() { return currentMonth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "currentYear", function() { return currentYear; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "daysInMonth", function() { return daysInMonth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startDay", function() { return startDay; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getWrittenMonth", function() { return getWrittenMonth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getIntMonth", function() { return getIntMonth; });
var currentDay = function currentDay() {
  return new Date().getDate();
};

var currentMonth = function currentMonth() {
  return new Date().getMonth() + 1;
};

var currentYear = function currentYear() {
  return new Date().getFullYear();
};

var daysInMonth = function daysInMonth(month, year) {
  return new Date(year, month, 0).getDate();
};

var startDay = function startDay(month, year) {
  //let daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  return new Date(year, month - 1, 1).getDay();
};

var getWrittenMonth = function getWrittenMonth(monthInt) {
  var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  return month[monthInt - 1];
};

var getIntMonth = function getIntMonth(monthString) {
  var month = {
    "Jan": 1,
    "Feb": 2,
    "Mar": 3,
    "Apr": 4,
    "May": 5,
    "Jun": 6,
    "Jul": 7,
    "Aug": 8,
    "Sep": 9,
    "Oct": 10,
    "Nov": 11,
    "Dec": 12
  };
  return month[monthString];
};



/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/array/from.js":
/*!*******************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/array/from.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/array/from */ "./node_modules/core-js/library/fn/array/from.js");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/array/is-array.js":
/*!***********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/array/is-array.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/array/is-array */ "./node_modules/core-js/library/fn/array/is-array.js");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/is-iterable.js":
/*!********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/is-iterable.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/is-iterable */ "./node_modules/core-js/library/fn/is-iterable.js");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/json/stringify.js":
/*!***********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/json/stringify.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/json/stringify */ "./node_modules/core-js/library/fn/json/stringify.js");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/define-property */ "./node_modules/core-js/library/fn/object/define-property.js");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/arrayWithoutHoles.js":
/*!******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/arrayWithoutHoles.js ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _arrayWithoutHoles; });
/* harmony import */ var _core_js_array_is_array__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/array/is-array */ "./node_modules/@babel/runtime-corejs2/core-js/array/is-array.js");
/* harmony import */ var _core_js_array_is_array__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_array_is_array__WEBPACK_IMPORTED_MODULE_0__);

function _arrayWithoutHoles(arr) {
  if (_core_js_array_is_array__WEBPACK_IMPORTED_MODULE_0___default()(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js":
/*!***************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _defineProperty; });
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__);

function _defineProperty(obj, key, value) {
  if (key in obj) {
    _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default()(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/iterableToArray.js":
/*!****************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/iterableToArray.js ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _iterableToArray; });
/* harmony import */ var _core_js_array_from__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/array/from */ "./node_modules/@babel/runtime-corejs2/core-js/array/from.js");
/* harmony import */ var _core_js_array_from__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_array_from__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _core_js_is_iterable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core-js/is-iterable */ "./node_modules/@babel/runtime-corejs2/core-js/is-iterable.js");
/* harmony import */ var _core_js_is_iterable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_core_js_is_iterable__WEBPACK_IMPORTED_MODULE_1__);


function _iterableToArray(iter) {
  if (_core_js_is_iterable__WEBPACK_IMPORTED_MODULE_1___default()(Object(iter)) || Object.prototype.toString.call(iter) === "[object Arguments]") return _core_js_array_from__WEBPACK_IMPORTED_MODULE_0___default()(iter);
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/nonIterableSpread.js":
/*!******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/nonIterableSpread.js ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _nonIterableSpread; });
function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/toConsumableArray.js":
/*!******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/toConsumableArray.js ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _toConsumableArray; });
/* harmony import */ var _arrayWithoutHoles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./arrayWithoutHoles */ "./node_modules/@babel/runtime-corejs2/helpers/esm/arrayWithoutHoles.js");
/* harmony import */ var _iterableToArray__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./iterableToArray */ "./node_modules/@babel/runtime-corejs2/helpers/esm/iterableToArray.js");
/* harmony import */ var _nonIterableSpread__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./nonIterableSpread */ "./node_modules/@babel/runtime-corejs2/helpers/esm/nonIterableSpread.js");



function _toConsumableArray(arr) {
  return Object(_arrayWithoutHoles__WEBPACK_IMPORTED_MODULE_0__["default"])(arr) || Object(_iterableToArray__WEBPACK_IMPORTED_MODULE_1__["default"])(arr) || Object(_nonIterableSpread__WEBPACK_IMPORTED_MODULE_2__["default"])();
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/regenerator/index.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/regenerator/index.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./node_modules/@emotion/is-prop-valid/dist/is-prop-valid.browser.esm.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@emotion/is-prop-valid/dist/is-prop-valid.browser.esm.js ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _emotion_memoize__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @emotion/memoize */ "./node_modules/@emotion/memoize/dist/memoize.browser.esm.js");


var reactPropsRegex = /^((children|dangerouslySetInnerHTML|key|ref|autoFocus|defaultValue|defaultChecked|innerHTML|suppressContentEditableWarning|suppressHydrationWarning|valueLink|accept|acceptCharset|accessKey|action|allow|allowUserMedia|allowPaymentRequest|allowFullScreen|allowTransparency|alt|async|autoComplete|autoPlay|capture|cellPadding|cellSpacing|challenge|charSet|checked|cite|classID|className|cols|colSpan|content|contentEditable|contextMenu|controls|controlsList|coords|crossOrigin|data|dateTime|decoding|default|defer|dir|disabled|download|draggable|encType|form|formAction|formEncType|formMethod|formNoValidate|formTarget|frameBorder|headers|height|hidden|high|href|hrefLang|htmlFor|httpEquiv|id|inputMode|integrity|is|keyParams|keyType|kind|label|lang|list|loading|loop|low|marginHeight|marginWidth|max|maxLength|media|mediaGroup|method|min|minLength|multiple|muted|name|nonce|noValidate|open|optimum|pattern|placeholder|playsInline|poster|preload|profile|radioGroup|readOnly|referrerPolicy|rel|required|reversed|role|rows|rowSpan|sandbox|scope|scoped|scrolling|seamless|selected|shape|size|sizes|slot|span|spellCheck|src|srcDoc|srcLang|srcSet|start|step|style|summary|tabIndex|target|title|type|useMap|value|width|wmode|wrap|about|datatype|inlist|prefix|property|resource|typeof|vocab|autoCapitalize|autoCorrect|autoSave|color|itemProp|itemScope|itemType|itemID|itemRef|on|results|security|unselectable|accentHeight|accumulate|additive|alignmentBaseline|allowReorder|alphabetic|amplitude|arabicForm|ascent|attributeName|attributeType|autoReverse|azimuth|baseFrequency|baselineShift|baseProfile|bbox|begin|bias|by|calcMode|capHeight|clip|clipPathUnits|clipPath|clipRule|colorInterpolation|colorInterpolationFilters|colorProfile|colorRendering|contentScriptType|contentStyleType|cursor|cx|cy|d|decelerate|descent|diffuseConstant|direction|display|divisor|dominantBaseline|dur|dx|dy|edgeMode|elevation|enableBackground|end|exponent|externalResourcesRequired|fill|fillOpacity|fillRule|filter|filterRes|filterUnits|floodColor|floodOpacity|focusable|fontFamily|fontSize|fontSizeAdjust|fontStretch|fontStyle|fontVariant|fontWeight|format|from|fr|fx|fy|g1|g2|glyphName|glyphOrientationHorizontal|glyphOrientationVertical|glyphRef|gradientTransform|gradientUnits|hanging|horizAdvX|horizOriginX|ideographic|imageRendering|in|in2|intercept|k|k1|k2|k3|k4|kernelMatrix|kernelUnitLength|kerning|keyPoints|keySplines|keyTimes|lengthAdjust|letterSpacing|lightingColor|limitingConeAngle|local|markerEnd|markerMid|markerStart|markerHeight|markerUnits|markerWidth|mask|maskContentUnits|maskUnits|mathematical|mode|numOctaves|offset|opacity|operator|order|orient|orientation|origin|overflow|overlinePosition|overlineThickness|panose1|paintOrder|pathLength|patternContentUnits|patternTransform|patternUnits|pointerEvents|points|pointsAtX|pointsAtY|pointsAtZ|preserveAlpha|preserveAspectRatio|primitiveUnits|r|radius|refX|refY|renderingIntent|repeatCount|repeatDur|requiredExtensions|requiredFeatures|restart|result|rotate|rx|ry|scale|seed|shapeRendering|slope|spacing|specularConstant|specularExponent|speed|spreadMethod|startOffset|stdDeviation|stemh|stemv|stitchTiles|stopColor|stopOpacity|strikethroughPosition|strikethroughThickness|string|stroke|strokeDasharray|strokeDashoffset|strokeLinecap|strokeLinejoin|strokeMiterlimit|strokeOpacity|strokeWidth|surfaceScale|systemLanguage|tableValues|targetX|targetY|textAnchor|textDecoration|textRendering|textLength|to|transform|u1|u2|underlinePosition|underlineThickness|unicode|unicodeBidi|unicodeRange|unitsPerEm|vAlphabetic|vHanging|vIdeographic|vMathematical|values|vectorEffect|version|vertAdvY|vertOriginX|vertOriginY|viewBox|viewTarget|visibility|widths|wordSpacing|writingMode|x|xHeight|x1|x2|xChannelSelector|xlinkActuate|xlinkArcrole|xlinkHref|xlinkRole|xlinkShow|xlinkTitle|xlinkType|xmlBase|xmlns|xmlnsXlink|xmlLang|xmlSpace|y|y1|y2|yChannelSelector|z|zoomAndPan|for|class|autofocus)|(([Dd][Aa][Tt][Aa]|[Aa][Rr][Ii][Aa]|x)-.*))$/; // https://esbench.com/bench/5bfee68a4cd7e6009ef61d23

var index = Object(_emotion_memoize__WEBPACK_IMPORTED_MODULE_0__["default"])(function (prop) {
  return reactPropsRegex.test(prop) || prop.charCodeAt(0) === 111
  /* o */
  && prop.charCodeAt(1) === 110
  /* n */
  && prop.charCodeAt(2) < 91;
}
/* Z+1 */
);

/* harmony default export */ __webpack_exports__["default"] = (index);


/***/ }),

/***/ "./node_modules/@emotion/memoize/dist/memoize.browser.esm.js":
/*!*******************************************************************!*\
  !*** ./node_modules/@emotion/memoize/dist/memoize.browser.esm.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function memoize(fn) {
  var cache = {};
  return function (arg) {
    if (cache[arg] === undefined) cache[arg] = fn(arg);
    return cache[arg];
  };
}

/* harmony default export */ __webpack_exports__["default"] = (memoize);


/***/ }),

/***/ "./node_modules/@emotion/unitless/dist/unitless.browser.esm.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@emotion/unitless/dist/unitless.browser.esm.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var unitlessKeys = {
  animationIterationCount: 1,
  borderImageOutset: 1,
  borderImageSlice: 1,
  borderImageWidth: 1,
  boxFlex: 1,
  boxFlexGroup: 1,
  boxOrdinalGroup: 1,
  columnCount: 1,
  columns: 1,
  flex: 1,
  flexGrow: 1,
  flexPositive: 1,
  flexShrink: 1,
  flexNegative: 1,
  flexOrder: 1,
  gridRow: 1,
  gridRowEnd: 1,
  gridRowSpan: 1,
  gridRowStart: 1,
  gridColumn: 1,
  gridColumnEnd: 1,
  gridColumnSpan: 1,
  gridColumnStart: 1,
  msGridRow: 1,
  msGridRowSpan: 1,
  msGridColumn: 1,
  msGridColumnSpan: 1,
  fontWeight: 1,
  lineHeight: 1,
  opacity: 1,
  order: 1,
  orphans: 1,
  tabSize: 1,
  widows: 1,
  zIndex: 1,
  zoom: 1,
  WebkitLineClamp: 1,
  // SVG-related properties
  fillOpacity: 1,
  floodOpacity: 1,
  stopOpacity: 1,
  strokeDasharray: 1,
  strokeDashoffset: 1,
  strokeMiterlimit: 1,
  strokeOpacity: 1,
  strokeWidth: 1
};

/* harmony default export */ __webpack_exports__["default"] = (unitlessKeys);


/***/ }),

/***/ "./node_modules/core-js/library/fn/array/from.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/fn/array/from.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../../modules/es6.string.iterator */ "./node_modules/core-js/library/modules/es6.string.iterator.js");
__webpack_require__(/*! ../../modules/es6.array.from */ "./node_modules/core-js/library/modules/es6.array.from.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Array.from;


/***/ }),

/***/ "./node_modules/core-js/library/fn/array/is-array.js":
/*!***********************************************************!*\
  !*** ./node_modules/core-js/library/fn/array/is-array.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../../modules/es6.array.is-array */ "./node_modules/core-js/library/modules/es6.array.is-array.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Array.isArray;


/***/ }),

/***/ "./node_modules/core-js/library/fn/is-iterable.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/fn/is-iterable.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../modules/web.dom.iterable */ "./node_modules/core-js/library/modules/web.dom.iterable.js");
__webpack_require__(/*! ../modules/es6.string.iterator */ "./node_modules/core-js/library/modules/es6.string.iterator.js");
module.exports = __webpack_require__(/*! ../modules/core.is-iterable */ "./node_modules/core-js/library/modules/core.is-iterable.js");


/***/ }),

/***/ "./node_modules/core-js/library/fn/json/stringify.js":
/*!***********************************************************!*\
  !*** ./node_modules/core-js/library/fn/json/stringify.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js");
var $JSON = core.JSON || (core.JSON = { stringify: JSON.stringify });
module.exports = function stringify(it) { // eslint-disable-line no-unused-vars
  return $JSON.stringify.apply($JSON, arguments);
};


/***/ }),

/***/ "./node_modules/core-js/library/fn/object/define-property.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/define-property.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../../modules/es6.object.define-property */ "./node_modules/core-js/library/modules/es6.object.define-property.js");
var $Object = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_a-function.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_a-function.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_add-to-unscopables.js":
/*!*********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_add-to-unscopables.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function () { /* empty */ };


/***/ }),

/***/ "./node_modules/core-js/library/modules/_an-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_an-object.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_array-includes.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_array-includes.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var toLength = __webpack_require__(/*! ./_to-length */ "./node_modules/core-js/library/modules/_to-length.js");
var toAbsoluteIndex = __webpack_require__(/*! ./_to-absolute-index */ "./node_modules/core-js/library/modules/_to-absolute-index.js");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_classof.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_classof.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = __webpack_require__(/*! ./_cof */ "./node_modules/core-js/library/modules/_cof.js");
var TAG = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('toStringTag');
// ES3 wrong here
var ARG = cof(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (e) { /* empty */ }
};

module.exports = function (it) {
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_cof.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_cof.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_core.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_core.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var core = module.exports = { version: '2.6.11' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_create-property.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_create-property.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $defineProperty = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");

module.exports = function (object, index, value) {
  if (index in object) $defineProperty.f(object, index, createDesc(0, value));
  else object[index] = value;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ctx.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ctx.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__(/*! ./_a-function */ "./node_modules/core-js/library/modules/_a-function.js");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_defined.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_defined.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_descriptors.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_descriptors.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_dom-create.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_dom-create.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
var document = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_enum-bug-keys.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_enum-bug-keys.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "./node_modules/core-js/library/modules/_export.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_export.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var ctx = __webpack_require__(/*! ./_ctx */ "./node_modules/core-js/library/modules/_ctx.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_fails.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_fails.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_global.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_global.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_has.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_has.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_hide.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_hide.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
module.exports = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_html.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_html.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").document;
module.exports = document && document.documentElement;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ie8-dom-define.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ie8-dom-define.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") && !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty(__webpack_require__(/*! ./_dom-create */ "./node_modules/core-js/library/modules/_dom-create.js")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iobject.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iobject.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(/*! ./_cof */ "./node_modules/core-js/library/modules/_cof.js");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-array-iter.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-array-iter.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// check on default Array iterator
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
var ITERATOR = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator');
var ArrayProto = Array.prototype;

module.exports = function (it) {
  return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-array.js":
/*!***********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-array.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// 7.2.2 IsArray(argument)
var cof = __webpack_require__(/*! ./_cof */ "./node_modules/core-js/library/modules/_cof.js");
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-object.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-call.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-call.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// call something on iterator step with safe closing on error
var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
module.exports = function (iterator, fn, value, entries) {
  try {
    return entries ? fn(anObject(value)[0], value[1]) : fn(value);
  // 7.4.6 IteratorClose(iterator, completion)
  } catch (e) {
    var ret = iterator['return'];
    if (ret !== undefined) anObject(ret.call(iterator));
    throw e;
  }
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-create.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-create.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__(/*! ./_object-create */ "./node_modules/core-js/library/modules/_object-create.js");
var descriptor = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
var setToStringTag = __webpack_require__(/*! ./_set-to-string-tag */ "./node_modules/core-js/library/modules/_set-to-string-tag.js");
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js")(IteratorPrototype, __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-define.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-define.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__(/*! ./_library */ "./node_modules/core-js/library/modules/_library.js");
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var redefine = __webpack_require__(/*! ./_redefine */ "./node_modules/core-js/library/modules/_redefine.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
var $iterCreate = __webpack_require__(/*! ./_iter-create */ "./node_modules/core-js/library/modules/_iter-create.js");
var setToStringTag = __webpack_require__(/*! ./_set-to-string-tag */ "./node_modules/core-js/library/modules/_set-to-string-tag.js");
var getPrototypeOf = __webpack_require__(/*! ./_object-gpo */ "./node_modules/core-js/library/modules/_object-gpo.js");
var ITERATOR = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-detect.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-detect.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var ITERATOR = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator');
var SAFE_CLOSING = false;

try {
  var riter = [7][ITERATOR]();
  riter['return'] = function () { SAFE_CLOSING = true; };
  // eslint-disable-next-line no-throw-literal
  Array.from(riter, function () { throw 2; });
} catch (e) { /* empty */ }

module.exports = function (exec, skipClosing) {
  if (!skipClosing && !SAFE_CLOSING) return false;
  var safe = false;
  try {
    var arr = [7];
    var iter = arr[ITERATOR]();
    iter.next = function () { return { done: safe = true }; };
    arr[ITERATOR] = function () { return iter; };
    exec(arr);
  } catch (e) { /* empty */ }
  return safe;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-step.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-step.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iterators.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iterators.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_library.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_library.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = true;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-create.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-create.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var dPs = __webpack_require__(/*! ./_object-dps */ "./node_modules/core-js/library/modules/_object-dps.js");
var enumBugKeys = __webpack_require__(/*! ./_enum-bug-keys */ "./node_modules/core-js/library/modules/_enum-bug-keys.js");
var IE_PROTO = __webpack_require__(/*! ./_shared-key */ "./node_modules/core-js/library/modules/_shared-key.js")('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__(/*! ./_dom-create */ "./node_modules/core-js/library/modules/_dom-create.js")('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__(/*! ./_html */ "./node_modules/core-js/library/modules/_html.js").appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dp.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dp.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var IE8_DOM_DEFINE = __webpack_require__(/*! ./_ie8-dom-define */ "./node_modules/core-js/library/modules/_ie8-dom-define.js");
var toPrimitive = __webpack_require__(/*! ./_to-primitive */ "./node_modules/core-js/library/modules/_to-primitive.js");
var dP = Object.defineProperty;

exports.f = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dps.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dps.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var getKeys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");

module.exports = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-gpo.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-gpo.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var toObject = __webpack_require__(/*! ./_to-object */ "./node_modules/core-js/library/modules/_to-object.js");
var IE_PROTO = __webpack_require__(/*! ./_shared-key */ "./node_modules/core-js/library/modules/_shared-key.js")('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-keys-internal.js":
/*!***********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-keys-internal.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var arrayIndexOf = __webpack_require__(/*! ./_array-includes */ "./node_modules/core-js/library/modules/_array-includes.js")(false);
var IE_PROTO = __webpack_require__(/*! ./_shared-key */ "./node_modules/core-js/library/modules/_shared-key.js")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-keys.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-keys.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(/*! ./_object-keys-internal */ "./node_modules/core-js/library/modules/_object-keys-internal.js");
var enumBugKeys = __webpack_require__(/*! ./_enum-bug-keys */ "./node_modules/core-js/library/modules/_enum-bug-keys.js");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_property-desc.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_property-desc.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_redefine.js":
/*!***********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_redefine.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");


/***/ }),

/***/ "./node_modules/core-js/library/modules/_set-to-string-tag.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_set-to-string-tag.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f;
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var TAG = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_shared-key.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_shared-key.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__(/*! ./_shared */ "./node_modules/core-js/library/modules/_shared.js")('keys');
var uid = __webpack_require__(/*! ./_uid */ "./node_modules/core-js/library/modules/_uid.js");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_shared.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_shared.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__(/*! ./_library */ "./node_modules/core-js/library/modules/_library.js") ? 'pure' : 'global',
  copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_string-at.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_string-at.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var defined = __webpack_require__(/*! ./_defined */ "./node_modules/core-js/library/modules/_defined.js");
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-absolute-index.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-absolute-index.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-integer.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-integer.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-iobject.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-iobject.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(/*! ./_iobject */ "./node_modules/core-js/library/modules/_iobject.js");
var defined = __webpack_require__(/*! ./_defined */ "./node_modules/core-js/library/modules/_defined.js");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-length.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-length.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-object.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__(/*! ./_defined */ "./node_modules/core-js/library/modules/_defined.js");
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-primitive.js":
/*!***************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-primitive.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_uid.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_uid.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_wks.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_wks.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__(/*! ./_shared */ "./node_modules/core-js/library/modules/_shared.js")('wks');
var uid = __webpack_require__(/*! ./_uid */ "./node_modules/core-js/library/modules/_uid.js");
var Symbol = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),

/***/ "./node_modules/core-js/library/modules/core.get-iterator-method.js":
/*!**************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/core.get-iterator-method.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__(/*! ./_classof */ "./node_modules/core-js/library/modules/_classof.js");
var ITERATOR = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator');
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
module.exports = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js").getIteratorMethod = function (it) {
  if (it != undefined) return it[ITERATOR]
    || it['@@iterator']
    || Iterators[classof(it)];
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/core.is-iterable.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/core.is-iterable.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__(/*! ./_classof */ "./node_modules/core-js/library/modules/_classof.js");
var ITERATOR = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator');
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
module.exports = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js").isIterable = function (it) {
  var O = Object(it);
  return O[ITERATOR] !== undefined
    || '@@iterator' in O
    // eslint-disable-next-line no-prototype-builtins
    || Iterators.hasOwnProperty(classof(O));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.array.from.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.array.from.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var ctx = __webpack_require__(/*! ./_ctx */ "./node_modules/core-js/library/modules/_ctx.js");
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var toObject = __webpack_require__(/*! ./_to-object */ "./node_modules/core-js/library/modules/_to-object.js");
var call = __webpack_require__(/*! ./_iter-call */ "./node_modules/core-js/library/modules/_iter-call.js");
var isArrayIter = __webpack_require__(/*! ./_is-array-iter */ "./node_modules/core-js/library/modules/_is-array-iter.js");
var toLength = __webpack_require__(/*! ./_to-length */ "./node_modules/core-js/library/modules/_to-length.js");
var createProperty = __webpack_require__(/*! ./_create-property */ "./node_modules/core-js/library/modules/_create-property.js");
var getIterFn = __webpack_require__(/*! ./core.get-iterator-method */ "./node_modules/core-js/library/modules/core.get-iterator-method.js");

$export($export.S + $export.F * !__webpack_require__(/*! ./_iter-detect */ "./node_modules/core-js/library/modules/_iter-detect.js")(function (iter) { Array.from(iter); }), 'Array', {
  // 22.1.2.1 Array.from(arrayLike, mapfn = undefined, thisArg = undefined)
  from: function from(arrayLike /* , mapfn = undefined, thisArg = undefined */) {
    var O = toObject(arrayLike);
    var C = typeof this == 'function' ? this : Array;
    var aLen = arguments.length;
    var mapfn = aLen > 1 ? arguments[1] : undefined;
    var mapping = mapfn !== undefined;
    var index = 0;
    var iterFn = getIterFn(O);
    var length, result, step, iterator;
    if (mapping) mapfn = ctx(mapfn, aLen > 2 ? arguments[2] : undefined, 2);
    // if object isn't iterable or it's array with default iterator - use simple case
    if (iterFn != undefined && !(C == Array && isArrayIter(iterFn))) {
      for (iterator = iterFn.call(O), result = new C(); !(step = iterator.next()).done; index++) {
        createProperty(result, index, mapping ? call(iterator, mapfn, [step.value, index], true) : step.value);
      }
    } else {
      length = toLength(O.length);
      for (result = new C(length); length > index; index++) {
        createProperty(result, index, mapping ? mapfn(O[index], index) : O[index]);
      }
    }
    result.length = index;
    return result;
  }
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.array.is-array.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.array.is-array.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// 22.1.2.2 / 15.4.3.2 Array.isArray(arg)
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");

$export($export.S, 'Array', { isArray: __webpack_require__(/*! ./_is-array */ "./node_modules/core-js/library/modules/_is-array.js") });


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.array.iterator.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.array.iterator.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__(/*! ./_add-to-unscopables */ "./node_modules/core-js/library/modules/_add-to-unscopables.js");
var step = __webpack_require__(/*! ./_iter-step */ "./node_modules/core-js/library/modules/_iter-step.js");
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__(/*! ./_iter-define */ "./node_modules/core-js/library/modules/_iter-define.js")(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.define-property.js":
/*!****************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.define-property.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js"), 'Object', { defineProperty: __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f });


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.string.iterator.js":
/*!*********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.string.iterator.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $at = __webpack_require__(/*! ./_string-at */ "./node_modules/core-js/library/modules/_string-at.js")(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__(/*! ./_iter-define */ "./node_modules/core-js/library/modules/_iter-define.js")(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/web.dom.iterable.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/web.dom.iterable.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./es6.array.iterator */ "./node_modules/core-js/library/modules/es6.array.iterator.js");
var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
var TO_STRING_TAG = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}


/***/ }),

/***/ "./node_modules/is-what/dist/index.esm.js":
/*!************************************************!*\
  !*** ./node_modules/is-what/dist/index.esm.js ***!
  \************************************************/
/*! exports provided: getType, isAnyObject, isArray, isBlob, isBoolean, isDate, isEmptyString, isFile, isFullString, isFunction, isNull, isNullOrUndefined, isNumber, isObject, isObjectLike, isPlainObject, isPrimitive, isPromise, isRegExp, isString, isSymbol, isType, isUndefined */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getType", function() { return getType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isAnyObject", function() { return isAnyObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isArray", function() { return isArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isBlob", function() { return isBlob; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isBoolean", function() { return isBoolean; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isDate", function() { return isDate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isEmptyString", function() { return isEmptyString; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isFile", function() { return isFile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isFullString", function() { return isFullString; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isFunction", function() { return isFunction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNull", function() { return isNull; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNullOrUndefined", function() { return isNullOrUndefined; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNumber", function() { return isNumber; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isObject", function() { return isObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isObjectLike", function() { return isObjectLike; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isPlainObject", function() { return isPlainObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isPrimitive", function() { return isPrimitive; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isPromise", function() { return isPromise; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isRegExp", function() { return isRegExp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isString", function() { return isString; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isSymbol", function() { return isSymbol; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isType", function() { return isType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isUndefined", function() { return isUndefined; });
/**
 * Returns the object type of the given payload
 *
 * @param {*} payload
 * @returns {string}
 */
function getType(payload) {
    return Object.prototype.toString.call(payload).slice(8, -1);
}
/**
 * Returns whether the payload is undefined
 *
 * @param {*} payload
 * @returns {payload is undefined}
 */
function isUndefined(payload) {
    return getType(payload) === 'Undefined';
}
/**
 * Returns whether the payload is null
 *
 * @param {*} payload
 * @returns {payload is null}
 */
function isNull(payload) {
    return getType(payload) === 'Null';
}
/**
 * Returns whether the payload is a plain JavaScript object (excluding special classes or objects with other prototypes)
 *
 * @param {*} payload
 * @returns {payload is {[key: string]: any}}
 */
function isPlainObject(payload) {
    if (getType(payload) !== 'Object')
        return false;
    return payload.constructor === Object && Object.getPrototypeOf(payload) === Object.prototype;
}
/**
 * Returns whether the payload is a plain JavaScript object (excluding special classes or objects with other prototypes)
 *
 * @param {*} payload
 * @returns {payload is {[key: string]: any}}
 */
function isObject(payload) {
    return isPlainObject(payload);
}
/**
 * Returns whether the payload is an any kind of object (including special classes or objects with different prototypes)
 *
 * @param {*} payload
 * @returns {payload is {[key: string]: any}}
 */
function isAnyObject(payload) {
    return getType(payload) === 'Object';
}
/**
 * Returns whether the payload is an object like a type passed in < >
 *
 * Usage: isObjectLike<{id: any}>(payload) // will make sure it's an object and has an `id` prop.
 *
 * @template T this must be passed in < >
 * @param {*} payload
 * @returns {payload is T}
 */
function isObjectLike(payload) {
    return isAnyObject(payload);
}
/**
 * Returns whether the payload is a function
 *
 * @param {*} payload
 * @returns {payload is Function}
 */
function isFunction(payload) {
    return getType(payload) === 'Function';
}
/**
 * Returns whether the payload is an array
 *
 * @param {*} payload
 * @returns {payload is undefined}
 */
function isArray(payload) {
    return getType(payload) === 'Array';
}
/**
 * Returns whether the payload is a string
 *
 * @param {*} payload
 * @returns {payload is string}
 */
function isString(payload) {
    return getType(payload) === 'String';
}
/**
 * Returns whether the payload is a string, BUT returns false for ''
 *
 * @param {*} payload
 * @returns {payload is string}
 */
function isFullString(payload) {
    return isString(payload) && payload !== '';
}
/**
 * Returns whether the payload is ''
 *
 * @param {*} payload
 * @returns {payload is string}
 */
function isEmptyString(payload) {
    return payload === '';
}
/**
 * Returns whether the payload is a number
 *
 * This will return false for NaN
 *
 * @param {*} payload
 * @returns {payload is number}
 */
function isNumber(payload) {
    return getType(payload) === 'Number' && !isNaN(payload);
}
/**
 * Returns whether the payload is a boolean
 *
 * @param {*} payload
 * @returns {payload is boolean}
 */
function isBoolean(payload) {
    return getType(payload) === 'Boolean';
}
/**
 * Returns whether the payload is a regular expression
 *
 * @param {*} payload
 * @returns {payload is RegExp}
 */
function isRegExp(payload) {
    return getType(payload) === 'RegExp';
}
/**
 * Returns whether the payload is a Symbol
 *
 * @param {*} payload
 * @returns {payload is symbol}
 */
function isSymbol(payload) {
    return getType(payload) === 'Symbol';
}
/**
 * Returns whether the payload is a date, and that the date is Valid
 *
 * @param {*} payload
 * @returns {payload is Date}
 */
function isDate(payload) {
    return getType(payload) === 'Date' && !isNaN(payload);
}
/**
 * Returns whether the payload is a blob
 *
 * @param {*} payload
 * @returns {payload is Blob}
 */
function isBlob(payload) {
    return getType(payload) === 'Blob';
}
/**
 * Returns whether the payload is a file
 *
 * @param {*} payload
 * @returns {payload is File}
 */
function isFile(payload) {
    return getType(payload) === 'File';
}
/**
 * Returns whether the payload is a promise
 *
 * @param {*} payload
 * @returns {payload is Promise}
 */
function isPromise(payload) {
    return getType(payload) === 'Promise';
}
/**
 * Returns whether the payload is a primitive type (eg. Boolean | Null | Undefined | Number | String | Symbol)
 *
 * @param {*} payload
 * @returns {(payload is boolean | null | undefined | number | string | symbol)}
 */
function isPrimitive(payload) {
    return (isBoolean(payload) ||
        isNull(payload) ||
        isUndefined(payload) ||
        isNumber(payload) ||
        isString(payload) ||
        isSymbol(payload));
}
/**
 * Returns true whether the payload is null or undefined
 *
 * @param {*} payload
 * @returns {(payload is null | undefined)}
 */
function isNullOrUndefined(payload) {
    return isNull(payload) || isUndefined(payload);
}
/**
 * Does a generic check to check that the given payload is of a given type.
 * In cases like Number, it will return true for NaN as NaN is a Number (thanks javascript!);
 * It will, however, differentiate between object and null
 *
 * @template T
 * @param {*} payload
 * @param {T} type
 * @throws {TypeError} Will throw type error if type is an invalid type
 * @returns {payload is T}
 */
function isType(payload, type) {
    if (!(type instanceof Function)) {
        throw new TypeError('Type must be a function');
    }
    if (!Object.prototype.hasOwnProperty.call(type, 'prototype')) {
        throw new TypeError('Type is not a class');
    }
    // Classes usually have names (as functions usually have names)
    var name = type.name;
    return getType(payload) === name || Boolean(payload && payload.constructor === type);
}




/***/ }),

/***/ "./node_modules/memoize-one/dist/memoize-one.esm.js":
/*!**********************************************************!*\
  !*** ./node_modules/memoize-one/dist/memoize-one.esm.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function areInputsEqual(newInputs, lastInputs) {
    if (newInputs.length !== lastInputs.length) {
        return false;
    }
    for (var i = 0; i < newInputs.length; i++) {
        if (newInputs[i] !== lastInputs[i]) {
            return false;
        }
    }
    return true;
}

function memoizeOne(resultFn, isEqual) {
    if (isEqual === void 0) { isEqual = areInputsEqual; }
    var lastThis;
    var lastArgs = [];
    var lastResult;
    var calledOnce = false;
    function memoized() {
        var newArgs = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            newArgs[_i] = arguments[_i];
        }
        if (calledOnce && lastThis === this && isEqual(newArgs, lastArgs)) {
            return lastResult;
        }
        lastResult = resultFn.apply(this, newArgs);
        calledOnce = true;
        lastThis = this;
        lastArgs = newArgs;
        return lastResult;
    }
    return memoized;
}

/* harmony default export */ __webpack_exports__["default"] = (memoizeOne);


/***/ }),

/***/ "./node_modules/merge-anything/dist/index.esm.js":
/*!*******************************************************!*\
  !*** ./node_modules/merge-anything/dist/index.esm.js ***!
  \*******************************************************/
/*! exports provided: default, concatArrays, merge */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "concatArrays", function() { return concatArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "merge", function() { return merge; });
/* harmony import */ var is_what__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! is-what */ "./node_modules/is-what/dist/index.esm.js");


/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
}

function assignProp(carry, key, newVal, originalObject) {
    var propType = originalObject.propertyIsEnumerable(key)
        ? 'enumerable'
        : 'nonenumerable';
    if (propType === 'enumerable')
        carry[key] = newVal;
    if (propType === 'nonenumerable') {
        Object.defineProperty(carry, key, {
            value: newVal,
            enumerable: false,
            writable: true,
            configurable: true
        });
    }
}
function mergeRecursively(origin, newComer, extensions) {
    // work directly on newComer if its not an object
    if (!Object(is_what__WEBPACK_IMPORTED_MODULE_0__["isPlainObject"])(newComer)) {
        // extend merge rules
        if (extensions && Object(is_what__WEBPACK_IMPORTED_MODULE_0__["isArray"])(extensions)) {
            extensions.forEach(function (extend) {
                newComer = extend(origin, newComer);
            });
        }
        return newComer;
    }
    // define newObject to merge all values upon
    var newObject = {};
    if (Object(is_what__WEBPACK_IMPORTED_MODULE_0__["isPlainObject"])(origin)) {
        var props_1 = Object.getOwnPropertyNames(origin);
        var symbols_1 = Object.getOwnPropertySymbols(origin);
        newObject = __spreadArrays(props_1, symbols_1).reduce(function (carry, key) {
            // @ts-ignore
            var targetVal = origin[key];
            if ((!Object(is_what__WEBPACK_IMPORTED_MODULE_0__["isSymbol"])(key) && !Object.getOwnPropertyNames(newComer).includes(key)) ||
                (Object(is_what__WEBPACK_IMPORTED_MODULE_0__["isSymbol"])(key) && !Object.getOwnPropertySymbols(newComer).includes(key))) {
                assignProp(carry, key, targetVal, origin);
            }
            return carry;
        }, {});
    }
    var props = Object.getOwnPropertyNames(newComer);
    var symbols = Object.getOwnPropertySymbols(newComer);
    var result = __spreadArrays(props, symbols).reduce(function (carry, key) {
        // re-define the origin and newComer as targetVal and newVal
        var newVal = newComer[key];
        var targetVal = (Object(is_what__WEBPACK_IMPORTED_MODULE_0__["isPlainObject"])(origin))
            // @ts-ignore
            ? origin[key]
            : undefined;
        // extend merge rules
        if (extensions && Object(is_what__WEBPACK_IMPORTED_MODULE_0__["isArray"])(extensions)) {
            extensions.forEach(function (extend) {
                newVal = extend(targetVal, newVal);
            });
        }
        // When newVal is an object do the merge recursively
        if (targetVal !== undefined && Object(is_what__WEBPACK_IMPORTED_MODULE_0__["isPlainObject"])(newVal)) {
            newVal = mergeRecursively(targetVal, newVal, extensions);
        }
        assignProp(carry, key, newVal, newComer);
        return carry;
    }, newObject);
    return result;
}
/**
 * Merge anything recursively.
 * Objects get merged, special objects (classes etc.) are re-assigned "as is".
 * Basic types overwrite objects or other basic types.
 *
 * @param {(IConfig | any)} origin
 * @param {...any[]} newComers
 * @returns the result
 */
function merge(origin) {
    var newComers = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        newComers[_i - 1] = arguments[_i];
    }
    var extensions = null;
    var base = origin;
    if (Object(is_what__WEBPACK_IMPORTED_MODULE_0__["isPlainObject"])(origin) && origin.extensions && Object.keys(origin).length === 1) {
        base = {};
        extensions = origin.extensions;
    }
    return newComers.reduce(function (result, newComer) {
        return mergeRecursively(result, newComer, extensions);
    }, base);
}

function concatArrays(originVal, newVal) {
    if (Object(is_what__WEBPACK_IMPORTED_MODULE_0__["isArray"])(originVal) && Object(is_what__WEBPACK_IMPORTED_MODULE_0__["isArray"])(newVal)) {
        // concat logic
        return originVal.concat(newVal);
    }
    return newVal; // always return newVal as fallback!!
}

/* harmony default export */ __webpack_exports__["default"] = (merge);



/***/ }),

/***/ "./node_modules/next/dist/build/polyfills/fetch/index.js":
/*!***************************************************************!*\
  !*** ./node_modules/next/dist/build/polyfills/fetch/index.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* globals self */var fetch=self.fetch.bind(self);module.exports=fetch;module.exports.default=module.exports;

/***/ }),

/***/ "./node_modules/next/dist/build/polyfills/object-assign.js":
/*!***********************************************************************************************************************!*\
  !*** delegated ./node_modules/next/dist/build/polyfills/object-assign.js from dll-reference dll_5f137288facb1107b491 ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(/*! dll-reference dll_5f137288facb1107b491 */ "dll-reference dll_5f137288facb1107b491"))("./node_modules/next/dist/build/polyfills/object-assign.js");

/***/ }),

/***/ "./node_modules/next/dist/build/webpack/loaders/next-client-pages-loader.js?page=%2F&absolutePagePath=%2FUsers%2Fpaulbeaudon%2Fcalendar%2Fnext-calendar%2Fpages%2Findex.js!./":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/next/dist/build/webpack/loaders/next-client-pages-loader.js?page=%2F&absolutePagePath=%2FUsers%2Fpaulbeaudon%2Fcalendar%2Fnext-calendar%2Fpages%2Findex.js ***!
  \*********************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


    (window.__NEXT_P=window.__NEXT_P||[]).push(["/", function() {
      var mod = __webpack_require__(/*! ./pages/index.js */ "./pages/index.js")
      if(true) {
        module.hot.accept(/*! ./pages/index.js */ "./pages/index.js", function() {
          if(!next.router.components["/"]) return
          var updatedPage = __webpack_require__(/*! ./pages/index.js */ "./pages/index.js")
          next.router.update("/", updatedPage)
        })
      }
      return mod
    }]);
  

/***/ }),

/***/ "./node_modules/process/browser.js":
/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ "./node_modules/prop-types/checkPropTypes.js":
/*!*********************************************************************************************************!*\
  !*** delegated ./node_modules/prop-types/checkPropTypes.js from dll-reference dll_5f137288facb1107b491 ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(/*! dll-reference dll_5f137288facb1107b491 */ "dll-reference dll_5f137288facb1107b491"))("./node_modules/prop-types/checkPropTypes.js");

/***/ }),

/***/ "./node_modules/prop-types/factoryWithTypeCheckers.js":
/*!************************************************************!*\
  !*** ./node_modules/prop-types/factoryWithTypeCheckers.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var ReactIs = __webpack_require__(/*! react-is */ "./node_modules/react-is/index.js");
var assign = __webpack_require__(/*! object-assign */ "./node_modules/next/dist/build/polyfills/object-assign.js");

var ReactPropTypesSecret = __webpack_require__(/*! ./lib/ReactPropTypesSecret */ "./node_modules/prop-types/lib/ReactPropTypesSecret.js");
var checkPropTypes = __webpack_require__(/*! ./checkPropTypes */ "./node_modules/prop-types/checkPropTypes.js");

var has = Function.call.bind(Object.prototype.hasOwnProperty);
var printWarning = function() {};

if (true) {
  printWarning = function(text) {
    var message = 'Warning: ' + text;
    if (typeof console !== 'undefined') {
      console.error(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };
}

function emptyFunctionThatReturnsNull() {
  return null;
}

module.exports = function(isValidElement, throwOnDirectAccess) {
  /* global Symbol */
  var ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
  var FAUX_ITERATOR_SYMBOL = '@@iterator'; // Before Symbol spec.

  /**
   * Returns the iterator method function contained on the iterable object.
   *
   * Be sure to invoke the function with the iterable as context:
   *
   *     var iteratorFn = getIteratorFn(myIterable);
   *     if (iteratorFn) {
   *       var iterator = iteratorFn.call(myIterable);
   *       ...
   *     }
   *
   * @param {?object} maybeIterable
   * @return {?function}
   */
  function getIteratorFn(maybeIterable) {
    var iteratorFn = maybeIterable && (ITERATOR_SYMBOL && maybeIterable[ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL]);
    if (typeof iteratorFn === 'function') {
      return iteratorFn;
    }
  }

  /**
   * Collection of methods that allow declaration and validation of props that are
   * supplied to React components. Example usage:
   *
   *   var Props = require('ReactPropTypes');
   *   var MyArticle = React.createClass({
   *     propTypes: {
   *       // An optional string prop named "description".
   *       description: Props.string,
   *
   *       // A required enum prop named "category".
   *       category: Props.oneOf(['News','Photos']).isRequired,
   *
   *       // A prop named "dialog" that requires an instance of Dialog.
   *       dialog: Props.instanceOf(Dialog).isRequired
   *     },
   *     render: function() { ... }
   *   });
   *
   * A more formal specification of how these methods are used:
   *
   *   type := array|bool|func|object|number|string|oneOf([...])|instanceOf(...)
   *   decl := ReactPropTypes.{type}(.isRequired)?
   *
   * Each and every declaration produces a function with the same signature. This
   * allows the creation of custom validation functions. For example:
   *
   *  var MyLink = React.createClass({
   *    propTypes: {
   *      // An optional string or URI prop named "href".
   *      href: function(props, propName, componentName) {
   *        var propValue = props[propName];
   *        if (propValue != null && typeof propValue !== 'string' &&
   *            !(propValue instanceof URI)) {
   *          return new Error(
   *            'Expected a string or an URI for ' + propName + ' in ' +
   *            componentName
   *          );
   *        }
   *      }
   *    },
   *    render: function() {...}
   *  });
   *
   * @internal
   */

  var ANONYMOUS = '<<anonymous>>';

  // Important!
  // Keep this list in sync with production version in `./factoryWithThrowingShims.js`.
  var ReactPropTypes = {
    array: createPrimitiveTypeChecker('array'),
    bool: createPrimitiveTypeChecker('boolean'),
    func: createPrimitiveTypeChecker('function'),
    number: createPrimitiveTypeChecker('number'),
    object: createPrimitiveTypeChecker('object'),
    string: createPrimitiveTypeChecker('string'),
    symbol: createPrimitiveTypeChecker('symbol'),

    any: createAnyTypeChecker(),
    arrayOf: createArrayOfTypeChecker,
    element: createElementTypeChecker(),
    elementType: createElementTypeTypeChecker(),
    instanceOf: createInstanceTypeChecker,
    node: createNodeChecker(),
    objectOf: createObjectOfTypeChecker,
    oneOf: createEnumTypeChecker,
    oneOfType: createUnionTypeChecker,
    shape: createShapeTypeChecker,
    exact: createStrictShapeTypeChecker,
  };

  /**
   * inlined Object.is polyfill to avoid requiring consumers ship their own
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
   */
  /*eslint-disable no-self-compare*/
  function is(x, y) {
    // SameValue algorithm
    if (x === y) {
      // Steps 1-5, 7-10
      // Steps 6.b-6.e: +0 != -0
      return x !== 0 || 1 / x === 1 / y;
    } else {
      // Step 6.a: NaN == NaN
      return x !== x && y !== y;
    }
  }
  /*eslint-enable no-self-compare*/

  /**
   * We use an Error-like object for backward compatibility as people may call
   * PropTypes directly and inspect their output. However, we don't use real
   * Errors anymore. We don't inspect their stack anyway, and creating them
   * is prohibitively expensive if they are created too often, such as what
   * happens in oneOfType() for any type before the one that matched.
   */
  function PropTypeError(message) {
    this.message = message;
    this.stack = '';
  }
  // Make `instanceof Error` still work for returned errors.
  PropTypeError.prototype = Error.prototype;

  function createChainableTypeChecker(validate) {
    if (true) {
      var manualPropTypeCallCache = {};
      var manualPropTypeWarningCount = 0;
    }
    function checkType(isRequired, props, propName, componentName, location, propFullName, secret) {
      componentName = componentName || ANONYMOUS;
      propFullName = propFullName || propName;

      if (secret !== ReactPropTypesSecret) {
        if (throwOnDirectAccess) {
          // New behavior only for users of `prop-types` package
          var err = new Error(
            'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
            'Use `PropTypes.checkPropTypes()` to call them. ' +
            'Read more at http://fb.me/use-check-prop-types'
          );
          err.name = 'Invariant Violation';
          throw err;
        } else if ( true && typeof console !== 'undefined') {
          // Old behavior for people using React.PropTypes
          var cacheKey = componentName + ':' + propName;
          if (
            !manualPropTypeCallCache[cacheKey] &&
            // Avoid spamming the console because they are often not actionable except for lib authors
            manualPropTypeWarningCount < 3
          ) {
            printWarning(
              'You are manually calling a React.PropTypes validation ' +
              'function for the `' + propFullName + '` prop on `' + componentName  + '`. This is deprecated ' +
              'and will throw in the standalone `prop-types` package. ' +
              'You may be seeing this warning due to a third-party PropTypes ' +
              'library. See https://fb.me/react-warning-dont-call-proptypes ' + 'for details.'
            );
            manualPropTypeCallCache[cacheKey] = true;
            manualPropTypeWarningCount++;
          }
        }
      }
      if (props[propName] == null) {
        if (isRequired) {
          if (props[propName] === null) {
            return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required ' + ('in `' + componentName + '`, but its value is `null`.'));
          }
          return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required in ' + ('`' + componentName + '`, but its value is `undefined`.'));
        }
        return null;
      } else {
        return validate(props, propName, componentName, location, propFullName);
      }
    }

    var chainedCheckType = checkType.bind(null, false);
    chainedCheckType.isRequired = checkType.bind(null, true);

    return chainedCheckType;
  }

  function createPrimitiveTypeChecker(expectedType) {
    function validate(props, propName, componentName, location, propFullName, secret) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== expectedType) {
        // `propValue` being instance of, say, date/regexp, pass the 'object'
        // check, but we can offer a more precise error message here rather than
        // 'of type `object`'.
        var preciseType = getPreciseType(propValue);

        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + preciseType + '` supplied to `' + componentName + '`, expected ') + ('`' + expectedType + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createAnyTypeChecker() {
    return createChainableTypeChecker(emptyFunctionThatReturnsNull);
  }

  function createArrayOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside arrayOf.');
      }
      var propValue = props[propName];
      if (!Array.isArray(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an array.'));
      }
      for (var i = 0; i < propValue.length; i++) {
        var error = typeChecker(propValue, i, componentName, location, propFullName + '[' + i + ']', ReactPropTypesSecret);
        if (error instanceof Error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createElementTypeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      if (!isValidElement(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createElementTypeTypeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      if (!ReactIs.isValidElementType(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement type.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createInstanceTypeChecker(expectedClass) {
    function validate(props, propName, componentName, location, propFullName) {
      if (!(props[propName] instanceof expectedClass)) {
        var expectedClassName = expectedClass.name || ANONYMOUS;
        var actualClassName = getClassName(props[propName]);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + actualClassName + '` supplied to `' + componentName + '`, expected ') + ('instance of `' + expectedClassName + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createEnumTypeChecker(expectedValues) {
    if (!Array.isArray(expectedValues)) {
      if (true) {
        if (arguments.length > 1) {
          printWarning(
            'Invalid arguments supplied to oneOf, expected an array, got ' + arguments.length + ' arguments. ' +
            'A common mistake is to write oneOf(x, y, z) instead of oneOf([x, y, z]).'
          );
        } else {
          printWarning('Invalid argument supplied to oneOf, expected an array.');
        }
      }
      return emptyFunctionThatReturnsNull;
    }

    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      for (var i = 0; i < expectedValues.length; i++) {
        if (is(propValue, expectedValues[i])) {
          return null;
        }
      }

      var valuesString = JSON.stringify(expectedValues, function replacer(key, value) {
        var type = getPreciseType(value);
        if (type === 'symbol') {
          return String(value);
        }
        return value;
      });
      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of value `' + String(propValue) + '` ' + ('supplied to `' + componentName + '`, expected one of ' + valuesString + '.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createObjectOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside objectOf.');
      }
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an object.'));
      }
      for (var key in propValue) {
        if (has(propValue, key)) {
          var error = typeChecker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
          if (error instanceof Error) {
            return error;
          }
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createUnionTypeChecker(arrayOfTypeCheckers) {
    if (!Array.isArray(arrayOfTypeCheckers)) {
       true ? printWarning('Invalid argument supplied to oneOfType, expected an instance of array.') : undefined;
      return emptyFunctionThatReturnsNull;
    }

    for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
      var checker = arrayOfTypeCheckers[i];
      if (typeof checker !== 'function') {
        printWarning(
          'Invalid argument supplied to oneOfType. Expected an array of check functions, but ' +
          'received ' + getPostfixForTypeWarning(checker) + ' at index ' + i + '.'
        );
        return emptyFunctionThatReturnsNull;
      }
    }

    function validate(props, propName, componentName, location, propFullName) {
      for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
        var checker = arrayOfTypeCheckers[i];
        if (checker(props, propName, componentName, location, propFullName, ReactPropTypesSecret) == null) {
          return null;
        }
      }

      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createNodeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      if (!isNode(props[propName])) {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`, expected a ReactNode.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      for (var key in shapeTypes) {
        var checker = shapeTypes[key];
        if (!checker) {
          continue;
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
        if (error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createStrictShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      // We need to check all keys in case some are required but missing from
      // props.
      var allKeys = assign({}, props[propName], shapeTypes);
      for (var key in allKeys) {
        var checker = shapeTypes[key];
        if (!checker) {
          return new PropTypeError(
            'Invalid ' + location + ' `' + propFullName + '` key `' + key + '` supplied to `' + componentName + '`.' +
            '\nBad object: ' + JSON.stringify(props[propName], null, '  ') +
            '\nValid keys: ' +  JSON.stringify(Object.keys(shapeTypes), null, '  ')
          );
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
        if (error) {
          return error;
        }
      }
      return null;
    }

    return createChainableTypeChecker(validate);
  }

  function isNode(propValue) {
    switch (typeof propValue) {
      case 'number':
      case 'string':
      case 'undefined':
        return true;
      case 'boolean':
        return !propValue;
      case 'object':
        if (Array.isArray(propValue)) {
          return propValue.every(isNode);
        }
        if (propValue === null || isValidElement(propValue)) {
          return true;
        }

        var iteratorFn = getIteratorFn(propValue);
        if (iteratorFn) {
          var iterator = iteratorFn.call(propValue);
          var step;
          if (iteratorFn !== propValue.entries) {
            while (!(step = iterator.next()).done) {
              if (!isNode(step.value)) {
                return false;
              }
            }
          } else {
            // Iterator will provide entry [k,v] tuples rather than values.
            while (!(step = iterator.next()).done) {
              var entry = step.value;
              if (entry) {
                if (!isNode(entry[1])) {
                  return false;
                }
              }
            }
          }
        } else {
          return false;
        }

        return true;
      default:
        return false;
    }
  }

  function isSymbol(propType, propValue) {
    // Native Symbol.
    if (propType === 'symbol') {
      return true;
    }

    // falsy value can't be a Symbol
    if (!propValue) {
      return false;
    }

    // 19.4.3.5 Symbol.prototype[@@toStringTag] === 'Symbol'
    if (propValue['@@toStringTag'] === 'Symbol') {
      return true;
    }

    // Fallback for non-spec compliant Symbols which are polyfilled.
    if (typeof Symbol === 'function' && propValue instanceof Symbol) {
      return true;
    }

    return false;
  }

  // Equivalent of `typeof` but with special handling for array and regexp.
  function getPropType(propValue) {
    var propType = typeof propValue;
    if (Array.isArray(propValue)) {
      return 'array';
    }
    if (propValue instanceof RegExp) {
      // Old webkits (at least until Android 4.0) return 'function' rather than
      // 'object' for typeof a RegExp. We'll normalize this here so that /bla/
      // passes PropTypes.object.
      return 'object';
    }
    if (isSymbol(propType, propValue)) {
      return 'symbol';
    }
    return propType;
  }

  // This handles more types than `getPropType`. Only used for error messages.
  // See `createPrimitiveTypeChecker`.
  function getPreciseType(propValue) {
    if (typeof propValue === 'undefined' || propValue === null) {
      return '' + propValue;
    }
    var propType = getPropType(propValue);
    if (propType === 'object') {
      if (propValue instanceof Date) {
        return 'date';
      } else if (propValue instanceof RegExp) {
        return 'regexp';
      }
    }
    return propType;
  }

  // Returns a string that is postfixed to a warning about an invalid type.
  // For example, "undefined" or "of type array"
  function getPostfixForTypeWarning(value) {
    var type = getPreciseType(value);
    switch (type) {
      case 'array':
      case 'object':
        return 'an ' + type;
      case 'boolean':
      case 'date':
      case 'regexp':
        return 'a ' + type;
      default:
        return type;
    }
  }

  // Returns class name of the object, if any.
  function getClassName(propValue) {
    if (!propValue.constructor || !propValue.constructor.name) {
      return ANONYMOUS;
    }
    return propValue.constructor.name;
  }

  ReactPropTypes.checkPropTypes = checkPropTypes;
  ReactPropTypes.resetWarningCache = checkPropTypes.resetWarningCache;
  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};


/***/ }),

/***/ "./node_modules/prop-types/index.js":
/*!******************************************!*\
  !*** ./node_modules/prop-types/index.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

if (true) {
  var ReactIs = __webpack_require__(/*! react-is */ "./node_modules/react-is/index.js");

  // By explicitly using `prop-types` you are opting into new development behavior.
  // http://fb.me/prop-types-in-prod
  var throwOnDirectAccess = true;
  module.exports = __webpack_require__(/*! ./factoryWithTypeCheckers */ "./node_modules/prop-types/factoryWithTypeCheckers.js")(ReactIs.isElement, throwOnDirectAccess);
} else {}


/***/ }),

/***/ "./node_modules/prop-types/lib/ReactPropTypesSecret.js":
/*!*******************************************************************************************************************!*\
  !*** delegated ./node_modules/prop-types/lib/ReactPropTypesSecret.js from dll-reference dll_5f137288facb1107b491 ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(/*! dll-reference dll_5f137288facb1107b491 */ "dll-reference dll_5f137288facb1107b491"))("./node_modules/prop-types/lib/ReactPropTypesSecret.js");

/***/ }),

/***/ "./node_modules/react-is/cjs/react-is.development.js":
/*!***********************************************************!*\
  !*** ./node_modules/react-is/cjs/react-is.development.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/** @license React v16.8.6
 * react-is.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */





if (true) {
  (function() {
'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

// The Symbol used to tag the ReactElement-like types. If there is no native Symbol
// nor polyfill, then a plain number is used for performance.
var hasSymbol = typeof Symbol === 'function' && Symbol.for;

var REACT_ELEMENT_TYPE = hasSymbol ? Symbol.for('react.element') : 0xeac7;
var REACT_PORTAL_TYPE = hasSymbol ? Symbol.for('react.portal') : 0xeaca;
var REACT_FRAGMENT_TYPE = hasSymbol ? Symbol.for('react.fragment') : 0xeacb;
var REACT_STRICT_MODE_TYPE = hasSymbol ? Symbol.for('react.strict_mode') : 0xeacc;
var REACT_PROFILER_TYPE = hasSymbol ? Symbol.for('react.profiler') : 0xead2;
var REACT_PROVIDER_TYPE = hasSymbol ? Symbol.for('react.provider') : 0xeacd;
var REACT_CONTEXT_TYPE = hasSymbol ? Symbol.for('react.context') : 0xeace;
var REACT_ASYNC_MODE_TYPE = hasSymbol ? Symbol.for('react.async_mode') : 0xeacf;
var REACT_CONCURRENT_MODE_TYPE = hasSymbol ? Symbol.for('react.concurrent_mode') : 0xeacf;
var REACT_FORWARD_REF_TYPE = hasSymbol ? Symbol.for('react.forward_ref') : 0xead0;
var REACT_SUSPENSE_TYPE = hasSymbol ? Symbol.for('react.suspense') : 0xead1;
var REACT_MEMO_TYPE = hasSymbol ? Symbol.for('react.memo') : 0xead3;
var REACT_LAZY_TYPE = hasSymbol ? Symbol.for('react.lazy') : 0xead4;

function isValidElementType(type) {
  return typeof type === 'string' || typeof type === 'function' ||
  // Note: its typeof might be other than 'symbol' or 'number' if it's a polyfill.
  type === REACT_FRAGMENT_TYPE || type === REACT_CONCURRENT_MODE_TYPE || type === REACT_PROFILER_TYPE || type === REACT_STRICT_MODE_TYPE || type === REACT_SUSPENSE_TYPE || typeof type === 'object' && type !== null && (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE);
}

/**
 * Forked from fbjs/warning:
 * https://github.com/facebook/fbjs/blob/e66ba20ad5be433eb54423f2b097d829324d9de6/packages/fbjs/src/__forks__/warning.js
 *
 * Only change is we use console.warn instead of console.error,
 * and do nothing when 'console' is not supported.
 * This really simplifies the code.
 * ---
 * Similar to invariant but only logs a warning if the condition is not met.
 * This can be used to log issues in development environments in critical
 * paths. Removing the logging code for production environments will keep the
 * same logic and follow the same code paths.
 */

var lowPriorityWarning = function () {};

{
  var printWarning = function (format) {
    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    var argIndex = 0;
    var message = 'Warning: ' + format.replace(/%s/g, function () {
      return args[argIndex++];
    });
    if (typeof console !== 'undefined') {
      console.warn(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };

  lowPriorityWarning = function (condition, format) {
    if (format === undefined) {
      throw new Error('`lowPriorityWarning(condition, format, ...args)` requires a warning ' + 'message argument');
    }
    if (!condition) {
      for (var _len2 = arguments.length, args = Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
        args[_key2 - 2] = arguments[_key2];
      }

      printWarning.apply(undefined, [format].concat(args));
    }
  };
}

var lowPriorityWarning$1 = lowPriorityWarning;

function typeOf(object) {
  if (typeof object === 'object' && object !== null) {
    var $$typeof = object.$$typeof;
    switch ($$typeof) {
      case REACT_ELEMENT_TYPE:
        var type = object.type;

        switch (type) {
          case REACT_ASYNC_MODE_TYPE:
          case REACT_CONCURRENT_MODE_TYPE:
          case REACT_FRAGMENT_TYPE:
          case REACT_PROFILER_TYPE:
          case REACT_STRICT_MODE_TYPE:
          case REACT_SUSPENSE_TYPE:
            return type;
          default:
            var $$typeofType = type && type.$$typeof;

            switch ($$typeofType) {
              case REACT_CONTEXT_TYPE:
              case REACT_FORWARD_REF_TYPE:
              case REACT_PROVIDER_TYPE:
                return $$typeofType;
              default:
                return $$typeof;
            }
        }
      case REACT_LAZY_TYPE:
      case REACT_MEMO_TYPE:
      case REACT_PORTAL_TYPE:
        return $$typeof;
    }
  }

  return undefined;
}

// AsyncMode is deprecated along with isAsyncMode
var AsyncMode = REACT_ASYNC_MODE_TYPE;
var ConcurrentMode = REACT_CONCURRENT_MODE_TYPE;
var ContextConsumer = REACT_CONTEXT_TYPE;
var ContextProvider = REACT_PROVIDER_TYPE;
var Element = REACT_ELEMENT_TYPE;
var ForwardRef = REACT_FORWARD_REF_TYPE;
var Fragment = REACT_FRAGMENT_TYPE;
var Lazy = REACT_LAZY_TYPE;
var Memo = REACT_MEMO_TYPE;
var Portal = REACT_PORTAL_TYPE;
var Profiler = REACT_PROFILER_TYPE;
var StrictMode = REACT_STRICT_MODE_TYPE;
var Suspense = REACT_SUSPENSE_TYPE;

var hasWarnedAboutDeprecatedIsAsyncMode = false;

// AsyncMode should be deprecated
function isAsyncMode(object) {
  {
    if (!hasWarnedAboutDeprecatedIsAsyncMode) {
      hasWarnedAboutDeprecatedIsAsyncMode = true;
      lowPriorityWarning$1(false, 'The ReactIs.isAsyncMode() alias has been deprecated, ' + 'and will be removed in React 17+. Update your code to use ' + 'ReactIs.isConcurrentMode() instead. It has the exact same API.');
    }
  }
  return isConcurrentMode(object) || typeOf(object) === REACT_ASYNC_MODE_TYPE;
}
function isConcurrentMode(object) {
  return typeOf(object) === REACT_CONCURRENT_MODE_TYPE;
}
function isContextConsumer(object) {
  return typeOf(object) === REACT_CONTEXT_TYPE;
}
function isContextProvider(object) {
  return typeOf(object) === REACT_PROVIDER_TYPE;
}
function isElement(object) {
  return typeof object === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
}
function isForwardRef(object) {
  return typeOf(object) === REACT_FORWARD_REF_TYPE;
}
function isFragment(object) {
  return typeOf(object) === REACT_FRAGMENT_TYPE;
}
function isLazy(object) {
  return typeOf(object) === REACT_LAZY_TYPE;
}
function isMemo(object) {
  return typeOf(object) === REACT_MEMO_TYPE;
}
function isPortal(object) {
  return typeOf(object) === REACT_PORTAL_TYPE;
}
function isProfiler(object) {
  return typeOf(object) === REACT_PROFILER_TYPE;
}
function isStrictMode(object) {
  return typeOf(object) === REACT_STRICT_MODE_TYPE;
}
function isSuspense(object) {
  return typeOf(object) === REACT_SUSPENSE_TYPE;
}

exports.typeOf = typeOf;
exports.AsyncMode = AsyncMode;
exports.ConcurrentMode = ConcurrentMode;
exports.ContextConsumer = ContextConsumer;
exports.ContextProvider = ContextProvider;
exports.Element = Element;
exports.ForwardRef = ForwardRef;
exports.Fragment = Fragment;
exports.Lazy = Lazy;
exports.Memo = Memo;
exports.Portal = Portal;
exports.Profiler = Profiler;
exports.StrictMode = StrictMode;
exports.Suspense = Suspense;
exports.isValidElementType = isValidElementType;
exports.isAsyncMode = isAsyncMode;
exports.isConcurrentMode = isConcurrentMode;
exports.isContextConsumer = isContextConsumer;
exports.isContextProvider = isContextProvider;
exports.isElement = isElement;
exports.isForwardRef = isForwardRef;
exports.isFragment = isFragment;
exports.isLazy = isLazy;
exports.isMemo = isMemo;
exports.isPortal = isPortal;
exports.isProfiler = isProfiler;
exports.isStrictMode = isStrictMode;
exports.isSuspense = isSuspense;
  })();
}


/***/ }),

/***/ "./node_modules/react-is/index.js":
/*!****************************************!*\
  !*** ./node_modules/react-is/index.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


if (false) {} else {
  module.exports = __webpack_require__(/*! ./cjs/react-is.development.js */ "./node_modules/react-is/cjs/react-is.development.js");
}


/***/ }),

/***/ "./node_modules/react-spinners-kit/lib/index.js":
/*!******************************************************!*\
  !*** ./node_modules/react-spinners-kit/lib/index.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

!function(n,e){for(var t in e)n[t]=e[t]}(exports,function(n){var e={};function t(i){if(e[i])return e[i].exports;var r=e[i]={i:i,l:!1,exports:{}};return n[i].call(r.exports,r,r.exports,t),r.l=!0,r.exports}return t.m=n,t.c=e,t.d=function(n,e,i){t.o(n,e)||Object.defineProperty(n,e,{enumerable:!0,get:i})},t.r=function(n){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(n,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(n,"__esModule",{value:!0})},t.t=function(n,e){if(1&e&&(n=t(n)),8&e)return n;if(4&e&&"object"==typeof n&&n&&n.__esModule)return n;var i=Object.create(null);if(t.r(i),Object.defineProperty(i,"default",{enumerable:!0,value:n}),2&e&&"string"!=typeof n)for(var r in n)t.d(i,r,function(e){return n[e]}.bind(null,r));return i},t.n=function(n){var e=n&&n.__esModule?function(){return n.default}:function(){return n};return t.d(e,"a",e),e},t.o=function(n,e){return Object.prototype.hasOwnProperty.call(n,e)},t.p="",t(t.s=3)}([function(n,e){n.exports=__webpack_require__(/*! react */ "./node_modules/react/index.js")},function(n,e){n.exports=__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js")},function(n,e){n.exports=__webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js")},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.MetroSpinner=e.WhisperSpinner=e.ClassicSpinner=e.TraceSpinner=e.JellyfishSpinner=e.MagicSpinner=e.FlapperSpinner=e.HoopSpinner=e.RingSpinner=e.RainbowSpinner=e.PongSpinner=e.CombSpinner=e.GooSpinner=e.SwishSpinner=e.RotateSpinner=e.ClapSpinner=e.FlagSpinner=e.SphereSpinner=e.FillSpinner=e.CubeSpinner=e.ImpulseSpinner=e.DominoSpinner=e.SequenceSpinner=e.PulseSpinner=e.SpiralSpinner=e.CircleSpinner=e.GuardSpinner=e.HeartSpinner=e.StageSpinner=e.FireworkSpinner=e.PushSpinner=e.WaveSpinner=e.BarsSpinner=e.SwapSpinner=e.GridSpinner=e.BallSpinner=void 0;var i=t(4),r=t(5),o=t(6),a=t(7),s=t(8),l=t(9),u=t(10),c=t(11),f=t(12),d=t(13),p=t(14),z=t(15),m=t(16),g=t(17),h=t(18),b=t(19),y=t(20),U=t(21),v=t(22),w=t(23),_=t(24),x=t(25),S=t(26),k=t(27),C=t(28),j=t(29),P=t(30),O=t(31),I=t(32),M=t(33),E=t(34),B=t(35),X=t(36),Y=t(37),N=t(38),W=t(39);e.BallSpinner=i.BallSpinner,e.GridSpinner=r.GridSpinner,e.SwapSpinner=o.SwapSpinner,e.BarsSpinner=a.BarsSpinner,e.WaveSpinner=s.WaveSpinner,e.PushSpinner=l.PushSpinner,e.FireworkSpinner=u.FireworkSpinner,e.StageSpinner=c.StageSpinner,e.HeartSpinner=f.HeartSpinner,e.GuardSpinner=d.GuardSpinner,e.CircleSpinner=p.CircleSpinner,e.SpiralSpinner=z.SpiralSpinner,e.PulseSpinner=m.PulseSpinner,e.SequenceSpinner=g.SequenceSpinner,e.DominoSpinner=h.DominoSpinner,e.ImpulseSpinner=b.ImpulseSpinner,e.CubeSpinner=y.CubeSpinner,e.FillSpinner=U.FillSpinner,e.SphereSpinner=v.SphereSpinner,e.FlagSpinner=w.FlagSpinner,e.ClapSpinner=_.ClapSpinner,e.RotateSpinner=x.RotateSpinner,e.SwishSpinner=S.SwishSpinner,e.GooSpinner=k.GooSpinner,e.CombSpinner=C.CombSpinner,e.PongSpinner=j.PongSpinner,e.RainbowSpinner=P.RainbowSpinner,e.RingSpinner=O.RingSpinner,e.HoopSpinner=I.HoopSpinner,e.FlapperSpinner=M.FlapperSpinner,e.MagicSpinner=E.MagicSpinner,e.JellyfishSpinner=B.JellyfishSpinner,e.TraceSpinner=X.TraceSpinner,e.ClassicSpinner=Y.ClassicSpinner,e.WhisperSpinner=N.WhisperSpinner,e.MetroSpinner=W.MetroSpinner},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.BallSpinner=void 0;var i=f(["\n    0% {\n        transform: translateX(0) scale(1);\n    }\n    25% {\n        transform: translateX(","",") scale(0.25);\n    }\n    50% {\n        transform: translateX(0) scale(1);\n    }\n    75% {\n        transform: translateX(","",") scale(0.25);\n    }\n    100% {\n        transform: translateX(0) scale(1);\n\n    }\n"],["\n    0% {\n        transform: translateX(0) scale(1);\n    }\n    25% {\n        transform: translateX(","",") scale(0.25);\n    }\n    50% {\n        transform: translateX(0) scale(1);\n    }\n    75% {\n        transform: translateX(","",") scale(0.25);\n    }\n    100% {\n        transform: translateX(0) scale(1);\n\n    }\n"]),r=f(["\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"],["\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"]),o=f(["\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    animation: "," 3s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n"],["\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    animation: "," 3s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=e.BallSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&a.default.createElement(p,{size:e},a.default.createElement(z,{color:t,size:e,sizeUnit:r})," ")},p=u.default.div.withConfig({displayName:"ball__Wrapper",componentId:"sc-1edcqkl-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size/2+n.sizeUnit})),z=u.default.div.withConfig({displayName:"ball__Ball",componentId:"sc-1edcqkl-1"})(o,(function(n){return""+n.size/3+n.sizeUnit}),(function(n){return""+n.size/3+n.sizeUnit}),(function(n){return n.color}),(function(n){return function(n){return(0,l.keyframes)(i,n.size/2,n.sizeUnit,-n.size/2,n.sizeUnit)}(n)}));d.defaultProps={loading:!0,size:40,color:"#00ff89",sizeUnit:"px"},d.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.GridSpinner=void 0;var i=f(["\n    0% {\n        top: ","",";\n        left: ","",";\n    }\n    50% {\n        width: ","",";\n        height: ","",";\n        top: ","",";\n        left: ","",";\n    }\n    100% {\n        top: ","",";\n        left: ","",";\n    }\n"],["\n    0% {\n        top: ","",";\n        left: ","",";\n    }\n    50% {\n        width: ","",";\n        height: ","",";\n        top: ","",";\n        left: ","",";\n    }\n    100% {\n        top: ","",";\n        left: ","",";\n    }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"]),o=f(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    animation: "," 1.5s cubic-bezier(0.23, 1, 0.32, 1) infinite;\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    animation: "," 1.5s cubic-bezier(0.23, 1, 0.32, 1) infinite;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=e.GridSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&a.default.createElement(p,{size:e,sizeUnit:r},function(n){for(var e=n.countBallsInLine,t=n.color,i=n.size,r=n.sizeUnit,o=[],s=0,l=0;l<e;l++)for(var u=0;u<e;u++)o.push(a.default.createElement(z,{color:t,size:i,x:l*(i/3+i/12),y:u*(i/3+i/12),key:s.toString(),sizeUnit:r})),s++;return o}({countBallsInLine:3,color:t,size:e,sizeUnit:r}))},p=u.default.div.withConfig({displayName:"grid__Wrapper",componentId:"sc-11vno70-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),z=u.default.div.withConfig({displayName:"grid__Ball",componentId:"sc-11vno70-1"})(o,(function(n){return""+n.y+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.size/6+n.sizeUnit}),(function(n){return""+n.size/6+n.sizeUnit}),(function(n){return n.color}),(function(n){return(0,l.keyframes)(i,n.y,n.sizeUnit,n.x,n.sizeUnit,n.size/4,n.sizeUnit,n.size/4,n.sizeUnit,n.size/2-n.size/8,n.sizeUnit,n.size/2-n.size/8,n.sizeUnit,n.y,n.sizeUnit,n.x,n.sizeUnit)}));d.defaultProps={loading:!0,size:40,color:"#fff",sizeUnit:"px"},d.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.SwapSpinner=void 0;var i=f(["\n    0% {\n        top: ","px;\n        left: ","px;\n    }\n    50%{\n        top: ","px;\n        left: ","px;\n    }\n    100% {\n        top: ","px;\n        left: ","px;\n    }\n"],["\n    0% {\n        top: ","px;\n        left: ","px;\n    }\n    50%{\n        top: ","px;\n        left: ","px;\n    }\n    100% {\n        top: ","px;\n        left: ","px;\n    }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"]),o=f(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    animation: "," 1.5s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    &:nth-child(2) {\n        animation-timing-function: cubic-bezier(1, 0, 0, 1);\n        animation-duration: 1.5s;\n    }\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    animation: "," 1.5s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    &:nth-child(2) {\n        animation-timing-function: cubic-bezier(1, 0, 0, 1);\n        animation-duration: 1.5s;\n    }\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=function(n){switch(n.index){case 0:return{x:n.size-n.size/4,y:n.y};case 1:return{x:n.x,y:n.y-n.size/2+n.size/8};case 2:return{x:0,y:n.y}}},p=e.SwapSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&a.default.createElement(z,{size:e,sizeUnit:r},function(n){for(var e=n.countBalls,t=n.color,i=n.size,r=n.sizeUnit,o=[],s=0;s<e;s++)o.push(a.default.createElement(m,{color:t,size:i,x:s*(i/4+i/8),y:i/2-i/8,key:s.toString(),index:s,sizeUnit:r}));return o}({countBalls:3,color:t,size:e,sizeUnit:r}))},z=u.default.div.withConfig({displayName:"swap__Wrapper",componentId:"sc-1a8o1b-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+(n.size/2+n.size/8)+n.sizeUnit})),m=u.default.div.withConfig({displayName:"swap__Ball",componentId:"sc-1a8o1b-1"})(o,(function(n){return""+n.y+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.size/4+n.sizeUnit}),(function(n){return""+n.size/4+n.sizeUnit}),(function(n){return n.color}),(function(n){return(0,l.keyframes)(i,n.y,n.x,d(n).y,d(n).x,n.y,n.x)}));p.defaultProps={loading:!0,size:40,color:"#4b4c56",sizeUnit:"px"},p.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.BarsSpinner=void 0;var i=f(["\n    0% {\n        width: ","","\n    }\n    50% {\n        width: ","","\n    }\n    100% {\n        width: ","","\n    }\n"],["\n    0% {\n        width: ","","\n    }\n    50% {\n        width: ","","\n    }\n    100% {\n        width: ","","\n    }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"]),o=f(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    background-color: ",";\n    animation: "," 1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    background-color: ",";\n    animation: "," 1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    animation-delay: ","s;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=e.BarsSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&a.default.createElement(p,{size:e,sizeUnit:r},function(n,e,t,i){for(var r=[],o=0;o<n;o++)r.push(a.default.createElement(z,{color:e,size:t,sizeUnit:i,x:o*(t/5+t/25)-t/12,key:o.toString(),index:o}));return r}(5,t,e,r))},p=u.default.div.withConfig({displayName:"bars__Wrapper",componentId:"sc-1sb659-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),z=u.default.div.withConfig({displayName:"bars__Bar",componentId:"sc-1sb659-1"})(o,(function(n){return""+n.y+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.size/20+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),(function(n){return n.color}),(function(n){return(0,l.keyframes)(i,n.size/20,n.sizeUnit,n.size/6,n.sizeUnit,n.size/20,n.sizeUnit)}),(function(n){return.15*n.index}));d.defaultProps={loading:!0,size:40,color:"#00ff89",sizeUnit:"px"},d.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.WaveSpinner=void 0;var i=f(["\n    25% {\n        transform: skewY(25deg);\n    }\n    50% {\n        height: 100%;\n        margin-top: 0;\n    }\n    75% {\n        transform: skewY(-25deg);\n    }\n"],["\n    25% {\n        transform: skewY(25deg);\n    }\n    50% {\n        height: 100%;\n        margin-top: 0;\n    }\n    75% {\n        transform: skewY(-25deg);\n    }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    overflow: hidden;\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    overflow: hidden;\n"]),o=f(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    margin-top: ",";\n    transform: skewY(0deg);\n    background-color: ",";\n    animation: "," 1.25s ease-in-out infinite;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    margin-top: ",";\n    transform: skewY(0deg);\n    background-color: ",";\n    animation: "," 1.25s ease-in-out infinite;\n    animation-delay: ","s;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=(0,l.keyframes)(i),p=e.WaveSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&a.default.createElement(z,{size:e,sizeUnit:r},function(n){for(var e=n.countBars,t=n.color,i=n.size,r=n.sizeUnit,o=[],s=0;s<e;s++)o.push(a.default.createElement(m,{color:t,size:i,x:s*(i/5+(i/15-i/100)),y:0,key:s.toString(),index:s,sizeUnit:r}));return o}({countBars:10,color:t,size:e,sizeUnit:r}))},z=u.default.div.withConfig({displayName:"wave__Wrapper",componentId:"sc-8a0z7x-0"})(r,(function(n){return""+2.5*n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),m=u.default.div.withConfig({displayName:"wave__Bar",componentId:"sc-8a0z7x-1"})(o,(function(n){return""+(n.y+n.size/10)+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return""+n.size/10+n.sizeUnit}),(function(n){return""+(n.size-n.size/10)+n.sizeUnit}),(function(n){return n.color}),d,(function(n){return.15*n.index}));p.defaultProps={loading:!0,size:30,color:"#fff",sizeUnit:"px"},p.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.PushSpinner=void 0;var i=f(["\n    15% {\n        transform: scaleY(1) translateX(10",");\n    }\n    90% {\n        transform: scaleY(0.05) translateX(-5",");\n    }\n    100%{\n        transform: scaleY(0.05) translateX(-5",");\n    }\n"],["\n    15% {\n        transform: scaleY(1) translateX(10",");\n    }\n    90% {\n        transform: scaleY(0.05) translateX(-5",");\n    }\n    100%{\n        transform: scaleY(0.05) translateX(-5",");\n    }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    overflow: hidden;\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    overflow: hidden;\n"]),o=f(["\n    position: absolute;\n    top: 0;\n    left: ",";\n    width: ",";\n    height: 100%;\n    transform: scaleY(0.05) translateX(-5px);\n    background-color: ",";\n    animation: "," 1.25s ease-in-out infinite;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    top: 0;\n    left: ",";\n    width: ",";\n    height: 100%;\n    transform: scaleY(0.05) translateX(-5px);\n    background-color: ",";\n    animation: "," 1.25s ease-in-out infinite;\n    animation-delay: ","s;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=e.PushSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&a.default.createElement(p,{size:e,sizeUnit:r},function(n){for(var e=n.countBars,t=n.color,i=n.size,r=n.sizeUnit,o=[],s=0;s<e;s++)o.push(a.default.createElement(z,{color:t,size:i,x:s*(i/5+(i/15-i/100)),y:0,key:s.toString(),index:s,sizeUnit:r}));return o}({countBars:10,color:t,size:e,sizeUnit:r}))},p=u.default.div.withConfig({displayName:"push__Wrapper",componentId:"ypksxs-0"})(r,(function(n){return""+2.5*n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),z=u.default.div.withConfig({displayName:"push__Bar",componentId:"ypksxs-1"})(o,(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return n.color}),(function(n){return(0,l.keyframes)(i,n.sizeUnit,n.sizeUnit,n.sizeUnit)}),(function(n){return.15*n.index}));d.defaultProps={loading:!0,size:30,color:"#4b4c56",sizeUnit:"px"},d.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.FireworkSpinner=void 0;var i=c(["\n    0% {\n        opacity: 1;\n        transform: scale(0.1);\n    }\n    25% {\n        opacity: 0.85;\n    }\n    100% {\n        transform: scale(1);\n        opacity: 0;\n    }\n"],["\n    0% {\n        opacity: 1;\n        transform: scale(0.1);\n    }\n    25% {\n        opacity: 0.85;\n    }\n    100% {\n        transform: scale(1);\n        opacity: 0;\n    }\n"]),r=c(["\n    border: "," dotted ",";\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    animation: "," 1.25s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n"],["\n    border: "," dotted ",";\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    animation: "," 1.25s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n"]),o=u(t(0)),a=u(t(1)),s=t(2),l=u(s);function u(n){return n&&n.__esModule?n:{default:n}}function c(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var f=(0,s.keyframes)(i),d=e.FireworkSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&o.default.createElement(p,{size:e,color:t,sizeUnit:r})},p=l.default.div.withConfig({displayName:"firework__Wrapper",componentId:"sc-1bn5a2-0"})(r,(function(n){return""+n.size/10+n.sizeUnit}),(function(n){return n.color}),(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),f);d.defaultProps={loading:!0,size:40,color:"#fff",sizeUnit:"px"},d.propTypes={loading:a.default.bool,size:a.default.number,color:a.default.string,sizeUnit:a.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.StageSpinner=void 0;var i=f(["\n    0% {\n        top: ","",";\n        left: ","",";\n    }\n    25% {\n        top: ","",";\n        left: ","",";\n        opacity: 0;\n    }\n    50% {\n        top: ","",";\n        left: ","",";\n        opacity: 0;\n    }\n    100% {\n        top: ","",";\n        left: ","",";\n        opacity: 1;\n    }\n"],["\n    0% {\n        top: ","",";\n        left: ","",";\n    }\n    25% {\n        top: ","",";\n        left: ","",";\n        opacity: 0;\n    }\n    50% {\n        top: ","",";\n        left: ","",";\n        opacity: 0;\n    }\n    100% {\n        top: ","",";\n        left: ","",";\n        opacity: 1;\n    }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"]),o=f(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    animation: "," 1s ease-in-out infinite;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    animation: "," 1s ease-in-out infinite;\n    animation-delay: ","s;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=e.StageSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&a.default.createElement(p,{size:e,sizeUnit:r},function(n){for(var e=n.countBalls,t=n.color,i=n.size,r=n.sizeUnit,o=[],s=0,l=0;l<e;l++)o.push(a.default.createElement(z,{color:t,size:i,index:l,x:l*(i/2.5),y:i/2-i/10,key:s.toString(),sizeUnit:r})),s++;return o}({countBalls:3,color:t,size:e,sizeUnit:r}))},p=u.default.div.withConfig({displayName:"stage__Wrapper",componentId:"sc-161krao-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),z=u.default.div.withConfig({displayName:"stage__Ball",componentId:"sc-161krao-1"})(o,(function(n){return""+n.y+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return n.color}),(function(n){return(0,l.keyframes)(i,n.y,n.sizeUnit,n.x,n.sizeUnit,n.y,n.sizeUnit,n.x,n.sizeUnit,n.y+n.size/2,n.sizeUnit,n.x,n.sizeUnit,n.y,n.sizeUnit,n.x,n.sizeUnit)}),(function(n){return.2*n.index}));d.defaultProps={loading:!0,size:40,color:"#fff",sizeUnit:"px"},d.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.HeartSpinner=void 0;var i=c(["\n    0% {\n        transform: scale(1);\n    }\n    25% {\n        transform: scale(0.75);\n    }\n    50% {\n        transform: scale(1);\n    }\n    65% {\n        transform: scale(1);\n    }\n    80% {\n        transform: scale(0.75);\n    }\n    100% {\n        transform: scale(1);\n    }\n"],["\n    0% {\n        transform: scale(1);\n    }\n    25% {\n        transform: scale(0.75);\n    }\n    50% {\n        transform: scale(1);\n    }\n    65% {\n        transform: scale(1);\n    }\n    80% {\n        transform: scale(0.75);\n    }\n    100% {\n        transform: scale(1);\n    }\n"]),r=c(["\n    position: relative;\n    width: ",";\n    height: ",";\n    animation: ",' 1s ease-in-out infinite;\n    &::before,\n    &::after {\n        content: "";\n        position: absolute;\n        top: ',";\n        left: ",";\n        width: ",";\n        height: ",";\n        background-color: ",";\n        border-radius: ","\n            "," 0 0;\n        transform: rotate(-45deg);\n        transform-origin: 0 100%;\n    }\n    &::after {\n        left: 0;\n        transform: rotate(45deg);\n        transform-origin: 100% 100%;\n    }\n"],["\n    position: relative;\n    width: ",";\n    height: ",";\n    animation: ",' 1s ease-in-out infinite;\n    &::before,\n    &::after {\n        content: "";\n        position: absolute;\n        top: ',";\n        left: ",";\n        width: ",";\n        height: ",";\n        background-color: ",";\n        border-radius: ","\n            "," 0 0;\n        transform: rotate(-45deg);\n        transform-origin: 0 100%;\n    }\n    &::after {\n        left: 0;\n        transform: rotate(45deg);\n        transform-origin: 100% 100%;\n    }\n"]),o=u(t(0)),a=u(t(1)),s=t(2),l=u(s);function u(n){return n&&n.__esModule?n:{default:n}}function c(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var f=(0,s.keyframes)(i),d=e.HeartSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&o.default.createElement(p,{size:e,color:t,sizeUnit:r})},p=l.default.div.withConfig({displayName:"heart__Wrapper",componentId:"sc-12jb06u-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+(n.size-n.size/10)+n.sizeUnit}),f,(function(n){return""+n.size/20+n.sizeUnit}),(function(n){return""+n.size/2+n.sizeUnit}),(function(n){return""+n.size/2+n.sizeUnit}),(function(n){return""+(n.size-n.size/5)+n.sizeUnit}),(function(n){return n.color}),(function(n){return""+n.size/2+n.sizeUnit}),(function(n){return""+n.size/2+n.sizeUnit}));d.defaultProps={loading:!0,size:40,color:"#fff",sizeUnit:"px"},d.propTypes={loading:a.default.bool,size:a.default.number,color:a.default.string,sizeUnit:a.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.GuardSpinner=void 0;var i=p(["\n    0% {\n        transform: rotateY(90deg);\n    }\n    50% {\n        transform: rotateY(0deg);\n    }\n    100% {\n        transform: rotateY(90deg);\n    }\n"],["\n    0% {\n        transform: rotateY(90deg);\n    }\n    50% {\n        transform: rotateY(0deg);\n    }\n    100% {\n        transform: rotateY(90deg);\n    }\n"]),r=p(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    perspective: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    perspective: ",";\n"]),o=p(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n"]),a=p(["\n    position: relative;\n    width: ",";\n    height: ",";\n    transform-style: preserve-3d;\n    animation: "," 1.5s cubic-bezier(0.23, 1, 0.32, 1) infinite;\n    animation-delay: ","s;\n"],["\n    position: relative;\n    width: ",";\n    height: ",";\n    transform-style: preserve-3d;\n    animation: "," 1.5s cubic-bezier(0.23, 1, 0.32, 1) infinite;\n    animation-delay: ","s;\n"]),s=p(["\n    display: block;\n    position: absolute;\n    width: inherit;\n    height: inherit;\n    background-color: ",";\n    transform: rotateY(","deg)\n        translateZ(",");\n"],["\n    display: block;\n    position: absolute;\n    width: inherit;\n    height: inherit;\n    background-color: ",";\n    transform: rotateY(","deg)\n        translateZ(",");\n"]),l=d(t(0)),u=d(t(1)),c=t(2),f=d(c);function d(n){return n&&n.__esModule?n:{default:n}}function p(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var z=(0,c.keyframes)(i),m=e.GuardSpinner=function(n){var e=n.size,t=n.backColor,i=n.frontColor,r=n.loading,o=n.sizeUnit;return r&&l.default.createElement(g,{size:e,sizeUnit:o},function(n){for(var e=n.countCubesInLine,t=n.backColor,i=n.frontColor,r=n.size,o=n.sizeUnit,a=[],s=0,u=0;u<e;u++)for(var c=0;c<e;c++)a.push(l.default.createElement(h,{size:r,x:u*(r/4+r/8),y:c*(r/4+r/8),key:s.toString(),sizeUnit:o},l.default.createElement(b,{size:r,index:s,sizeUnit:o},l.default.createElement(y,{front:!0,size:r,color:i,sizeUnit:o}),l.default.createElement(y,{left:!0,size:r,color:t,sizeUnit:o})))),s++;return a}({countCubesInLine:3,backColor:t,frontColor:i,size:e,sizeUnit:o}))},g=f.default.div.withConfig({displayName:"guard__Wrapper",componentId:"sc-13axfn9-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+3*n.size+n.sizeUnit})),h=f.default.div.withConfig({displayName:"guard__CubeWrapper",componentId:"sc-13axfn9-1"})(o,(function(n){return""+n.y+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),b=f.default.div.withConfig({displayName:"guard__Cube",componentId:"sc-13axfn9-2"})(a,(function(n){return""+n.size/4+n.sizeUnit}),(function(n){return""+n.size/4+n.sizeUnit}),z,(function(n){return.125*n.index})),y=f.default.div.withConfig({displayName:"guard__Side",componentId:"sc-13axfn9-3"})(s,(function(n){return n.color}),(function(n){return n.front?0:-90}),(function(n){return""+n.size/8+n.sizeUnit}));m.defaultProps={loading:!0,size:40,frontColor:"#00ff89",backColor:"#373846",sizeUnit:"px"},m.propTypes={loading:u.default.bool,size:u.default.number,frontColor:u.default.string,backColor:u.default.string,sizeUnit:u.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.CircleSpinner=void 0;var i=c(["\n    0% {\n        transform: rotate(0);\n    }\n    100% {\n        transform: rotate(360deg);\n    }\n"],["\n    0% {\n        transform: rotate(0);\n    }\n    100% {\n        transform: rotate(360deg);\n    }\n"]),r=c(["\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    border: "," solid ",";\n    border-right-color: transparent;\n    border-radius: 50%;\n    animation: "," 0.75s linear infinite;\n"],["\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    border: "," solid ",";\n    border-right-color: transparent;\n    border-radius: 50%;\n    animation: "," 0.75s linear infinite;\n"]),o=u(t(0)),a=u(t(1)),s=t(2),l=u(s);function u(n){return n&&n.__esModule?n:{default:n}}function c(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var f=(0,s.keyframes)(i),d=e.CircleSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&o.default.createElement(p,{size:e,color:t,sizeUnit:r})},p=l.default.div.withConfig({displayName:"circle__Wrapper",componentId:"sc-16bbsoy-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return n.color}),f);d.defaultProps={loading:!0,size:30,color:"#fff",sizeUnit:"px"},d.propTypes={loading:a.default.bool,size:a.default.number,color:a.default.string,sizeUnit:a.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.SpiralSpinner=void 0;var i=p(["\n    0% {\n        transform: rotateX(0deg);\n    }\n    25% {\n        transform: rotateX(-90deg);\n    }\n    50% {\n        transform: rotateX(-180deg);\n    }\n    75% {\n        transform: rotateX(-270deg);\n    }\n    100% {\n        transform: rotateX(-360deg);\n    }\n"],["\n    0% {\n        transform: rotateX(0deg);\n    }\n    25% {\n        transform: rotateX(-90deg);\n    }\n    50% {\n        transform: rotateX(-180deg);\n    }\n    75% {\n        transform: rotateX(-270deg);\n    }\n    100% {\n        transform: rotateX(-360deg);\n    }\n"]),r=p(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    perspective: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    perspective: ",";\n"]),o=p(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: inherit;\n    height: inherit;\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: inherit;\n    height: inherit;\n"]),a=p(["\n    position: relative;\n    width: ",";\n    height: ",";\n    transform-style: preserve-3d;\n    animation: "," 3s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    animation-delay: ","s;\n"],["\n    position: relative;\n    width: ",";\n    height: ",";\n    transform-style: preserve-3d;\n    animation: "," 3s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    animation-delay: ","s;\n"]),s=p(["\n    backface-visibility: hidden;\n    display: block;\n    position: absolute;\n    width: inherit;\n    height: inherit;\n    background-color: ",";\n    transform: rotateX(","deg) rotateY(","deg)\n        translateZ(",");\n"],["\n    backface-visibility: hidden;\n    display: block;\n    position: absolute;\n    width: inherit;\n    height: inherit;\n    background-color: ",";\n    transform: rotateX(","deg) rotateY(","deg)\n        translateZ(",");\n"]),l=d(t(0)),u=d(t(1)),c=t(2),f=d(c);function d(n){return n&&n.__esModule?n:{default:n}}function p(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var z=(0,c.keyframes)(i),m=e.SpiralSpinner=function(n){var e=n.size,t=n.backColor,i=n.frontColor,r=n.loading,o=n.sizeUnit;return r&&l.default.createElement(g,{size:e,sizeUnit:o},function(n){for(var e=n.countCubesInLine,t=n.backColor,i=n.frontColor,r=n.size,o=n.sizeUnit,a=[],s=0,u=0;u<e;u++)a.push(l.default.createElement(h,{x:u*(r/4),y:0,key:s.toString(),sizeUnit:o},l.default.createElement(b,{size:r,index:s,sizeUnit:o},l.default.createElement(y,{front:!0,size:r,color:i,sizeUnit:o}),l.default.createElement(y,{back:!0,size:r,color:i,sizeUnit:o}),l.default.createElement(y,{bottom:!0,size:r,color:t,sizeUnit:o}),l.default.createElement(y,{top:!0,size:r,color:t,sizeUnit:o})))),s++;return a}({countCubesInLine:4,backColor:t,frontColor:i,size:e,sizeUnit:o}))},g=f.default.div.withConfig({displayName:"spiral__Wrapper",componentId:"sc-1898s0q-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size/4+n.sizeUnit}),(function(n){return""+3*n.size+n.sizeUnit})),h=f.default.div.withConfig({displayName:"spiral__CubeWrapper",componentId:"sc-1898s0q-1"})(o,(function(n){return""+n.y+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit})),b=f.default.div.withConfig({displayName:"spiral__Cube",componentId:"sc-1898s0q-2"})(a,(function(n){return""+n.size/4+n.sizeUnit}),(function(n){return""+n.size/4+n.sizeUnit}),z,(function(n){return.15*n.index})),y=f.default.div.withConfig({displayName:"spiral__Side",componentId:"sc-1898s0q-3"})(s,(function(n){return n.color}),(function(n){return function(n){return n.top?90:n.bottom?-90:0}(n)}),(function(n){return n.back?180:0}),(function(n){return""+n.size/8+n.sizeUnit}));m.defaultProps={loading:!0,size:40,frontColor:"#00ff89",backColor:"#4b4c56",sizeUnit:"px"},m.propTypes={loading:u.default.bool,size:u.default.number,frontColor:u.default.string,backColor:u.default.string,sizeUnit:u.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.PulseSpinner=void 0;var i=f(["\n    0% {\n        opacity: 1;\n    }\n    50% {\n        opacity: 0;\n    }\n    100% {\n        opacity: 1;\n    }\n"],["\n    0% {\n        opacity: 1;\n    }\n    50% {\n        opacity: 0;\n    }\n    100% {\n        opacity: 1;\n    }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"]),o=f(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    background-color: ",";\n    animation: "," 1.5s cubic-bezier(0.895, 0.03, 0.685, 0.22) infinite;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    background-color: ",";\n    animation: "," 1.5s cubic-bezier(0.895, 0.03, 0.685, 0.22) infinite;\n    animation-delay: ","s;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=(0,l.keyframes)(i),p=e.PulseSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&a.default.createElement(z,{size:e,sizeUnit:r},function(n){for(var e=n.countCubeInLine,t=n.color,i=n.size,r=n.sizeUnit,o=[],s=0,l=0;l<e;l++)o.push(a.default.createElement(m,{color:t,size:i,x:l*(i/3+i/15),y:0,key:s.toString(),index:l,sizeUnit:r})),s++;return o}({countCubeInLine:3,color:t,size:e,sizeUnit:r}))},z=u.default.div.withConfig({displayName:"pulse__Wrapper",componentId:"sc-1yr0qmr-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size/2.5+n.sizeUnit})),m=u.default.div.withConfig({displayName:"pulse__Cube",componentId:"sc-1yr0qmr-1"})(o,(function(n){return""+n.y+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return""+n.size/2.5+n.sizeUnit}),(function(n){return n.color}),d,(function(n){return.15*n.index}));p.defaultProps={loading:!0,size:40,color:"#fff",sizeUnit:"px"},p.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.SequenceSpinner=void 0;var i=p(["\n    0% {\n        top: ","",";\n        transform: rotateY(0deg);\n    }\n    30%{\n        top: 0;\n        transform: rotateY(90deg);\n    }\n    100% {\n        transform: rotateY(0deg);\n        top: -","",";\n    }\n"],["\n    0% {\n        top: ","",";\n        transform: rotateY(0deg);\n    }\n    30%{\n        top: 0;\n        transform: rotateY(90deg);\n    }\n    100% {\n        transform: rotateY(0deg);\n        top: -","",";\n    }\n"]),r=p(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    perspective: ",";\n    overflow: hidden;\n    transform: rotateY(20deg);\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    perspective: ",";\n    overflow: hidden;\n    transform: rotateY(20deg);\n"]),o=p(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: inherit;\n    height: inherit;\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: inherit;\n    height: inherit;\n"]),a=p(["\n    position: relative;\n    width: ",";\n    height: ",";\n    transform-style: preserve-3d;\n    animation: "," 1.75s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    animation-delay: ","s;\n"],["\n    position: relative;\n    width: ",";\n    height: ",";\n    transform-style: preserve-3d;\n    animation: "," 1.75s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    animation-delay: ","s;\n"]),s=p(["\n    backface-visibility: hidden;\n    display: block;\n    position: absolute;\n    width: inherit;\n    height: inherit;\n    background-color: ",";\n    transform: rotateY(","deg)\n        translateZ(",");\n"],["\n    backface-visibility: hidden;\n    display: block;\n    position: absolute;\n    width: inherit;\n    height: inherit;\n    background-color: ",";\n    transform: rotateY(","deg)\n        translateZ(",");\n"]),l=d(t(0)),u=d(t(1)),c=t(2),f=d(c);function d(n){return n&&n.__esModule?n:{default:n}}function p(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var z=e.SequenceSpinner=function(n){var e=n.size,t=n.backColor,i=n.frontColor,r=n.loading,o=n.sizeUnit;return r&&l.default.createElement(m,{size:e,sizeUnit:o},function(n){for(var e=n.countCubesInLine,t=n.backColor,i=n.frontColor,r=n.size,o=n.sizeUnit,a=[],s=0,u=0;u<e;u++)a.push(l.default.createElement(g,{x:u*(r/8+r/11),y:0,key:s.toString(),sizeUnit:o},l.default.createElement(h,{size:r,index:s,sizeUnit:o},l.default.createElement(b,{front:!0,size:r,color:i,sizeUnit:o}),l.default.createElement(b,{left:!0,size:r,color:t,sizeUnit:o})))),s++;return a}({countCubesInLine:5,backColor:t,frontColor:i,size:e,sizeUnit:o}))},m=f.default.div.withConfig({displayName:"sequence__Wrapper",componentId:"sc-61fmm1-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size/1.75+n.sizeUnit}),(function(n){return""+3*n.size+n.sizeUnit})),g=f.default.div.withConfig({displayName:"sequence__CubeWrapper",componentId:"sc-61fmm1-1"})(o,(function(n){return""+n.y+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit})),h=f.default.div.withConfig({displayName:"sequence__Cube",componentId:"sc-61fmm1-2"})(a,(function(n){return""+n.size/8+n.sizeUnit}),(function(n){return""+n.size/1.75+n.sizeUnit}),(function(n){return(0,c.keyframes)(i,n.size,n.sizeUnit,n.size,n.sizeUnit)}),(function(n){return.1*n.index})),b=f.default.div.withConfig({displayName:"sequence__Side",componentId:"sc-61fmm1-3"})(s,(function(n){return n.color}),(function(n){return n.front?0:-90}),(function(n){return""+n.size/16+n.sizeUnit}));z.defaultProps={loading:!0,size:40,frontColor:"#4b4c56",backColor:"#00ff89",sizeUnit:"px"},z.propTypes={loading:u.default.bool,size:u.default.number,frontColor:u.default.string,backColor:u.default.string,sizeUnit:u.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.DominoSpinner=void 0;var i=f(["\n    0% {\n        transform: translateX(","",") rotate(0deg);\n        opacity: 0;\n    }\n    14.28% {\n        transform: translateX(","",") rotate(0deg);\n        opacity: 1;\n    }\n    28.56% {\n        transform: translateX(","",") rotate(0deg);\n        opacity: 1;\n    }\n    37.12% {\n        transform: translateX(","",") rotate(0deg);\n        opacity: 1;\n    }\n    42.84% {\n        transform: translateX(","",") rotate(10deg);\n        opacity: 1;\n    }\n    57.12% {\n        transform: translateX(","",") rotate(40deg);\n        opacity: 1;\n    }\n    71.4% {\n        transform: translateX(","",") rotate(62deg);\n        opacity: 1;\n    }\n    85.68% {\n        transform: translateX(","",") rotate(72deg);\n        opacity: 1;\n    }\n    100% {\n        transform: translateX(","",") rotate(74deg);\n        opacity: 0;\n    }\n"],["\n    0% {\n        transform: translateX(","",") rotate(0deg);\n        opacity: 0;\n    }\n    14.28% {\n        transform: translateX(","",") rotate(0deg);\n        opacity: 1;\n    }\n    28.56% {\n        transform: translateX(","",") rotate(0deg);\n        opacity: 1;\n    }\n    37.12% {\n        transform: translateX(","",") rotate(0deg);\n        opacity: 1;\n    }\n    42.84% {\n        transform: translateX(","",") rotate(10deg);\n        opacity: 1;\n    }\n    57.12% {\n        transform: translateX(","",") rotate(40deg);\n        opacity: 1;\n    }\n    71.4% {\n        transform: translateX(","",") rotate(62deg);\n        opacity: 1;\n    }\n    85.68% {\n        transform: translateX(","",") rotate(72deg);\n        opacity: 1;\n    }\n    100% {\n        transform: translateX(","",") rotate(74deg);\n        opacity: 0;\n    }\n"]),r=f(["\n    position: relative;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    width: ",";\n    height: ",";\n"]),o=f(["\n    position: absolute;\n    right: 0;\n    width: ",";\n    height: ",";\n    transform-origin: 50% 100%;\n    background-color: ",";\n    animation: "," 3s linear infinite;\n    animation-delay: ","s;\n    transform: translateX(",")\n        rotate(","deg);\n    &:nth-child(1) {\n        opacity: 0.22;\n    }\n"],["\n    position: absolute;\n    right: 0;\n    width: ",";\n    height: ",";\n    transform-origin: 50% 100%;\n    background-color: ",";\n    animation: "," 3s linear infinite;\n    animation-delay: ","s;\n    transform: translateX(",")\n        rotate(","deg);\n    &:nth-child(1) {\n        opacity: 0.22;\n    }\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=e.DominoSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit,o=function(n,e){for(var t=[],i=0;i<=n+1;i++)t.push(e/8*-i);return t}(7,e);return i&&a.default.createElement(p,{size:e,sizeUnit:r},function(n){for(var e=n.countBars,t=n.rotatesPoints,i=n.translatesPoints,r=n.color,o=n.size,s=n.sizeUnit,l=[],u=0;u<e;u++)l.push(a.default.createElement(z,{color:r,size:o,translatesPoints:i,rotate:t[u],key:u.toString(),index:u,sizeUnit:s}));return l}({countBars:7,rotatesPoints:[0,0,0,10,40,60,70],translatesPoints:o,color:t,size:e,sizeUnit:r}))},p=u.default.div.withConfig({displayName:"domino__Wrapper",componentId:"sc-81zu9-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit})),z=u.default.div.withConfig({displayName:"domino__Bar",componentId:"sc-81zu9-1"})(o,(function(n){return""+n.size/30+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return n.color}),(function(n){return(0,l.keyframes)(i,n.translatesPoints[0],n.sizeUnit,n.translatesPoints[1],n.sizeUnit,n.translatesPoints[2],n.sizeUnit,n.translatesPoints[3],n.sizeUnit,n.translatesPoints[4],n.sizeUnit,n.translatesPoints[5],n.sizeUnit,n.translatesPoints[6],n.sizeUnit,n.translatesPoints[7],n.sizeUnit,n.translatesPoints[8],n.sizeUnit)}),(function(n){return-.42*n.index}),(function(n){return""+(n.size-15*n.index)+n.sizeUnit}),(function(n){return n.rotate}));d.defaultProps={loading:!0,size:100,color:"#4b4c56",sizeUnit:"px"},d.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.ImpulseSpinner=void 0;var i=f(["\n    0% {\n        background: ",";\n        transform: scale(1);\n        animation-timing-function: cubic-bezier(1,0,0.7,1);\n    }\n    40% {\n        background: ",";\n        transform: scale(1.5);\n        animation-timing-function: cubic-bezier(0.3,0,0,1);\n    }\n    72.5% {\n        background:",";\n        transform: scale(1);\n        animation-timing-function: linear;\n    }\n    100% {\n        background: ",";\n        transform: scale(1);\n    }\n"],["\n    0% {\n        background: ",";\n        transform: scale(1);\n        animation-timing-function: cubic-bezier(1,0,0.7,1);\n    }\n    40% {\n        background: ",";\n        transform: scale(1.5);\n        animation-timing-function: cubic-bezier(0.3,0,0,1);\n    }\n    72.5% {\n        background:",";\n        transform: scale(1);\n        animation-timing-function: linear;\n    }\n    100% {\n        background: ",";\n        transform: scale(1);\n    }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"]),o=f(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    animation: "," 1.25s linear infinite;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    animation: "," 1.25s linear infinite;\n    animation-delay: ","s;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=e.ImpulseSpinner=function(n){var e=n.size,t=n.frontColor,i=n.backColor,r=n.loading,o=n.sizeUnit;return r&&a.default.createElement(p,{size:e,sizeUnit:o},function(n){for(var e=n.countBalls,t=n.frontColor,i=n.backColor,r=n.size,o=n.sizeUnit,s=[],l=0;l<e;l++)s.push(a.default.createElement(z,{frontColor:t,backColor:i,size:r,x:l*(r/5+r/5),y:0,key:l.toString(),index:l,sizeUnit:o}));return s}({countBalls:3,frontColor:t,backColor:i,size:e,sizeUnit:o}))},p=u.default.div.withConfig({displayName:"impulse__Wrapper",componentId:"sc-1eafdhu-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit})),z=u.default.div.withConfig({displayName:"impulse__Ball",componentId:"sc-1eafdhu-1"})(o,(function(n){return""+n.y+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return n.frontColor}),(function(n){return(0,l.keyframes)(i,n.backColor,n.frontColor,n.backColor,n.backColor)}),(function(n){return.125*n.index}));d.defaultProps={loading:!0,size:40,frontColor:"#00ff89",backColor:"#4b4c56",sizeUnit:"px"},d.propTypes={loading:s.default.bool,size:s.default.number,frontColor:s.default.string,backColor:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.CubeSpinner=void 0;var i=p(["\n    from { transform: rotateX(0) rotateY(0); }\n    to   { transform: rotateX(360deg) rotateY(360deg); }\n"],["\n    from { transform: rotateX(0) rotateY(0); }\n    to   { transform: rotateX(360deg) rotateY(360deg); }\n"]),r=p(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    perspective: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    perspective: ",";\n"]),o=p(["\n    sposition: absolute;\n    top: ",";\n    left: ",";\n    width: inherit;\n    height: inherit;\n"],["\n    sposition: absolute;\n    top: ",";\n    left: ",";\n    width: inherit;\n    height: inherit;\n"]),a=p(["\n    position: relative;\n    width: ",";\n    height: ",";\n    transform-style: preserve-3d;\n    animation: "," 3s cubic-bezier(0.68, -0.55, 0.265, 1.55) infinite;\n"],["\n    position: relative;\n    width: ",";\n    height: ",";\n    transform-style: preserve-3d;\n    animation: "," 3s cubic-bezier(0.68, -0.55, 0.265, 1.55) infinite;\n"]),s=p(["\n    backface-visibility: hidden;\n    display: block;\n    position: absolute;\n    width: inherit;\n    height: inherit;\n    background-color: ",";\n    transform: rotateX(","deg) rotateY(","deg)\n        translateZ(",");\n"],["\n    backface-visibility: hidden;\n    display: block;\n    position: absolute;\n    width: inherit;\n    height: inherit;\n    background-color: ",";\n    transform: rotateX(","deg) rotateY(","deg)\n        translateZ(",");\n"]),l=d(t(0)),u=d(t(1)),c=t(2),f=d(c);function d(n){return n&&n.__esModule?n:{default:n}}function p(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var z=(0,c.keyframes)(i),m=e.CubeSpinner=function(n){var e=n.size,t=n.backColor,i=n.frontColor,r=n.loading,o=n.sizeUnit;return r&&l.default.createElement(g,{size:e,sizeUnit:o},l.default.createElement(h,{x:0,y:0,sizeUnit:o},l.default.createElement(b,{size:e,sizeUnit:o},l.default.createElement(y,{front:!0,size:e,color:i,sizeUnit:o}),l.default.createElement(y,{back:!0,size:e,color:i,sizeUnit:o}),l.default.createElement(y,{bottom:!0,size:e,color:t,sizeUnit:o}),l.default.createElement(y,{top:!0,size:e,color:t,sizeUnit:o}),l.default.createElement(y,{left:!0,size:e,color:t,sizeUnit:o}),l.default.createElement(y,{right:!0,size:e,color:t,sizeUnit:o}))))},g=f.default.div.withConfig({displayName:"cube__Wrapper",componentId:"sc-1iks05k-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+4*n.size+n.sizeUnit})),h=f.default.div.withConfig({displayName:"cube__CubeWrapper",componentId:"sc-1iks05k-1"})(o,(function(n){return""+n.y+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit})),b=f.default.div.withConfig({displayName:"cube__Cube",componentId:"sc-1iks05k-2"})(a,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),z),y=f.default.div.withConfig({displayName:"cube__Side",componentId:"sc-1iks05k-3"})(s,(function(n){return n.color}),(function(n){return function(n){return n.top?90:n.bottom?-90:0}(n)}),(function(n){return function(n){return n.left?90:n.right?-90:n.back?180:0}(n)}),(function(n){return""+n.size/2+n.sizeUnit}));m.defaultProps={loading:!0,size:25,frontColor:"#00ff89",backColor:"#4b4c56",sizeUnit:"px"},m.propTypes={loading:u.default.bool,size:u.default.number,frontColor:u.default.string,backColor:u.default.string,sizeUnit:u.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.FillSpinner=void 0;var i=d(["\n    0% {\n        transform: rotate(0deg);\n    }\n    25% {\n        transform: rotate(180deg);\n    }\n    50% {\n        transform: rotate(180deg);\n    }\n    75% {\n        transform: rotate(360deg);\n    }\n    100% {\n        transform: rotate(360deg);\n    }\n"],["\n    0% {\n        transform: rotate(0deg);\n    }\n    25% {\n        transform: rotate(180deg);\n    }\n    50% {\n        transform: rotate(180deg);\n    }\n    75% {\n        transform: rotate(360deg);\n    }\n    100% {\n        transform: rotate(360deg);\n    }\n"]),r=d(["\n    0% {\n        height: 0%;\n    }\n    25% {\n        height: 0%;\n    }\n    50% {\n        height: 100%;\n    }\n    75% {\n        height: 100%;\n    }\n    100% {\n        height: 0%;\n    }\n"],["\n    0% {\n        height: 0%;\n    }\n    25% {\n        height: 0%;\n    }\n    50% {\n        height: 100%;\n    }\n    75% {\n        height: 100%;\n    }\n    100% {\n        height: 0%;\n    }\n"]),o=d(["\n    display: flex;\n    align-items: flex-end;\n    justify-content: center;\n    width: ",";\n    height: ",";\n    border: "," solid ",";\n    animation: "," 3s cubic-bezier(0.77, 0, 0.175, 1) infinite;\n"],["\n    display: flex;\n    align-items: flex-end;\n    justify-content: center;\n    width: ",";\n    height: ",";\n    border: "," solid ",";\n    animation: "," 3s cubic-bezier(0.77, 0, 0.175, 1) infinite;\n"]),a=d(["\n    width: 100%;\n    background-color: ",";\n    animation: "," 3s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n"],["\n    width: 100%;\n    background-color: ",";\n    animation: "," 3s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n"]),s=f(t(0)),l=f(t(1)),u=t(2),c=f(u);function f(n){return n&&n.__esModule?n:{default:n}}function d(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var p=(0,u.keyframes)(i),z=(0,u.keyframes)(r),m=e.FillSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&s.default.createElement(g,{size:e,color:t,sizeUnit:r},s.default.createElement(h,{color:t,size:e,sizeUnit:r}))},g=c.default.div.withConfig({displayName:"fill__Wrapper",componentId:"ehv7z4-0"})(o,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size/8+n.sizeUnit}),(function(n){return n.color}),p),h=c.default.div.withConfig({displayName:"fill__Plane",componentId:"ehv7z4-1"})(a,(function(n){return n.color}),z);m.defaultProps={loading:!0,size:20,color:"#4b4c56",sizeUnit:"px"},m.propTypes={loading:l.default.bool,size:l.default.number,color:l.default.string,sizeUnit:l.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.SphereSpinner=void 0;var i=d(["\n    to{\n        transform: rotate(360deg);\n    }\n"],["\n    to{\n        transform: rotate(360deg);\n    }\n"]),r=d(["\n    0% {\n        transform: translateX(","",") translateY(","",") scale(1) ;\n    }\n    5% {\n        transform: translateX(-","",") translateY(-","",") scale(0);\n    }\n    50% {\n        transform: translateX(-","",") translateY(-","",") scale(0);\n    }\n    55% {\n        transform: translateX(","",") translateY(","",") scale(1) ;\n    }\n"],["\n    0% {\n        transform: translateX(","",") translateY(","",") scale(1) ;\n    }\n    5% {\n        transform: translateX(-","",") translateY(-","",") scale(0);\n    }\n    50% {\n        transform: translateX(-","",") translateY(-","",") scale(0);\n    }\n    55% {\n        transform: translateX(","",") translateY(","",") scale(1) ;\n    }\n"]),o=d(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    animation: "," 8s linear infinite;\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    animation: "," 8s linear infinite;\n"]),a=d(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    transform: translateX(",")\n        translateY(",");\n    animation: "," 5s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    transform: translateX(",")\n        translateY(",");\n    animation: "," 5s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    animation-delay: ","s;\n"]),s=f(t(0)),l=f(t(1)),u=t(2),c=f(u);function f(n){return n&&n.__esModule?n:{default:n}}function d(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var p=(0,u.keyframes)(i),z=e.SphereSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit,o=e/2,a=e/5;return i&&s.default.createElement(m,{size:e,sizeUnit:r},function(n){for(var e=n.countBalls,t=n.radius,i=n.angle,r=n.color,o=n.size,a=n.ballSize,l=n.sizeUnit,u=[],c=a/2,f=0;f<e;f++){var d=Math.sin(i*f*(Math.PI/180))*t-c,p=Math.cos(i*f*(Math.PI/180))*t-c;u.push(s.default.createElement(g,{color:r,ballSize:a,size:o,x:d,y:p,key:f.toString(),index:f,sizeUnit:l}))}return u}({countBalls:7,radius:o,angle:360/7,color:t,size:e,ballSize:a,sizeUnit:r}))},m=c.default.div.withConfig({displayName:"sphere__Wrapper",componentId:"sc-1t5xu9p-0"})(o,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),p),g=c.default.div.withConfig({displayName:"sphere__Ball",componentId:"sc-1t5xu9p-1"})(a,(function(n){return""+n.size/2+n.sizeUnit}),(function(n){return""+n.size/2+n.sizeUnit}),(function(n){return""+n.ballSize+n.sizeUnit}),(function(n){return""+n.ballSize+n.sizeUnit}),(function(n){return n.color}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.y+n.sizeUnit}),(function(n){return function(n){return(0,u.keyframes)(r,n.x,n.sizeUnit,n.y,n.sizeUnit,n.size/12,n.sizeUnit,n.size/12,n.sizeUnit,n.size/12,n.sizeUnit,n.size/12,n.sizeUnit,n.x,n.sizeUnit,n.y,n.sizeUnit)}(n)}),(function(n){return.3*n.index}));z.defaultProps={loading:!0,size:30,color:"#fff",sizeUnit:"px"},z.propTypes={loading:l.default.bool,size:l.default.number,color:l.default.string,sizeUnit:l.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.FlagSpinner=void 0;var i=d(["\n    0% {\n        transform: translateY(0);\n        opacity: 1;\n    }\n    50% {\n        transform: translateY(","",");\n        opacity: 0.25;\n    }\n    100% {\n        transform: translateY(0);\n        opacity: 1;\n    }\n"],["\n    0% {\n        transform: translateY(0);\n        opacity: 1;\n    }\n    50% {\n        transform: translateY(","",");\n        opacity: 0.25;\n    }\n    100% {\n        transform: translateY(0);\n        opacity: 1;\n    }\n"]),r=d(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"]),o=d(["\n    position: absolute;\n    left: 0;\n    top: 0;\n    animation: "," 1.5s cubic-bezier(0.86, 0, 0.07, 1) infinite;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    left: 0;\n    top: 0;\n    animation: "," 1.5s cubic-bezier(0.86, 0, 0.07, 1) infinite;\n    animation-delay: ","s;\n"]),a=d(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    background-color: ",";\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    background-color: ",";\n"]),s=f(t(0)),l=f(t(1)),u=t(2),c=f(u);function f(n){return n&&n.__esModule?n:{default:n}}function d(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var p=e.FlagSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&s.default.createElement(z,{size:e,sizeUnit:r},function(n){for(var e=n.countPlaneInLine,t=n.color,i=n.size,r=n.sizeUnit,o=[],a=[],l=0,u=0;u<e;u++){for(var c=0;c<e;c++)a.push(s.default.createElement(g,{color:t,size:i,x:u*(i/6+i/9),y:c*(i/6+i/9)+i/10,key:u+c.toString(),index:l,sizeUnit:r})),l++;o.push(s.default.createElement(m,{index:l,key:l.toString(),size:i,sizeUnit:r},[].concat(a))),a.length=0}return o}({countPlaneInLine:4,color:t,size:e,sizeUnit:r}))},z=c.default.div.withConfig({displayName:"flag__Wrapper",componentId:"sc-3eh05c-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),m=c.default.div.withConfig({displayName:"flag__Line",componentId:"sc-3eh05c-1"})(o,(function(n){return(0,u.keyframes)(i,-n.size/5,n.sizeUnit)}),(function(n){return.05*n.index})),g=c.default.div.withConfig({displayName:"flag__Plane",componentId:"sc-3eh05c-2"})(a,(function(n){return""+n.y+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.size/6+n.sizeUnit}),(function(n){return""+n.size/6+n.sizeUnit}),(function(n){return n.color}));p.defaultProps={loading:!0,size:40,color:"#fff",sizeUnit:"px"},p.propTypes={loading:l.default.bool,size:l.default.number,color:l.default.string,sizeUnit:l.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.ClapSpinner=void 0;var i=d(["\n    50% {\n        transform: rotate(180deg) scale(1.125);\n    }\n    100%{\n        transform: rotate(360deg);\n    }\n"],["\n    50% {\n        transform: rotate(180deg) scale(1.125);\n    }\n    100%{\n        transform: rotate(360deg);\n    }\n"]),r=d(["\n    0% {\n        transform: translateX(","",") translateY(","",") scale(1.25) ;\n    }\n    5% {\n        transform: translateX(","",") translateY(","",") scale(1.75);\n    }\n    50% {\n        transform: translateX(","",") translateY(","",") scale(.25);\n    }\n    55% {\n        background-color: ",";\n        transform: translateX(","",") translateY(","",") scale(1) ;\n    }\n"],["\n    0% {\n        transform: translateX(","",") translateY(","",") scale(1.25) ;\n    }\n    5% {\n        transform: translateX(","",") translateY(","",") scale(1.75);\n    }\n    50% {\n        transform: translateX(","",") translateY(","",") scale(.25);\n    }\n    55% {\n        background-color: ",";\n        transform: translateX(","",") translateY(","",") scale(1) ;\n    }\n"]),o=d(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    animation: "," 1.5s linear infinite;\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    animation: "," 1.5s linear infinite;\n"]),a=d(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    transform: translateX(",")\n        translateY(",");\n    animation: "," 2.5s cubic-bezier(0.075, 0.82, 0.165, 1) infinite;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    transform: translateX(",")\n        translateY(",");\n    animation: "," 2.5s cubic-bezier(0.075, 0.82, 0.165, 1) infinite;\n    animation-delay: ","s;\n"]),s=f(t(0)),l=f(t(1)),u=t(2),c=f(u);function f(n){return n&&n.__esModule?n:{default:n}}function d(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var p=(0,u.keyframes)(i),z=e.ClapSpinner=function(n){var e=n.size,t=n.frontColor,i=n.backColor,r=n.loading,o=n.sizeUnit,a=e/2,l=e/5;return r&&s.default.createElement(m,{size:e,sizeUnit:o},function(n){for(var e=n.countBalls,t=n.radius,i=n.angle,r=n.frontColor,o=n.backColor,a=n.size,l=n.ballSize,u=n.sizeUnit,c=[],f=l/2,d=0;d<e;d++){var p=Math.sin(i*d*(Math.PI/180))*t-f,z=Math.cos(i*d*(Math.PI/180))*t-f;c.push(s.default.createElement(g,{frontColor:r,backColor:o,ballSize:l,size:a,sizeUnit:u,x:p,y:z,key:d.toString(),index:d}))}return c}({countBalls:7,radius:a,angle:360/7,frontColor:t,backColor:i,size:e,ballSize:l,sizeUnit:o}))},m=c.default.div.withConfig({displayName:"clap__Wrapper",componentId:"sc-12p9tb5-0"})(o,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),p),g=c.default.div.withConfig({displayName:"clap__Ball",componentId:"sc-12p9tb5-1"})(a,(function(n){return""+n.size/2+n.sizeUnit}),(function(n){return""+n.size/2+n.sizeUnit}),(function(n){return""+n.ballSize+n.sizeUnit}),(function(n){return""+n.ballSize+n.sizeUnit}),(function(n){return n.frontColor}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.y+n.sizeUnit}),(function(n){return function(n){return(0,u.keyframes)(r,n.x,n.sizeUnit,n.y,n.sizeUnit,n.x,n.sizeUnit,n.y,n.sizeUnit,n.x,n.sizeUnit,n.y,n.sizeUnit,n.backColor,n.x,n.sizeUnit,n.y,n.sizeUnit)}(n)}),(function(n){return.2*n.index}));z.defaultProps={loading:!0,size:30,frontColor:"#00ff89",backColor:"#4b4c56",sizeUnit:"px"},z.propTypes={loading:l.default.bool,size:l.default.number,frontColor:l.default.string,backColor:l.default.string,sizeUnit:l.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.RotateSpinner=void 0;var i=f(["\n    0% {\n        transform: rotate(0deg);\n    }\n    100% { \n        transform: rotate(1440deg); \n        opacity: 0.05;\n    }\n"],["\n    0% {\n        transform: rotate(0deg);\n    }\n    100% { \n        transform: rotate(1440deg); \n        opacity: 0.05;\n    }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"]),o=f(["\n    position: absolute;\n    left: 50%;\n    top: 0%;\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    transform: translateX(-50%) translateY(100%);\n    transform-origin: 0 250% 0;\n    animation: "," 4s both infinite;\n    animation-timing-function: cubic-bezier(0.5, ",", 0.9, 0.9);\n"],["\n    position: absolute;\n    left: 50%;\n    top: 0%;\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    transform: translateX(-50%) translateY(100%);\n    transform-origin: 0 250% 0;\n    animation: "," 4s both infinite;\n    animation-timing-function: cubic-bezier(0.5, ",", 0.9, 0.9);\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=(0,l.keyframes)(i),p=e.RotateSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit,o=e/2,s=e/5;return i&&a.default.createElement(z,{size:e,sizeUnit:r},function(n){for(var e=n.countBalls,t=n.radius,i=n.angle,r=n.color,o=n.size,s=n.ballSize,l=n.sizeUnit,u=[],c=s/2,f=0;f<e;f++){var d=Math.sin(i*f*(Math.PI/180))*t-c,p=Math.cos(i*f*(Math.PI/180))*t-c;u.push(a.default.createElement(m,{color:r,ballSize:s,size:o,x:d,y:p,key:f.toString(),index:f,sizeUnit:l}))}return u}({countBalls:8,radius:o,angle:45,color:t,size:e,ballSize:s,sizeUnit:r}))},z=u.default.div.withConfig({displayName:"rotate__Wrapper",componentId:"sc-1b62bh9-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),m=u.default.div.withConfig({displayName:"rotate__Ball",componentId:"sc-1b62bh9-1"})(o,(function(n){return""+n.ballSize+n.sizeUnit}),(function(n){return""+n.ballSize+n.sizeUnit}),(function(n){return n.color}),d,(function(n){return.3*n.index}));p.defaultProps={loading:!0,size:45,color:"#00ff89",sizeUnit:"px"},p.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.SwishSpinner=void 0;var i=f(["\n    50% {\n        transform: scale(0);\n        background-color: ",";\n    }\n"],["\n    50% {\n        transform: scale(0);\n        background-color: ",";\n    }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"]),o=f(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 3px;\n    background-color: ",";\n    animation: "," 0.9s ease infinite;\n    transition: all 0.3s ease;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 3px;\n    background-color: ",";\n    animation: "," 0.9s ease infinite;\n    transition: all 0.3s ease;\n    animation-delay: ","s;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=e.SwishSpinner=function(n){var e=n.size,t=n.frontColor,i=n.backColor,r=n.loading,o=n.sizeUnit;return r&&a.default.createElement(p,{size:e,sizeUnit:o},function(n){for(var e=n.countBallsInLine,t=n.frontColor,i=n.backColor,r=n.size,o=n.sizeUnit,s=[],l=0,u=0;u<e;u++)for(var c=0;c<e;c++)s.push(a.default.createElement(z,{frontColor:t,backColor:i,size:r,x:u*(r/3+r/15),y:c*(r/3+r/15),key:l.toString(),index:l,sizeUnit:o})),l++;return s}({countBallsInLine:3,frontColor:t,backColor:i,size:e,sizeUnit:o}))},p=u.default.div.withConfig({displayName:"swish__Wrapper",componentId:"e0szto-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),z=u.default.div.withConfig({displayName:"swish__Ball",componentId:"e0szto-1"})(o,(function(n){return""+n.y+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return n.frontColor}),(function(n){return(0,l.keyframes)(i,n.backColor)}),(function(n){return.1*n.index}));d.defaultProps={loading:!0,size:40,frontColor:"#4b4c56",backColor:"#fff",sizeUnit:"px"},d.propTypes={loading:s.default.bool,size:s.default.number,frontColor:s.default.string,backColor:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.GooSpinner=void 0;var i=p(["\n    0% {\n        transform: rotate(0deg);\n    }\n    100% {\n        transform: rotate(360deg);\n    }\n"],["\n    0% {\n        transform: rotate(0deg);\n    }\n    100% {\n        transform: rotate(360deg);\n    }\n"]),r=p(["\n    0%{\n        transform: translateY(0) scale(1);\n    }\n    50%{\n        transform: translateY(","",") scale(0.8);\n    }\n    100%{\n        transform: translateY(0) scale(1);\n    }\n"],["\n    0%{\n        transform: translateY(0) scale(1);\n    }\n    50%{\n        transform: translateY(","",") scale(0.8);\n    }\n    100%{\n        transform: translateY(0) scale(1);\n    }\n"]),o=p(["\n    width: ",";\n    height: ",';\n    filter: url("#goo");\n'],["\n    width: ",";\n    height: ",';\n    filter: url("#goo");\n']),a=p(["\n    position: relative;\n    width: ",";\n    height: ",";\n    animation: "," 1.7s linear infinite;\n"],["\n    position: relative;\n    width: ",";\n    height: ",";\n    animation: "," 1.7s linear infinite;\n"]),s=p(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    animation: "," 1.5s ease-in-out infinite;\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    animation: "," 1.5s ease-in-out infinite;\n"]),l=d(t(0)),u=d(t(1)),c=t(2),f=d(c);function d(n){return n&&n.__esModule?n:{default:n}}function p(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var z=e.GooSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&l.default.createElement(m,{size:e,sizeUnit:r},l.default.createElement(g,{size:e,sizeUnit:r},function(n){for(var e=n.countBalls,t=n.color,i=n.size,r=n.sizeUnit,o=[],a=i/4,s=[-a,a],u=0;u<e;u++)o.push(l.default.createElement(h,{color:t,size:i,x:i/2-i/6,y:i/2-i/6,key:u.toString(),translateTo:s[u],index:u,sizeUnit:r}));return o}({countBalls:2,color:t,size:e,sizeUnit:r})),l.default.createElement("svg",{xmlns:"http://www.w3.org/2000/svg",version:"1.1"},l.default.createElement("defs",null,l.default.createElement("filter",{id:"goo"},l.default.createElement("feGaussianBlur",{in:"SourceGraphic",stdDeviation:"6",result:"blur"}),l.default.createElement("feColorMatrix",{in:"blur",mode:"matrix",values:"1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 15 -5",result:"goo"}),l.default.createElement("feBlend",{in:"SourceGraphic",in2:"goo"})))))},m=f.default.div.withConfig({displayName:"goo__Wrapper",componentId:"sc-12keask-0"})(o,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),g=f.default.div.withConfig({displayName:"goo__BallsWrapper",componentId:"sc-12keask-1"})(a,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),(function(){return(0,c.keyframes)(i)})),h=f.default.div.withConfig({displayName:"goo__Ball",componentId:"sc-12keask-2"})(s,(function(n){return""+n.y+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.size/3+n.sizeUnit}),(function(n){return""+n.size/3+n.sizeUnit}),(function(n){return n.color}),(function(n){return(0,c.keyframes)(r,n.translateTo,n.sizeUnit)}));z.defaultProps={loading:!0,size:55,color:"#fff",sizeUnit:"px"},z.propTypes={loading:u.default.bool,size:u.default.number,color:u.default.string,sizeUnit:u.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.CombSpinner=void 0;var i=f(["\n    to {\n        transform: rotate(450deg);\n    }\n"],["\n    to {\n        transform: rotate(450deg);\n    }\n"]),r=f(["\n    position: relative;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    width: ",";\n    height: ",";\n"]),o=f(["\n    position: absolute;\n    left: 0;\n    width: ",";\n    height: ",";\n    left: ",";\n    transform-origin: center bottom;\n    transform: rotate(-90deg);\n    background-color: ",";\n    animation: "," 3s ease infinite;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    left: 0;\n    width: ",";\n    height: ",";\n    left: ",";\n    transform-origin: center bottom;\n    transform: rotate(-90deg);\n    background-color: ",";\n    animation: "," 3s ease infinite;\n    animation-delay: ","s;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=e.CombSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit,o=e/9;return i&&a.default.createElement(p,{size:e,sizeUnit:r},function(n){for(var e=n.countBars,t=n.color,i=n.size,r=n.sizeUnit,o=[],s=0;s<e;s++)o.push(a.default.createElement(z,{color:t,size:i,key:s.toString(),sizeUnit:r,index:s}));return o}({countBars:o,color:t,size:e,sizeUnit:r}))},p=u.default.div.withConfig({displayName:"comb__Wrapper",componentId:"x9t4ur-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit})),z=u.default.div.withConfig({displayName:"comb__Bar",componentId:"x9t4ur-1"})(o,(function(n){return""+n.size/60+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return""+9*n.index+n.sizeUnit}),(function(n){return n.color}),(function(){return(0,l.keyframes)(i)}),(function(n){return.05*n.index}));d.defaultProps={loading:!0,size:100,color:"#fff",sizeUnit:"px"},d.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.PongSpinner=void 0;var i=p(["\n    0% {\n        top: ","",";\n    }\n    50% {\n        top: ","",";\n    }\n    100% {\n        top: ","",";\n    }\n"],["\n    0% {\n        top: ","",";\n    }\n    50% {\n        top: ","",";\n    }\n    100% {\n        top: ","",";\n    }\n"]),r=p(["\n    0% {\n        top: ","",";\n        left: ","",";\n    }\n    25% {\n        top: ","",";\n        left: ","","; \n    }\n    50% {\n        top: ","",";\n        left: ","","; \n    }\n    75% {\n        top: ","",";\n        left: ","",";\n    }\n    100% {\n        top: ","",";\n        left: ","","; \n    }\n"],["\n    0% {\n        top: ","",";\n        left: ","",";\n    }\n    25% {\n        top: ","",";\n        left: ","","; \n    }\n    50% {\n        top: ","",";\n        left: ","","; \n    }\n    75% {\n        top: ","",";\n        left: ","",";\n    }\n    100% {\n        top: ","",";\n        left: ","","; \n    }\n"]),o=p(["\n    position: relative;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    width: ",";\n    height: ",";\n"]),a=p(["\n    position: absolute;\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    animation: "," 2s linear infinite;\n"],["\n    position: absolute;\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    animation: "," 2s linear infinite;\n"]),s=p(["\n    position: absolute;\n    width: ",";\n    height: ",";\n    background-color: ",";\n    left: ",";\n    right: ",";\n    border-radius: 4px;\n    animation: "," 2s linear infinite;\n"],["\n    position: absolute;\n    width: ",";\n    height: ",";\n    background-color: ",";\n    left: ",";\n    right: ",";\n    border-radius: 4px;\n    animation: "," 2s linear infinite;\n"]),l=d(t(0)),u=d(t(1)),c=t(2),f=d(c);function d(n){return n&&n.__esModule?n:{default:n}}function p(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var z=e.PongSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&l.default.createElement(m,{size:e,sizeUnit:r},l.default.createElement(h,{left:!0,color:t,size:e,sizeUnit:r}),l.default.createElement(g,{color:t,size:e,sizeUnit:r}),l.default.createElement(h,{right:!0,color:t,size:e,sizeUnit:r}))},m=f.default.div.withConfig({displayName:"pong__Wrapper",componentId:"sc-1lbqo08-0"})(o,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size/1.75+n.sizeUnit})),g=f.default.div.withConfig({displayName:"pong__Ball",componentId:"sc-1lbqo08-1"})(a,(function(n){return""+n.size/8+n.sizeUnit}),(function(n){return""+n.size/8+n.sizeUnit}),(function(n){return n.color}),(function(n){return function(n){return(0,c.keyframes)(r,n.size/3.5-n.size/8,n.sizeUnit,n.size/12,n.sizeUnit,n.size/3.5,n.sizeUnit,n.size-n.size/8,n.sizeUnit,n.size/1.75-n.size/8,n.sizeUnit,n.size/12,n.sizeUnit,n.size/3.5,n.sizeUnit,n.size-n.size/8,n.sizeUnit,n.size/3.5-n.size/8,n.sizeUnit,n.size/12,n.sizeUnit)}(n)})),h=f.default.div.withConfig({displayName:"pong__Player",componentId:"sc-1lbqo08-2"})(s,(function(n){return""+n.size/12+n.sizeUnit}),(function(n){return""+n.size/3+n.sizeUnit}),(function(n){return n.color}),(function(n){return n.left?0:n.size}),(function(n){return n.right?0:n.size}),(function(n){return function(n){return(0,c.keyframes)(i,n.left?0:n.size/3.5,n.sizeUnit,n.left?n.size/3.5:0,n.sizeUnit,n.left?0:n.size/3.5,n.sizeUnit)}(n)}));z.defaultProps={loading:!0,size:60,color:"#4b4c56",sizeUnit:"px"},z.propTypes={loading:u.default.bool,size:u.default.number,color:u.default.string,sizeUnit:u.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.RainbowSpinner=void 0;var i=f(["\n    0% {\n        border-width: 10","; \n    }\n    25% {\n        border-width: 3","; \n    }\n    50% {\n        transform: rotate(115deg); \n        border-width: 10",";\n    }\n    75% {\n        border-width: 3",";\n    }\n    100% {\n        border-width: 10",";\n    }\n"],["\n    0% {\n        border-width: 10","; \n    }\n    25% {\n        border-width: 3","; \n    }\n    50% {\n        transform: rotate(115deg); \n        border-width: 10",";\n    }\n    75% {\n        border-width: 3",";\n    }\n    100% {\n        border-width: 10",";\n    }\n"]),r=f(["\n    width: ",";\n    height: ",";\n    overflow: hidden;\n"],["\n    width: ",";\n    height: ",";\n    overflow: hidden;\n"]),o=f(["\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    border-style: solid;\n    border-top-color: ",";\n    border-right-color: ",";\n    border-left-color: transparent;\n    border-bottom-color: transparent;\n    box-sizing: border-box;\n    transform: rotate(-200deg);\n    animation: "," 3s ease-in-out infinite;\n"],["\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    border-style: solid;\n    border-top-color: ",";\n    border-right-color: ",";\n    border-left-color: transparent;\n    border-bottom-color: transparent;\n    box-sizing: border-box;\n    transform: rotate(-200deg);\n    animation: "," 3s ease-in-out infinite;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=e.RainbowSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&a.default.createElement(p,{size:e,sizeUnit:r},a.default.createElement(z,{size:e,color:t,sizeUnit:r}))},p=u.default.div.withConfig({displayName:"rainbow__Wrapper",componentId:"sc-1ugdhww-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size/2+n.sizeUnit})),z=u.default.div.withConfig({displayName:"rainbow__Line",componentId:"sc-1ugdhww-1"})(o,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),(function(n){return n.color}),(function(n){return n.color}),(function(n){return(0,l.keyframes)(i,n.sizeUnit,n.sizeUnit,n.sizeUnit,n.sizeUnit,n.sizeUnit)}));d.defaultProps={loading:!0,size:50,color:"#fff",sizeUnit:"px"},d.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.RingSpinner=void 0;var i=d(["\n    0% { \n        box-shadow: inset 0 0 0 ",""," ",";\n        opacity: 1;\n    }\n    50%, 100% {\n        box-shadow: inset 0 0 0 0 ",";\n        opacity: 0;\n    }\n"],["\n    0% { \n        box-shadow: inset 0 0 0 ",""," ",";\n        opacity: 1;\n    }\n    50%, 100% {\n        box-shadow: inset 0 0 0 0 ",";\n        opacity: 0;\n    }\n"]),r=d(["\n    0%, 50% { \n        box-shadow: inset 0 0 0 0 ",";\n        opacity: 0;\n    }\n    100% { \n        box-shadow: inset 0 0 0 ",""," ",";\n        opacity: 1;\n    }\n"],["\n    0%, 50% { \n        box-shadow: inset 0 0 0 0 ",";\n        opacity: 0;\n    }\n    100% { \n        box-shadow: inset 0 0 0 ",""," ",";\n        opacity: 1;\n    }\n"]),o=d(["\n    width: ",";\n    height: ",";\n"],["\n    width: ",";\n    height: ",";\n"]),a=d(['\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: 100%;\n    max-width: 100%;\n    height: 100%;\n    &:before,\n    &:after {\n        width: 100%;\n        height: 100%;\n        content: "";\n        position: absolute;\n        border-radius: 50%;\n        animation-duration: 2s;\n        animation-iteration-count: infinite;\n        animation-timing-function: ease-in-out;\n    }\n    &:before {\n        box-shadow: ',";\n        animation-name: ",";\n    }\n    &:after {\n        box-shadow: 0 0 0 0 ",";\n        animation-name: ",";\n    }\n"],['\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: 100%;\n    max-width: 100%;\n    height: 100%;\n    &:before,\n    &:after {\n        width: 100%;\n        height: 100%;\n        content: "";\n        position: absolute;\n        border-radius: 50%;\n        animation-duration: 2s;\n        animation-iteration-count: infinite;\n        animation-timing-function: ease-in-out;\n    }\n    &:before {\n        box-shadow: ',";\n        animation-name: ",";\n    }\n    &:after {\n        box-shadow: 0 0 0 0 ",";\n        animation-name: ",";\n    }\n"]),s=f(t(0)),l=f(t(1)),u=t(2),c=f(u);function f(n){return n&&n.__esModule?n:{default:n}}function d(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var p=e.RingSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&s.default.createElement(z,{size:e,sizeUnit:r},s.default.createElement(m,{size:e,color:t,sizeUnit:r}))},z=c.default.div.withConfig({displayName:"ring__Wrapper",componentId:"sc-1ki0q4s-0"})(o,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),m=c.default.div.withConfig({displayName:"ring__Circle",componentId:"sc-1ki0q4s-1"})(a,(function(n){return"inset 0 0 0 "+n.size/10+n.sizeUnit+" "+n.color}),(function(n){return(0,u.keyframes)(i,n.size/10,n.sizeUnit,n.color,n.color)}),(function(n){return n.color}),(function(n){return(0,u.keyframes)(r,n.color,n.size/10,n.sizeUnit,n.color)}));p.defaultProps={loading:!0,size:30,color:"#00ff89",sizeUnit:"px"},p.propTypes={loading:l.default.bool,size:l.default.number,color:l.default.string,sizeUnit:l.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.HoopSpinner=void 0;var i=f(["\n    0% {\n        opacity: 0;\n        border-color: ",";\n        transform: ",";\n    }\n    40% {\n        opacity: 1;\n        transform: rotateX(0deg) rotateY(20deg) translateZ(0) scale(1);\n    }\n    100% {\n        opacity: 0;\n        transform: ",";\n    }\n"],["\n    0% {\n        opacity: 0;\n        border-color: ",";\n        transform: ",";\n    }\n    40% {\n        opacity: 1;\n        transform: rotateX(0deg) rotateY(20deg) translateZ(0) scale(1);\n    }\n    100% {\n        opacity: 0;\n        transform: ",";\n    }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    perspective: 600px;\n    transform-style: perserve-3d;\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    perspective: 600px;\n    transform-style: perserve-3d;\n"]),o=f(["\n    position: absolute;\n    width: ",";\n    height: ",";\n    border: "," solid ",";\n    border-radius: 50%;\n    background-color: transparent;\n    transform-style: perserve-3d;\n    transform: scale(0) rotateX(60deg);\n    opacity: ",";\n    animation: "," 3s cubic-bezier(0.67, 0.08, 0.46, 1.5) infinite;\n    animation-delay: ","ms;\n"],["\n    position: absolute;\n    width: ",";\n    height: ",";\n    border: "," solid ",";\n    border-radius: 50%;\n    background-color: transparent;\n    transform-style: perserve-3d;\n    transform: scale(0) rotateX(60deg);\n    opacity: ",";\n    animation: "," 3s cubic-bezier(0.67, 0.08, 0.46, 1.5) infinite;\n    animation-delay: ","ms;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=e.HoopSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&a.default.createElement(p,{size:e,sizeUnit:r},function(n){for(var e=n.countBallsInLine,t=n.color,i=n.size,r=n.sizeUnit,o=[],s=0,l=0;l<e;l++)o.push(a.default.createElement(z,{color:t,size:i,key:s.toString(),index:l,sizeUnit:r})),s++;return o}({countBallsInLine:6,color:t,size:e,sizeUnit:r}))},p=u.default.div.withConfig({displayName:"hoop__Wrapper",componentId:"sc-6dao9o-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),z=u.default.div.withConfig({displayName:"hoop__Ball",componentId:"sc-6dao9o-1"})(o,(function(n){return""+n.size/1.5+n.sizeUnit}),(function(n){return""+n.size/1.5+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return n.color}),(function(n){return 1-.2*n.index}),(function(n){return(0,l.keyframes)(i,n.color,"rotateX(65deg) rotateY(45deg) translateZ(-"+1.5*n.size+n.sizeUnit+") scale(0.1)","rotateX(65deg) rotateY(-45deg) translateZ(-"+1.5*n.size+n.sizeUnit+") scale(0.1)")}),(function(n){return 200*n.index}));d.defaultProps={loading:!0,size:45,color:"#4b4c56",sizeUnit:"px"},d.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.FlapperSpinner=void 0;var i=f(["\n    100% {\n        opacity: 0;\n        transform: translateX(","",")\n        translateY(","",") scale(1);\n   }\n"],["\n    100% {\n        opacity: 0;\n        transform: translateX(","",")\n        translateY(","",") scale(1);\n   }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"]),o=f(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    transform: translateX(",")\n        translateY(",") scale(0);\n    animation: "," 0.8s linear infinite;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: ",";\n    transform: translateX(",")\n        translateY(",") scale(0);\n    animation: "," 0.8s linear infinite;\n    animation-delay: ","s;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=e.FlapperSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit,o=e/2,s=e/1.5;return i&&a.default.createElement(p,{size:e,sizeUnit:r},function(n){for(var e=n.countBalls,t=n.radius,i=n.angle,r=n.color,o=n.size,s=n.ballSize,l=n.sizeUnit,u=[],c=s/2,f=0;f<e;f++){var d=Math.sin(i*f*(Math.PI/180))*t-c,p=Math.cos(i*f*(Math.PI/180))*t-c;u.push(a.default.createElement(z,{color:r,ballSize:s,size:o,x:d,y:p,key:f.toString(),index:f,sizeUnit:l}))}return u}({countBalls:7,radius:o,angle:360/7,color:t,size:e,ballSize:s,sizeUnit:r}))},p=u.default.div.withConfig({displayName:"flapper__Wrapper",componentId:"kzbmuk-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),z=u.default.div.withConfig({displayName:"flapper__Ball",componentId:"kzbmuk-1"})(o,(function(n){return""+n.size/2+n.sizeUnit}),(function(n){return""+n.size/2+n.sizeUnit}),(function(n){return""+n.ballSize+n.sizeUnit}),(function(n){return""+n.ballSize+n.sizeUnit}),(function(n){return n.color}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.y+n.sizeUnit}),(function(n){return function(n){return(0,l.keyframes)(i,n.x,n.sizeUnit,n.y,n.sizeUnit)}(n)}),(function(n){return.1*n.index}));d.defaultProps={loading:!0,size:30,color:"#00ff89",sizeUnit:"px"},d.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.MagicSpinner=void 0;var i=f(["\n    100% {\n        transform: translateX(-50%) translateY(-50%) rotate(360deg);\n    }\n"],["\n    100% {\n        transform: translateX(-50%) translateY(-50%) rotate(360deg);\n    }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    overflow: hidden;\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    overflow: hidden;\n"]),o=f(["\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    transform: translateX(-50%) translateY(-50%) rotate(0deg);\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: transparent;\n    border: 2px solid transparent;\n    border-top-color: ",";\n    animation: "," 2s cubic-bezier(0.68, -0.75, 0.265, 1.75) infinite forwards;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    transform: translateX(-50%) translateY(-50%) rotate(0deg);\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: transparent;\n    border: 2px solid transparent;\n    border-top-color: ",";\n    animation: "," 2s cubic-bezier(0.68, -0.75, 0.265, 1.75) infinite forwards;\n    animation-delay: ","s;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=e.MagicSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit,o=e/12;return i&&a.default.createElement(p,{size:e,sizeUnit:r},function(n){for(var e=n.countBalls,t=n.color,i=n.size,r=n.sizeUnit,o=[],s=0;s<e;s++)o.push(a.default.createElement(z,{color:t,countBalls:e,size:i,key:s.toString(),index:s,sizeUnit:r}));return o}({countBalls:o,color:t,size:e,sizeUnit:r}))},p=u.default.div.withConfig({displayName:"magic__Wrapper",componentId:"dtlj8d-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),z=u.default.div.withConfig({displayName:"magic__Ball",componentId:"dtlj8d-1"})(o,(function(n){return""+n.index*(n.size/n.countBalls)+n.sizeUnit}),(function(n){return""+n.index*(n.size/n.countBalls)+n.sizeUnit}),(function(n){return n.color}),(function(){return(0,l.keyframes)(i)}),(function(n){return.05*n.index}));d.defaultProps={loading:!0,size:70,color:"#fff",sizeUnit:"px"},d.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.JellyfishSpinner=void 0;var i=f(["\n    0% {\n        transform: ",";\n    }\n    50% {\n        transform: ",";\n    }\n    100% {\n        transform: ",";\n    }\n"],["\n    0% {\n        transform: ",";\n    }\n    50% {\n        transform: ",";\n    }\n    100% {\n        transform: ",";\n    }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"]),o=f(["\n    position: absolute;\n    width: ",";\n    height: ",";\n    border: 2px solid ",";\n    border-radius: 50%;\n    background-color: transparent;\n    animation: "," 2.5s ease infinite;\n    animation-delay: ","ms;\n"],["\n    position: absolute;\n    width: ",";\n    height: ",";\n    border: 2px solid ",";\n    border-radius: 50%;\n    background-color: transparent;\n    animation: "," 2.5s ease infinite;\n    animation-delay: ","ms;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=e.JellyfishSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit;return i&&a.default.createElement(p,{size:e,sizeUnit:r},function(n){for(var e=n.countBalls,t=n.color,i=n.size,r=n.sizeUnit,o=[],s=0,l=0;l<e;l++)o.push(a.default.createElement(z,{color:t,size:i,countRings:e,key:s.toString(),index:l,sizeUnit:r})),s++;return o}({countBalls:6,color:t,size:e,sizeUnit:r}))},p=u.default.div.withConfig({displayName:"jellyfish__Wrapper",componentId:"qquojd-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),z=u.default.div.withConfig({displayName:"jellyfish__Ring",componentId:"qquojd-1"})(o,(function(n){return""+n.index*(n.size/n.countRings)+n.sizeUnit}),(function(n){return""+n.index*(n.size/n.countRings)/2+n.sizeUnit}),(function(n){return n.color}),(function(n){return(0,l.keyframes)(i,"translateY("+-n.size/5+n.sizeUnit+");","translateY("+n.size/4+n.sizeUnit+")","translateY("+-n.size/5+n.sizeUnit+")")}),(function(n){return 100*n.index}));d.defaultProps={loading:!0,size:60,color:"#4b4c56",sizeUnit:"px"},d.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.TraceSpinner=void 0;var i=p(["\n    0% {\n        border: ",""," solid ",";\n    }\n    25% {\n        border: ",""," solid ",";\n    }\n    50% {\n        border: ",""," solid ",";\n    }\n    100% {\n        border: ",""," solid ",";\n    }\n"],["\n    0% {\n        border: ",""," solid ",";\n    }\n    25% {\n        border: ",""," solid ",";\n    }\n    50% {\n        border: ",""," solid ",";\n    }\n    100% {\n        border: ",""," solid ",";\n    }\n"]),r=p(["\n    25% {\n        transform: translate(","",", 0);\n    }\n    50% {\n        transform: translate(","",", ","",");\n    }\n    75% {\n        transform: translate(0, ","",");\n    }\n    100% {\n        transform: translate(0, 0);\n    }\n"],["\n    25% {\n        transform: translate(","",", 0);\n    }\n    50% {\n        transform: translate(","",", ","",");\n    }\n    75% {\n        transform: translate(0, ","",");\n    }\n    100% {\n        transform: translate(0, 0);\n    }\n"]),o=p(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    transform: rotate(45deg);\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    transform: rotate(45deg);\n"]),a=p(["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: transparent;\n    border: "," solid ",";\n    animation: "," 4s cubic-bezier(0.75, 0, 0.5, 1) infinite normal;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    top: ",";\n    left: ",";\n    width: ",";\n    height: ",";\n    border-radius: 50%;\n    background-color: transparent;\n    border: "," solid ",";\n    animation: "," 4s cubic-bezier(0.75, 0, 0.5, 1) infinite normal;\n    animation-delay: ","s;\n"]),s=p(["\n    top: 0;\n    left: 0;\n    border-color: ",";\n    background-color: ",";\n    animation: "," 4s cubic-bezier(0.75, 0, 0.5, 1) infinite;\n    z-index: 10;\n"],["\n    top: 0;\n    left: 0;\n    border-color: ",";\n    background-color: ",";\n    animation: "," 4s cubic-bezier(0.75, 0, 0.5, 1) infinite;\n    z-index: 10;\n"]),l=d(t(0)),u=d(t(1)),c=t(2),f=d(c);function d(n){return n&&n.__esModule?n:{default:n}}function p(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var z=e.TraceSpinner=function(n){var e=n.size,t=n.frontColor,i=n.backColor,r=n.loading,o=n.sizeUnit;return r&&l.default.createElement(m,{size:e,sizeUnit:o},function(n){for(var e=n.countBalls,t=n.frontColor,i=n.backColor,r=n.size,o=n.sizeUnit,a=[],s=[0,1,3,2],u=0,c=0;c<e/2;c++)for(var f=0;f<e/2;f++)a.push(l.default.createElement(g,{frontColor:t,backColor:i,size:r,x:f*(r/2+r/10),y:c*(r/2+r/10),key:s[u].toString(),index:s[u],sizeUnit:o})),u++;return a}({countBalls:4,frontColor:t,backColor:i,size:e,sizeUnit:o}),l.default.createElement(h,{frontColor:t,size:e,sizeUnit:o}))},m=f.default.div.withConfig({displayName:"trace__Wrapper",componentId:"k17r81-0"})(o,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),g=f.default.div.withConfig({displayName:"trace__Ball",componentId:"k17r81-1"})(a,(function(n){return""+n.y+n.sizeUnit}),(function(n){return""+n.x+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return""+n.size/10+n.sizeUnit}),(function(n){return n.backColor}),(function(n){return(0,c.keyframes)(i,n.size/10,n.sizeUnit,n.backColor,n.size/10,n.sizeUnit,n.frontColor,n.size/10,n.sizeUnit,n.backColor,n.size/10,n.sizeUnit,n.backColor)}),(function(n){return 1*n.index})),h=(0,f.default)(g)(s,(function(n){return n.frontColor}),(function(n){return n.frontColor}),(function(n){return(0,c.keyframes)(r,n.size/2+n.size/10,n.sizeUnit,n.size/2+n.size/10,n.sizeUnit,n.size/2+n.size/10,n.sizeUnit,n.size/2+n.size/10,n.sizeUnit)}));z.defaultProps={loading:!0,size:35,frontColor:"#00ff89",backColor:"#4b4c56",sizeUnit:"px"},z.propTypes={loading:u.default.bool,size:u.default.number,frontColor:u.default.string,backColor:u.default.string,sizeUnit:u.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.ClassicSpinner=void 0;var i=f(["\n    0% {\n        opacity: 1\n    }\n    100% { \n        opacity: 0;\n    }\n"],["\n    0% {\n        opacity: 1\n    }\n    100% { \n        opacity: 0;\n    }\n"]),r=f(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n"]),o=f(["\n    position: absolute;\n    width: ",";\n    height: ",";\n    background-color: ",";\n    opacity: 0.05;\n    transform: ","\n        ",";\n    animation: "," ","s linear infinite;\n    animation-delay: ","s;\n"],["\n    position: absolute;\n    width: ",";\n    height: ",";\n    background-color: ",";\n    opacity: 0.05;\n    transform: ","\n        ",";\n    animation: "," ","s linear infinite;\n    animation-delay: ","s;\n"]),a=c(t(0)),s=c(t(1)),l=t(2),u=c(l);function c(n){return n&&n.__esModule?n:{default:n}}function f(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var d=(0,l.keyframes)(i),p=e.ClassicSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit,o=e/2;return i&&a.default.createElement(z,{size:e,sizeUnit:r},function(n){for(var e=n.countBars,t=n.color,i=n.size,r=n.barSize,o=n.sizeUnit,s=[],l=0;l<e;l++){var u=360/e*l,c=-i/2;s.push(a.default.createElement(m,{countBars:e,color:t,barSize:r,size:i,rotate:u,translate:c,key:l.toString(),index:l,sizeUnit:o}))}return s}({countBars:16,radius:o,color:t,size:e,sizeUnit:r}))},z=u.default.div.withConfig({displayName:"classic__Wrapper",componentId:"sc-1ycp7u6-0"})(r,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit})),m=u.default.div.withConfig({displayName:"classic__Bar",componentId:"sc-1ycp7u6-1"})(o,(function(n){return""+n.size/10+n.sizeUnit}),(function(n){return""+n.size/4+n.sizeUnit}),(function(n){return n.color}),(function(n){return"rotate("+n.rotate+"deg)"}),(function(n){return"translate(0, "+n.translate+n.sizeUnit+")"}),d,(function(n){return.06*n.countBars}),(function(n){return.06*n.index}));p.defaultProps={loading:!0,size:30,color:"#fff",sizeUnit:"px"},p.propTypes={loading:s.default.bool,size:s.default.number,color:s.default.string,sizeUnit:s.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.WhisperSpinner=void 0;var i=d(["\n    0% {\n        transform: scale(1, 1);\n        opacity: 1;\n        background-color: ",";\n    }\n    100% {\n        transform: scale(0, 0);\n        opacity: 0;\n        background-color: ",";\n    }\n"],["\n    0% {\n        transform: scale(1, 1);\n        opacity: 1;\n        background-color: ",";\n    }\n    100% {\n        transform: scale(0, 0);\n        opacity: 0;\n        background-color: ",";\n    }\n"]),r=d(["\n    0% {\n        transform: rotate(0deg);\n    }\n    25% {\n        transform: rotate(90deg);\n    }\n    50% {\n        transform: rotate(180deg);\n    }\n    75% {\n        transform: rotate(270deg);\n    }\n    100% {\n        transform: rotate(360deg);\n    }\n"],["\n    0% {\n        transform: rotate(0deg);\n    }\n    25% {\n        transform: rotate(90deg);\n    }\n    50% {\n        transform: rotate(180deg);\n    }\n    75% {\n        transform: rotate(270deg);\n    }\n    100% {\n        transform: rotate(360deg);\n    }\n"]),o=d(["\n    position: relative;\n    width: ",";\n    height: ",";\n    animation: "," 10s infinite;\n"],["\n    position: relative;\n    width: ",";\n    height: ",";\n    animation: "," 10s infinite;\n"]),a=d(["\n    float: left;\n    clear: right;\n    margin: ",";\n    width: ",";\n    height: ",";\n    border-radius: 2px;\n    background-color: ",";\n    animation-name: ",";\n    animation-direction: alternate;\n    animation-duration: 800ms;\n    animation-iteration-count: infinite;\n    &:nth-child(1) {\n        animation-delay: 200ms;\n    }\n    &:nth-child(2) {\n        animation-delay: 400ms;\n    }\n    &:nth-child(3) {\n        animation-delay: 600ms;\n    }\n    &:nth-child(4) {\n        animation-delay: 400ms;\n    }\n    &:nth-child(5) {\n        animation-delay: 600ms;\n    }\n    &:nth-child(6) {\n        animation-delay: 800ms;\n    }\n    &:nth-child(7) {\n        animation-delay: 600ms;\n    }\n    &:nth-child(8) {\n        animation-delay: 800ms;\n    }\n    &:nth-child(9) {\n        animation-delay: 1s;\n    }\n"],["\n    float: left;\n    clear: right;\n    margin: ",";\n    width: ",";\n    height: ",";\n    border-radius: 2px;\n    background-color: ",";\n    animation-name: ",";\n    animation-direction: alternate;\n    animation-duration: 800ms;\n    animation-iteration-count: infinite;\n    &:nth-child(1) {\n        animation-delay: 200ms;\n    }\n    &:nth-child(2) {\n        animation-delay: 400ms;\n    }\n    &:nth-child(3) {\n        animation-delay: 600ms;\n    }\n    &:nth-child(4) {\n        animation-delay: 400ms;\n    }\n    &:nth-child(5) {\n        animation-delay: 600ms;\n    }\n    &:nth-child(6) {\n        animation-delay: 800ms;\n    }\n    &:nth-child(7) {\n        animation-delay: 600ms;\n    }\n    &:nth-child(8) {\n        animation-delay: 800ms;\n    }\n    &:nth-child(9) {\n        animation-delay: 1s;\n    }\n"]),s=f(t(0)),l=f(t(1)),u=t(2),c=f(u);function f(n){return n&&n.__esModule?n:{default:n}}function d(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var p=e.WhisperSpinner=function(n){var e=n.size,t=n.frontColor,i=n.backColor,r=n.loading,o=n.sizeUnit;return r&&s.default.createElement(z,{size:e,sizeUnit:o},function(n){for(var e=n.countBallsInLine,t=n.frontColor,i=n.backColor,r=n.size,o=n.sizeUnit,a=[],l=0,u=0;u<e;u++)for(var c=0;c<e;c++)a.push(s.default.createElement(m,{frontColor:t,backColor:i,size:r,key:l.toString(),index:l,sizeUnit:o})),l++;return a}({countBallsInLine:3,frontColor:t,backColor:i,size:e,sizeUnit:o}))},z=c.default.div.withConfig({displayName:"whisper__Wrapper",componentId:"k8uff3-0"})(o,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),(function(){return(0,u.keyframes)(r)})),m=c.default.div.withConfig({displayName:"whisper__Ball",componentId:"k8uff3-1"})(a,(function(n){return""+n.size/15+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return""+n.size/5+n.sizeUnit}),(function(n){return n.frontColor}),(function(n){return(0,u.keyframes)(i,n.backColor,n.frontColor)}));p.defaultProps={loading:!0,size:50,frontColor:"#4b4c56",backColor:"#00ff89",sizeUnit:"px"},p.propTypes={loading:l.default.bool,size:l.default.number,frontColor:l.default.string,backColor:l.default.string,sizeUnit:l.default.string}},function(n,e,t){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.MetroSpinner=void 0;var i=d(["\n    0% {\n        transform: rotate(0deg);\n    }\n    100%{\n        transform: rotate(-360deg);\n    }\n"],["\n    0% {\n        transform: rotate(0deg);\n    }\n    100%{\n        transform: rotate(-360deg);\n    }\n"]),r=d(["\n    ","% {\n        opacity: 0;\n    }\n    ","% {\n        opacity: 1;\n        transform: ",";\n    }\n    ","% {\n        transform: ",";\n    }\n    ","% {\n        transform: ",";\n    }\n    100% {\n        transform: ",";\n        opacity: 1;\n    }\n"],["\n    ","% {\n        opacity: 0;\n    }\n    ","% {\n        opacity: 1;\n        transform: ",";\n    }\n    ","% {\n        transform: ",";\n    }\n    ","% {\n        transform: ",";\n    }\n    100% {\n        transform: ",";\n        opacity: 1;\n    }\n"]),o=d(["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    animation: "," 3s infinite ease-in;\n"],["\n    position: relative;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: ",";\n    height: ",";\n    animation: "," 3s infinite ease-in;\n"]),a=d(["\n    position: absolute;\n    width: ",";\n    height: ",";\n    animation: "," 2s infinite linear;\n    transform: ",';\n    opacity: 0;\n    &:before {\n        content: "";\n        position: absolute;\n        left: 50%;\n        top: 0%;\n        width: ',";\n        height: ",";\n        background-color: ",";\n        transform: translateX(-50%);\n        border-radius: 50%;\n    }\n"],["\n    position: absolute;\n    width: ",";\n    height: ",";\n    animation: "," 2s infinite linear;\n    transform: ",';\n    opacity: 0;\n    &:before {\n        content: "";\n        position: absolute;\n        left: 50%;\n        top: 0%;\n        width: ',";\n        height: ",";\n        background-color: ",";\n        transform: translateX(-50%);\n        border-radius: 50%;\n    }\n"]),s=f(t(0)),l=f(t(1)),u=t(2),c=f(u);function f(n){return n&&n.__esModule?n:{default:n}}function d(n,e){return Object.freeze(Object.defineProperties(n,{raw:{value:Object.freeze(e)}}))}var p=(0,u.keyframes)(i),z=e.MetroSpinner=function(n){var e=n.size,t=n.color,i=n.loading,r=n.sizeUnit,o=e/2,a=e/8;return i&&s.default.createElement(m,{size:e,sizeUnit:r},function(n){for(var e=n.countBalls,t=n.radius,i=n.angle,r=n.color,o=n.size,a=n.ballSize,l=n.sizeUnit,u=[],c=a/2,f=0;f<e;f++){var d=Math.sin(i*f*(Math.PI/180))*t-c,p=Math.cos(i*f*(Math.PI/180))*t-c;u.push(s.default.createElement(g,{countBalls:e,color:r,ballSize:a,size:o,sizeUnit:l,x:d,y:p,key:f.toString(),index:f+1}))}return u}({countBalls:9,radius:o,angle:40,color:t,size:e,ballSize:a,sizeUnit:r}))},m=c.default.div.withConfig({displayName:"metro__Wrapper",componentId:"sc-2iqssn-0"})(o,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),p),g=c.default.div.withConfig({displayName:"metro__Ball",componentId:"sc-2iqssn-1"})(a,(function(n){return""+n.size+n.sizeUnit}),(function(n){return""+n.size+n.sizeUnit}),(function(n){return(0,u.keyframes)(r,n.size/2/n.countBalls*(n.index-1)/n.size*100,(n.size/2/n.countBalls+1e-4)*(n.index-1)/n.size*100,"rotate("+(0-360/n.countBalls*(n.index-2))+"deg)",(n.size/2/n.countBalls*(n.index-0)+2)/n.size*100,"rotate("+(0-360/n.countBalls*(n.index-1))+"deg)",(n.size/2+n.size/2/n.countBalls*(n.index-0)+2)/n.size*100,"rotate("+(0-360/n.countBalls*(n.index-1))+"deg)","rotate("+(0-360/n.countBalls*(n.countBalls-1))+"deg)")}),(function(n){return"rotate("+360/n.countBalls*n.index+"deg)"}),(function(n){return""+n.ballSize+n.sizeUnit}),(function(n){return""+n.ballSize+n.sizeUnit}),(function(n){return""+n.color}));z.defaultProps={loading:!0,size:40,color:"#fff",sizeUnit:"px"},z.propTypes={loading:l.default.bool,size:l.default.number,color:l.default.string,sizeUnit:l.default.string}}]));

/***/ }),

/***/ "./node_modules/react/index.js":
/*!*******************************************************************************************!*\
  !*** delegated ./node_modules/react/index.js from dll-reference dll_5f137288facb1107b491 ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(/*! dll-reference dll_5f137288facb1107b491 */ "dll-reference dll_5f137288facb1107b491"))("./node_modules/react/index.js");

/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return Promise.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return Promise.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new Promise(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList) {
    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList)
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : undefined
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  Function("r", "regeneratorRuntime = r")(runtime);
}


/***/ }),

/***/ "./node_modules/string-hash/index.js":
/*!*******************************************!*\
  !*** ./node_modules/string-hash/index.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function hash(str) {
  var hash = 5381,
      i    = str.length;

  while(i) {
    hash = (hash * 33) ^ str.charCodeAt(--i);
  }

  /* JavaScript does bitwise operations (like XOR, above) on 32-bit signed
   * integers. Since we want the results to be always positive, convert the
   * signed int to an unsigned by doing an unsigned bitshift. */
  return hash >>> 0;
}

module.exports = hash;


/***/ }),

/***/ "./node_modules/styled-components/dist/styled-components.browser.esm.js":
/*!******************************************************************************!*\
  !*** ./node_modules/styled-components/dist/styled-components.browser.esm.js ***!
  \******************************************************************************/
/*! exports provided: default, createGlobalStyle, css, isStyledComponent, keyframes, ServerStyleSheet, StyleSheetConsumer, StyleSheetContext, StyleSheetManager, ThemeConsumer, ThemeContext, ThemeProvider, withTheme, __DO_NOT_USE_OR_YOU_WILL_BE_HAUNTED_BY_SPOOKY_GHOSTS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createGlobalStyle", function() { return createGlobalStyle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "css", function() { return css; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isStyledComponent", function() { return isStyledComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "keyframes", function() { return keyframes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServerStyleSheet", function() { return ServerStyleSheet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StyleSheetConsumer", function() { return StyleSheetConsumer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StyleSheetContext", function() { return StyleSheetContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StyleSheetManager", function() { return StyleSheetManager; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThemeConsumer", function() { return ThemeConsumer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThemeContext", function() { return ThemeContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThemeProvider", function() { return ThemeProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "withTheme", function() { return withTheme; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__DO_NOT_USE_OR_YOU_WILL_BE_HAUNTED_BY_SPOOKY_GHOSTS", function() { return __DO_NOT_USE_OR_YOU_WILL_BE_HAUNTED_BY_SPOOKY_GHOSTS; });
/* harmony import */ var stylis_stylis_min__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stylis/stylis.min */ "./node_modules/stylis/stylis.min.js");
/* harmony import */ var stylis_stylis_min__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(stylis_stylis_min__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var stylis_rule_sheet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! stylis-rule-sheet */ "./node_modules/stylis-rule-sheet/index.js");
/* harmony import */ var stylis_rule_sheet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(stylis_rule_sheet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _emotion_unitless__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @emotion/unitless */ "./node_modules/@emotion/unitless/dist/unitless.browser.esm.js");
/* harmony import */ var react_is__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-is */ "./node_modules/react-is/index.js");
/* harmony import */ var react_is__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_is__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var memoize_one__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! memoize-one */ "./node_modules/memoize-one/dist/memoize-one.esm.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _emotion_is_prop_valid__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @emotion/is-prop-valid */ "./node_modules/@emotion/is-prop-valid/dist/is-prop-valid.browser.esm.js");
/* harmony import */ var merge_anything__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! merge-anything */ "./node_modules/merge-anything/dist/index.esm.js");










// 

var interleave = (function (strings, interpolations) {
  var result = [strings[0]];

  for (var i = 0, len = interpolations.length; i < len; i += 1) {
    result.push(interpolations[i], strings[i + 1]);
  }

  return result;
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};

var objectWithoutProperties = function (obj, keys) {
  var target = {};

  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }

  return target;
};

var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

// 
var isPlainObject = (function (x) {
  return (typeof x === 'undefined' ? 'undefined' : _typeof(x)) === 'object' && x.constructor === Object;
});

// 
var EMPTY_ARRAY = Object.freeze([]);
var EMPTY_OBJECT = Object.freeze({});

// 
function isFunction(test) {
  return typeof test === 'function';
}

// 

function getComponentName(target) {
  return ( true ? typeof target === 'string' && target : undefined) || target.displayName || target.name || 'Component';
}

// 
function isStatelessFunction(test) {
  return typeof test === 'function' && !(test.prototype && test.prototype.isReactComponent);
}

// 
function isStyledComponent(target) {
  return target && typeof target.styledComponentId === 'string';
}

// 

var SC_ATTR = typeof process !== 'undefined' && (process.env.REACT_APP_SC_ATTR || process.env.SC_ATTR) || 'data-styled';

var SC_VERSION_ATTR = 'data-styled-version';

var SC_STREAM_ATTR = 'data-styled-streamed';

var IS_BROWSER = typeof window !== 'undefined' && 'HTMLElement' in window;

var DISABLE_SPEEDY = typeof SC_DISABLE_SPEEDY === 'boolean' && SC_DISABLE_SPEEDY || typeof process !== 'undefined' && (process.env.REACT_APP_SC_DISABLE_SPEEDY || process.env.SC_DISABLE_SPEEDY) || "development" !== 'production';

// Shared empty execution context when generating static styles
var STATIC_EXECUTION_CONTEXT = {};

// 


/**
 * Parse errors.md and turn it into a simple hash of code: message
 */
var ERRORS =  true ? {
  "1": "Cannot create styled-component for component: %s.\n\n",
  "2": "Can't collect styles once you've consumed a `ServerStyleSheet`'s styles! `ServerStyleSheet` is a one off instance for each server-side render cycle.\n\n- Are you trying to reuse it across renders?\n- Are you accidentally calling collectStyles twice?\n\n",
  "3": "Streaming SSR is only supported in a Node.js environment; Please do not try to call this method in the browser.\n\n",
  "4": "The `StyleSheetManager` expects a valid target or sheet prop!\n\n- Does this error occur on the client and is your target falsy?\n- Does this error occur on the server and is the sheet falsy?\n\n",
  "5": "The clone method cannot be used on the client!\n\n- Are you running in a client-like environment on the server?\n- Are you trying to run SSR on the client?\n\n",
  "6": "Trying to insert a new style tag, but the given Node is unmounted!\n\n- Are you using a custom target that isn't mounted?\n- Does your document not have a valid head element?\n- Have you accidentally removed a style tag manually?\n\n",
  "7": "ThemeProvider: Please return an object from your \"theme\" prop function, e.g.\n\n```js\ntheme={() => ({})}\n```\n\n",
  "8": "ThemeProvider: Please make your \"theme\" prop an object.\n\n",
  "9": "Missing document `<head>`\n\n",
  "10": "Cannot find a StyleSheet instance. Usually this happens if there are multiple copies of styled-components loaded at once. Check out this issue for how to troubleshoot and fix the common cases where this situation can happen: https://github.com/styled-components/styled-components/issues/1941#issuecomment-417862021\n\n",
  "11": "_This error was replaced with a dev-time warning, it will be deleted for v4 final._ [createGlobalStyle] received children which will not be rendered. Please use the component without passing children elements.\n\n",
  "12": "It seems you are interpolating a keyframe declaration (%s) into an untagged string. This was supported in styled-components v3, but is not longer supported in v4 as keyframes are now injected on-demand. Please wrap your string in the css\\`\\` helper which ensures the styles are injected correctly. See https://www.styled-components.com/docs/api#css\n\n",
  "13": "%s is not a styled component and cannot be referred to via component selector. See https://www.styled-components.com/docs/advanced#referring-to-other-components for more details.\n"
} : undefined;

/**
 * super basic version of sprintf
 */
function format() {
  var a = arguments.length <= 0 ? undefined : arguments[0];
  var b = [];

  for (var c = 1, len = arguments.length; c < len; c += 1) {
    b.push(arguments.length <= c ? undefined : arguments[c]);
  }

  b.forEach(function (d) {
    a = a.replace(/%[a-z]/, d);
  });

  return a;
}

/**
 * Create an error file out of errors.md for development and a simple web link to the full errors
 * in production mode.
 */

var StyledComponentsError = function (_Error) {
  inherits(StyledComponentsError, _Error);

  function StyledComponentsError(code) {
    classCallCheck(this, StyledComponentsError);

    for (var _len = arguments.length, interpolations = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      interpolations[_key - 1] = arguments[_key];
    }

    if (false) { var _this; } else {
      var _this = possibleConstructorReturn(this, _Error.call(this, format.apply(undefined, [ERRORS[code]].concat(interpolations)).trim()));
    }
    return possibleConstructorReturn(_this);
  }

  return StyledComponentsError;
}(Error);

// 
var SC_COMPONENT_ID = /^[^\S\n]*?\/\* sc-component-id:\s*(\S+)\s+\*\//gm;

var extractComps = (function (maybeCSS) {
  var css = '' + (maybeCSS || ''); // Definitely a string, and a clone
  var existingComponents = [];
  css.replace(SC_COMPONENT_ID, function (match, componentId, matchIndex) {
    existingComponents.push({ componentId: componentId, matchIndex: matchIndex });
    return match;
  });
  return existingComponents.map(function (_ref, i) {
    var componentId = _ref.componentId,
        matchIndex = _ref.matchIndex;

    var nextComp = existingComponents[i + 1];
    var cssFromDOM = nextComp ? css.slice(matchIndex, nextComp.matchIndex) : css.slice(matchIndex);
    return { componentId: componentId, cssFromDOM: cssFromDOM };
  });
});

// 

var COMMENT_REGEX = /^\s*\/\/.*$/gm;

// NOTE: This stylis instance is only used to split rules from SSR'd style tags
var stylisSplitter = new stylis_stylis_min__WEBPACK_IMPORTED_MODULE_0___default.a({
  global: false,
  cascade: true,
  keyframe: false,
  prefix: false,
  compress: false,
  semicolon: true
});

var stylis = new stylis_stylis_min__WEBPACK_IMPORTED_MODULE_0___default.a({
  global: false,
  cascade: true,
  keyframe: false,
  prefix: true,
  compress: false,
  semicolon: false // NOTE: This means "autocomplete missing semicolons"
});

// Wrap `insertRulePlugin to build a list of rules,
// and then make our own plugin to return the rules. This
// makes it easier to hook into the existing SSR architecture

var parsingRules = [];

// eslint-disable-next-line consistent-return
var returnRulesPlugin = function returnRulesPlugin(context) {
  if (context === -2) {
    var parsedRules = parsingRules;
    parsingRules = [];
    return parsedRules;
  }
};

var parseRulesPlugin = stylis_rule_sheet__WEBPACK_IMPORTED_MODULE_1___default()(function (rule) {
  parsingRules.push(rule);
});

var _componentId = void 0;
var _selector = void 0;
var _selectorRegexp = void 0;

var selfReferenceReplacer = function selfReferenceReplacer(match, offset, string) {
  if (
  // the first self-ref is always untouched
  offset > 0 &&
  // there should be at least two self-refs to do a replacement (.b > .b)
  string.slice(0, offset).indexOf(_selector) !== -1 &&
  // no consecutive self refs (.b.b); that is a precedence boost and treated differently
  string.slice(offset - _selector.length, offset) !== _selector) {
    return '.' + _componentId;
  }

  return match;
};

/**
 * When writing a style like
 *
 * & + & {
 *   color: red;
 * }
 *
 * The second ampersand should be a reference to the static component class. stylis
 * has no knowledge of static class so we have to intelligently replace the base selector.
 */
var selfReferenceReplacementPlugin = function selfReferenceReplacementPlugin(context, _, selectors) {
  if (context === 2 && selectors.length && selectors[0].lastIndexOf(_selector) > 0) {
    // eslint-disable-next-line no-param-reassign
    selectors[0] = selectors[0].replace(_selectorRegexp, selfReferenceReplacer);
  }
};

stylis.use([selfReferenceReplacementPlugin, parseRulesPlugin, returnRulesPlugin]);
stylisSplitter.use([parseRulesPlugin, returnRulesPlugin]);

var splitByRules = function splitByRules(css) {
  return stylisSplitter('', css);
};

function stringifyRules(rules, selector, prefix) {
  var componentId = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '&';

  var flatCSS = rules.join('').replace(COMMENT_REGEX, ''); // replace JS comments

  var cssStr = selector && prefix ? prefix + ' ' + selector + ' { ' + flatCSS + ' }' : flatCSS;

  // stylis has no concept of state to be passed to plugins
  // but since JS is single=threaded, we can rely on that to ensure
  // these properties stay in sync with the current stylis run
  _componentId = componentId;
  _selector = selector;
  _selectorRegexp = new RegExp('\\' + _selector + '\\b', 'g');

  return stylis(prefix || !selector ? '' : selector, cssStr);
}

// 
/* eslint-disable camelcase, no-undef */

var getNonce = (function () {
  return  true ? __webpack_require__.nc : undefined;
});

// 
/* These are helpers for the StyleTags to keep track of the injected
 * rule names for each (component) ID that they're keeping track of.
 * They're crucial for detecting whether a name has already been
 * injected.
 * (This excludes rehydrated names) */

/* adds a new ID:name pairing to a names dictionary */
var addNameForId = function addNameForId(names, id, name) {
  if (name) {
    // eslint-disable-next-line no-param-reassign
    var namesForId = names[id] || (names[id] = Object.create(null));
    namesForId[name] = true;
  }
};

/* resets an ID entirely by overwriting it in the dictionary */
var resetIdNames = function resetIdNames(names, id) {
  // eslint-disable-next-line no-param-reassign
  names[id] = Object.create(null);
};

/* factory for a names dictionary checking the existance of an ID:name pairing */
var hasNameForId = function hasNameForId(names) {
  return function (id, name) {
    return names[id] !== undefined && names[id][name];
  };
};

/* stringifies names for the html/element output */
var stringifyNames = function stringifyNames(names) {
  var str = '';
  // eslint-disable-next-line guard-for-in
  for (var id in names) {
    str += Object.keys(names[id]).join(' ') + ' ';
  }
  return str.trim();
};

/* clones the nested names dictionary */
var cloneNames = function cloneNames(names) {
  var clone = Object.create(null);
  // eslint-disable-next-line guard-for-in
  for (var id in names) {
    clone[id] = _extends({}, names[id]);
  }
  return clone;
};

// 

/* These are helpers that deal with the insertRule (aka speedy) API
 * They are used in the StyleTags and specifically the speedy tag
 */

/* retrieve a sheet for a given style tag */
var sheetForTag = function sheetForTag(tag) {
  // $FlowFixMe
  if (tag.sheet) return tag.sheet;

  /* Firefox quirk requires us to step through all stylesheets to find one owned by the given tag */
  var size = tag.ownerDocument.styleSheets.length;
  for (var i = 0; i < size; i += 1) {
    var sheet = tag.ownerDocument.styleSheets[i];
    // $FlowFixMe
    if (sheet.ownerNode === tag) return sheet;
  }

  /* we should always be able to find a tag */
  throw new StyledComponentsError(10);
};

/* insert a rule safely and return whether it was actually injected */
var safeInsertRule = function safeInsertRule(sheet, cssRule, index) {
  /* abort early if cssRule string is falsy */
  if (!cssRule) return false;

  var maxIndex = sheet.cssRules.length;

  try {
    /* use insertRule and cap passed index with maxIndex (no of cssRules) */
    sheet.insertRule(cssRule, index <= maxIndex ? index : maxIndex);
  } catch (err) {
    /* any error indicates an invalid rule */
    return false;
  }

  return true;
};

/* deletes `size` rules starting from `removalIndex` */
var deleteRules = function deleteRules(sheet, removalIndex, size) {
  var lowerBound = removalIndex - size;
  for (var i = removalIndex; i > lowerBound; i -= 1) {
    sheet.deleteRule(i);
  }
};

// 

/* this marker separates component styles and is important for rehydration */
var makeTextMarker = function makeTextMarker(id) {
  return '\n/* sc-component-id: ' + id + ' */\n';
};

/* add up all numbers in array up until and including the index */
var addUpUntilIndex = function addUpUntilIndex(sizes, index) {
  var totalUpToIndex = 0;
  for (var i = 0; i <= index; i += 1) {
    totalUpToIndex += sizes[i];
  }

  return totalUpToIndex;
};

/* create a new style tag after lastEl */
var makeStyleTag = function makeStyleTag(target, tagEl, insertBefore) {
  var targetDocument = document;
  if (target) targetDocument = target.ownerDocument;else if (tagEl) targetDocument = tagEl.ownerDocument;

  var el = targetDocument.createElement('style');
  el.setAttribute(SC_ATTR, '');
  el.setAttribute(SC_VERSION_ATTR, "4.4.1");

  var nonce = getNonce();
  if (nonce) {
    el.setAttribute('nonce', nonce);
  }

  /* Work around insertRule quirk in EdgeHTML */
  el.appendChild(targetDocument.createTextNode(''));

  if (target && !tagEl) {
    /* Append to target when no previous element was passed */
    target.appendChild(el);
  } else {
    if (!tagEl || !target || !tagEl.parentNode) {
      throw new StyledComponentsError(6);
    }

    /* Insert new style tag after the previous one */
    tagEl.parentNode.insertBefore(el, insertBefore ? tagEl : tagEl.nextSibling);
  }

  return el;
};

/* takes a css factory function and outputs an html styled tag factory */
var wrapAsHtmlTag = function wrapAsHtmlTag(css, names) {
  return function (additionalAttrs) {
    var nonce = getNonce();
    var attrs = [nonce && 'nonce="' + nonce + '"', SC_ATTR + '="' + stringifyNames(names) + '"', SC_VERSION_ATTR + '="' + "4.4.1" + '"', additionalAttrs];

    var htmlAttr = attrs.filter(Boolean).join(' ');
    return '<style ' + htmlAttr + '>' + css() + '</style>';
  };
};

/* takes a css factory function and outputs an element factory */
var wrapAsElement = function wrapAsElement(css, names) {
  return function () {
    var _props;

    var props = (_props = {}, _props[SC_ATTR] = stringifyNames(names), _props[SC_VERSION_ATTR] = "4.4.1", _props);

    var nonce = getNonce();
    if (nonce) {
      // $FlowFixMe
      props.nonce = nonce;
    }

    // eslint-disable-next-line react/no-danger
    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement('style', _extends({}, props, { dangerouslySetInnerHTML: { __html: css() } }));
  };
};

var getIdsFromMarkersFactory = function getIdsFromMarkersFactory(markers) {
  return function () {
    return Object.keys(markers);
  };
};

/* speedy tags utilise insertRule */
var makeSpeedyTag = function makeSpeedyTag(el, getImportRuleTag) {
  var names = Object.create(null);
  var markers = Object.create(null);
  var sizes = [];

  var extractImport = getImportRuleTag !== undefined;
  /* indicates whether getImportRuleTag was called */
  var usedImportRuleTag = false;

  var insertMarker = function insertMarker(id) {
    var prev = markers[id];
    if (prev !== undefined) {
      return prev;
    }

    markers[id] = sizes.length;
    sizes.push(0);
    resetIdNames(names, id);

    return markers[id];
  };

  var insertRules = function insertRules(id, cssRules, name) {
    var marker = insertMarker(id);
    var sheet = sheetForTag(el);
    var insertIndex = addUpUntilIndex(sizes, marker);

    var injectedRules = 0;
    var importRules = [];
    var cssRulesSize = cssRules.length;

    for (var i = 0; i < cssRulesSize; i += 1) {
      var cssRule = cssRules[i];
      var mayHaveImport = extractImport; /* @import rules are reordered to appear first */
      if (mayHaveImport && cssRule.indexOf('@import') !== -1) {
        importRules.push(cssRule);
      } else if (safeInsertRule(sheet, cssRule, insertIndex + injectedRules)) {
        mayHaveImport = false;
        injectedRules += 1;
      }
    }

    if (extractImport && importRules.length > 0) {
      usedImportRuleTag = true;
      // $FlowFixMe
      getImportRuleTag().insertRules(id + '-import', importRules);
    }

    sizes[marker] += injectedRules; /* add up no of injected rules */
    addNameForId(names, id, name);
  };

  var removeRules = function removeRules(id) {
    var marker = markers[id];
    if (marker === undefined) return;
    // $FlowFixMe
    if (el.isConnected === false) return;

    var size = sizes[marker];
    var sheet = sheetForTag(el);
    var removalIndex = addUpUntilIndex(sizes, marker) - 1;
    deleteRules(sheet, removalIndex, size);
    sizes[marker] = 0;
    resetIdNames(names, id);

    if (extractImport && usedImportRuleTag) {
      // $FlowFixMe
      getImportRuleTag().removeRules(id + '-import');
    }
  };

  var css = function css() {
    var _sheetForTag = sheetForTag(el),
        cssRules = _sheetForTag.cssRules;

    var str = '';

    // eslint-disable-next-line guard-for-in
    for (var id in markers) {
      str += makeTextMarker(id);
      var marker = markers[id];
      var end = addUpUntilIndex(sizes, marker);
      var size = sizes[marker];
      for (var i = end - size; i < end; i += 1) {
        var rule = cssRules[i];
        if (rule !== undefined) {
          str += rule.cssText;
        }
      }
    }

    return str;
  };

  return {
    clone: function clone() {
      throw new StyledComponentsError(5);
    },

    css: css,
    getIds: getIdsFromMarkersFactory(markers),
    hasNameForId: hasNameForId(names),
    insertMarker: insertMarker,
    insertRules: insertRules,
    removeRules: removeRules,
    sealed: false,
    styleTag: el,
    toElement: wrapAsElement(css, names),
    toHTML: wrapAsHtmlTag(css, names)
  };
};

var makeTextNode = function makeTextNode(targetDocument, id) {
  return targetDocument.createTextNode(makeTextMarker(id));
};

var makeBrowserTag = function makeBrowserTag(el, getImportRuleTag) {
  var names = Object.create(null);
  var markers = Object.create(null);

  var extractImport = getImportRuleTag !== undefined;

  /* indicates whether getImportRuleTag was called */
  var usedImportRuleTag = false;

  var insertMarker = function insertMarker(id) {
    var prev = markers[id];
    if (prev !== undefined) {
      return prev;
    }

    markers[id] = makeTextNode(el.ownerDocument, id);
    el.appendChild(markers[id]);
    names[id] = Object.create(null);

    return markers[id];
  };

  var insertRules = function insertRules(id, cssRules, name) {
    var marker = insertMarker(id);
    var importRules = [];
    var cssRulesSize = cssRules.length;

    for (var i = 0; i < cssRulesSize; i += 1) {
      var rule = cssRules[i];
      var mayHaveImport = extractImport;
      if (mayHaveImport && rule.indexOf('@import') !== -1) {
        importRules.push(rule);
      } else {
        mayHaveImport = false;
        var separator = i === cssRulesSize - 1 ? '' : ' ';
        marker.appendData('' + rule + separator);
      }
    }

    addNameForId(names, id, name);

    if (extractImport && importRules.length > 0) {
      usedImportRuleTag = true;
      // $FlowFixMe
      getImportRuleTag().insertRules(id + '-import', importRules);
    }
  };

  var removeRules = function removeRules(id) {
    var marker = markers[id];
    if (marker === undefined) return;

    /* create new empty text node and replace the current one */
    var newMarker = makeTextNode(el.ownerDocument, id);
    el.replaceChild(newMarker, marker);
    markers[id] = newMarker;
    resetIdNames(names, id);

    if (extractImport && usedImportRuleTag) {
      // $FlowFixMe
      getImportRuleTag().removeRules(id + '-import');
    }
  };

  var css = function css() {
    var str = '';

    // eslint-disable-next-line guard-for-in
    for (var id in markers) {
      str += markers[id].data;
    }

    return str;
  };

  return {
    clone: function clone() {
      throw new StyledComponentsError(5);
    },

    css: css,
    getIds: getIdsFromMarkersFactory(markers),
    hasNameForId: hasNameForId(names),
    insertMarker: insertMarker,
    insertRules: insertRules,
    removeRules: removeRules,
    sealed: false,
    styleTag: el,
    toElement: wrapAsElement(css, names),
    toHTML: wrapAsHtmlTag(css, names)
  };
};

var makeServerTag = function makeServerTag(namesArg, markersArg) {
  var names = namesArg === undefined ? Object.create(null) : namesArg;
  var markers = markersArg === undefined ? Object.create(null) : markersArg;

  var insertMarker = function insertMarker(id) {
    var prev = markers[id];
    if (prev !== undefined) {
      return prev;
    }

    return markers[id] = [''];
  };

  var insertRules = function insertRules(id, cssRules, name) {
    var marker = insertMarker(id);
    marker[0] += cssRules.join(' ');
    addNameForId(names, id, name);
  };

  var removeRules = function removeRules(id) {
    var marker = markers[id];
    if (marker === undefined) return;
    marker[0] = '';
    resetIdNames(names, id);
  };

  var css = function css() {
    var str = '';
    // eslint-disable-next-line guard-for-in
    for (var id in markers) {
      var cssForId = markers[id][0];
      if (cssForId) {
        str += makeTextMarker(id) + cssForId;
      }
    }
    return str;
  };

  var clone = function clone() {
    var namesClone = cloneNames(names);
    var markersClone = Object.create(null);

    // eslint-disable-next-line guard-for-in
    for (var id in markers) {
      markersClone[id] = [markers[id][0]];
    }

    return makeServerTag(namesClone, markersClone);
  };

  var tag = {
    clone: clone,
    css: css,
    getIds: getIdsFromMarkersFactory(markers),
    hasNameForId: hasNameForId(names),
    insertMarker: insertMarker,
    insertRules: insertRules,
    removeRules: removeRules,
    sealed: false,
    styleTag: null,
    toElement: wrapAsElement(css, names),
    toHTML: wrapAsHtmlTag(css, names)
  };

  return tag;
};

var makeTag = function makeTag(target, tagEl, forceServer, insertBefore, getImportRuleTag) {
  if (IS_BROWSER && !forceServer) {
    var el = makeStyleTag(target, tagEl, insertBefore);

    if (DISABLE_SPEEDY) {
      return makeBrowserTag(el, getImportRuleTag);
    } else {
      return makeSpeedyTag(el, getImportRuleTag);
    }
  }

  return makeServerTag();
};

var rehydrate = function rehydrate(tag, els, extracted) {
  /* add all extracted components to the new tag */
  for (var i = 0, len = extracted.length; i < len; i += 1) {
    var _extracted$i = extracted[i],
        componentId = _extracted$i.componentId,
        cssFromDOM = _extracted$i.cssFromDOM;

    var cssRules = splitByRules(cssFromDOM);
    tag.insertRules(componentId, cssRules);
  }

  /* remove old HTMLStyleElements, since they have been rehydrated */
  for (var _i = 0, _len = els.length; _i < _len; _i += 1) {
    var el = els[_i];
    if (el.parentNode) {
      el.parentNode.removeChild(el);
    }
  }
};

// 

var SPLIT_REGEX = /\s+/;

/* determine the maximum number of components before tags are sharded */
var MAX_SIZE = void 0;
if (IS_BROWSER) {
  /* in speedy mode we can keep a lot more rules in a sheet before a slowdown can be expected */
  MAX_SIZE = DISABLE_SPEEDY ? 40 : 1000;
} else {
  /* for servers we do not need to shard at all */
  MAX_SIZE = -1;
}

var sheetRunningId = 0;
var master = void 0;

var StyleSheet = function () {

  /* a map from ids to tags */

  /* deferred rules for a given id */

  /* this is used for not reinjecting rules via hasNameForId() */

  /* when rules for an id are removed using remove() we have to ignore rehydratedNames for it */

  /* a list of tags belonging to this StyleSheet */

  /* a tag for import rules */

  /* current capacity until a new tag must be created */

  /* children (aka clones) of this StyleSheet inheriting all and future injections */

  function StyleSheet() {
    var _this = this;

    var target = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : IS_BROWSER ? document.head : null;
    var forceServer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    classCallCheck(this, StyleSheet);

    this.getImportRuleTag = function () {
      var importRuleTag = _this.importRuleTag;

      if (importRuleTag !== undefined) {
        return importRuleTag;
      }

      var firstTag = _this.tags[0];
      var insertBefore = true;

      return _this.importRuleTag = makeTag(_this.target, firstTag ? firstTag.styleTag : null, _this.forceServer, insertBefore);
    };

    sheetRunningId += 1;
    this.id = sheetRunningId;
    this.forceServer = forceServer;
    this.target = forceServer ? null : target;
    this.tagMap = {};
    this.deferred = {};
    this.rehydratedNames = {};
    this.ignoreRehydratedNames = {};
    this.tags = [];
    this.capacity = 1;
    this.clones = [];
  }

  /* rehydrate all SSR'd style tags */


  StyleSheet.prototype.rehydrate = function rehydrate$$1() {
    if (!IS_BROWSER || this.forceServer) return this;

    var els = [];
    var extracted = [];
    var isStreamed = false;

    /* retrieve all of our SSR style elements from the DOM */
    var nodes = document.querySelectorAll('style[' + SC_ATTR + '][' + SC_VERSION_ATTR + '="' + "4.4.1" + '"]');

    var nodesSize = nodes.length;

    /* abort rehydration if no previous style tags were found */
    if (!nodesSize) return this;

    for (var i = 0; i < nodesSize; i += 1) {
      var el = nodes[i];

      /* check if style tag is a streamed tag */
      if (!isStreamed) isStreamed = !!el.getAttribute(SC_STREAM_ATTR);

      /* retrieve all component names */
      var elNames = (el.getAttribute(SC_ATTR) || '').trim().split(SPLIT_REGEX);
      var elNamesSize = elNames.length;
      for (var j = 0, name; j < elNamesSize; j += 1) {
        name = elNames[j];
        /* add rehydrated name to sheet to avoid re-adding styles */
        this.rehydratedNames[name] = true;
      }

      /* extract all components and their CSS */
      extracted.push.apply(extracted, extractComps(el.textContent));

      /* store original HTMLStyleElement */
      els.push(el);
    }

    /* abort rehydration if nothing was extracted */
    var extractedSize = extracted.length;
    if (!extractedSize) return this;

    /* create a tag to be used for rehydration */
    var tag = this.makeTag(null);

    rehydrate(tag, els, extracted);

    /* reset capacity and adjust MAX_SIZE by the initial size of the rehydration */
    this.capacity = Math.max(1, MAX_SIZE - extractedSize);
    this.tags.push(tag);

    /* retrieve all component ids */
    for (var _j = 0; _j < extractedSize; _j += 1) {
      this.tagMap[extracted[_j].componentId] = tag;
    }

    return this;
  };

  /* retrieve a "master" instance of StyleSheet which is typically used when no other is available
   * The master StyleSheet is targeted by createGlobalStyle, keyframes, and components outside of any
    * StyleSheetManager's context */


  /* reset the internal "master" instance */
  StyleSheet.reset = function reset() {
    var forceServer = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

    master = new StyleSheet(undefined, forceServer).rehydrate();
  };

  /* adds "children" to the StyleSheet that inherit all of the parents' rules
   * while their own rules do not affect the parent */


  StyleSheet.prototype.clone = function clone() {
    var sheet = new StyleSheet(this.target, this.forceServer);

    /* add to clone array */
    this.clones.push(sheet);

    /* clone all tags */
    sheet.tags = this.tags.map(function (tag) {
      var ids = tag.getIds();
      var newTag = tag.clone();

      /* reconstruct tagMap */
      for (var i = 0; i < ids.length; i += 1) {
        sheet.tagMap[ids[i]] = newTag;
      }

      return newTag;
    });

    /* clone other maps */
    sheet.rehydratedNames = _extends({}, this.rehydratedNames);
    sheet.deferred = _extends({}, this.deferred);

    return sheet;
  };

  /* force StyleSheet to create a new tag on the next injection */


  StyleSheet.prototype.sealAllTags = function sealAllTags() {
    this.capacity = 1;

    this.tags.forEach(function (tag) {
      // eslint-disable-next-line no-param-reassign
      tag.sealed = true;
    });
  };

  StyleSheet.prototype.makeTag = function makeTag$$1(tag) {
    var lastEl = tag ? tag.styleTag : null;
    var insertBefore = false;

    return makeTag(this.target, lastEl, this.forceServer, insertBefore, this.getImportRuleTag);
  };

  /* get a tag for a given componentId, assign the componentId to one, or shard */
  StyleSheet.prototype.getTagForId = function getTagForId(id) {
    /* simply return a tag, when the componentId was already assigned one */
    var prev = this.tagMap[id];
    if (prev !== undefined && !prev.sealed) {
      return prev;
    }

    var tag = this.tags[this.tags.length - 1];

    /* shard (create a new tag) if the tag is exhausted (See MAX_SIZE) */
    this.capacity -= 1;

    if (this.capacity === 0) {
      this.capacity = MAX_SIZE;
      tag = this.makeTag(tag);
      this.tags.push(tag);
    }

    return this.tagMap[id] = tag;
  };

  /* mainly for createGlobalStyle to check for its id */


  StyleSheet.prototype.hasId = function hasId(id) {
    return this.tagMap[id] !== undefined;
  };

  /* caching layer checking id+name to already have a corresponding tag and injected rules */


  StyleSheet.prototype.hasNameForId = function hasNameForId(id, name) {
    /* exception for rehydrated names which are checked separately */
    if (this.ignoreRehydratedNames[id] === undefined && this.rehydratedNames[name]) {
      return true;
    }

    var tag = this.tagMap[id];
    return tag !== undefined && tag.hasNameForId(id, name);
  };

  /* registers a componentId and registers it on its tag */


  StyleSheet.prototype.deferredInject = function deferredInject(id, cssRules) {
    /* don't inject when the id is already registered */
    if (this.tagMap[id] !== undefined) return;

    var clones = this.clones;

    for (var i = 0; i < clones.length; i += 1) {
      clones[i].deferredInject(id, cssRules);
    }

    this.getTagForId(id).insertMarker(id);
    this.deferred[id] = cssRules;
  };

  /* injects rules for a given id with a name that will need to be cached */


  StyleSheet.prototype.inject = function inject(id, cssRules, name) {
    var clones = this.clones;


    for (var i = 0; i < clones.length; i += 1) {
      clones[i].inject(id, cssRules, name);
    }

    var tag = this.getTagForId(id);

    /* add deferred rules for component */
    if (this.deferred[id] !== undefined) {
      // Combine passed cssRules with previously deferred CSS rules
      // NOTE: We cannot mutate the deferred array itself as all clones
      // do the same (see clones[i].inject)
      var rules = this.deferred[id].concat(cssRules);
      tag.insertRules(id, rules, name);

      this.deferred[id] = undefined;
    } else {
      tag.insertRules(id, cssRules, name);
    }
  };

  /* removes all rules for a given id, which doesn't remove its marker but resets it */


  StyleSheet.prototype.remove = function remove(id) {
    var tag = this.tagMap[id];
    if (tag === undefined) return;

    var clones = this.clones;

    for (var i = 0; i < clones.length; i += 1) {
      clones[i].remove(id);
    }

    /* remove all rules from the tag */
    tag.removeRules(id);

    /* ignore possible rehydrated names */
    this.ignoreRehydratedNames[id] = true;

    /* delete possible deferred rules */
    this.deferred[id] = undefined;
  };

  StyleSheet.prototype.toHTML = function toHTML() {
    return this.tags.map(function (tag) {
      return tag.toHTML();
    }).join('');
  };

  StyleSheet.prototype.toReactElements = function toReactElements() {
    var id = this.id;


    return this.tags.map(function (tag, i) {
      var key = 'sc-' + id + '-' + i;
      return Object(react__WEBPACK_IMPORTED_MODULE_2__["cloneElement"])(tag.toElement(), { key: key });
    });
  };

  createClass(StyleSheet, null, [{
    key: 'master',
    get: function get$$1() {
      return master || (master = new StyleSheet().rehydrate());
    }

    /* NOTE: This is just for backwards-compatibility with jest-styled-components */

  }, {
    key: 'instance',
    get: function get$$1() {
      return StyleSheet.master;
    }
  }]);
  return StyleSheet;
}();

// 

var Keyframes = function () {
  function Keyframes(name, rules) {
    var _this = this;

    classCallCheck(this, Keyframes);

    this.inject = function (styleSheet) {
      if (!styleSheet.hasNameForId(_this.id, _this.name)) {
        styleSheet.inject(_this.id, _this.rules, _this.name);
      }
    };

    this.toString = function () {
      throw new StyledComponentsError(12, String(_this.name));
    };

    this.name = name;
    this.rules = rules;

    this.id = 'sc-keyframes-' + name;
  }

  Keyframes.prototype.getName = function getName() {
    return this.name;
  };

  return Keyframes;
}();

// 

/**
 * inlined version of
 * https://github.com/facebook/fbjs/blob/master/packages/fbjs/src/core/hyphenateStyleName.js
 */

var uppercasePattern = /([A-Z])/g;
var msPattern = /^ms-/;

/**
 * Hyphenates a camelcased CSS property name, for example:
 *
 *   > hyphenateStyleName('backgroundColor')
 *   < "background-color"
 *   > hyphenateStyleName('MozTransition')
 *   < "-moz-transition"
 *   > hyphenateStyleName('msTransition')
 *   < "-ms-transition"
 *
 * As Modernizr suggests (http://modernizr.com/docs/#prefixed), an `ms` prefix
 * is converted to `-ms-`.
 *
 * @param {string} string
 * @return {string}
 */
function hyphenateStyleName(string) {
  return string.replace(uppercasePattern, '-$1').toLowerCase().replace(msPattern, '-ms-');
}

// 

// Taken from https://github.com/facebook/react/blob/b87aabdfe1b7461e7331abb3601d9e6bb27544bc/packages/react-dom/src/shared/dangerousStyleValue.js
function addUnitIfNeeded(name, value) {
  // https://github.com/amilajack/eslint-plugin-flowtype-errors/issues/133
  // $FlowFixMe
  if (value == null || typeof value === 'boolean' || value === '') {
    return '';
  }

  if (typeof value === 'number' && value !== 0 && !(name in _emotion_unitless__WEBPACK_IMPORTED_MODULE_3__["default"])) {
    return value + 'px'; // Presumes implicit 'px' suffix for unitless numbers
  }

  return String(value).trim();
}

// 

/**
 * It's falsish not falsy because 0 is allowed.
 */
var isFalsish = function isFalsish(chunk) {
  return chunk === undefined || chunk === null || chunk === false || chunk === '';
};

var objToCssArray = function objToCssArray(obj, prevKey) {
  var rules = [];
  var keys = Object.keys(obj);

  keys.forEach(function (key) {
    if (!isFalsish(obj[key])) {
      if (isPlainObject(obj[key])) {
        rules.push.apply(rules, objToCssArray(obj[key], key));

        return rules;
      } else if (isFunction(obj[key])) {
        rules.push(hyphenateStyleName(key) + ':', obj[key], ';');

        return rules;
      }
      rules.push(hyphenateStyleName(key) + ': ' + addUnitIfNeeded(key, obj[key]) + ';');
    }
    return rules;
  });

  return prevKey ? [prevKey + ' {'].concat(rules, ['}']) : rules;
};

function flatten(chunk, executionContext, styleSheet) {
  if (Array.isArray(chunk)) {
    var ruleSet = [];

    for (var i = 0, len = chunk.length, result; i < len; i += 1) {
      result = flatten(chunk[i], executionContext, styleSheet);

      if (result === null) continue;else if (Array.isArray(result)) ruleSet.push.apply(ruleSet, result);else ruleSet.push(result);
    }

    return ruleSet;
  }

  if (isFalsish(chunk)) {
    return null;
  }

  /* Handle other components */
  if (isStyledComponent(chunk)) {
    return '.' + chunk.styledComponentId;
  }

  /* Either execute or defer the function */
  if (isFunction(chunk)) {
    if (isStatelessFunction(chunk) && executionContext) {
      var _result = chunk(executionContext);

      if ( true && Object(react_is__WEBPACK_IMPORTED_MODULE_4__["isElement"])(_result)) {
        // eslint-disable-next-line no-console
        console.warn(getComponentName(chunk) + ' is not a styled component and cannot be referred to via component selector. See https://www.styled-components.com/docs/advanced#referring-to-other-components for more details.');
      }

      return flatten(_result, executionContext, styleSheet);
    } else return chunk;
  }

  if (chunk instanceof Keyframes) {
    if (styleSheet) {
      chunk.inject(styleSheet);
      return chunk.getName();
    } else return chunk;
  }

  /* Handle objects */
  return isPlainObject(chunk) ? objToCssArray(chunk) : chunk.toString();
}

// 

function css(styles) {
  for (var _len = arguments.length, interpolations = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    interpolations[_key - 1] = arguments[_key];
  }

  if (isFunction(styles) || isPlainObject(styles)) {
    // $FlowFixMe
    return flatten(interleave(EMPTY_ARRAY, [styles].concat(interpolations)));
  }

  // $FlowFixMe
  return flatten(interleave(styles, interpolations));
}

// 

function constructWithOptions(componentConstructor, tag) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : EMPTY_OBJECT;

  if (!Object(react_is__WEBPACK_IMPORTED_MODULE_4__["isValidElementType"])(tag)) {
    throw new StyledComponentsError(1, String(tag));
  }

  /* This is callable directly as a template function */
  // $FlowFixMe: Not typed to avoid destructuring arguments
  var templateFunction = function templateFunction() {
    return componentConstructor(tag, options, css.apply(undefined, arguments));
  };

  /* If config methods are called, wrap up a new template function and merge options */
  templateFunction.withConfig = function (config) {
    return constructWithOptions(componentConstructor, tag, _extends({}, options, config));
  };

  /* Modify/inject new props at runtime */
  templateFunction.attrs = function (attrs) {
    return constructWithOptions(componentConstructor, tag, _extends({}, options, {
      attrs: Array.prototype.concat(options.attrs, attrs).filter(Boolean)
    }));
  };

  return templateFunction;
}

// 
// Source: https://github.com/garycourt/murmurhash-js/blob/master/murmurhash2_gc.js
function murmurhash(c) {
  for (var e = c.length | 0, a = e | 0, d = 0, b; e >= 4;) {
    b = c.charCodeAt(d) & 255 | (c.charCodeAt(++d) & 255) << 8 | (c.charCodeAt(++d) & 255) << 16 | (c.charCodeAt(++d) & 255) << 24, b = 1540483477 * (b & 65535) + ((1540483477 * (b >>> 16) & 65535) << 16), b ^= b >>> 24, b = 1540483477 * (b & 65535) + ((1540483477 * (b >>> 16) & 65535) << 16), a = 1540483477 * (a & 65535) + ((1540483477 * (a >>> 16) & 65535) << 16) ^ b, e -= 4, ++d;
  }
  switch (e) {
    case 3:
      a ^= (c.charCodeAt(d + 2) & 255) << 16;
    case 2:
      a ^= (c.charCodeAt(d + 1) & 255) << 8;
    case 1:
      a ^= c.charCodeAt(d) & 255, a = 1540483477 * (a & 65535) + ((1540483477 * (a >>> 16) & 65535) << 16);
  }
  a ^= a >>> 13;
  a = 1540483477 * (a & 65535) + ((1540483477 * (a >>> 16) & 65535) << 16);
  return (a ^ a >>> 15) >>> 0;
}

// 
/* eslint-disable no-bitwise */

/* This is the "capacity" of our alphabet i.e. 2x26 for all letters plus their capitalised
 * counterparts */
var charsLength = 52;

/* start at 75 for 'a' until 'z' (25) and then start at 65 for capitalised letters */
var getAlphabeticChar = function getAlphabeticChar(code) {
  return String.fromCharCode(code + (code > 25 ? 39 : 97));
};

/* input a number, usually a hash and convert it to base-52 */
function generateAlphabeticName(code) {
  var name = '';
  var x = void 0;

  /* get a char and divide by alphabet-length */
  for (x = code; x > charsLength; x = Math.floor(x / charsLength)) {
    name = getAlphabeticChar(x % charsLength) + name;
  }

  return getAlphabeticChar(x % charsLength) + name;
}

// 

function hasFunctionObjectKey(obj) {
  // eslint-disable-next-line guard-for-in, no-restricted-syntax
  for (var key in obj) {
    if (isFunction(obj[key])) {
      return true;
    }
  }

  return false;
}

function isStaticRules(rules, attrs) {
  for (var i = 0; i < rules.length; i += 1) {
    var rule = rules[i];

    // recursive case
    if (Array.isArray(rule) && !isStaticRules(rule, attrs)) {
      return false;
    } else if (isFunction(rule) && !isStyledComponent(rule)) {
      // functions are allowed to be static if they're just being
      // used to get the classname of a nested styled component
      return false;
    }
  }

  if (attrs.some(function (x) {
    return isFunction(x) || hasFunctionObjectKey(x);
  })) return false;

  return true;
}

// 

/* combines hashStr (murmurhash) and nameGenerator for convenience */
var hasher = function hasher(str) {
  return generateAlphabeticName(murmurhash(str));
};

/*
 ComponentStyle is all the CSS-specific stuff, not
 the React-specific stuff.
 */

var ComponentStyle = function () {
  function ComponentStyle(rules, attrs, componentId) {
    classCallCheck(this, ComponentStyle);

    this.rules = rules;
    this.isStatic =  false && false;
    this.componentId = componentId;

    if (!StyleSheet.master.hasId(componentId)) {
      StyleSheet.master.deferredInject(componentId, []);
    }
  }

  /*
   * Flattens a rule set into valid CSS
   * Hashes it, wraps the whole chunk in a .hash1234 {}
   * Returns the hash to be injected on render()
   * */


  ComponentStyle.prototype.generateAndInjectStyles = function generateAndInjectStyles(executionContext, styleSheet) {
    var isStatic = this.isStatic,
        componentId = this.componentId,
        lastClassName = this.lastClassName;

    if (IS_BROWSER && isStatic && typeof lastClassName === 'string' && styleSheet.hasNameForId(componentId, lastClassName)) {
      return lastClassName;
    }

    var flatCSS = flatten(this.rules, executionContext, styleSheet);
    var name = hasher(this.componentId + flatCSS.join(''));
    if (!styleSheet.hasNameForId(componentId, name)) {
      styleSheet.inject(this.componentId, stringifyRules(flatCSS, '.' + name, undefined, componentId), name);
    }

    this.lastClassName = name;
    return name;
  };

  ComponentStyle.generateName = function generateName(str) {
    return hasher(str);
  };

  return ComponentStyle;
}();

// 

var LIMIT = 200;

var createWarnTooManyClasses = (function (displayName) {
  var generatedClasses = {};
  var warningSeen = false;

  return function (className) {
    if (!warningSeen) {
      generatedClasses[className] = true;
      if (Object.keys(generatedClasses).length >= LIMIT) {
        // Unable to find latestRule in test environment.
        /* eslint-disable no-console, prefer-template */
        console.warn('Over ' + LIMIT + ' classes were generated for component ' + displayName + '. \n' + 'Consider using the attrs method, together with a style object for frequently changed styles.\n' + 'Example:\n' + '  const Component = styled.div.attrs(props => ({\n' + '    style: {\n' + '      background: props.background,\n' + '    },\n' + '  }))`width: 100%;`\n\n' + '  <Component />');
        warningSeen = true;
        generatedClasses = {};
      }
    }
  };
});

// 

var determineTheme = (function (props, fallbackTheme) {
  var defaultProps = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : EMPTY_OBJECT;

  // Props should take precedence over ThemeProvider, which should take precedence over
  // defaultProps, but React automatically puts defaultProps on props.

  /* eslint-disable react/prop-types, flowtype-errors/show-errors */
  var isDefaultTheme = defaultProps ? props.theme === defaultProps.theme : false;
  var theme = props.theme && !isDefaultTheme ? props.theme : fallbackTheme || defaultProps.theme;
  /* eslint-enable */

  return theme;
});

// 
var escapeRegex = /[[\].#*$><+~=|^:(),"'`-]+/g;
var dashesAtEnds = /(^-|-$)/g;

/**
 * TODO: Explore using CSS.escape when it becomes more available
 * in evergreen browsers.
 */
function escape(str) {
  return str
  // Replace all possible CSS selectors
  .replace(escapeRegex, '-')

  // Remove extraneous hyphens at the start and end
  .replace(dashesAtEnds, '');
}

// 

function isTag(target) {
  return typeof target === 'string' && ( true ? target.charAt(0) === target.charAt(0).toLowerCase() : undefined);
}

// 

function generateDisplayName(target) {
  // $FlowFixMe
  return isTag(target) ? 'styled.' + target : 'Styled(' + getComponentName(target) + ')';
}

var _TYPE_STATICS;

var REACT_STATICS = {
  childContextTypes: true,
  contextTypes: true,
  defaultProps: true,
  displayName: true,
  getDerivedStateFromProps: true,
  propTypes: true,
  type: true
};

var KNOWN_STATICS = {
  name: true,
  length: true,
  prototype: true,
  caller: true,
  callee: true,
  arguments: true,
  arity: true
};

var TYPE_STATICS = (_TYPE_STATICS = {}, _TYPE_STATICS[react_is__WEBPACK_IMPORTED_MODULE_4__["ForwardRef"]] = {
  $$typeof: true,
  render: true
}, _TYPE_STATICS);

var defineProperty$1 = Object.defineProperty,
    getOwnPropertyNames = Object.getOwnPropertyNames,
    _Object$getOwnPropert = Object.getOwnPropertySymbols,
    getOwnPropertySymbols = _Object$getOwnPropert === undefined ? function () {
  return [];
} : _Object$getOwnPropert,
    getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor,
    getPrototypeOf = Object.getPrototypeOf,
    objectPrototype = Object.prototype;
var arrayPrototype = Array.prototype;


function hoistNonReactStatics(targetComponent, sourceComponent, blacklist) {
  if (typeof sourceComponent !== 'string') {
    // don't hoist over string (html) components

    var inheritedComponent = getPrototypeOf(sourceComponent);

    if (inheritedComponent && inheritedComponent !== objectPrototype) {
      hoistNonReactStatics(targetComponent, inheritedComponent, blacklist);
    }

    var keys = arrayPrototype.concat(getOwnPropertyNames(sourceComponent),
    // $FlowFixMe
    getOwnPropertySymbols(sourceComponent));

    var targetStatics = TYPE_STATICS[targetComponent.$$typeof] || REACT_STATICS;

    var sourceStatics = TYPE_STATICS[sourceComponent.$$typeof] || REACT_STATICS;

    var i = keys.length;
    var descriptor = void 0;
    var key = void 0;

    // eslint-disable-next-line no-plusplus
    while (i--) {
      key = keys[i];

      if (
      // $FlowFixMe
      !KNOWN_STATICS[key] && !(blacklist && blacklist[key]) && !(sourceStatics && sourceStatics[key]) &&
      // $FlowFixMe
      !(targetStatics && targetStatics[key])) {
        descriptor = getOwnPropertyDescriptor(sourceComponent, key);

        if (descriptor) {
          try {
            // Avoid failures from read-only properties
            defineProperty$1(targetComponent, key, descriptor);
          } catch (e) {
            /* fail silently */
          }
        }
      }
    }

    return targetComponent;
  }

  return targetComponent;
}

// 
function isDerivedReactComponent(fn) {
  return !!(fn && fn.prototype && fn.prototype.isReactComponent);
}

// 
// Helper to call a given function, only once
var once = (function (cb) {
  var called = false;

  return function () {
    if (!called) {
      called = true;
      cb.apply(undefined, arguments);
    }
  };
});

// 

var ThemeContext = Object(react__WEBPACK_IMPORTED_MODULE_2__["createContext"])();

var ThemeConsumer = ThemeContext.Consumer;

/**
 * Provide a theme to an entire react component tree via context
 */

var ThemeProvider = function (_Component) {
  inherits(ThemeProvider, _Component);

  function ThemeProvider(props) {
    classCallCheck(this, ThemeProvider);

    var _this = possibleConstructorReturn(this, _Component.call(this, props));

    _this.getContext = Object(memoize_one__WEBPACK_IMPORTED_MODULE_5__["default"])(_this.getContext.bind(_this));
    _this.renderInner = _this.renderInner.bind(_this);
    return _this;
  }

  ThemeProvider.prototype.render = function render() {
    if (!this.props.children) return null;

    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(
      ThemeContext.Consumer,
      null,
      this.renderInner
    );
  };

  ThemeProvider.prototype.renderInner = function renderInner(outerTheme) {
    var context = this.getContext(this.props.theme, outerTheme);

    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(
      ThemeContext.Provider,
      { value: context },
      this.props.children
    );
  };

  /**
   * Get the theme from the props, supporting both (outerTheme) => {}
   * as well as object notation
   */


  ThemeProvider.prototype.getTheme = function getTheme(theme, outerTheme) {
    if (isFunction(theme)) {
      var mergedTheme = theme(outerTheme);

      if ( true && (mergedTheme === null || Array.isArray(mergedTheme) || (typeof mergedTheme === 'undefined' ? 'undefined' : _typeof(mergedTheme)) !== 'object')) {
        throw new StyledComponentsError(7);
      }

      return mergedTheme;
    }

    if (theme === null || Array.isArray(theme) || (typeof theme === 'undefined' ? 'undefined' : _typeof(theme)) !== 'object') {
      throw new StyledComponentsError(8);
    }

    return _extends({}, outerTheme, theme);
  };

  ThemeProvider.prototype.getContext = function getContext(theme, outerTheme) {
    return this.getTheme(theme, outerTheme);
  };

  return ThemeProvider;
}(react__WEBPACK_IMPORTED_MODULE_2__["Component"]);

// 

var CLOSING_TAG_R = /^\s*<\/[a-z]/i;

var ServerStyleSheet = function () {
  function ServerStyleSheet() {
    classCallCheck(this, ServerStyleSheet);

    /* The master sheet might be reset, so keep a reference here */
    this.masterSheet = StyleSheet.master;
    this.instance = this.masterSheet.clone();
    this.sealed = false;
  }

  /**
   * Mark the ServerStyleSheet as being fully emitted and manually GC it from the
   * StyleSheet singleton.
   */


  ServerStyleSheet.prototype.seal = function seal() {
    if (!this.sealed) {
      /* Remove sealed StyleSheets from the master sheet */
      var index = this.masterSheet.clones.indexOf(this.instance);
      this.masterSheet.clones.splice(index, 1);
      this.sealed = true;
    }
  };

  ServerStyleSheet.prototype.collectStyles = function collectStyles(children) {
    if (this.sealed) {
      throw new StyledComponentsError(2);
    }

    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(
      StyleSheetManager,
      { sheet: this.instance },
      children
    );
  };

  ServerStyleSheet.prototype.getStyleTags = function getStyleTags() {
    this.seal();
    return this.instance.toHTML();
  };

  ServerStyleSheet.prototype.getStyleElement = function getStyleElement() {
    this.seal();
    return this.instance.toReactElements();
  };

  ServerStyleSheet.prototype.interleaveWithNodeStream = function interleaveWithNodeStream(readableStream) {
    var _this = this;

    {
      throw new StyledComponentsError(3);
    }

    /* the tag index keeps track of which tags have already been emitted */
    var instance = this.instance;

    var instanceTagIndex = 0;

    var streamAttr = SC_STREAM_ATTR + '="true"';

    var transformer = new stream.Transform({
      transform: function appendStyleChunks(chunk, /* encoding */_, callback) {
        var tags = instance.tags;

        var html = '';

        /* retrieve html for each new style tag */
        for (; instanceTagIndex < tags.length; instanceTagIndex += 1) {
          var tag = tags[instanceTagIndex];
          html += tag.toHTML(streamAttr);
        }

        /* force our StyleSheets to emit entirely new tags */
        instance.sealAllTags();

        var renderedHtml = chunk.toString();

        /* prepend style html to chunk, unless the start of the chunk is a closing tag in which case append right after that */
        if (CLOSING_TAG_R.test(renderedHtml)) {
          var endOfClosingTag = renderedHtml.indexOf('>');

          this.push(renderedHtml.slice(0, endOfClosingTag + 1) + html + renderedHtml.slice(endOfClosingTag + 1));
        } else this.push(html + renderedHtml);

        callback();
      }
    });

    readableStream.on('end', function () {
      return _this.seal();
    });

    readableStream.on('error', function (err) {
      _this.seal();

      // forward the error to the transform stream
      transformer.emit('error', err);
    });

    return readableStream.pipe(transformer);
  };

  return ServerStyleSheet;
}();

// 

var StyleSheetContext = Object(react__WEBPACK_IMPORTED_MODULE_2__["createContext"])();
var StyleSheetConsumer = StyleSheetContext.Consumer;

var StyleSheetManager = function (_Component) {
  inherits(StyleSheetManager, _Component);

  function StyleSheetManager(props) {
    classCallCheck(this, StyleSheetManager);

    var _this = possibleConstructorReturn(this, _Component.call(this, props));

    _this.getContext = Object(memoize_one__WEBPACK_IMPORTED_MODULE_5__["default"])(_this.getContext);
    return _this;
  }

  StyleSheetManager.prototype.getContext = function getContext(sheet, target) {
    if (sheet) {
      return sheet;
    } else if (target) {
      return new StyleSheet(target);
    } else {
      throw new StyledComponentsError(4);
    }
  };

  StyleSheetManager.prototype.render = function render() {
    var _props = this.props,
        children = _props.children,
        sheet = _props.sheet,
        target = _props.target;


    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(
      StyleSheetContext.Provider,
      { value: this.getContext(sheet, target) },
       true ? react__WEBPACK_IMPORTED_MODULE_2___default.a.Children.only(children) : undefined
    );
  };

  return StyleSheetManager;
}(react__WEBPACK_IMPORTED_MODULE_2__["Component"]);
 true ? StyleSheetManager.propTypes = {
  sheet: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.instanceOf(StyleSheet), prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.instanceOf(ServerStyleSheet)]),

  target: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.shape({
    appendChild: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func.isRequired
  })
} : undefined;

// 

var identifiers = {};

/* We depend on components having unique IDs */
function generateId(_ComponentStyle, _displayName, parentComponentId) {
  var displayName = typeof _displayName !== 'string' ? 'sc' : escape(_displayName);

  /**
   * This ensures uniqueness if two components happen to share
   * the same displayName.
   */
  var nr = (identifiers[displayName] || 0) + 1;
  identifiers[displayName] = nr;

  var componentId = displayName + '-' + _ComponentStyle.generateName(displayName + nr);

  return parentComponentId ? parentComponentId + '-' + componentId : componentId;
}

// $FlowFixMe

var StyledComponent = function (_Component) {
  inherits(StyledComponent, _Component);

  function StyledComponent() {
    classCallCheck(this, StyledComponent);

    var _this = possibleConstructorReturn(this, _Component.call(this));

    _this.attrs = {};

    _this.renderOuter = _this.renderOuter.bind(_this);
    _this.renderInner = _this.renderInner.bind(_this);

    if (true) {
      _this.warnInnerRef = once(function (displayName) {
        return (
          // eslint-disable-next-line no-console
          console.warn('The "innerRef" API has been removed in styled-components v4 in favor of React 16 ref forwarding, use "ref" instead like a typical component. "innerRef" was detected on component "' + displayName + '".')
        );
      });

      _this.warnAttrsFnObjectKeyDeprecated = once(function (key, displayName) {
        return (
          // eslint-disable-next-line no-console
          console.warn('Functions as object-form attrs({}) keys are now deprecated and will be removed in a future version of styled-components. Switch to the new attrs(props => ({})) syntax instead for easier and more powerful composition. The attrs key in question is "' + key + '" on component "' + displayName + '".', '\n ' + new Error().stack)
        );
      });

      _this.warnNonStyledComponentAttrsObjectKey = once(function (key, displayName) {
        return (
          // eslint-disable-next-line no-console
          console.warn('It looks like you\'ve used a non styled-component as the value for the "' + key + '" prop in an object-form attrs constructor of "' + displayName + '".\n' + 'You should use the new function-form attrs constructor which avoids this issue: attrs(props => ({ yourStuff }))\n' + "To continue using the deprecated object syntax, you'll need to wrap your component prop in a function to make it available inside the styled component (you'll still get the deprecation warning though.)\n" + ('For example, { ' + key + ': () => InnerComponent } instead of { ' + key + ': InnerComponent }'))
        );
      });
    }
    return _this;
  }

  StyledComponent.prototype.render = function render() {
    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(
      StyleSheetConsumer,
      null,
      this.renderOuter
    );
  };

  StyledComponent.prototype.renderOuter = function renderOuter() {
    var styleSheet = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : StyleSheet.master;

    this.styleSheet = styleSheet;

    // No need to subscribe a static component to theme changes, it won't change anything
    if (this.props.forwardedComponent.componentStyle.isStatic) return this.renderInner();

    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(
      ThemeConsumer,
      null,
      this.renderInner
    );
  };

  StyledComponent.prototype.renderInner = function renderInner(theme) {
    var _props$forwardedCompo = this.props.forwardedComponent,
        componentStyle = _props$forwardedCompo.componentStyle,
        defaultProps = _props$forwardedCompo.defaultProps,
        displayName = _props$forwardedCompo.displayName,
        foldedComponentIds = _props$forwardedCompo.foldedComponentIds,
        styledComponentId = _props$forwardedCompo.styledComponentId,
        target = _props$forwardedCompo.target;


    var generatedClassName = void 0;
    if (componentStyle.isStatic) {
      generatedClassName = this.generateAndInjectStyles(EMPTY_OBJECT, this.props);
    } else {
      generatedClassName = this.generateAndInjectStyles(determineTheme(this.props, theme, defaultProps) || EMPTY_OBJECT, this.props);
    }

    var elementToBeCreated = this.props.as || this.attrs.as || target;
    var isTargetTag = isTag(elementToBeCreated);

    var propsForElement = {};
    var computedProps = _extends({}, this.props, this.attrs);

    var key = void 0;
    // eslint-disable-next-line guard-for-in
    for (key in computedProps) {
      if ( true && key === 'innerRef' && isTargetTag) {
        this.warnInnerRef(displayName);
      }

      if (key === 'forwardedComponent' || key === 'as') {
        continue;
      } else if (key === 'forwardedRef') propsForElement.ref = computedProps[key];else if (key === 'forwardedAs') propsForElement.as = computedProps[key];else if (!isTargetTag || Object(_emotion_is_prop_valid__WEBPACK_IMPORTED_MODULE_7__["default"])(key)) {
        // Don't pass through non HTML tags through to HTML elements
        propsForElement[key] = computedProps[key];
      }
    }

    if (this.props.style && this.attrs.style) {
      propsForElement.style = _extends({}, this.attrs.style, this.props.style);
    }

    propsForElement.className = Array.prototype.concat(foldedComponentIds, styledComponentId, generatedClassName !== styledComponentId ? generatedClassName : null, this.props.className, this.attrs.className).filter(Boolean).join(' ');

    return Object(react__WEBPACK_IMPORTED_MODULE_2__["createElement"])(elementToBeCreated, propsForElement);
  };

  StyledComponent.prototype.buildExecutionContext = function buildExecutionContext(theme, props, attrs) {
    var _this2 = this;

    var context = _extends({}, props, { theme: theme });

    if (!attrs.length) return context;

    this.attrs = {};

    attrs.forEach(function (attrDef) {
      var resolvedAttrDef = attrDef;
      var attrDefWasFn = false;
      var attr = void 0;
      var key = void 0;

      if (isFunction(resolvedAttrDef)) {
        // $FlowFixMe
        resolvedAttrDef = resolvedAttrDef(context);
        attrDefWasFn = true;
      }

      /* eslint-disable guard-for-in */
      // $FlowFixMe
      for (key in resolvedAttrDef) {
        attr = resolvedAttrDef[key];

        if (!attrDefWasFn) {
          if (isFunction(attr) && !isDerivedReactComponent(attr) && !isStyledComponent(attr)) {
            if (true) {
              _this2.warnAttrsFnObjectKeyDeprecated(key, props.forwardedComponent.displayName);
            }

            attr = attr(context);

            if ( true && react__WEBPACK_IMPORTED_MODULE_2___default.a.isValidElement(attr)) {
              _this2.warnNonStyledComponentAttrsObjectKey(key, props.forwardedComponent.displayName);
            }
          }
        }

        _this2.attrs[key] = attr;
        context[key] = attr;
      }
      /* eslint-enable */
    });

    return context;
  };

  StyledComponent.prototype.generateAndInjectStyles = function generateAndInjectStyles(theme, props) {
    var _props$forwardedCompo2 = props.forwardedComponent,
        attrs = _props$forwardedCompo2.attrs,
        componentStyle = _props$forwardedCompo2.componentStyle,
        warnTooManyClasses = _props$forwardedCompo2.warnTooManyClasses;

    // statically styled-components don't need to build an execution context object,
    // and shouldn't be increasing the number of class names

    if (componentStyle.isStatic && !attrs.length) {
      return componentStyle.generateAndInjectStyles(EMPTY_OBJECT, this.styleSheet);
    }

    var className = componentStyle.generateAndInjectStyles(this.buildExecutionContext(theme, props, attrs), this.styleSheet);

    if ( true && warnTooManyClasses) warnTooManyClasses(className);

    return className;
  };

  return StyledComponent;
}(react__WEBPACK_IMPORTED_MODULE_2__["Component"]);

function createStyledComponent(target, options, rules) {
  var isTargetStyledComp = isStyledComponent(target);
  var isClass = !isTag(target);

  var _options$displayName = options.displayName,
      displayName = _options$displayName === undefined ? generateDisplayName(target) : _options$displayName,
      _options$componentId = options.componentId,
      componentId = _options$componentId === undefined ? generateId(ComponentStyle, options.displayName, options.parentComponentId) : _options$componentId,
      _options$ParentCompon = options.ParentComponent,
      ParentComponent = _options$ParentCompon === undefined ? StyledComponent : _options$ParentCompon,
      _options$attrs = options.attrs,
      attrs = _options$attrs === undefined ? EMPTY_ARRAY : _options$attrs;


  var styledComponentId = options.displayName && options.componentId ? escape(options.displayName) + '-' + options.componentId : options.componentId || componentId;

  // fold the underlying StyledComponent attrs up (implicit extend)
  var finalAttrs =
  // $FlowFixMe
  isTargetStyledComp && target.attrs ? Array.prototype.concat(target.attrs, attrs).filter(Boolean) : attrs;

  var componentStyle = new ComponentStyle(isTargetStyledComp ? // fold the underlying StyledComponent rules up (implicit extend)
  // $FlowFixMe
  target.componentStyle.rules.concat(rules) : rules, finalAttrs, styledComponentId);

  /**
   * forwardRef creates a new interim component, which we'll take advantage of
   * instead of extending ParentComponent to create _another_ interim class
   */
  var WrappedStyledComponent = void 0;
  var forwardRef = function forwardRef(props, ref) {
    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(ParentComponent, _extends({}, props, { forwardedComponent: WrappedStyledComponent, forwardedRef: ref }));
  };
  forwardRef.displayName = displayName;
  WrappedStyledComponent = react__WEBPACK_IMPORTED_MODULE_2___default.a.forwardRef(forwardRef);
  WrappedStyledComponent.displayName = displayName;

  // $FlowFixMe
  WrappedStyledComponent.attrs = finalAttrs;
  // $FlowFixMe
  WrappedStyledComponent.componentStyle = componentStyle;

  // $FlowFixMe
  WrappedStyledComponent.foldedComponentIds = isTargetStyledComp ? // $FlowFixMe
  Array.prototype.concat(target.foldedComponentIds, target.styledComponentId) : EMPTY_ARRAY;

  // $FlowFixMe
  WrappedStyledComponent.styledComponentId = styledComponentId;

  // fold the underlying StyledComponent target up since we folded the styles
  // $FlowFixMe
  WrappedStyledComponent.target = isTargetStyledComp ? target.target : target;

  // $FlowFixMe
  WrappedStyledComponent.withComponent = function withComponent(tag) {
    var previousComponentId = options.componentId,
        optionsToCopy = objectWithoutProperties(options, ['componentId']);


    var newComponentId = previousComponentId && previousComponentId + '-' + (isTag(tag) ? tag : escape(getComponentName(tag)));

    var newOptions = _extends({}, optionsToCopy, {
      attrs: finalAttrs,
      componentId: newComponentId,
      ParentComponent: ParentComponent
    });

    return createStyledComponent(tag, newOptions, rules);
  };

  // $FlowFixMe
  Object.defineProperty(WrappedStyledComponent, 'defaultProps', {
    get: function get$$1() {
      return this._foldedDefaultProps;
    },
    set: function set$$1(obj) {
      // $FlowFixMe
      this._foldedDefaultProps = isTargetStyledComp ? Object(merge_anything__WEBPACK_IMPORTED_MODULE_8__["default"])(target.defaultProps, obj) : obj;
    }
  });

  if (true) {
    // $FlowFixMe
    WrappedStyledComponent.warnTooManyClasses = createWarnTooManyClasses(displayName);
  }

  // $FlowFixMe
  WrappedStyledComponent.toString = function () {
    return '.' + WrappedStyledComponent.styledComponentId;
  };

  if (isClass) {
    hoistNonReactStatics(WrappedStyledComponent, target, {
      // all SC-specific things should not be hoisted
      attrs: true,
      componentStyle: true,
      displayName: true,
      foldedComponentIds: true,
      styledComponentId: true,
      target: true,
      withComponent: true
    });
  }

  return WrappedStyledComponent;
}

// 
// Thanks to ReactDOMFactories for this handy list!

var domElements = ['a', 'abbr', 'address', 'area', 'article', 'aside', 'audio', 'b', 'base', 'bdi', 'bdo', 'big', 'blockquote', 'body', 'br', 'button', 'canvas', 'caption', 'cite', 'code', 'col', 'colgroup', 'data', 'datalist', 'dd', 'del', 'details', 'dfn', 'dialog', 'div', 'dl', 'dt', 'em', 'embed', 'fieldset', 'figcaption', 'figure', 'footer', 'form', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head', 'header', 'hgroup', 'hr', 'html', 'i', 'iframe', 'img', 'input', 'ins', 'kbd', 'keygen', 'label', 'legend', 'li', 'link', 'main', 'map', 'mark', 'marquee', 'menu', 'menuitem', 'meta', 'meter', 'nav', 'noscript', 'object', 'ol', 'optgroup', 'option', 'output', 'p', 'param', 'picture', 'pre', 'progress', 'q', 'rp', 'rt', 'ruby', 's', 'samp', 'script', 'section', 'select', 'small', 'source', 'span', 'strong', 'style', 'sub', 'summary', 'sup', 'table', 'tbody', 'td', 'textarea', 'tfoot', 'th', 'thead', 'time', 'title', 'tr', 'track', 'u', 'ul', 'var', 'video', 'wbr',

// SVG
'circle', 'clipPath', 'defs', 'ellipse', 'foreignObject', 'g', 'image', 'line', 'linearGradient', 'marker', 'mask', 'path', 'pattern', 'polygon', 'polyline', 'radialGradient', 'rect', 'stop', 'svg', 'text', 'tspan'];

// 

var styled = function styled(tag) {
  return constructWithOptions(createStyledComponent, tag);
};

// Shorthands for all valid HTML Elements
domElements.forEach(function (domElement) {
  styled[domElement] = styled(domElement);
});

// 

var GlobalStyle = function () {
  function GlobalStyle(rules, componentId) {
    classCallCheck(this, GlobalStyle);

    this.rules = rules;
    this.componentId = componentId;
    this.isStatic = isStaticRules(rules, EMPTY_ARRAY);

    if (!StyleSheet.master.hasId(componentId)) {
      StyleSheet.master.deferredInject(componentId, []);
    }
  }

  GlobalStyle.prototype.createStyles = function createStyles(executionContext, styleSheet) {
    var flatCSS = flatten(this.rules, executionContext, styleSheet);
    var css = stringifyRules(flatCSS, '');

    styleSheet.inject(this.componentId, css);
  };

  GlobalStyle.prototype.removeStyles = function removeStyles(styleSheet) {
    var componentId = this.componentId;

    if (styleSheet.hasId(componentId)) {
      styleSheet.remove(componentId);
    }
  };

  // TODO: overwrite in-place instead of remove+create?


  GlobalStyle.prototype.renderStyles = function renderStyles(executionContext, styleSheet) {
    this.removeStyles(styleSheet);
    this.createStyles(executionContext, styleSheet);
  };

  return GlobalStyle;
}();

// 

// place our cache into shared context so it'll persist between HMRs
if (IS_BROWSER) {
  window.scCGSHMRCache = {};
}

function createGlobalStyle(strings) {
  for (var _len = arguments.length, interpolations = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    interpolations[_key - 1] = arguments[_key];
  }

  var rules = css.apply(undefined, [strings].concat(interpolations));
  var id = 'sc-global-' + murmurhash(JSON.stringify(rules));
  var style = new GlobalStyle(rules, id);

  var GlobalStyleComponent = function (_React$Component) {
    inherits(GlobalStyleComponent, _React$Component);

    function GlobalStyleComponent(props) {
      classCallCheck(this, GlobalStyleComponent);

      var _this = possibleConstructorReturn(this, _React$Component.call(this, props));

      var _this$constructor = _this.constructor,
          globalStyle = _this$constructor.globalStyle,
          styledComponentId = _this$constructor.styledComponentId;


      if (IS_BROWSER) {
        window.scCGSHMRCache[styledComponentId] = (window.scCGSHMRCache[styledComponentId] || 0) + 1;
      }

      /**
       * This fixes HMR compatibility. Don't ask me why, but this combination of
       * caching the closure variables via statics and then persisting the statics in
       * state works across HMR where no other combination did. ¯\_(ツ)_/¯
       */
      _this.state = {
        globalStyle: globalStyle,
        styledComponentId: styledComponentId
      };
      return _this;
    }

    GlobalStyleComponent.prototype.componentWillUnmount = function componentWillUnmount() {
      if (window.scCGSHMRCache[this.state.styledComponentId]) {
        window.scCGSHMRCache[this.state.styledComponentId] -= 1;
      }
      /**
       * Depending on the order "render" is called this can cause the styles to be lost
       * until the next render pass of the remaining instance, which may
       * not be immediate.
       */
      if (window.scCGSHMRCache[this.state.styledComponentId] === 0) {
        this.state.globalStyle.removeStyles(this.styleSheet);
      }
    };

    GlobalStyleComponent.prototype.render = function render() {
      var _this2 = this;

      if ( true && react__WEBPACK_IMPORTED_MODULE_2___default.a.Children.count(this.props.children)) {
        // eslint-disable-next-line no-console
        console.warn('The global style component ' + this.state.styledComponentId + ' was given child JSX. createGlobalStyle does not render children.');
      }

      return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(
        StyleSheetConsumer,
        null,
        function (styleSheet) {
          _this2.styleSheet = styleSheet || StyleSheet.master;

          var globalStyle = _this2.state.globalStyle;


          if (globalStyle.isStatic) {
            globalStyle.renderStyles(STATIC_EXECUTION_CONTEXT, _this2.styleSheet);

            return null;
          } else {
            return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(
              ThemeConsumer,
              null,
              function (theme) {
                // $FlowFixMe
                var defaultProps = _this2.constructor.defaultProps;


                var context = _extends({}, _this2.props);

                if (typeof theme !== 'undefined') {
                  context.theme = determineTheme(_this2.props, theme, defaultProps);
                }

                globalStyle.renderStyles(context, _this2.styleSheet);

                return null;
              }
            );
          }
        }
      );
    };

    return GlobalStyleComponent;
  }(react__WEBPACK_IMPORTED_MODULE_2___default.a.Component);

  GlobalStyleComponent.globalStyle = style;
  GlobalStyleComponent.styledComponentId = id;


  return GlobalStyleComponent;
}

// 

var replaceWhitespace = function replaceWhitespace(str) {
  return str.replace(/\s|\\n/g, '');
};

function keyframes(strings) {
  /* Warning if you've used keyframes on React Native */
  if ( true && typeof navigator !== 'undefined' && navigator.product === 'ReactNative') {
    // eslint-disable-next-line no-console
    console.warn('`keyframes` cannot be used on ReactNative, only on the web. To do animation in ReactNative please use Animated.');
  }

  for (var _len = arguments.length, interpolations = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    interpolations[_key - 1] = arguments[_key];
  }

  var rules = css.apply(undefined, [strings].concat(interpolations));

  var name = generateAlphabeticName(murmurhash(replaceWhitespace(JSON.stringify(rules))));

  return new Keyframes(name, stringifyRules(rules, name, '@keyframes'));
}

// 

var withTheme = (function (Component$$1) {
  var WithTheme = react__WEBPACK_IMPORTED_MODULE_2___default.a.forwardRef(function (props, ref) {
    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(
      ThemeConsumer,
      null,
      function (theme) {
        // $FlowFixMe
        var defaultProps = Component$$1.defaultProps;

        var themeProp = determineTheme(props, theme, defaultProps);

        if ( true && themeProp === undefined) {
          // eslint-disable-next-line no-console
          console.warn('[withTheme] You are not using a ThemeProvider nor passing a theme prop or a theme in defaultProps in component class "' + getComponentName(Component$$1) + '"');
        }

        return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(Component$$1, _extends({}, props, { theme: themeProp, ref: ref }));
      }
    );
  });

  hoistNonReactStatics(WithTheme, Component$$1);

  WithTheme.displayName = 'WithTheme(' + getComponentName(Component$$1) + ')';

  return WithTheme;
});

// 

/* eslint-disable */
var __DO_NOT_USE_OR_YOU_WILL_BE_HAUNTED_BY_SPOOKY_GHOSTS = {
  StyleSheet: StyleSheet
};

// 

/* Warning if you've imported this file on React Native */
if ( true && typeof navigator !== 'undefined' && navigator.product === 'ReactNative') {
  // eslint-disable-next-line no-console
  console.warn("It looks like you've imported 'styled-components' on React Native.\n" + "Perhaps you're looking to import 'styled-components/native'?\n" + 'Read more about this at https://www.styled-components.com/docs/basics#react-native');
}

/* Warning if there are several instances of styled-components */
if ( true && typeof window !== 'undefined' && typeof navigator !== 'undefined' && typeof navigator.userAgent === 'string' && navigator.userAgent.indexOf('Node.js') === -1 && navigator.userAgent.indexOf('jsdom') === -1) {
  window['__styled-components-init__'] = window['__styled-components-init__'] || 0;

  if (window['__styled-components-init__'] === 1) {
    // eslint-disable-next-line no-console
    console.warn("It looks like there are several instances of 'styled-components' initialized in this application. " + 'This may cause dynamic styles not rendering properly, errors happening during rehydration process ' + 'and makes your application bigger without a good reason.\n\n' + 'See https://s-c.sh/2BAXzed for more info.');
  }

  window['__styled-components-init__'] += 1;
}

//

/* harmony default export */ __webpack_exports__["default"] = (styled);

//# sourceMappingURL=styled-components.browser.esm.js.map

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/styled-jsx/dist/lib/stylesheet.js":
/*!********************************************************!*\
  !*** ./node_modules/styled-jsx/dist/lib/stylesheet.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

exports.__esModule = true;
exports["default"] = void 0;

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/*
Based on Glamor's sheet
https://github.com/threepointone/glamor/blob/667b480d31b3721a905021b26e1290ce92ca2879/src/sheet.js
*/
var isProd = typeof process !== 'undefined' && process.env && "development" === 'production';

var isString = function isString(o) {
  return Object.prototype.toString.call(o) === '[object String]';
};

var StyleSheet =
/*#__PURE__*/
function () {
  function StyleSheet(_temp) {
    var _ref = _temp === void 0 ? {} : _temp,
        _ref$name = _ref.name,
        name = _ref$name === void 0 ? 'stylesheet' : _ref$name,
        _ref$optimizeForSpeed = _ref.optimizeForSpeed,
        optimizeForSpeed = _ref$optimizeForSpeed === void 0 ? isProd : _ref$optimizeForSpeed,
        _ref$isBrowser = _ref.isBrowser,
        isBrowser = _ref$isBrowser === void 0 ? typeof window !== 'undefined' : _ref$isBrowser;

    invariant(isString(name), '`name` must be a string');
    this._name = name;
    this._deletedRulePlaceholder = "#" + name + "-deleted-rule____{}";
    invariant(typeof optimizeForSpeed === 'boolean', '`optimizeForSpeed` must be a boolean');
    this._optimizeForSpeed = optimizeForSpeed;
    this._isBrowser = isBrowser;
    this._serverSheet = undefined;
    this._tags = [];
    this._injected = false;
    this._rulesCount = 0;
    var node = this._isBrowser && document.querySelector('meta[property="csp-nonce"]');
    this._nonce = node ? node.getAttribute('content') : null;
  }

  var _proto = StyleSheet.prototype;

  _proto.setOptimizeForSpeed = function setOptimizeForSpeed(bool) {
    invariant(typeof bool === 'boolean', '`setOptimizeForSpeed` accepts a boolean');
    invariant(this._rulesCount === 0, 'optimizeForSpeed cannot be when rules have already been inserted');
    this.flush();
    this._optimizeForSpeed = bool;
    this.inject();
  };

  _proto.isOptimizeForSpeed = function isOptimizeForSpeed() {
    return this._optimizeForSpeed;
  };

  _proto.inject = function inject() {
    var _this = this;

    invariant(!this._injected, 'sheet already injected');
    this._injected = true;

    if (this._isBrowser && this._optimizeForSpeed) {
      this._tags[0] = this.makeStyleTag(this._name);
      this._optimizeForSpeed = 'insertRule' in this.getSheet();

      if (!this._optimizeForSpeed) {
        if (!isProd) {
          console.warn('StyleSheet: optimizeForSpeed mode not supported falling back to standard mode.');
        }

        this.flush();
        this._injected = true;
      }

      return;
    }

    this._serverSheet = {
      cssRules: [],
      insertRule: function insertRule(rule, index) {
        if (typeof index === 'number') {
          _this._serverSheet.cssRules[index] = {
            cssText: rule
          };
        } else {
          _this._serverSheet.cssRules.push({
            cssText: rule
          });
        }

        return index;
      },
      deleteRule: function deleteRule(index) {
        _this._serverSheet.cssRules[index] = null;
      }
    };
  };

  _proto.getSheetForTag = function getSheetForTag(tag) {
    if (tag.sheet) {
      return tag.sheet;
    } // this weirdness brought to you by firefox


    for (var i = 0; i < document.styleSheets.length; i++) {
      if (document.styleSheets[i].ownerNode === tag) {
        return document.styleSheets[i];
      }
    }
  };

  _proto.getSheet = function getSheet() {
    return this.getSheetForTag(this._tags[this._tags.length - 1]);
  };

  _proto.insertRule = function insertRule(rule, index) {
    invariant(isString(rule), '`insertRule` accepts only strings');

    if (!this._isBrowser) {
      if (typeof index !== 'number') {
        index = this._serverSheet.cssRules.length;
      }

      this._serverSheet.insertRule(rule, index);

      return this._rulesCount++;
    }

    if (this._optimizeForSpeed) {
      var sheet = this.getSheet();

      if (typeof index !== 'number') {
        index = sheet.cssRules.length;
      } // this weirdness for perf, and chrome's weird bug
      // https://stackoverflow.com/questions/20007992/chrome-suddenly-stopped-accepting-insertrule


      try {
        sheet.insertRule(rule, index);
      } catch (error) {
        if (!isProd) {
          console.warn("StyleSheet: illegal rule: \n\n" + rule + "\n\nSee https://stackoverflow.com/q/20007992 for more info");
        }

        return -1;
      }
    } else {
      var insertionPoint = this._tags[index];

      this._tags.push(this.makeStyleTag(this._name, rule, insertionPoint));
    }

    return this._rulesCount++;
  };

  _proto.replaceRule = function replaceRule(index, rule) {
    if (this._optimizeForSpeed || !this._isBrowser) {
      var sheet = this._isBrowser ? this.getSheet() : this._serverSheet;

      if (!rule.trim()) {
        rule = this._deletedRulePlaceholder;
      }

      if (!sheet.cssRules[index]) {
        // @TBD Should we throw an error?
        return index;
      }

      sheet.deleteRule(index);

      try {
        sheet.insertRule(rule, index);
      } catch (error) {
        if (!isProd) {
          console.warn("StyleSheet: illegal rule: \n\n" + rule + "\n\nSee https://stackoverflow.com/q/20007992 for more info");
        } // In order to preserve the indices we insert a deleteRulePlaceholder


        sheet.insertRule(this._deletedRulePlaceholder, index);
      }
    } else {
      var tag = this._tags[index];
      invariant(tag, "old rule at index `" + index + "` not found");
      tag.textContent = rule;
    }

    return index;
  };

  _proto.deleteRule = function deleteRule(index) {
    if (!this._isBrowser) {
      this._serverSheet.deleteRule(index);

      return;
    }

    if (this._optimizeForSpeed) {
      this.replaceRule(index, '');
    } else {
      var tag = this._tags[index];
      invariant(tag, "rule at index `" + index + "` not found");
      tag.parentNode.removeChild(tag);
      this._tags[index] = null;
    }
  };

  _proto.flush = function flush() {
    this._injected = false;
    this._rulesCount = 0;

    if (this._isBrowser) {
      this._tags.forEach(function (tag) {
        return tag && tag.parentNode.removeChild(tag);
      });

      this._tags = [];
    } else {
      // simpler on server
      this._serverSheet.cssRules = [];
    }
  };

  _proto.cssRules = function cssRules() {
    var _this2 = this;

    if (!this._isBrowser) {
      return this._serverSheet.cssRules;
    }

    return this._tags.reduce(function (rules, tag) {
      if (tag) {
        rules = rules.concat(Array.prototype.map.call(_this2.getSheetForTag(tag).cssRules, function (rule) {
          return rule.cssText === _this2._deletedRulePlaceholder ? null : rule;
        }));
      } else {
        rules.push(null);
      }

      return rules;
    }, []);
  };

  _proto.makeStyleTag = function makeStyleTag(name, cssString, relativeToTag) {
    if (cssString) {
      invariant(isString(cssString), 'makeStyleTag acceps only strings as second parameter');
    }

    var tag = document.createElement('style');
    if (this._nonce) tag.setAttribute('nonce', this._nonce);
    tag.type = 'text/css';
    tag.setAttribute("data-" + name, '');

    if (cssString) {
      tag.appendChild(document.createTextNode(cssString));
    }

    var head = document.head || document.getElementsByTagName('head')[0];

    if (relativeToTag) {
      head.insertBefore(tag, relativeToTag);
    } else {
      head.appendChild(tag);
    }

    return tag;
  };

  _createClass(StyleSheet, [{
    key: "length",
    get: function get() {
      return this._rulesCount;
    }
  }]);

  return StyleSheet;
}();

exports["default"] = StyleSheet;

function invariant(condition, message) {
  if (!condition) {
    throw new Error("StyleSheet: " + message + ".");
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/styled-jsx/dist/style.js":
/*!***********************************************!*\
  !*** ./node_modules/styled-jsx/dist/style.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.flush = flush;
exports["default"] = void 0;

var _react = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var _stylesheetRegistry = _interopRequireDefault(__webpack_require__(/*! ./stylesheet-registry */ "./node_modules/styled-jsx/dist/stylesheet-registry.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _inheritsLoose(subClass, superClass) { subClass.prototype = Object.create(superClass.prototype); subClass.prototype.constructor = subClass; subClass.__proto__ = superClass; }

var styleSheetRegistry = new _stylesheetRegistry["default"]();

var JSXStyle =
/*#__PURE__*/
function (_Component) {
  _inheritsLoose(JSXStyle, _Component);

  function JSXStyle(props) {
    var _this;

    _this = _Component.call(this, props) || this;
    _this.prevProps = {};
    return _this;
  }

  JSXStyle.dynamic = function dynamic(info) {
    return info.map(function (tagInfo) {
      var baseId = tagInfo[0];
      var props = tagInfo[1];
      return styleSheetRegistry.computeId(baseId, props);
    }).join(' ');
  } // probably faster than PureComponent (shallowEqual)
  ;

  var _proto = JSXStyle.prototype;

  _proto.shouldComponentUpdate = function shouldComponentUpdate(otherProps) {
    return this.props.id !== otherProps.id || // We do this check because `dynamic` is an array of strings or undefined.
    // These are the computed values for dynamic styles.
    String(this.props.dynamic) !== String(otherProps.dynamic);
  };

  _proto.componentWillUnmount = function componentWillUnmount() {
    styleSheetRegistry.remove(this.props);
  };

  _proto.render = function render() {
    // This is a workaround to make the side effect async safe in the "render" phase.
    // See https://github.com/zeit/styled-jsx/pull/484
    if (this.shouldComponentUpdate(this.prevProps)) {
      // Updates
      if (this.prevProps.id) {
        styleSheetRegistry.remove(this.prevProps);
      }

      styleSheetRegistry.add(this.props);
      this.prevProps = this.props;
    }

    return null;
  };

  return JSXStyle;
}(_react.Component);

exports["default"] = JSXStyle;

function flush() {
  var cssRules = styleSheetRegistry.cssRules();
  styleSheetRegistry.flush();
  return cssRules;
}

/***/ }),

/***/ "./node_modules/styled-jsx/dist/stylesheet-registry.js":
/*!*************************************************************!*\
  !*** ./node_modules/styled-jsx/dist/stylesheet-registry.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports["default"] = void 0;

var _stringHash = _interopRequireDefault(__webpack_require__(/*! string-hash */ "./node_modules/string-hash/index.js"));

var _stylesheet = _interopRequireDefault(__webpack_require__(/*! ./lib/stylesheet */ "./node_modules/styled-jsx/dist/lib/stylesheet.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var sanitize = function sanitize(rule) {
  return rule.replace(/\/style/gi, '\\/style');
};

var StyleSheetRegistry =
/*#__PURE__*/
function () {
  function StyleSheetRegistry(_temp) {
    var _ref = _temp === void 0 ? {} : _temp,
        _ref$styleSheet = _ref.styleSheet,
        styleSheet = _ref$styleSheet === void 0 ? null : _ref$styleSheet,
        _ref$optimizeForSpeed = _ref.optimizeForSpeed,
        optimizeForSpeed = _ref$optimizeForSpeed === void 0 ? false : _ref$optimizeForSpeed,
        _ref$isBrowser = _ref.isBrowser,
        isBrowser = _ref$isBrowser === void 0 ? typeof window !== 'undefined' : _ref$isBrowser;

    this._sheet = styleSheet || new _stylesheet["default"]({
      name: 'styled-jsx',
      optimizeForSpeed: optimizeForSpeed
    });

    this._sheet.inject();

    if (styleSheet && typeof optimizeForSpeed === 'boolean') {
      this._sheet.setOptimizeForSpeed(optimizeForSpeed);

      this._optimizeForSpeed = this._sheet.isOptimizeForSpeed();
    }

    this._isBrowser = isBrowser;
    this._fromServer = undefined;
    this._indices = {};
    this._instancesCounts = {};
    this.computeId = this.createComputeId();
    this.computeSelector = this.createComputeSelector();
  }

  var _proto = StyleSheetRegistry.prototype;

  _proto.add = function add(props) {
    var _this = this;

    if (undefined === this._optimizeForSpeed) {
      this._optimizeForSpeed = Array.isArray(props.children);

      this._sheet.setOptimizeForSpeed(this._optimizeForSpeed);

      this._optimizeForSpeed = this._sheet.isOptimizeForSpeed();
    }

    if (this._isBrowser && !this._fromServer) {
      this._fromServer = this.selectFromServer();
      this._instancesCounts = Object.keys(this._fromServer).reduce(function (acc, tagName) {
        acc[tagName] = 0;
        return acc;
      }, {});
    }

    var _this$getIdAndRules = this.getIdAndRules(props),
        styleId = _this$getIdAndRules.styleId,
        rules = _this$getIdAndRules.rules; // Deduping: just increase the instances count.


    if (styleId in this._instancesCounts) {
      this._instancesCounts[styleId] += 1;
      return;
    }

    var indices = rules.map(function (rule) {
      return _this._sheet.insertRule(rule);
    }) // Filter out invalid rules
    .filter(function (index) {
      return index !== -1;
    });
    this._indices[styleId] = indices;
    this._instancesCounts[styleId] = 1;
  };

  _proto.remove = function remove(props) {
    var _this2 = this;

    var _this$getIdAndRules2 = this.getIdAndRules(props),
        styleId = _this$getIdAndRules2.styleId;

    invariant(styleId in this._instancesCounts, "styleId: `" + styleId + "` not found");
    this._instancesCounts[styleId] -= 1;

    if (this._instancesCounts[styleId] < 1) {
      var tagFromServer = this._fromServer && this._fromServer[styleId];

      if (tagFromServer) {
        tagFromServer.parentNode.removeChild(tagFromServer);
        delete this._fromServer[styleId];
      } else {
        this._indices[styleId].forEach(function (index) {
          return _this2._sheet.deleteRule(index);
        });

        delete this._indices[styleId];
      }

      delete this._instancesCounts[styleId];
    }
  };

  _proto.update = function update(props, nextProps) {
    this.add(nextProps);
    this.remove(props);
  };

  _proto.flush = function flush() {
    this._sheet.flush();

    this._sheet.inject();

    this._fromServer = undefined;
    this._indices = {};
    this._instancesCounts = {};
    this.computeId = this.createComputeId();
    this.computeSelector = this.createComputeSelector();
  };

  _proto.cssRules = function cssRules() {
    var _this3 = this;

    var fromServer = this._fromServer ? Object.keys(this._fromServer).map(function (styleId) {
      return [styleId, _this3._fromServer[styleId]];
    }) : [];

    var cssRules = this._sheet.cssRules();

    return fromServer.concat(Object.keys(this._indices).map(function (styleId) {
      return [styleId, _this3._indices[styleId].map(function (index) {
        return cssRules[index].cssText;
      }).join(_this3._optimizeForSpeed ? '' : '\n')];
    }) // filter out empty rules
    .filter(function (rule) {
      return Boolean(rule[1]);
    }));
  }
  /**
   * createComputeId
   *
   * Creates a function to compute and memoize a jsx id from a basedId and optionally props.
   */
  ;

  _proto.createComputeId = function createComputeId() {
    var cache = {};
    return function (baseId, props) {
      if (!props) {
        return "jsx-" + baseId;
      }

      var propsToString = String(props);
      var key = baseId + propsToString; // return `jsx-${hashString(`${baseId}-${propsToString}`)}`

      if (!cache[key]) {
        cache[key] = "jsx-" + (0, _stringHash["default"])(baseId + "-" + propsToString);
      }

      return cache[key];
    };
  }
  /**
   * createComputeSelector
   *
   * Creates a function to compute and memoize dynamic selectors.
   */
  ;

  _proto.createComputeSelector = function createComputeSelector(selectoPlaceholderRegexp) {
    if (selectoPlaceholderRegexp === void 0) {
      selectoPlaceholderRegexp = /__jsx-style-dynamic-selector/g;
    }

    var cache = {};
    return function (id, css) {
      // Sanitize SSR-ed CSS.
      // Client side code doesn't need to be sanitized since we use
      // document.createTextNode (dev) and the CSSOM api sheet.insertRule (prod).
      if (!this._isBrowser) {
        css = sanitize(css);
      }

      var idcss = id + css;

      if (!cache[idcss]) {
        cache[idcss] = css.replace(selectoPlaceholderRegexp, id);
      }

      return cache[idcss];
    };
  };

  _proto.getIdAndRules = function getIdAndRules(props) {
    var _this4 = this;

    var css = props.children,
        dynamic = props.dynamic,
        id = props.id;

    if (dynamic) {
      var styleId = this.computeId(id, dynamic);
      return {
        styleId: styleId,
        rules: Array.isArray(css) ? css.map(function (rule) {
          return _this4.computeSelector(styleId, rule);
        }) : [this.computeSelector(styleId, css)]
      };
    }

    return {
      styleId: this.computeId(id),
      rules: Array.isArray(css) ? css : [css]
    };
  }
  /**
   * selectFromServer
   *
   * Collects style tags from the document with id __jsx-XXX
   */
  ;

  _proto.selectFromServer = function selectFromServer() {
    var elements = Array.prototype.slice.call(document.querySelectorAll('[id^="__jsx-"]'));
    return elements.reduce(function (acc, element) {
      var id = element.id.slice(2);
      acc[id] = element;
      return acc;
    }, {});
  };

  return StyleSheetRegistry;
}();

exports["default"] = StyleSheetRegistry;

function invariant(condition, message) {
  if (!condition) {
    throw new Error("StyleSheetRegistry: " + message + ".");
  }
}

/***/ }),

/***/ "./node_modules/styled-jsx/style.js":
/*!******************************************!*\
  !*** ./node_modules/styled-jsx/style.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/style */ "./node_modules/styled-jsx/dist/style.js")


/***/ }),

/***/ "./node_modules/stylis-rule-sheet/index.js":
/*!*************************************************!*\
  !*** ./node_modules/stylis-rule-sheet/index.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function (factory) {
	 true ? (module['exports'] = factory()) :
		undefined
}(function () {

	'use strict'

	return function (insertRule) {
		var delimiter = '/*|*/'
		var needle = delimiter+'}'

		function toSheet (block) {
			if (block)
				try {
					insertRule(block + '}')
				} catch (e) {}
		}

		return function ruleSheet (context, content, selectors, parents, line, column, length, ns, depth, at) {
			switch (context) {
				// property
				case 1:
					// @import
					if (depth === 0 && content.charCodeAt(0) === 64)
						return insertRule(content+';'), ''
					break
				// selector
				case 2:
					if (ns === 0)
						return content + delimiter
					break
				// at-rule
				case 3:
					switch (ns) {
						// @font-face, @page
						case 102:
						case 112:
							return insertRule(selectors[0]+content), ''
						default:
							return content + (at === 0 ? delimiter : '')
					}
				case -2:
					content.split(needle).forEach(toSheet)
			}
		}
	}
}))


/***/ }),

/***/ "./node_modules/stylis/stylis.min.js":
/*!*******************************************!*\
  !*** ./node_modules/stylis/stylis.min.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

!function(e){ true?module.exports=e(null):undefined}(function e(a){"use strict";var r=/^\0+/g,c=/[\0\r\f]/g,s=/: */g,t=/zoo|gra/,i=/([,: ])(transform)/g,f=/,+\s*(?![^(]*[)])/g,n=/ +\s*(?![^(]*[)])/g,l=/ *[\0] */g,o=/,\r+?/g,h=/([\t\r\n ])*\f?&/g,u=/:global\(((?:[^\(\)\[\]]*|\[.*\]|\([^\(\)]*\))*)\)/g,d=/\W+/g,b=/@(k\w+)\s*(\S*)\s*/,p=/::(place)/g,k=/:(read-only)/g,g=/\s+(?=[{\];=:>])/g,A=/([[}=:>])\s+/g,C=/(\{[^{]+?);(?=\})/g,w=/\s{2,}/g,v=/([^\(])(:+) */g,m=/[svh]\w+-[tblr]{2}/,x=/\(\s*(.*)\s*\)/g,$=/([\s\S]*?);/g,y=/-self|flex-/g,O=/[^]*?(:[rp][el]a[\w-]+)[^]*/,j=/stretch|:\s*\w+\-(?:conte|avail)/,z=/([^-])(image-set\()/,N="-webkit-",S="-moz-",F="-ms-",W=59,q=125,B=123,D=40,E=41,G=91,H=93,I=10,J=13,K=9,L=64,M=32,P=38,Q=45,R=95,T=42,U=44,V=58,X=39,Y=34,Z=47,_=62,ee=43,ae=126,re=0,ce=12,se=11,te=107,ie=109,fe=115,ne=112,le=111,oe=105,he=99,ue=100,de=112,be=1,pe=1,ke=0,ge=1,Ae=1,Ce=1,we=0,ve=0,me=0,xe=[],$e=[],ye=0,Oe=null,je=-2,ze=-1,Ne=0,Se=1,Fe=2,We=3,qe=0,Be=1,De="",Ee="",Ge="";function He(e,a,s,t,i){for(var f,n,o=0,h=0,u=0,d=0,g=0,A=0,C=0,w=0,m=0,$=0,y=0,O=0,j=0,z=0,R=0,we=0,$e=0,Oe=0,je=0,ze=s.length,Je=ze-1,Re="",Te="",Ue="",Ve="",Xe="",Ye="";R<ze;){if(C=s.charCodeAt(R),R===Je)if(h+d+u+o!==0){if(0!==h)C=h===Z?I:Z;d=u=o=0,ze++,Je++}if(h+d+u+o===0){if(R===Je){if(we>0)Te=Te.replace(c,"");if(Te.trim().length>0){switch(C){case M:case K:case W:case J:case I:break;default:Te+=s.charAt(R)}C=W}}if(1===$e)switch(C){case B:case q:case W:case Y:case X:case D:case E:case U:$e=0;case K:case J:case I:case M:break;default:for($e=0,je=R,g=C,R--,C=W;je<ze;)switch(s.charCodeAt(je++)){case I:case J:case W:++R,C=g,je=ze;break;case V:if(we>0)++R,C=g;case B:je=ze}}switch(C){case B:for(g=(Te=Te.trim()).charCodeAt(0),y=1,je=++R;R<ze;){switch(C=s.charCodeAt(R)){case B:y++;break;case q:y--;break;case Z:switch(A=s.charCodeAt(R+1)){case T:case Z:R=Qe(A,R,Je,s)}break;case G:C++;case D:C++;case Y:case X:for(;R++<Je&&s.charCodeAt(R)!==C;);}if(0===y)break;R++}if(Ue=s.substring(je,R),g===re)g=(Te=Te.replace(r,"").trim()).charCodeAt(0);switch(g){case L:if(we>0)Te=Te.replace(c,"");switch(A=Te.charCodeAt(1)){case ue:case ie:case fe:case Q:f=a;break;default:f=xe}if(je=(Ue=He(a,f,Ue,A,i+1)).length,me>0&&0===je)je=Te.length;if(ye>0)if(f=Ie(xe,Te,Oe),n=Pe(We,Ue,f,a,pe,be,je,A,i,t),Te=f.join(""),void 0!==n)if(0===(je=(Ue=n.trim()).length))A=0,Ue="";if(je>0)switch(A){case fe:Te=Te.replace(x,Me);case ue:case ie:case Q:Ue=Te+"{"+Ue+"}";break;case te:if(Ue=(Te=Te.replace(b,"$1 $2"+(Be>0?De:"")))+"{"+Ue+"}",1===Ae||2===Ae&&Le("@"+Ue,3))Ue="@"+N+Ue+"@"+Ue;else Ue="@"+Ue;break;default:if(Ue=Te+Ue,t===de)Ve+=Ue,Ue=""}else Ue="";break;default:Ue=He(a,Ie(a,Te,Oe),Ue,t,i+1)}Xe+=Ue,O=0,$e=0,z=0,we=0,Oe=0,j=0,Te="",Ue="",C=s.charCodeAt(++R);break;case q:case W:if((je=(Te=(we>0?Te.replace(c,""):Te).trim()).length)>1){if(0===z)if((g=Te.charCodeAt(0))===Q||g>96&&g<123)je=(Te=Te.replace(" ",":")).length;if(ye>0)if(void 0!==(n=Pe(Se,Te,a,e,pe,be,Ve.length,t,i,t)))if(0===(je=(Te=n.trim()).length))Te="\0\0";switch(g=Te.charCodeAt(0),A=Te.charCodeAt(1),g){case re:break;case L:if(A===oe||A===he){Ye+=Te+s.charAt(R);break}default:if(Te.charCodeAt(je-1)===V)break;Ve+=Ke(Te,g,A,Te.charCodeAt(2))}}O=0,$e=0,z=0,we=0,Oe=0,Te="",C=s.charCodeAt(++R)}}switch(C){case J:case I:if(h+d+u+o+ve===0)switch($){case E:case X:case Y:case L:case ae:case _:case T:case ee:case Z:case Q:case V:case U:case W:case B:case q:break;default:if(z>0)$e=1}if(h===Z)h=0;else if(ge+O===0&&t!==te&&Te.length>0)we=1,Te+="\0";if(ye*qe>0)Pe(Ne,Te,a,e,pe,be,Ve.length,t,i,t);be=1,pe++;break;case W:case q:if(h+d+u+o===0){be++;break}default:switch(be++,Re=s.charAt(R),C){case K:case M:if(d+o+h===0)switch(w){case U:case V:case K:case M:Re="";break;default:if(C!==M)Re=" "}break;case re:Re="\\0";break;case ce:Re="\\f";break;case se:Re="\\v";break;case P:if(d+h+o===0&&ge>0)Oe=1,we=1,Re="\f"+Re;break;case 108:if(d+h+o+ke===0&&z>0)switch(R-z){case 2:if(w===ne&&s.charCodeAt(R-3)===V)ke=w;case 8:if(m===le)ke=m}break;case V:if(d+h+o===0)z=R;break;case U:if(h+u+d+o===0)we=1,Re+="\r";break;case Y:case X:if(0===h)d=d===C?0:0===d?C:d;break;case G:if(d+h+u===0)o++;break;case H:if(d+h+u===0)o--;break;case E:if(d+h+o===0)u--;break;case D:if(d+h+o===0){if(0===O)switch(2*w+3*m){case 533:break;default:y=0,O=1}u++}break;case L:if(h+u+d+o+z+j===0)j=1;break;case T:case Z:if(d+o+u>0)break;switch(h){case 0:switch(2*C+3*s.charCodeAt(R+1)){case 235:h=Z;break;case 220:je=R,h=T}break;case T:if(C===Z&&w===T&&je+2!==R){if(33===s.charCodeAt(je+2))Ve+=s.substring(je,R+1);Re="",h=0}}}if(0===h){if(ge+d+o+j===0&&t!==te&&C!==W)switch(C){case U:case ae:case _:case ee:case E:case D:if(0===O){switch(w){case K:case M:case I:case J:Re+="\0";break;default:Re="\0"+Re+(C===U?"":"\0")}we=1}else switch(C){case D:if(z+7===R&&108===w)z=0;O=++y;break;case E:if(0==(O=--y))we=1,Re+="\0"}break;case K:case M:switch(w){case re:case B:case q:case W:case U:case ce:case K:case M:case I:case J:break;default:if(0===O)we=1,Re+="\0"}}if(Te+=Re,C!==M&&C!==K)$=C}}m=w,w=C,R++}if(je=Ve.length,me>0)if(0===je&&0===Xe.length&&0===a[0].length==false)if(t!==ie||1===a.length&&(ge>0?Ee:Ge)===a[0])je=a.join(",").length+2;if(je>0){if(f=0===ge&&t!==te?function(e){for(var a,r,s=0,t=e.length,i=Array(t);s<t;++s){for(var f=e[s].split(l),n="",o=0,h=0,u=0,d=0,b=f.length;o<b;++o){if(0===(h=(r=f[o]).length)&&b>1)continue;if(u=n.charCodeAt(n.length-1),d=r.charCodeAt(0),a="",0!==o)switch(u){case T:case ae:case _:case ee:case M:case D:break;default:a=" "}switch(d){case P:r=a+Ee;case ae:case _:case ee:case M:case E:case D:break;case G:r=a+r+Ee;break;case V:switch(2*r.charCodeAt(1)+3*r.charCodeAt(2)){case 530:if(Ce>0){r=a+r.substring(8,h-1);break}default:if(o<1||f[o-1].length<1)r=a+Ee+r}break;case U:a="";default:if(h>1&&r.indexOf(":")>0)r=a+r.replace(v,"$1"+Ee+"$2");else r=a+r+Ee}n+=r}i[s]=n.replace(c,"").trim()}return i}(a):a,ye>0)if(void 0!==(n=Pe(Fe,Ve,f,e,pe,be,je,t,i,t))&&0===(Ve=n).length)return Ye+Ve+Xe;if(Ve=f.join(",")+"{"+Ve+"}",Ae*ke!=0){if(2===Ae&&!Le(Ve,2))ke=0;switch(ke){case le:Ve=Ve.replace(k,":"+S+"$1")+Ve;break;case ne:Ve=Ve.replace(p,"::"+N+"input-$1")+Ve.replace(p,"::"+S+"$1")+Ve.replace(p,":"+F+"input-$1")+Ve}ke=0}}return Ye+Ve+Xe}function Ie(e,a,r){var c=a.trim().split(o),s=c,t=c.length,i=e.length;switch(i){case 0:case 1:for(var f=0,n=0===i?"":e[0]+" ";f<t;++f)s[f]=Je(n,s[f],r,i).trim();break;default:f=0;var l=0;for(s=[];f<t;++f)for(var h=0;h<i;++h)s[l++]=Je(e[h]+" ",c[f],r,i).trim()}return s}function Je(e,a,r,c){var s=a,t=s.charCodeAt(0);if(t<33)t=(s=s.trim()).charCodeAt(0);switch(t){case P:switch(ge+c){case 0:case 1:if(0===e.trim().length)break;default:return s.replace(h,"$1"+e.trim())}break;case V:switch(s.charCodeAt(1)){case 103:if(Ce>0&&ge>0)return s.replace(u,"$1").replace(h,"$1"+Ge);break;default:return e.trim()+s.replace(h,"$1"+e.trim())}default:if(r*ge>0&&s.indexOf("\f")>0)return s.replace(h,(e.charCodeAt(0)===V?"":"$1")+e.trim())}return e+s}function Ke(e,a,r,c){var l,o=0,h=e+";",u=2*a+3*r+4*c;if(944===u)return function(e){var a=e.length,r=e.indexOf(":",9)+1,c=e.substring(0,r).trim(),s=e.substring(r,a-1).trim();switch(e.charCodeAt(9)*Be){case 0:break;case Q:if(110!==e.charCodeAt(10))break;default:for(var t=s.split((s="",f)),i=0,r=0,a=t.length;i<a;r=0,++i){for(var l=t[i],o=l.split(n);l=o[r];){var h=l.charCodeAt(0);if(1===Be&&(h>L&&h<90||h>96&&h<123||h===R||h===Q&&l.charCodeAt(1)!==Q))switch(isNaN(parseFloat(l))+(-1!==l.indexOf("("))){case 1:switch(l){case"infinite":case"alternate":case"backwards":case"running":case"normal":case"forwards":case"both":case"none":case"linear":case"ease":case"ease-in":case"ease-out":case"ease-in-out":case"paused":case"reverse":case"alternate-reverse":case"inherit":case"initial":case"unset":case"step-start":case"step-end":break;default:l+=De}}o[r++]=l}s+=(0===i?"":",")+o.join(" ")}}if(s=c+s+";",1===Ae||2===Ae&&Le(s,1))return N+s+s;return s}(h);else if(0===Ae||2===Ae&&!Le(h,1))return h;switch(u){case 1015:return 97===h.charCodeAt(10)?N+h+h:h;case 951:return 116===h.charCodeAt(3)?N+h+h:h;case 963:return 110===h.charCodeAt(5)?N+h+h:h;case 1009:if(100!==h.charCodeAt(4))break;case 969:case 942:return N+h+h;case 978:return N+h+S+h+h;case 1019:case 983:return N+h+S+h+F+h+h;case 883:if(h.charCodeAt(8)===Q)return N+h+h;if(h.indexOf("image-set(",11)>0)return h.replace(z,"$1"+N+"$2")+h;return h;case 932:if(h.charCodeAt(4)===Q)switch(h.charCodeAt(5)){case 103:return N+"box-"+h.replace("-grow","")+N+h+F+h.replace("grow","positive")+h;case 115:return N+h+F+h.replace("shrink","negative")+h;case 98:return N+h+F+h.replace("basis","preferred-size")+h}return N+h+F+h+h;case 964:return N+h+F+"flex-"+h+h;case 1023:if(99!==h.charCodeAt(8))break;return l=h.substring(h.indexOf(":",15)).replace("flex-","").replace("space-between","justify"),N+"box-pack"+l+N+h+F+"flex-pack"+l+h;case 1005:return t.test(h)?h.replace(s,":"+N)+h.replace(s,":"+S)+h:h;case 1e3:switch(o=(l=h.substring(13).trim()).indexOf("-")+1,l.charCodeAt(0)+l.charCodeAt(o)){case 226:l=h.replace(m,"tb");break;case 232:l=h.replace(m,"tb-rl");break;case 220:l=h.replace(m,"lr");break;default:return h}return N+h+F+l+h;case 1017:if(-1===h.indexOf("sticky",9))return h;case 975:switch(o=(h=e).length-10,u=(l=(33===h.charCodeAt(o)?h.substring(0,o):h).substring(e.indexOf(":",7)+1).trim()).charCodeAt(0)+(0|l.charCodeAt(7))){case 203:if(l.charCodeAt(8)<111)break;case 115:h=h.replace(l,N+l)+";"+h;break;case 207:case 102:h=h.replace(l,N+(u>102?"inline-":"")+"box")+";"+h.replace(l,N+l)+";"+h.replace(l,F+l+"box")+";"+h}return h+";";case 938:if(h.charCodeAt(5)===Q)switch(h.charCodeAt(6)){case 105:return l=h.replace("-items",""),N+h+N+"box-"+l+F+"flex-"+l+h;case 115:return N+h+F+"flex-item-"+h.replace(y,"")+h;default:return N+h+F+"flex-line-pack"+h.replace("align-content","").replace(y,"")+h}break;case 973:case 989:if(h.charCodeAt(3)!==Q||122===h.charCodeAt(4))break;case 931:case 953:if(true===j.test(e))if(115===(l=e.substring(e.indexOf(":")+1)).charCodeAt(0))return Ke(e.replace("stretch","fill-available"),a,r,c).replace(":fill-available",":stretch");else return h.replace(l,N+l)+h.replace(l,S+l.replace("fill-",""))+h;break;case 962:if(h=N+h+(102===h.charCodeAt(5)?F+h:"")+h,r+c===211&&105===h.charCodeAt(13)&&h.indexOf("transform",10)>0)return h.substring(0,h.indexOf(";",27)+1).replace(i,"$1"+N+"$2")+h}return h}function Le(e,a){var r=e.indexOf(1===a?":":"{"),c=e.substring(0,3!==a?r:10),s=e.substring(r+1,e.length-1);return Oe(2!==a?c:c.replace(O,"$1"),s,a)}function Me(e,a){var r=Ke(a,a.charCodeAt(0),a.charCodeAt(1),a.charCodeAt(2));return r!==a+";"?r.replace($," or ($1)").substring(4):"("+a+")"}function Pe(e,a,r,c,s,t,i,f,n,l){for(var o,h=0,u=a;h<ye;++h)switch(o=$e[h].call(Te,e,u,r,c,s,t,i,f,n,l)){case void 0:case false:case true:case null:break;default:u=o}if(u!==a)return u}function Qe(e,a,r,c){for(var s=a+1;s<r;++s)switch(c.charCodeAt(s)){case Z:if(e===T)if(c.charCodeAt(s-1)===T&&a+2!==s)return s+1;break;case I:if(e===Z)return s+1}return s}function Re(e){for(var a in e){var r=e[a];switch(a){case"keyframe":Be=0|r;break;case"global":Ce=0|r;break;case"cascade":ge=0|r;break;case"compress":we=0|r;break;case"semicolon":ve=0|r;break;case"preserve":me=0|r;break;case"prefix":if(Oe=null,!r)Ae=0;else if("function"!=typeof r)Ae=1;else Ae=2,Oe=r}}return Re}function Te(a,r){if(void 0!==this&&this.constructor===Te)return e(a);var s=a,t=s.charCodeAt(0);if(t<33)t=(s=s.trim()).charCodeAt(0);if(Be>0)De=s.replace(d,t===G?"":"-");if(t=1,1===ge)Ge=s;else Ee=s;var i,f=[Ge];if(ye>0)if(void 0!==(i=Pe(ze,r,f,f,pe,be,0,0,0,0))&&"string"==typeof i)r=i;var n=He(xe,f,r,0,0);if(ye>0)if(void 0!==(i=Pe(je,n,f,f,pe,be,n.length,0,0,0))&&"string"!=typeof(n=i))t=0;return De="",Ge="",Ee="",ke=0,pe=1,be=1,we*t==0?n:n.replace(c,"").replace(g,"").replace(A,"$1").replace(C,"$1").replace(w," ")}if(Te.use=function e(a){switch(a){case void 0:case null:ye=$e.length=0;break;default:if("function"==typeof a)$e[ye++]=a;else if("object"==typeof a)for(var r=0,c=a.length;r<c;++r)e(a[r]);else qe=0|!!a}return e},Te.set=Re,void 0!==a)Re(a);return Te});
//# sourceMappingURL=stylis.min.js.map

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/json/stringify */ "./node_modules/@babel/runtime-corejs2/core-js/json/stringify.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/toConsumableArray */ "./node_modules/@babel/runtime-corejs2/helpers/esm/toConsumableArray.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_Calendar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/Calendar */ "./components/Calendar.js");
/* harmony import */ var _components_EmployeeButton__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/EmployeeButton */ "./components/EmployeeButton.js");
/* harmony import */ var _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/helperfunctions */ "./components/helperfunctions.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/next/dist/build/polyfills/fetch/index.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_spinners_kit__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-spinners-kit */ "./node_modules/react-spinners-kit/lib/index.js");
/* harmony import */ var react_spinners_kit__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_spinners_kit__WEBPACK_IMPORTED_MODULE_10__);




var _jsxFileName = "/Users/paulbeaudon/calendar/next-calendar/pages/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement;







var Index = function Index(props) {
  var location = props.location;
  var initialMonthInfo = props.collections.length > 0 ? props.collections[0].data[_components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["getWrittenMonth"](props.currentMonth)] : [];
  var users = props.users.map(function (user) {
    return user.data;
  });
  users = users.filter(function (user) {
    if (user.status === 'activated' && (user.access === 'dispatch' || user.access === 'admin')) {
      return user;
    }
  });
  users.sort(function (a, b) {
    if (a.user_name.toUpperCase() < b.user_name.toUpperCase()) return -1;
    if (a.user_name.toUpperCase() > b.user_name.toUpperCase()) return 1;
    return 0;
  });
  var signedInUser = 'Beaudon';

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_9__["useState"])(props.currentMonth),
      month = _useState[0],
      setMonth = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_9__["useState"])(props.currentYear),
      year = _useState2[0],
      setYear = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_9__["useState"])({}),
      draggedButton = _useState3[0],
      setDraggedButton = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_9__["useState"])(initialMonthInfo),
      monthInfo = _useState4[0],
      setMonthInfo = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_9__["useState"])(monthInfo.length),
      monthInfoCount = _useState5[0],
      setMonthInfoCount = _useState5[1];

  var _useState6 = Object(react__WEBPACK_IMPORTED_MODULE_9__["useState"])(0),
      monthInfoSaveFlag = _useState6[0],
      setMonthInfoSaveFlag = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_9__["useState"])(false),
      loading = _useState7[0],
      setLoading = _useState7[1];

  var onDragStart = function onDragStart(event, user) {
    //event.preventDefault();
    setDraggedButton({
      userInfo: user,
      userId: event.target.id
    });
    console.log('on drag start');
    console.log(event.target.id);
  };
  /*const onDrag = (event, user) =>{
    event.preventDefault();
    console.log('dragging');
    console.log(draggedButton.userInfo.user_name);
  };*/


  var _onDragOver = function onDragOver(event) {
    event.preventDefault();
  };

  var onDropDelete = function onDropDelete(event) {
    console.log('Delete:');
    console.log(draggedButton);
    console.log(event.target.id);

    if (draggedButton.userId) {
      console.log('delete this');

      for (var x = 0; x < monthInfo.length; x++) {
        if (monthInfo[x].id === draggedButton.userId) {
          console.log('delete: ' + draggedButton.userId);
          monthInfo.splice(x, 1);
          setMonthInfo[function (monthInfo) {
            return monthInfo;
          }];
          setMonthInfoSaveFlag(function (monthInfoSaveFlag) {
            return monthInfoSaveFlag + 1;
          });
          break;
        }
      }

      console.log(monthInfo);
    } else {
      console.log('do not delete this');
    }

    setDraggedButton({});
  };

  var onDropCopy = function onDropCopy(event) {
    if (!draggedButton.userId) {
      console.log(event.target.id);
      console.log(monthInfoCount);
      console.log('Copy:');
      console.log(draggedButton.userInfo.user_name);
      var newId = event.target.id.split('-')[0] + '-' + monthInfoCount;

      if (event.target.id) {
        // if target is not blank, then update props
        setMonthInfo([].concat(Object(_babel_runtime_corejs2_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_3__["default"])(monthInfo), [{
          access: draggedButton.userInfo.access,
          user_name: draggedButton.userInfo.user_name,
          status: draggedButton.userInfo.status,
          id: newId
        }]));
        setMonthInfoCount(function (monthInfoCount) {
          return monthInfoCount + 1;
        });
        setMonthInfoSaveFlag(function (monthInfoSaveFlag) {
          return monthInfoSaveFlag + 1;
        });
      }
    } else {
      console.log('move');

      var _newId = event.target.id + '-' + draggedButton.userId.split('-')[1];

      console.log(_newId);

      for (var x = 0; x < monthInfo.length; x++) {
        if (monthInfo[x].id === draggedButton.userId) {
          console.log('change: ' + draggedButton.userId + ' to ' + _newId);
          monthInfo[x].id = _newId;
          setMonthInfo[function (monthInfo) {
            return monthInfo;
          }];
          setMonthInfoSaveFlag(function (monthInfoSaveFlag) {
            return monthInfoSaveFlag + 1;
          });
          break;
        }
      }
    }

    setDraggedButton({});
  };

  var previousMonthCal = function previousMonthCal() {
    setLoading(true);

    if (month - 1 < 1) {
      var newMonth = 12;
      var newYear = year - 1;
      loadCalendar(newYear, newMonth);
    } else {
      var _newMonth = month - 1;

      var _newYear = year;
      loadCalendar(_newYear, _newMonth);
    }
  };

  var nextMonthCal = function nextMonthCal() {
    setLoading(true);

    if (month + 1 > 12) {
      var newMonth = 1;
      var newYear = year + 1;
      loadCalendar(newYear, newMonth);
    } else {
      var _newMonth2 = month + 1;

      var _newYear2 = year;
      loadCalendar(_newYear2, _newMonth2);
    }
  };

  var currentMonthCal = function currentMonthCal() {
    setLoading(true);
    var newMonth = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["currentMonth"]();
    var newYear = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["currentYear"]();
    loadCalendar(newYear, newMonth);
  };

  var loadCalendar = function loadCalendar(newYear, newMonth) {
    var resC, _ref, collections, err, calJson;

    return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.async(function loadCalendar$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8___default()("".concat(location, "/api/collections?year=").concat(newYear, "&month=").concat(_components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["getWrittenMonth"](newMonth))));

          case 2:
            resC = _context.sent;
            _context.next = 5;
            return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.awrap(resC.json());

          case 5:
            _ref = _context.sent;
            collections = _ref.collections;
            err = _ref.err;
            calJson = collections.length > 0 ? collections[0].data[_components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["getWrittenMonth"](newMonth)] : [];
            setMonthInfo(calJson);
            setMonthInfoCount(calJson.length);
            setMonth(newMonth);
            setYear(newYear);
            setLoading(false);

          case 14:
          case "end":
            return _context.stop();
        }
      }
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_9__["useEffect"])(function () {
    // used to save calendar when a drop has taken place, and need to wait for monthInfo to update before calling save.
    saveCalendar({
      type: 'drop'
    });
  }, [monthInfoSaveFlag]);

  var saveCalendar = function saveCalendar(action) {
    console.log(action);

    var data = Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({
      "year": year.toString(),
      "month": _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["getWrittenMonth"](month)
    }, _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["getWrittenMonth"](month), monthInfo);

    isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8___default()("".concat(location, "/api/collections"), {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: _babel_runtime_corejs2_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0___default()(data)
    }).then(function (res) {
      return res.json();
    }).then(function (json) {
      if (action.type === 'none') {
        console.log(json.my_status);
      }

      if (action.type === 'next') {
        console.log('next');
        console.log(json.my_status);
        nextMonthCal();
      }

      if (action.type === 'previous') {
        console.log('previous');
        console.log(json.my_status);
        previousMonthCal();
      }

      if (action.type === 'current') {
        console.log('current');
        conosle.log(json.my_status);
        currentMonthCal();
      }

      if (action.type === 'drop') {
        console.log('drop');
        console.log(json.my_status);
      }
    });
  };

  return __jsx("div", {
    className: "jsx-4013782617",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 199
    },
    __self: this
  }, loading ? __jsx("div", {
    className: "jsx-4013782617" + " " + "spinner",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 200
    },
    __self: this
  }, __jsx(react_spinners_kit__WEBPACK_IMPORTED_MODULE_10__["CircleSpinner"], {
    size: 80,
    color: "#354980",
    loading: loading,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 200
    },
    __self: this
  })) : __jsx("div", {
    className: "jsx-4013782617",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 200
    },
    __self: this
  }), __jsx("div", {
    id: "left_pane",
    className: "jsx-4013782617" + " " + "leftPane",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 201
    },
    __self: this
  }, __jsx("div", {
    className: "jsx-4013782617" + " " + "headerDiv",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 202
    },
    __self: this
  }, "Dispatch Staff"), users.map(function (user) {
    return __jsx(_components_EmployeeButton__WEBPACK_IMPORTED_MODULE_6__["default"], {
      key: user.user_name,
      onDragStartIndex: function onDragStartIndex(event) {
        return onDragStart(event, user);
      },
      name: user.user_name,
      signedinuser: signedInUser,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 204
      },
      __self: this
    });
  }), __jsx("div", {
    id: "delete_pane",
    onDrop: function onDrop(event) {
      return onDropDelete(event);
    },
    onDragOver: function onDragOver(event) {
      return _onDragOver(event);
    },
    className: "jsx-4013782617" + " " + "deletePane",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 206
    },
    __self: this
  }, "Drag here to Delete")), __jsx("div", {
    className: "jsx-4013782617" + " " + "rightPane",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 208
    },
    __self: this
  }, __jsx("div", {
    id: "monthTitle",
    className: "jsx-4013782617" + " " + "headerDiv title",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 209
    },
    __self: this
  }, _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["getWrittenMonth"](month), " ", year, " Desk Schedule"), __jsx("div", {
    className: "jsx-4013782617" + " " + "headerDiv",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 210
    },
    __self: this
  }, __jsx("button", {
    id: "today",
    onClick: function onClick(e) {
      return currentMonthCal();
    },
    className: "jsx-4013782617" + " " + "button",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 211
    },
    __self: this
  }, "Today"), " ", __jsx("button", {
    id: "leftarrow",
    onClick: function onClick(e) {
      return saveCalendar({
        type: 'previous'
      });
    },
    className: "jsx-4013782617" + " " + "button",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 211
    },
    __self: this
  }, "<<"), " ", __jsx("button", {
    id: "rightarrow",
    onClick: function onClick(e) {
      return saveCalendar({
        type: 'next'
      });
    },
    className: "jsx-4013782617" + " " + "button",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 211
    },
    __self: this
  }, ">>")), __jsx(_components_Calendar__WEBPACK_IMPORTED_MODULE_5__["default"], {
    onDragStartIndex: function onDragStartIndex(event) {
      return onDragStart(event, "");
    },
    onDragOverIndex: function onDragOverIndex(event) {
      return _onDragOver(event);
    },
    onDropCopyIndex: function onDropCopyIndex(event) {
      return onDropCopy(event);
    },
    month: month,
    year: year,
    monthInfo: monthInfo,
    signedinuser: signedInUser,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 213
    },
    __self: this
  })), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_4___default.a, {
    id: "4013782617",
    __self: this
  }, ".spinner{position:absolute;left:50%;top:50%;z-index:1;}.title{font-size:1.9em !important;}.headerDiv{margin:auto;width:80%;text-align:center;font-size:1.2em;padding:10px;}.fullPane{position:relative;width:100%;}.leftPane{position:absolute;left:60px;top:68px;bottom:10px;width:15%;}.rightPane{position:absolute;right:10px;top:10px;bottom:10px;width:80%;}.deletePane{text-align:center;margin:10px 10px 10px 10px;padding:10px 10px 10px 10px;border:1px dashed #354980;background-color:white;color:#354980;height:50%;}.button{background-color:#354980;border:none;color:#fbca37;padding:5px 25px;text-align:center;-webkit-text-decoration:none;text-decoration:none;display:inline-block;margin:4px 2px;cursor:pointer;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wYXVsYmVhdWRvbi9jYWxlbmRhci9uZXh0LWNhbGVuZGFyL3BhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXNOeUIsQUFHNkIsQUFNUyxBQUdoQixBQU9NLEFBSUEsQUFPQSxBQU9DLEFBU08sWUFqQ2hCLE1BVEEsQUFnQkMsQUFJRCxBQU9DLEFBT2dCLElBeEJULEdBaUNMLEVBMUNKLEFBS1YsQ0FlVSxDQUpWLEFBV1UsTUExQkUsRUFvQkMsQUFzQkcsQ0FmSCxFQWxCSSxLQVJqQixBQWdDNkIsSUFabEIsQ0FPRCxDQWVTLEtBakNMLEdBWWQsQ0FPQSxRQWVvQixDQWpDcEIsSUF1QjJCLGFBV0osYUFWQyx1QkFDUixjQUNILEFBU1UsV0FSdkIsVUFTaUIsZUFDQSxlQUNqQiIsImZpbGUiOiIvVXNlcnMvcGF1bGJlYXVkb24vY2FsZW5kYXIvbmV4dC1jYWxlbmRhci9wYWdlcy9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBDYWwgZnJvbSBcIi4uL2NvbXBvbmVudHMvQ2FsZW5kYXJcIjtcbmltcG9ydCBFbXBsb3llZUJ1dHRvbiBmcm9tIFwiLi4vY29tcG9uZW50cy9FbXBsb3llZUJ1dHRvblwiO1xuaW1wb3J0ICogYXMgaGVscEZ1bmMgZnJvbSBcIi4uL2NvbXBvbmVudHMvaGVscGVyZnVuY3Rpb25zXCI7XG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLXVuZmV0Y2gnO1xuaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBDaXJjbGVTcGlubmVyIH0gZnJvbSBcInJlYWN0LXNwaW5uZXJzLWtpdFwiO1xuXG5jb25zdCBJbmRleCA9IHByb3BzID0+e1xuICBjb25zdCBsb2NhdGlvbiA9IHByb3BzLmxvY2F0aW9uO1xuXG4gIGxldCBpbml0aWFsTW9udGhJbmZvID0gcHJvcHMuY29sbGVjdGlvbnMubGVuZ3RoID4gMCA/IHByb3BzLmNvbGxlY3Rpb25zWzBdLmRhdGFbaGVscEZ1bmMuZ2V0V3JpdHRlbk1vbnRoKHByb3BzLmN1cnJlbnRNb250aCldIDogW107XG5cbiAgbGV0IHVzZXJzID0gcHJvcHMudXNlcnMubWFwKCh1c2VyKT0+e1xuICAgIHJldHVybiB1c2VyLmRhdGE7XG4gIH0pO1xuXG4gIHVzZXJzID0gdXNlcnMuZmlsdGVyKCh1c2VyKT0+e1xuICAgIGlmKHVzZXIuc3RhdHVzID09PSAnYWN0aXZhdGVkJyAmJiAodXNlci5hY2Nlc3MgPT09ICdkaXNwYXRjaCcgfHwgdXNlci5hY2Nlc3MgPT09ICdhZG1pbicpKXtcbiAgICAgIHJldHVybiB1c2VyO1xuICAgIH1cbiAgfSk7XG4gIHVzZXJzLnNvcnQoZnVuY3Rpb24oYSxiKXtcbiAgICBpZihhLnVzZXJfbmFtZS50b1VwcGVyQ2FzZSgpIDwgYi51c2VyX25hbWUudG9VcHBlckNhc2UoKSkgcmV0dXJuIC0xO1xuICAgIGlmKGEudXNlcl9uYW1lLnRvVXBwZXJDYXNlKCkgPiBiLnVzZXJfbmFtZS50b1VwcGVyQ2FzZSgpKSByZXR1cm4gMTtcbiAgICByZXR1cm4gMDtcbiAgfSk7XG4gIGxldCBzaWduZWRJblVzZXIgPSAnQmVhdWRvbic7XG5cbiAgY29uc3QgW21vbnRoLCBzZXRNb250aF0gPSB1c2VTdGF0ZShwcm9wcy5jdXJyZW50TW9udGgpO1xuICBjb25zdCBbeWVhciwgc2V0WWVhcl0gPSB1c2VTdGF0ZShwcm9wcy5jdXJyZW50WWVhcik7XG4gIGNvbnN0IFtkcmFnZ2VkQnV0dG9uLCBzZXREcmFnZ2VkQnV0dG9uXSA9IHVzZVN0YXRlKHt9KTtcbiAgY29uc3QgW21vbnRoSW5mbywgc2V0TW9udGhJbmZvXSA9IHVzZVN0YXRlKGluaXRpYWxNb250aEluZm8pO1xuXG4gIGNvbnN0IFttb250aEluZm9Db3VudCwgc2V0TW9udGhJbmZvQ291bnRdID0gdXNlU3RhdGUobW9udGhJbmZvLmxlbmd0aCk7XG4gIGNvbnN0IFttb250aEluZm9TYXZlRmxhZywgc2V0TW9udGhJbmZvU2F2ZUZsYWddID0gdXNlU3RhdGUoMCk7XG4gIGNvbnN0IFtsb2FkaW5nLCBzZXRMb2FkaW5nXSA9IHVzZVN0YXRlKGZhbHNlKTtcblxuICBjb25zdCBvbkRyYWdTdGFydCA9IChldmVudCwgdXNlcikgPT4ge1xuICAgIC8vZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICBzZXREcmFnZ2VkQnV0dG9uKHtcbiAgICAgIHVzZXJJbmZvOiB1c2VyLFxuICAgICAgdXNlcklkOiBldmVudC50YXJnZXQuaWRcbiAgICB9KTtcbiAgICBjb25zb2xlLmxvZygnb24gZHJhZyBzdGFydCcpO1xuICAgIGNvbnNvbGUubG9nKGV2ZW50LnRhcmdldC5pZCk7XG4gIH07XG5cbiAgLypjb25zdCBvbkRyYWcgPSAoZXZlbnQsIHVzZXIpID0+e1xuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgY29uc29sZS5sb2coJ2RyYWdnaW5nJyk7XG4gICAgY29uc29sZS5sb2coZHJhZ2dlZEJ1dHRvbi51c2VySW5mby51c2VyX25hbWUpO1xuICB9OyovXG5cbiAgY29uc3Qgb25EcmFnT3ZlciA9IChldmVudCk9PntcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICB9O1xuXG4gIGNvbnN0IG9uRHJvcERlbGV0ZSA9IChldmVudCk9PntcbiAgICBjb25zb2xlLmxvZygnRGVsZXRlOicpO1xuICAgIGNvbnNvbGUubG9nKGRyYWdnZWRCdXR0b24pO1xuICAgIGNvbnNvbGUubG9nKGV2ZW50LnRhcmdldC5pZCk7XG4gICAgaWYoZHJhZ2dlZEJ1dHRvbi51c2VySWQpe1xuICAgICAgY29uc29sZS5sb2coJ2RlbGV0ZSB0aGlzJyk7XG4gICAgICBmb3IobGV0IHg9MDsgeDxtb250aEluZm8ubGVuZ3RoOyB4Kyspe1xuICAgICAgICBpZihtb250aEluZm9beF0uaWQgPT09IGRyYWdnZWRCdXR0b24udXNlcklkKXtcbiAgICAgICAgICBjb25zb2xlLmxvZygnZGVsZXRlOiAnICsgZHJhZ2dlZEJ1dHRvbi51c2VySWQpO1xuICAgICAgICAgIG1vbnRoSW5mby5zcGxpY2UoeCwgMSk7XG4gICAgICAgICAgc2V0TW9udGhJbmZvW21vbnRoSW5mbyA9PiBtb250aEluZm9dO1xuICAgICAgICAgIHNldE1vbnRoSW5mb1NhdmVGbGFnKG1vbnRoSW5mb1NhdmVGbGFnID0+IG1vbnRoSW5mb1NhdmVGbGFnICsgMSk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGNvbnNvbGUubG9nKG1vbnRoSW5mbyk7XG4gICAgfWVsc2V7XG4gICAgICBjb25zb2xlLmxvZygnZG8gbm90IGRlbGV0ZSB0aGlzJyk7XG4gICAgfVxuXG4gICAgc2V0RHJhZ2dlZEJ1dHRvbih7fSk7XG4gIH07XG5cbiAgY29uc3Qgb25Ecm9wQ29weSA9IChldmVudCk9PntcbiAgICBpZighZHJhZ2dlZEJ1dHRvbi51c2VySWQpe1xuICAgICAgY29uc29sZS5sb2coZXZlbnQudGFyZ2V0LmlkKTtcbiAgICAgIGNvbnNvbGUubG9nKG1vbnRoSW5mb0NvdW50KTtcbiAgICAgIGNvbnNvbGUubG9nKCdDb3B5OicpO1xuICAgICAgY29uc29sZS5sb2coZHJhZ2dlZEJ1dHRvbi51c2VySW5mby51c2VyX25hbWUpO1xuICAgICAgbGV0IG5ld0lkID0gZXZlbnQudGFyZ2V0LmlkLnNwbGl0KCctJylbMF0gKyAnLScgKyBtb250aEluZm9Db3VudDtcbiAgICAgIGlmKGV2ZW50LnRhcmdldC5pZCl7IC8vIGlmIHRhcmdldCBpcyBub3QgYmxhbmssIHRoZW4gdXBkYXRlIHByb3BzXG4gICAgICAgIHNldE1vbnRoSW5mbyhbLi4ubW9udGhJbmZvLCB7YWNjZXNzOiBkcmFnZ2VkQnV0dG9uLnVzZXJJbmZvLmFjY2VzcywgdXNlcl9uYW1lOmRyYWdnZWRCdXR0b24udXNlckluZm8udXNlcl9uYW1lLCBzdGF0dXM6ZHJhZ2dlZEJ1dHRvbi51c2VySW5mby5zdGF0dXMsIGlkOiBuZXdJZH1dKTtcbiAgICAgICAgc2V0TW9udGhJbmZvQ291bnQobW9udGhJbmZvQ291bnQgPT4gbW9udGhJbmZvQ291bnQgKyAxKTtcbiAgICAgICAgc2V0TW9udGhJbmZvU2F2ZUZsYWcobW9udGhJbmZvU2F2ZUZsYWcgPT4gbW9udGhJbmZvU2F2ZUZsYWcgKyAxKTtcbiAgICAgIH1cbiAgICB9ZWxzZXtcbiAgICAgIGNvbnNvbGUubG9nKCdtb3ZlJyk7XG4gICAgICBsZXQgbmV3SWQgPSBldmVudC50YXJnZXQuaWQgKyAnLScgKyBkcmFnZ2VkQnV0dG9uLnVzZXJJZC5zcGxpdCgnLScpWzFdO1xuICAgICAgY29uc29sZS5sb2cobmV3SWQpO1xuICAgICAgZm9yKGxldCB4PTA7IHg8bW9udGhJbmZvLmxlbmd0aDsgeCsrKXtcbiAgICAgICAgaWYobW9udGhJbmZvW3hdLmlkID09PSBkcmFnZ2VkQnV0dG9uLnVzZXJJZCl7XG4gICAgICAgICAgY29uc29sZS5sb2coJ2NoYW5nZTogJyArIGRyYWdnZWRCdXR0b24udXNlcklkICsgJyB0byAnICsgbmV3SWQpO1xuICAgICAgICAgIG1vbnRoSW5mb1t4XS5pZCA9IG5ld0lkO1xuICAgICAgICAgIHNldE1vbnRoSW5mb1ttb250aEluZm8gPT4gbW9udGhJbmZvXTtcbiAgICAgICAgICBzZXRNb250aEluZm9TYXZlRmxhZyhtb250aEluZm9TYXZlRmxhZyA9PiBtb250aEluZm9TYXZlRmxhZyArIDEpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgICAgc2V0RHJhZ2dlZEJ1dHRvbih7fSk7XG4gIH07XG5cbiAgbGV0IHByZXZpb3VzTW9udGhDYWwgPSAoKT0+e1xuICAgIHNldExvYWRpbmcodHJ1ZSk7XG4gICAgaWYobW9udGgtMSA8IDEpe1xuICAgICAgY29uc3QgbmV3TW9udGggPSAxMjtcbiAgICAgIGNvbnN0IG5ld1llYXIgPSB5ZWFyLTE7XG4gICAgICBsb2FkQ2FsZW5kYXIobmV3WWVhciwgbmV3TW9udGgpO1xuICAgIH1lbHNle1xuICAgICAgY29uc3QgbmV3TW9udGggPSBtb250aC0xO1xuICAgICAgY29uc3QgbmV3WWVhciA9IHllYXI7XG4gICAgICBsb2FkQ2FsZW5kYXIobmV3WWVhciwgbmV3TW9udGgpO1xuICAgIH1cbiAgfTtcblxuICBsZXQgbmV4dE1vbnRoQ2FsID0gKCk9PntcbiAgICBzZXRMb2FkaW5nKHRydWUpO1xuICAgIGlmKG1vbnRoKzEgPiAxMil7XG4gICAgICBjb25zdCBuZXdNb250aCA9IDE7XG4gICAgICBjb25zdCBuZXdZZWFyID0geWVhcisxO1xuICAgICAgbG9hZENhbGVuZGFyKG5ld1llYXIsIG5ld01vbnRoKTtcbiAgICB9ZWxzZXtcbiAgICAgIGNvbnN0IG5ld01vbnRoID0gbW9udGgrMTtcbiAgICAgIGNvbnN0IG5ld1llYXIgPSB5ZWFyO1xuICAgICAgbG9hZENhbGVuZGFyKG5ld1llYXIsIG5ld01vbnRoKTtcbiAgICB9XG4gIH07XG5cbiAgbGV0IGN1cnJlbnRNb250aENhbCA9ICgpPT57XG4gICAgc2V0TG9hZGluZyh0cnVlKTtcbiAgICBjb25zdCBuZXdNb250aCA9IGhlbHBGdW5jLmN1cnJlbnRNb250aCgpO1xuICAgIGNvbnN0IG5ld1llYXIgPSBoZWxwRnVuYy5jdXJyZW50WWVhcigpO1xuICAgIGxvYWRDYWxlbmRhcihuZXdZZWFyLCBuZXdNb250aCk7XG4gIH07XG5cbiAgbGV0IGxvYWRDYWxlbmRhciA9IGFzeW5jKG5ld1llYXIsIG5ld01vbnRoKT0+e1xuICAgIGNvbnN0IHJlc0MgPSBhd2FpdCBmZXRjaChgJHtsb2NhdGlvbn0vYXBpL2NvbGxlY3Rpb25zP3llYXI9JHtuZXdZZWFyfSZtb250aD0ke2hlbHBGdW5jLmdldFdyaXR0ZW5Nb250aChuZXdNb250aCl9YCk7XG4gICAgY29uc3QgeyBjb2xsZWN0aW9ucywgZXJyIH0gPSBhd2FpdCByZXNDLmpzb24oKTtcbiAgICBjb25zdCBjYWxKc29uID0gY29sbGVjdGlvbnMubGVuZ3RoID4gMCA/IGNvbGxlY3Rpb25zWzBdLmRhdGFbaGVscEZ1bmMuZ2V0V3JpdHRlbk1vbnRoKG5ld01vbnRoKV0gOiBbXTtcblxuICAgIHNldE1vbnRoSW5mbyhjYWxKc29uKTtcbiAgICBzZXRNb250aEluZm9Db3VudChjYWxKc29uLmxlbmd0aCk7XG4gICAgc2V0TW9udGgobmV3TW9udGgpO1xuICAgIHNldFllYXIobmV3WWVhcik7XG4gICAgc2V0TG9hZGluZyhmYWxzZSk7XG4gIH07XG5cbiAgdXNlRWZmZWN0KCgpID0+IHsgLy8gdXNlZCB0byBzYXZlIGNhbGVuZGFyIHdoZW4gYSBkcm9wIGhhcyB0YWtlbiBwbGFjZSwgYW5kIG5lZWQgdG8gd2FpdCBmb3IgbW9udGhJbmZvIHRvIHVwZGF0ZSBiZWZvcmUgY2FsbGluZyBzYXZlLlxuICAgICBzYXZlQ2FsZW5kYXIoe3R5cGU6ICdkcm9wJ30pO1xuICB9LCBbbW9udGhJbmZvU2F2ZUZsYWddKTtcblxuICBsZXQgc2F2ZUNhbGVuZGFyID0gKGFjdGlvbik9PntcbiAgICBjb25zb2xlLmxvZyhhY3Rpb24pO1xuICAgIGxldCBkYXRhID0ge1wieWVhclwiOiB5ZWFyLnRvU3RyaW5nKCksIFwibW9udGhcIjogaGVscEZ1bmMuZ2V0V3JpdHRlbk1vbnRoKG1vbnRoKSwgW2hlbHBGdW5jLmdldFdyaXR0ZW5Nb250aChtb250aCldOiBtb250aEluZm99O1xuICAgIGZldGNoKGAke2xvY2F0aW9ufS9hcGkvY29sbGVjdGlvbnNgLCB7XG4gICAgICBtZXRob2Q6ICdwb3N0JyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uLCB0ZXh0L3BsYWluLCAqLyonLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nXG4gICAgICB9LFxuICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoZGF0YSlcbiAgICB9KS50aGVuKChyZXMpID0+IHtcbiAgICAgIHJldHVybiByZXMuanNvbigpO1xuICAgIH0pLnRoZW4oKGpzb24pPT57XG4gICAgICBpZihhY3Rpb24udHlwZSA9PT0gJ25vbmUnKXtcbiAgICAgICAgY29uc29sZS5sb2coanNvbi5teV9zdGF0dXMpO1xuICAgICAgfVxuICAgICAgaWYoYWN0aW9uLnR5cGUgPT09ICduZXh0Jyl7XG4gICAgICAgIGNvbnNvbGUubG9nKCduZXh0Jyk7XG4gICAgICAgIGNvbnNvbGUubG9nKGpzb24ubXlfc3RhdHVzKTtcbiAgICAgICAgbmV4dE1vbnRoQ2FsKCk7XG4gICAgICB9XG4gICAgICBpZihhY3Rpb24udHlwZSA9PT0gJ3ByZXZpb3VzJyl7XG4gICAgICAgIGNvbnNvbGUubG9nKCdwcmV2aW91cycpO1xuICAgICAgICBjb25zb2xlLmxvZyhqc29uLm15X3N0YXR1cyk7XG4gICAgICAgIHByZXZpb3VzTW9udGhDYWwoKTtcbiAgICAgIH1cbiAgICAgIGlmKGFjdGlvbi50eXBlID09PSAnY3VycmVudCcpe1xuICAgICAgICBjb25zb2xlLmxvZygnY3VycmVudCcpO1xuICAgICAgICBjb25vc2xlLmxvZyhqc29uLm15X3N0YXR1cyk7XG4gICAgICAgIGN1cnJlbnRNb250aENhbCgpO1xuICAgICAgfVxuICAgICAgaWYoYWN0aW9uLnR5cGUgPT09ICdkcm9wJyl7XG4gICAgICAgIGNvbnNvbGUubG9nKCdkcm9wJyk7XG4gICAgICAgIGNvbnNvbGUubG9nKGpzb24ubXlfc3RhdHVzKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfTtcblxuICByZXR1cm4oXG4gICAgPGRpdj5cbiAgICAgIHtsb2FkaW5nID8gPGRpdiBjbGFzc05hbWU9XCJzcGlubmVyXCI+PENpcmNsZVNwaW5uZXIgc2l6ZT17ODB9IGNvbG9yPVwiIzM1NDk4MFwiIGxvYWRpbmc9e2xvYWRpbmd9IC8+PC9kaXY+IDogPGRpdj48L2Rpdj59XG4gICAgICA8ZGl2IGlkPVwibGVmdF9wYW5lXCIgY2xhc3NOYW1lPVwibGVmdFBhbmVcIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJoZWFkZXJEaXZcIj5EaXNwYXRjaCBTdGFmZjwvZGl2PlxuICAgICAgICB7dXNlcnMubWFwKHVzZXI9PihcbiAgICAgICAgICAgIDxFbXBsb3llZUJ1dHRvbiBrZXk9e3VzZXIudXNlcl9uYW1lfSBvbkRyYWdTdGFydEluZGV4PXtldmVudCA9PiBvbkRyYWdTdGFydChldmVudCwgdXNlcil9IG5hbWU9e3VzZXIudXNlcl9uYW1lfSBzaWduZWRpbnVzZXI9e3NpZ25lZEluVXNlcn0vPlxuICAgICAgICApKX1cbiAgICAgICAgPGRpdiBpZD1cImRlbGV0ZV9wYW5lXCIgY2xhc3NOYW1lPVwiZGVsZXRlUGFuZVwiIG9uRHJvcD17ZXZlbnQgPT4gb25Ecm9wRGVsZXRlKGV2ZW50KX0gb25EcmFnT3Zlcj17ZXZlbnQgPT4gb25EcmFnT3ZlcihldmVudCl9PkRyYWcgaGVyZSB0byBEZWxldGU8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJyaWdodFBhbmVcIj5cbiAgICBcdCAgPGRpdiBjbGFzc05hbWU9XCJoZWFkZXJEaXYgdGl0bGVcIiBpZD0nbW9udGhUaXRsZSc+e2hlbHBGdW5jLmdldFdyaXR0ZW5Nb250aChtb250aCl9IHt5ZWFyfSBEZXNrIFNjaGVkdWxlPC9kaXY+XG4gICAgXHQgIDxkaXYgY2xhc3NOYW1lPVwiaGVhZGVyRGl2XCI+XG4gICAgXHRcdCAgPGJ1dHRvbiBjbGFzc05hbWU9XCJidXR0b25cIiBpZD1cInRvZGF5XCIgb25DbGljaz17ZT0+KGN1cnJlbnRNb250aENhbCgpKX0+VG9kYXk8L2J1dHRvbj4gPGJ1dHRvbiBjbGFzc05hbWU9XCJidXR0b25cIiBpZD0nbGVmdGFycm93JyBvbkNsaWNrPXtlPT4oc2F2ZUNhbGVuZGFyKHt0eXBlOiAncHJldmlvdXMnfSkpfT4mbHQ7Jmx0OzwvYnV0dG9uPiA8YnV0dG9uIGNsYXNzTmFtZT1cImJ1dHRvblwiIGlkPSdyaWdodGFycm93JyBvbkNsaWNrPXtlPT4oc2F2ZUNhbGVuZGFyKHt0eXBlOiAnbmV4dCd9KSl9PiZndDsmZ3Q7PC9idXR0b24+XG4gICAgXHQgIDwvZGl2PlxuICAgIFx0ICA8Q2FsIG9uRHJhZ1N0YXJ0SW5kZXg9e2V2ZW50ID0+IG9uRHJhZ1N0YXJ0KGV2ZW50LCBcIlwiKX0gb25EcmFnT3ZlckluZGV4PXtldmVudCA9PiBvbkRyYWdPdmVyKGV2ZW50KX0gb25Ecm9wQ29weUluZGV4PXtldmVudCA9PiBvbkRyb3BDb3B5KGV2ZW50KX0gbW9udGg9e21vbnRofSB5ZWFyPXt5ZWFyfSBtb250aEluZm89e21vbnRoSW5mb30gc2lnbmVkaW51c2VyPXtzaWduZWRJblVzZXJ9Lz5cbiAgICBcdDwvZGl2PlxuICAgICAgPHN0eWxlIGpzeCBnbG9iYWw+e2BcbiAgICAgICAgLnNwaW5uZXJ7XG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgIGxlZnQ6IDUwJTtcbiAgICAgICAgICB0b3A6IDUwJTtcbiAgICAgICAgICB6LWluZGV4OiAxO1xuICAgICAgICB9XG4gICAgICAgIC50aXRsZXtcbiAgICAgICAgICBmb250LXNpemU6IDEuOWVtICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cbiAgICAgICAgLmhlYWRlckRpdntcbiAgICAgICAgXHRtYXJnaW46IGF1dG87XG4gICAgICAgIFx0d2lkdGg6IDgwJTtcbiAgICAgICAgXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIFx0Zm9udC1zaXplOiAxLjJlbTtcbiAgICAgICAgXHRwYWRkaW5nOiAxMHB4O1xuICAgICAgICB9XG4gICAgICAgIC5mdWxsUGFuZXtcbiAgICAgICAgXHRwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIFx0d2lkdGg6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgICAgLmxlZnRQYW5le1xuICAgICAgICBcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgXHRsZWZ0OiA2MHB4O1xuICAgICAgICBcdHRvcDogNjhweDtcbiAgICAgICAgXHRib3R0b206IDEwcHg7XG4gICAgICAgIFx0d2lkdGg6IDE1JTtcbiAgICAgICAgfVxuICAgICAgICAucmlnaHRQYW5le1xuICAgICAgICBcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgXHRyaWdodDogMTBweDtcbiAgICAgICAgXHR0b3A6IDEwcHg7XG4gICAgICAgIFx0Ym90dG9tOiAxMHB4O1xuICAgICAgICBcdHdpZHRoOjgwJTtcbiAgICAgICAgfVxuICAgICAgICAuZGVsZXRlUGFuZXtcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIFx0bWFyZ2luOiAxMHB4IDEwcHggMTBweCAxMHB4O1xuICAgICAgICBcdHBhZGRpbmc6IDEwcHggMTBweCAxMHB4IDEwcHg7XG4gICAgICAgIFx0Ym9yZGVyOiAxcHggZGFzaGVkICMzNTQ5ODA7XG4gICAgICAgIFx0YmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgICAgICAgY29sb3I6ICMzNTQ5ODA7XG4gICAgICAgICAgaGVpZ2h0OiA1MCU7XG4gICAgICAgIH1cbiAgICAgICAgLmJ1dHRvbiB7XG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM1NDk4MDtcbiAgICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgICAgICAgY29sb3I6ICNmYmNhMzc7XG4gICAgICAgICAgcGFkZGluZzogNXB4IDI1cHg7XG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgICAgbWFyZ2luOiA0cHggMnB4O1xuICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgfVxuICAgICAgYH08L3N0eWxlPlxuICAgIDwvZGl2PlxuICApO1xufTtcblxuSW5kZXguZ2V0SW5pdGlhbFByb3BzID0gYXN5bmMgKHtyZXEsIHJlc3BvbnNlfSk9PntcbiAgdHJ5e1xuXG4gICAgLy9jb25zdCB7ICd4LW5vdy1kZXBsb3ltZW50LXVybCc6IG5vd1VSTCB9ID0gcmVxLmhlYWRlcnM7XG4gICAgY29uc3QgeyAnaG9zdCc6IFVSTCB9ID0gcmVxLmhlYWRlcnM7XG4gICAgY29uc3QgbG9jYXRpb24gPSAoVVJMID09PSAnbG9jYWxob3N0OjMwMDAnKSA/ICdodHRwOi8vbG9jYWxob3N0OjMwMDAnIDogJ2h0dHBzOi8vbmV4dC1jYWxlbmRhci5ub3cuc2gnO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2goYCR7bG9jYXRpb259L2FwaS9nZXRVc2Vyc2ApO1xuICAgIC8vY29uc3QgcmVzID0gYXdhaXQgZmV0Y2goYGh0dHBzOi8vbmV4dC1jYWxlbmRhci5ub3cuc2gvYXBpL2dldFVzZXJzYCk7IC8vIGluaXRpYWwgY2FsbHMgdG8gZGF0YWJhc2UgLSBzZXJ2ZXIgc2lkZSByZW5kZXIgZm9yIHNwZWVkXG4gICAgY29uc3QgeyB1c2VycywgZXJyb3IgfSA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICBjb25zdCBjdXJyZW50TW9udGggPSBoZWxwRnVuYy5jdXJyZW50TW9udGgoKTtcbiAgICBjb25zdCBjdXJyZW50TW9udGhXcml0dGVuID0gaGVscEZ1bmMuZ2V0V3JpdHRlbk1vbnRoKGN1cnJlbnRNb250aCk7XG4gICAgY29uc3QgY3VycmVudFllYXIgPSBoZWxwRnVuYy5jdXJyZW50WWVhcigpO1xuXG4gICAgY29uc3QgcmVzQyA9IGF3YWl0IGZldGNoKGAke2xvY2F0aW9ufS9hcGkvY29sbGVjdGlvbnM/eWVhcj0ke2N1cnJlbnRZZWFyfSZtb250aD0ke2N1cnJlbnRNb250aFdyaXR0ZW59YCk7XG4gICAgLy9jb25zdCByZXNDID0gYXdhaXQgZmV0Y2goYGh0dHBzOi8vbmV4dC1jYWxlbmRhci5ub3cuc2gvYXBpL2NvbGxlY3Rpb25zP3llYXI9JHtjdXJyZW50WWVhcn0mbW9udGg9JHtjdXJyZW50TW9udGhXcml0dGVufWApO1xuICAgIGNvbnN0IHsgY29sbGVjdGlvbnMsIGVyciB9ID0gYXdhaXQgcmVzQy5qc29uKCk7XG5cbiAgICBjb25zb2xlLmxvZygnSW5pdGlhbFByb3BzOiBjYWxsIHRvIGRhdGFiYXNlJyk7XG4gICAgY29uc29sZS5sb2coJ3YyIGluZGV4LmpzJyk7XG4gICAgcmV0dXJuIHtsb2NhdGlvbjogbG9jYXRpb24sIGN1cnJlbnRNb250aDogY3VycmVudE1vbnRoLCBjdXJyZW50WWVhcjogY3VycmVudFllYXIsIGNvbGxlY3Rpb25zLCB1c2Vyc307XG4gIH1jYXRjaCAoZXJyb3Ipe1xuICAgIGNvbnNvbGUubG9nKGVycm9yKVxuICB9XG59O1xuXG5leHBvcnQgZGVmYXVsdCBJbmRleDtcbiJdfQ== */\n/*@ sourceURL=/Users/paulbeaudon/calendar/next-calendar/pages/index.js */"));
};

Index.getInitialProps = function _callee(_ref2) {
  var req, response, URL, location, res, _ref3, users, error, currentMonth, currentMonthWritten, currentYear, resC, _ref4, collections, err;

  return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.async(function _callee$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          req = _ref2.req, response = _ref2.response;
          _context2.prev = 1;
          //const { 'x-now-deployment-url': nowURL } = req.headers;
          URL = req.headers['host'];
          location = URL === 'localhost:3000' ? 'http://localhost:3000' : 'https://next-calendar.now.sh';
          _context2.next = 6;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8___default()("".concat(location, "/api/getUsers")));

        case 6:
          res = _context2.sent;
          _context2.next = 9;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.awrap(res.json());

        case 9:
          _ref3 = _context2.sent;
          users = _ref3.users;
          error = _ref3.error;
          currentMonth = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["currentMonth"]();
          currentMonthWritten = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["getWrittenMonth"](currentMonth);
          currentYear = _components_helperfunctions__WEBPACK_IMPORTED_MODULE_7__["currentYear"]();
          _context2.next = 17;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8___default()("".concat(location, "/api/collections?year=").concat(currentYear, "&month=").concat(currentMonthWritten)));

        case 17:
          resC = _context2.sent;
          _context2.next = 20;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.awrap(resC.json());

        case 20:
          _ref4 = _context2.sent;
          collections = _ref4.collections;
          err = _ref4.err;
          console.log('InitialProps: call to database');
          console.log('v2 index.js');
          return _context2.abrupt("return", {
            location: location,
            currentMonth: currentMonth,
            currentYear: currentYear,
            collections: collections,
            users: users
          });

        case 28:
          _context2.prev = 28;
          _context2.t0 = _context2["catch"](1);
          console.log(_context2.t0);

        case 31:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, [[1, 28]]);
};

/* harmony default export */ __webpack_exports__["default"] = (Index);

/***/ }),

/***/ 2:
/*!*************************************************************************************************************************************!*\
  !*** multi next-client-pages-loader?page=%2F&absolutePagePath=%2FUsers%2Fpaulbeaudon%2Fcalendar%2Fnext-calendar%2Fpages%2Findex.js ***!
  \*************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! next-client-pages-loader?page=%2F&absolutePagePath=%2FUsers%2Fpaulbeaudon%2Fcalendar%2Fnext-calendar%2Fpages%2Findex.js! */"./node_modules/next/dist/build/webpack/loaders/next-client-pages-loader.js?page=%2F&absolutePagePath=%2FUsers%2Fpaulbeaudon%2Fcalendar%2Fnext-calendar%2Fpages%2Findex.js!./");


/***/ }),

/***/ "dll-reference dll_5f137288facb1107b491":
/*!*******************************************!*\
  !*** external "dll_5f137288facb1107b491" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = dll_5f137288facb1107b491;

/***/ })

},[[2,"static/runtime/webpack.js"]]]);
//# sourceMappingURL=index.js.map