
const EmployeeButton = props =>{

  let loggedInUser = false;
  if(props.signedinuser === props.name){
    loggedInUser = true;
  };
  return(
    <div>
      { props.buttonsize === 'small' ?
        (loggedInUser ? <div className="myDList small" id={props.id} draggable onDragStart={props.onDragStartIndex}>{props.name}</div> : <div className="dListBorder small" id={props.id} draggable onDragStart={props.onDragStartIndex}>{props.name}</div>)
        :
        ((loggedInUser ? <div className="myDList" draggable onDragStart={props.onDragStartIndex}>{props.name}</div> : <div className="dListBorder" draggable onDragStart={props.onDragStartIndex}>{props.name}</div>))
      }
      <style jsx>{`
        .dListBorder{
        	text-align: center;
        	margin: 10px 10px 10px 10px;
        	padding: 10px 10px 10px 10px;
        	border: 1px solid #354980;
        	background-color: white;
          color: #354980;
      	  box-shadow: 2px 2px 2px grey;
        }
        .myDList{
      	  text-align: center;
        	margin: 10px 10px 10px 10px;
        	padding: 10px 10px 10px 10px;
        	border: 1px solid #354980;
        	background-color: #354980;
          color: #fbca37;
      	  box-shadow: 2px 2px 2px grey;
        }
        .small{
          font-size: .8em;
          margin: 5px 10px 5px 5px;
          padding: 2px 2px 2px 2px;
        }
      `}</style>
    </div>
  );

};

export default EmployeeButton;
