import * as helpFunc from "../components/helperfunctions";
import EmployeeButton from "../components/EmployeeButton";

const Calendar = props =>{
  const getTable = (month, year, monthInfo)=>{
      let rows = 6;
      let cols = 7;
      let dayCount = 1;
      let start_day = helpFunc.startDay(month, year);
      let days_in_month = helpFunc.daysInMonth(month, year);
      let table = [];

      let currentDay = helpFunc.currentDay();
      let currentMonth = helpFunc.currentMonth();
      let currentYear = helpFunc.currentYear();

      let monthInfoCount = 0;

      table.push(<tr key="header"><td className="headertd">Sunday</td><td className="headertd">Monday</td><td className="headertd">Tuesday</td><td className="headertd">Wednesday</td><td className="headertd">Thursday</td><td className="headertd">Friday</td><td className="headertd">Saturday</td></tr>);
      for(let row=0; row<rows; row++){
        let day = [];
        for(let col=0; col<cols; col++){
          if(row === 0 && col === start_day){
            let morning = [];
            morning = monthInfo.filter(morn => morn.id.split('-')[0] === `${row}_${col}_am`);
            let afternoon = [];
            afternoon = monthInfo.filter(aft => aft.id.split('-')[0] === `${row}_${col}_pm`);

            if(currentDay === dayCount && currentMonth === month && currentYear === year){
              day.push(<td id={`${row}_${col}`} key={`${row}_${col}`} className="daytd"><div className="datePos">{`${dayCount}`}</div><div id={`${row}_${col}_am`} className="am" onDrop={props.onDropCopyIndex} onDragOver={props.onDragOverIndex}>{morning.map(morn=><EmployeeButton key={morn.id} onDragStartIndex={props.onDragStartIndex} id={morn.id} name={morn.user_name} signedinuser={props.signedinuser} buttonsize="small"/>)}</div><div id={`${row}_${col}_pm`} className="pm" onDrop={props.onDropCopyIndex} onDragOver={props.onDragOverIndex}>{afternoon.map(aft=><EmployeeButton key={aft.id} onDragStartIndex={props.onDragStartIndex} id={aft.id} name={aft.user_name} signedinuser={props.signedinuser} buttonsize="small"/>)}</div></td>);
            }else{
              day.push(<td id={`${row}_${col}`} key={`${row}_${col}`} className="daytd"><div className="datePos">{`${dayCount}`}</div><div id={`${row}_${col}_am`} className="am" onDrop={props.onDropCopyIndex} onDragOver={props.onDragOverIndex}>{morning.map(morn=><EmployeeButton key={morn.id} onDragStartIndex={props.onDragStartIndex} id={morn.id} name={morn.user_name} signedinuser={props.signedinuser} buttonsize="small"/>)}</div><div id={`${row}_${col}_pm`} className="pm" onDrop={props.onDropCopyIndex} onDragOver={props.onDragOverIndex}>{afternoon.map(aft=><EmployeeButton key={aft.id} onDragStartIndex={props.onDragStartIndex} id={aft.id} name={aft.user_name} signedinuser={props.signedinuser} buttonsize="small"/>)}</div></td>);
            }
            dayCount++;
          }else{
            if(dayCount > 1 && dayCount <= days_in_month){
              let morning = [];
              morning = monthInfo.filter(morn => morn.id.split('-')[0] === `${row}_${col}_am`);
              let afternoon = [];
              afternoon = monthInfo.filter(aft => aft.id.split('-')[0] === `${row}_${col}_pm`);

              if(currentDay === dayCount && currentMonth === month && currentYear === year){
                day.push(<td id={`${row}_${col}`} key={`${row}_${col}`} className="daytd"><div className="datePos">{`${dayCount}`}</div><div id={`${row}_${col}_am`} className="am" onDrop={props.onDropCopyIndex} onDragOver={props.onDragOverIndex}>{morning.map(morn=><EmployeeButton key={morn.id} onDragStartIndex={props.onDragStartIndex} id={morn.id} name={morn.user_name} signedinuser={props.signedinuser} buttonsize="small"/>)}</div><div id={`${row}_${col}_pm`} className="pm" onDrop={props.onDropCopyIndex} onDragOver={props.onDragOverIndex}>{afternoon.map(aft=><EmployeeButton key={aft.id} onDragStartIndex={props.onDragStartIndex} id={aft.id} name={aft.user_name} signedinuser={props.signedinuser} buttonsize="small"/>)}</div></td>);
              }else{
                day.push(<td id={`${row}_${col}`} key={`${row}_${col}`} className="daytd"><div className="datePos">{`${dayCount}`}</div><div id={`${row}_${col}_am`} className="am" onDrop={props.onDropCopyIndex} onDragOver={props.onDragOverIndex}>{morning.map(morn=><EmployeeButton onDragStartIndex={props.onDragStartIndex} key={morn.id} id={morn.id} name={morn.user_name} signedinuser={props.signedinuser} buttonsize="small"/>)}</div><div id={`${row}_${col}_pm`} className="pm" onDrop={props.onDropCopyIndex} onDragOver={props.onDragOverIndex}>{afternoon.map(aft=><EmployeeButton key={aft.id} onDragStartIndex={props.onDragStartIndex} id={aft.id} name={aft.user_name} signedinuser={props.signedinuser} buttonsize="small"/>)}</div></td>);
              }
              dayCount++;
            }else{
              day.push(<td key={`${row}_${col}`} className="daytd"></td>);
            }
          }
        }
        table.push(<tr key={`${row}`}>{day}</tr>);
      }
      return table;
    };

  return(
    <div>
      <table className="caltable">
        <tbody>
          {getTable(props.month, props.year, props.monthInfo)}
        </tbody>
      </table>
      <style jsx global>{`
        .caltable {
          margin: auto;
          border-spacing: 0px;
          padding: 4px;
          font-size: 1em;
          border-collapse: separate;
          border: 0px solid #354980;
        }
        .headertd{
          text-align: center;
          width:14%;
          padding: 4px 50px 4px 50px;
          font-size: 1em;
          border: 1px solid #354980;
          background-color: #354980;
          color: #fbca37;
        }
        .daytd{
      	  position: relative;
          text-align: center;
          font-size: 1em;
          border: 1px solid #354980;
      	  height: 100px;
      	  min-height: 100px;
        }
        .currentdaytd{
      	  position: relative;
          text-align: center;
          font-size: 1em;
          border: 1px solid #354980;
          height: 100px;
          background-color: #fbca37;
          color: #fbca37;
          z-index: -1;
        }
        .datePos{
        	font-size: 1em;
        	position: absolute;
        	left: 0px;
        	top: 0px;
        	right: 0px;
        	text-align: right;
        	padding-right: 5px;
          background-color: #fbca37;
          color: #354980;
          z-index: -1;
        }
        .am{
        	width: 100%;
        	height: auto;
        	min-height: 50px !important;
        	border: 1px dotted #fbca37;
        	padding-top: 15px;
        }
        .amCurrentDay{
        	width: 100%;
        	height: auto;
        	min-height: 50px !important;
        	border-bottom: 1px dotted white;
        	padding-top: 15px;
        }
        .pm{
        	width: 100%;
        	height: auto;
        	min-height: 50px !important;
        	border: 1px dotted #fbca37;
        }
      `}</style>
    </div>
  );
};

export default Calendar;
