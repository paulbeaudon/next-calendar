import useSWR from 'swr';

export function fetcher(url){
  return fetch(url).then((res) => {
    return res.json();
  });
}

export function getUsers(){
  console.log('getUsers: call to database');
  const {data, error} = useSWR('/api/getUsers', fetcher);
  const users = data;
  if(users){
    console.log('success');
    return users;
  }else{
    console.log('No users as per request');
    return [];
  }
}

export function getUsersByUsername(user_name){
  console.log('getUsersByUsername: call to database');
  const {data, error} = useSWR('/api/getUsers?username=' + user_name, fetcher);
  let users = data;
  if(error){
    console.log(error);
  }
  if(users){
    console.log('success');
    return users;
  }else{
    if(user_name){
      console.log('No users by the name of: ' + user_name);
      return [];
    }else{
      console.log('No users as per request');
      return [];
    }
  }
}

export function getUsersByStatus(status){
  console.log('getUsersByStatus: call to database');
  const {data, error} = useSWR('/api/getUsers?status=' + status, fetcher);
  let users = data;
  if(error){
    console.log(error);
  }
  if(users){
    console.log('success');
    return users;
  }else{
    if(status){
      console.log('No users with the status of: ' + status);
      return [];
    }else{
      console.log('No users as per request');
      return [];
    }
  }
}

export function getUsersByAccess(access){
  console.log('getUsersByAccess: call to database');
  const {data, error} = useSWR('/api/getUsers?access=' + access, fetcher);
  let users = data;
  if(error){
    console.log(error);
  }
  if(users){
    console.log('success');
    return users;
  }else{
    if(access){
      console.log('No users with the access of: ' + access);
      return [];
    }else{
      console.log('No users as per request');
      return [];
    }
  }
}
