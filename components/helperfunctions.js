let currentDay = ()=>{
  return new Date().getDate();
};
let currentMonth = ()=>{
  return new Date().getMonth()+1;
};
let currentYear = ()=>{
return new Date().getFullYear();
};
let daysInMonth = (month, year)=>{
  return new Date(year, month,0).getDate();
};
let startDay = (month, year)=>{
  //let daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  return new Date(year, month-1, 1).getDay();
}
let getWrittenMonth = (monthInt)=>{
  let month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  return month[monthInt-1];
};
let getIntMonth = (monthString)=>{
  let month = {"Jan": 1, "Feb": 2, "Mar": 3, "Apr": 4, "May": 5, "Jun": 6, "Jul": 7, "Aug": 8, "Sep": 9, "Oct": 10, "Nov": 11, "Dec": 12};
  return month[monthString];
}

export {currentDay, currentMonth, currentYear, daysInMonth, startDay, getWrittenMonth, getIntMonth}
