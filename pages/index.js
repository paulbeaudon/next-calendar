import Cal from "../components/Calendar";
import EmployeeButton from "../components/EmployeeButton";
import * as helpFunc from "../components/helperfunctions";

import fetch from 'isomorphic-unfetch';
import React, { useState, useEffect } from 'react';
import { CircleSpinner } from "react-spinners-kit";

const Index = props =>{
  const location = props.location;

  let initialMonthInfo = props.collections.length > 0 ? props.collections[0].data[helpFunc.getWrittenMonth(props.currentMonth)] : [];

  let users = props.users.map((user)=>{
    return user.data;
  });

  users = users.filter((user)=>{
    if(user.status === 'activated' && (user.access === 'dispatch' || user.access === 'admin')){
      return user;
    }
  });
  users.sort(function(a,b){
    if(a.user_name.toUpperCase() < b.user_name.toUpperCase()) return -1;
    if(a.user_name.toUpperCase() > b.user_name.toUpperCase()) return 1;
    return 0;
  });
  let signedInUser = 'Beaudon';

  const [month, setMonth] = useState(props.currentMonth);
  const [year, setYear] = useState(props.currentYear);
  const [draggedButton, setDraggedButton] = useState({});
  const [monthInfo, setMonthInfo] = useState(initialMonthInfo);

  const [monthInfoCount, setMonthInfoCount] = useState(monthInfo.length);
  const [monthInfoSaveFlag, setMonthInfoSaveFlag] = useState(0);
  const [loading, setLoading] = useState(false);

  const onDragStart = (event, user) => {
    //event.preventDefault();
    setDraggedButton({
      userInfo: user,
      userId: event.target.id
    });
    console.log('on drag start');
    console.log(event.target.id);
  };

  /*const onDrag = (event, user) =>{
    event.preventDefault();
    console.log('dragging');
    console.log(draggedButton.userInfo.user_name);
  };*/

  const onDragOver = (event)=>{
    event.preventDefault();
  };

  const onDropDelete = (event)=>{
    console.log('Delete:');
    console.log(draggedButton);
    console.log(event.target.id);
    if(draggedButton.userId){
      console.log('delete this');
      for(let x=0; x<monthInfo.length; x++){
        if(monthInfo[x].id === draggedButton.userId){
          console.log('delete: ' + draggedButton.userId);
          monthInfo.splice(x, 1);
          setMonthInfo[monthInfo => monthInfo];
          setMonthInfoSaveFlag(monthInfoSaveFlag => monthInfoSaveFlag + 1);
          break;
        }
      }
      console.log(monthInfo);
    }else{
      console.log('do not delete this');
    }

    setDraggedButton({});
  };

  const onDropCopy = (event)=>{
    if(!draggedButton.userId){
      console.log(event.target.id);
      console.log(monthInfoCount);
      console.log('Copy:');
      console.log(draggedButton.userInfo.user_name);
      let newId = event.target.id.split('-')[0] + '-' + monthInfoCount;
      if(event.target.id){ // if target is not blank, then update props
        setMonthInfo([...monthInfo, {access: draggedButton.userInfo.access, user_name:draggedButton.userInfo.user_name, status:draggedButton.userInfo.status, id: newId}]);
        setMonthInfoCount(monthInfoCount => monthInfoCount + 1);
        setMonthInfoSaveFlag(monthInfoSaveFlag => monthInfoSaveFlag + 1);
      }
    }else{
      console.log('move');
      let newId = event.target.id + '-' + draggedButton.userId.split('-')[1];
      console.log(newId);
      for(let x=0; x<monthInfo.length; x++){
        if(monthInfo[x].id === draggedButton.userId){
          console.log('change: ' + draggedButton.userId + ' to ' + newId);
          monthInfo[x].id = newId;
          setMonthInfo[monthInfo => monthInfo];
          setMonthInfoSaveFlag(monthInfoSaveFlag => monthInfoSaveFlag + 1);
          break;
        }
      }
    }
      setDraggedButton({});
  };

  let previousMonthCal = ()=>{
    setLoading(true);
    if(month-1 < 1){
      const newMonth = 12;
      const newYear = year-1;
      loadCalendar(newYear, newMonth);
    }else{
      const newMonth = month-1;
      const newYear = year;
      loadCalendar(newYear, newMonth);
    }
  };

  let nextMonthCal = ()=>{
    setLoading(true);
    if(month+1 > 12){
      const newMonth = 1;
      const newYear = year+1;
      loadCalendar(newYear, newMonth);
    }else{
      const newMonth = month+1;
      const newYear = year;
      loadCalendar(newYear, newMonth);
    }
  };

  let currentMonthCal = ()=>{
    setLoading(true);
    const newMonth = helpFunc.currentMonth();
    const newYear = helpFunc.currentYear();
    loadCalendar(newYear, newMonth);
  };

  let loadCalendar = async(newYear, newMonth)=>{
    const resC = await fetch(`${location}/api/collections?year=${newYear}&month=${helpFunc.getWrittenMonth(newMonth)}`);
    const { collections, err } = await resC.json();
    const calJson = collections.length > 0 ? collections[0].data[helpFunc.getWrittenMonth(newMonth)] : [];

    setMonthInfo(calJson);
    setMonthInfoCount(calJson.length);
    setMonth(newMonth);
    setYear(newYear);
    setLoading(false);
  };

  useEffect(() => { // used to save calendar when a drop has taken place, and need to wait for monthInfo to update before calling save.
     saveCalendar({type: 'drop'});
  }, [monthInfoSaveFlag]);

  let saveCalendar = (action)=>{
    console.log(action);
    let data = {"year": year.toString(), "month": helpFunc.getWrittenMonth(month), [helpFunc.getWrittenMonth(month)]: monthInfo};
    fetch(`${location}/api/collections`, {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then((res) => {
      return res.json();
    }).then((json)=>{
      if(action.type === 'none'){
        console.log(json.my_status);
      }
      if(action.type === 'next'){
        console.log('next');
        console.log(json.my_status);
        nextMonthCal();
      }
      if(action.type === 'previous'){
        console.log('previous');
        console.log(json.my_status);
        previousMonthCal();
      }
      if(action.type === 'current'){
        console.log('current');
        conosle.log(json.my_status);
        currentMonthCal();
      }
      if(action.type === 'drop'){
        console.log('drop');
        console.log(json.my_status);
      }
    });
  };

  return(
    <div>
      {loading ? <div className="spinner"><CircleSpinner size={80} color="#354980" loading={loading} /></div> : <div></div>}
      <div id="left_pane" className="leftPane">
        <div className="headerDiv">Dispatch Staff</div>
        {users.map(user=>(
            <EmployeeButton key={user.user_name} onDragStartIndex={event => onDragStart(event, user)} name={user.user_name} signedinuser={signedInUser}/>
        ))}
        <div id="delete_pane" className="deletePane" onDrop={event => onDropDelete(event)} onDragOver={event => onDragOver(event)}>Drag here to Delete</div>
      </div>
      <div className="rightPane">
    	  <div className="headerDiv title" id='monthTitle'>{helpFunc.getWrittenMonth(month)} {year} Desk Schedule</div>
    	  <div className="headerDiv">
    		  <button className="button" id="today" onClick={e=>(currentMonthCal())}>Today</button> <button className="button" id='leftarrow' onClick={e=>(saveCalendar({type: 'previous'}))}>&lt;&lt;</button> <button className="button" id='rightarrow' onClick={e=>(saveCalendar({type: 'next'}))}>&gt;&gt;</button>
    	  </div>
    	  <Cal onDragStartIndex={event => onDragStart(event, "")} onDragOverIndex={event => onDragOver(event)} onDropCopyIndex={event => onDropCopy(event)} month={month} year={year} monthInfo={monthInfo} signedinuser={signedInUser}/>
    	</div>
      <style jsx global>{`
        .spinner{
          position: absolute;
          left: 50%;
          top: 50%;
          z-index: 1;
        }
        .title{
          font-size: 1.9em !important;
        }
        .headerDiv{
        	margin: auto;
        	width: 80%;
        	text-align: center;
        	font-size: 1.2em;
        	padding: 10px;
        }
        .fullPane{
        	position: relative;
        	width: 100%;
        }
        .leftPane{
        	position: absolute;
        	left: 60px;
        	top: 68px;
        	bottom: 10px;
        	width: 15%;
        }
        .rightPane{
        	position: absolute;
        	right: 10px;
        	top: 10px;
        	bottom: 10px;
        	width:80%;
        }
        .deletePane{
          text-align: center;
        	margin: 10px 10px 10px 10px;
        	padding: 10px 10px 10px 10px;
        	border: 1px dashed #354980;
        	background-color: white;
          color: #354980;
          height: 50%;
        }
        .button {
          background-color: #354980;
          border: none;
          color: #fbca37;
          padding: 5px 25px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          margin: 4px 2px;
          cursor: pointer;
        }
      `}</style>
    </div>
  );
};

Index.getInitialProps = async ({req, response})=>{
  try{

    //const { 'x-now-deployment-url': nowURL } = req.headers;
    const { 'host': URL } = req.headers;
    const location = (URL === 'localhost:3000') ? 'http://localhost:3000' : 'https://next-calendar.now.sh';

    const res = await fetch(`${location}/api/getUsers`);
    //const res = await fetch(`https://next-calendar.now.sh/api/getUsers`); // initial calls to database - server side render for speed
    const { users, error } = await res.json();

    const currentMonth = helpFunc.currentMonth();
    const currentMonthWritten = helpFunc.getWrittenMonth(currentMonth);
    const currentYear = helpFunc.currentYear();

    const resC = await fetch(`${location}/api/collections?year=${currentYear}&month=${currentMonthWritten}`);
    //const resC = await fetch(`https://next-calendar.now.sh/api/collections?year=${currentYear}&month=${currentMonthWritten}`);
    const { collections, err } = await resC.json();

    console.log('InitialProps: call to database');
    console.log('v2 index.js');
    return {location: location, currentMonth: currentMonth, currentYear: currentYear, collections, users};
  }catch (error){
    console.log(error)
  }
};

export default Index;
