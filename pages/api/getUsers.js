import faunadb, { query as q } from 'faunadb';
require('dotenv').config();

const { FAUNADB_SECRET: secret } = process.env;

let client;

if (secret) {
  client = new faunadb.Client({ secret });
}

export default async (req, res) => {
  try {
    let users = [];

    if (!client) {
      return [];
    }

    await client
    .paginate(q.Match(q.Index("users_by_status"), ["activated"]))
    .map(ref => q.Get(ref))
    .each(page =>{
      users = users.concat(page);
    });

    res.json({ users });
  } catch (error) {
    res.status(500).json({ error });
  }
};
