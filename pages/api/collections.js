import faunadb, { query as q } from 'faunadb';
require('dotenv').config();

const { FAUNADB_SECRET: secret } = process.env;

let client;

if (secret) {
  client = new faunadb.Client({ secret });
}

export default async (req, res) => {
  const month = req.query.month;
  const year = req.query.year;

  console.log(req.method);

  if(req.method === 'GET'){
    try {
      let collections = [];

      if (!client) {
        return [];
      }

      await client
      .paginate(q.Match(q.Index("calendar_by_year_month"), [year, month]))
      .map(ref => q.Get(ref))
      .each(page =>{
        collections = collections.concat(page);
      });

      res.json({ collections });

    } catch (error) {
      res.status(500).json({ error });
    }
  }

  if(req.method === 'POST'){
    try {
      let collections = [];

      if (!client) {
        return [];
      }

      await client
      .paginate(q.Match(q.Index("calendar_by_year_month"), [req.body.year, req.body.month]))
      .map(ref => q.Get(ref))
      .each(page =>{
        collections = collections.concat(page);
      });
      console.log(req.body.month);
      if(collections.length > 0){
        console.log(collections[0].ref);
        console.log('replace');
        client.query(
          q.Replace(
            collections[0].ref,
            {
              data: req.body
            },
          )
        ).then((ret)=>console.log(ret))
        res.status(200).json({'my_status': 'OK', 'type':'replace'});
      }else{
        console.log('create');
        client.query(
          q.Create(
            q.Collection('calendar'),
            {
              data: req.body
            },
          )
        ).then((ret)=>console.log(ret))
        res.status(200).json({'my_status': 'OK', 'type':'create'});
      }
      //res.json({ collections });

    } catch (error) {
      res.status(500).json({ error });
    }
  }
};
